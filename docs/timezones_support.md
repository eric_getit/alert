# Sending Alerts Based on Timezone #

## Related Tickets in JIRA ##

*  Timezones support logic is described in the ticket
   [ALT-44 Send alert based on timezone](http://jira.getitcorporate.com/browse/ALT-44)
*  Logic which should be used to determine user location and hence the timezone is descussed
   here [ALT-13](http://jira.getitcorporate.com/browse/ALT-13)

## Administrative Interface ##

Timezone support might be enable or disabled on
[Send Time](http://alert-admin.getitcorporate.com/configuration/send-time) configuration page.

We send alerts starting from the hour defined in `Time` configuration option.
When timezone support is disabled all the alerts are send starting from the specified time
in EST timezone. When timezone suport is enabled we send every alert starting on specified
time in subscribers timezone.

## Important Limitations ##

*  Timezone support has not been fully tested. Normal sending (with timezones disabled) has been tested.
*  We don't have any logic to populate information about subscribers' timezones.
   All subscribers are assigned EST timezone by default.
*  We launch scripts to generate and send messages on dedicated server. The server is turned on at 4 am EST
   and turned off when we are done sending alerts. The logic used to turn off the server might be changed.
