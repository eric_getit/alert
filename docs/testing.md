# Testing Alerts Application #

## General Notes ##

This is a draft document which describes the work we do now regularily
to check application health.

Original document is written in Russian by Olga. She has described all the tasks she does daily.
Victor has translated it to English without any changes.

## Servers and Scripts Checks ##

*  Check if we have enough space on the MySQL database server.
   We need to add more space in advance because it might take several hours to extend DB.
   
   *  The URL to check in [AWS Console](https://console.aws.amazon.com/rds/home?region=us-east-1#dbinstances)

*  Check if Elastic Search is in Green state.
   *  We should have launched additional servers for the time when running alets.
   *  We use [Paramedic plugin](http://api.buzzvertical.com:9200/_plugin/paramedic/index.html) to check ES cluster health
   
*  Check gearman workers
   *  On the [Gearman monitor](http://gm.buzzvertical.com/) (user: gearman, pwd: carltheman) do the following checks
     *  We have all workers running
     *  We don't have large queues for preparing and sending alert workers
   
## Testing Messages ##

*  Check generated messages on [Message Archive](http://alert-admin.getitcorporate.com/message-archive) screeb
   * Check gateway URLs for job listings
   * Check edit and unsubcribe URL
   * Check message text
*  Check if click counters are updated when opening gateway URL.
   *  Open any URL and find newly inserted record in the DB with the following SQL

   ```SQL
   select *
   from email_click
   order by id desc
   limit 10
   ```
*  Check tracking pixels in the message bodies
   *  Tracking pixel URLs are excluded from message bodies shown in the admin. This is why we should
      check message bodies directly in the database.
   *  Find tracking pixel URL in message bodies table.
      *  Table name message_bodies_2013<first date of the week>
      *  URL example  http://gettruckingjobs.getitcorporate.com/picture/23898b19-08bd-43ff-9a63-15a24787b8fc/logo.gif
   *  Test if URL is opening correctly
   *  Check if the counter for message views is incremented

      ```SQL
      select *
      from message_views
      order by viewId desc
      limit 10
      ```


## Testing Alerts API ##

*  Register new subscriber

   ```  
   curl http://alert.getitcorporate.com/api/subscribe -d 'alertType=DAILY&domain=gettruckingjobs.com&email=test1@test.getitcorporate.com&query=Truck&location=Dallas,+TX'
   curl http://alert.getitcorporate.com/api/subscribe -d 'alertType=DAILY&domain=gettruckingjobs.com&email=test-new@test.getitcorporate.com&query=Truck&location=Dallas,+TX&country=US&state=TX&city=Dallas&name=Brus&distance=25&ip=123.123.123.123'
   ```
*  Test if user exists in the application

   ```
   curl http://alert.getitcorporate.com/api/exists?email=test1@test.getitcorporate.com
   ```

TODO: Olga, put here other API calls we have in the application
