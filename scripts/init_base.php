<?php
/**
 * init_base.php
 *
 * PHP Version 5.3
 *
 * @category Scripts
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Init constants and autoloader
 *
 */
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'development'));
// Define path to configs folder
defined('CONFIGS_PATH')
    || define('CONFIGS_PATH', realpath(dirname(__FILE__) . '/../configs'));
defined('VAR_PATH')
    || define('VAR_PATH', realpath(dirname(__FILE__) . '/../var'));

require_once realpath(__DIR__ . '/../vendor/autoload.php');

/**
 * Setup autoloading for namespaces
 */
if (!function_exists('autoloaderNamespaces')) {
    function autoloaderNamespaces($class)
    {
        $class = ltrim($class, '\\');
        if (preg_match('#^(Twig)(_)#', $class)) {
            $file = realpath(APPLICATION_PATH . '/../library/TransEmail/src') . '/' . str_replace(array('_', "\0"), array('/', ''), $class).'.php';
        } elseif (preg_match('#^(GetIt|Zend|Ses|Sqs)(\\\\)#', $class)) {
            $segments = preg_split('#[\\\\_]#', $class);
            $file = realpath(APPLICATION_PATH . '/../library/TransEmail/src') . '/' . implode('/', $segments) . '.php';
        } else {
            return false;
        }

        return include_once $file;
    }
    spl_autoload_register('autoloaderNamespaces', true, true);
}

$config = include CONFIGS_PATH . '/application.php';
$autoloader = new Zend_Application_Module_Autoloader(array(
        'namespace' => '',
        'basePath' => APPLICATION_PATH
));
return $config;
