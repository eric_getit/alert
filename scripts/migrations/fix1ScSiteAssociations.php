#!/usr/bin/env php
<?php
/**
 * fix1ScSiteAssociations.php
 *
 * PHP Version 5.3
 *
 * @category Scripts
 * @package  migrations
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * The records I imported were messed up when they were merged down to single
 * contacts with multiple alerts.
 *
 * I've reviewed the export code, and some of the cases where I found issues
 * and sure enough I had originally passed in the proper data.
 *
 * This will need to look for Alerts with a UTM Source of 1sc, where the query
 * term doesn't == the site niche.
 *
 * For example, I see 'Event', and 'CNA' alerts registered to 'Flight Attendant'
 */

include realpath(__DIR__ . '/../init.php');

$dbh = Zend_Db_Table::getDefaultAdapter();

/*
 * Get the sites into a local array, so we don't have to do anything complex
 */
$sql = "SELECT siteNiche, siteId FROM sites";

/*
 * Returns array(siteNiche => siteId)
 */
$sites = $dbh->fetchPairs($sql);

/*
 * Now we start stepping through any results matching our queries for bad associations
 */
$sql = "
    SELECT alertId, queryTerms
    FROM alerts
    JOIN sites USING (siteId)
    WHERE alertStatus = 'ACTIVE'
        AND utmSource = '1sc'
        AND queryTerms <> siteNiche
    ORDER BY alertId ASC";

$rows = $dbh->fetchAll($sql);

foreach ($rows as $row) {

    $queryTerms = $row['queryTerms'];

    /*
     * Now we simply check if the queryTerms match another site
     */
    if (isset($queryTerms) && array_key_exists($queryTerms, $sites)) {

        $siteId = $sites[$queryTerms];

        if (empty($siteId)) continue;

        /*
         * Update the alert with the new siteId
         */
        $dbh->update('alerts', array('siteId' => $siteId), $dbh->quoteInto('alertId = ?', $row['alertId']));
        var_dump($row);

    } else {
        echo 'Missed hit...' . PHP_EOL;
    }
}

