#!/usr/bin/env php
<?php
/**
 * alertsWithDefaultNiche.php
 *
 * PHP Version 5.3
 *
 * @category Scripts
 * @package  migrations
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * We need to pull active alerts where the query terms don't match the siteNiche
 * We then will create a new alert with all the same properties, but the queryTerms
 * will be the siteNiche
 */

include realpath(__DIR__ . '/../init.php');

$dbh = Zend_Db_Table::getDefaultAdapter();
$dbh->getConnection()->setAttribute(PDO::MYSQL_ATTR_DIRECT_QUERY, false);
$dbh->getConnection()->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);

/*
 * Now we get items that need a new record.
 */
$sql = "
    SELECT alertId, alertSecureId, siteNiche
    FROM alerts
    JOIN sites USING (siteId)
    WHERE alertStatus = 'ACTIVE'
        AND utmSource <> '1sc'
        AND queryTerms <> siteNiche
";

$rows = $dbh->fetchAll($sql);

foreach ($rows as $row) {

    var_dump($row);

    /*
     * At this point we start a transaction, and only commit if
     * everything went to plan
     */
    $dbh->beginTransaction();

    try {

        $md5           = md5(json_encode($row));
        $alertSecureId = substr($md5, 0, 8);

        $sql = "INSERT INTO alerts (SELECT null, ?, alertType, contactId, location, ?, ip, createdDate, updatedDate, lastProcessDate, lastSendDate, alertStatus, nextProcessDate, utmSource, utmMedium, utmTerm, utmContent, utmCampaign, bouncesCount, complaintsCount, lastBounceDate, lastComplaintDate, null, distance, siteId FROM alerts WHERE alertId = ?)";
        $dbh->query($sql, array($alertSecureId, $row['siteNiche'], $row['alertId']));
        $dbh->query("UPDATE alerts SET alertStatus = 'ADM_PENDING' WHERE alertId = ?", array($row['alertId']));

        $dbh->commit();

    } catch (Exception $exc) {
        $dbh->rollBack();
        echo $exc->getTraceAsString();
    }

}
