#!/usr/bin/env php
<?php
/**
 * emailLinksMigrate.php
 *
 * PHP Version 5.4
 *
 * @category Scripts
 * @package  migrations
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Logic is described in the ticket
 * [ALT-154 Removed duplicated job links from email_link_20130610 table](http://jira.getitcorporate.com/browse/ALT-154)
 */

if ($_SERVER['argc'] < 4) {
    echo 'Relocate data between tables and change code structure if need.' . PHP_EOL;
    echo 'Usage:' . PHP_EOL;
    echo '    ' . $_SERVER['argv'][0] . ' <source_table_name> <target_table_name> <parse_code> [<last_id>]' . PHP_EOL;
    echo 'Example:' . PHP_EOL;
    echo '    ' . $_SERVER['argv'][0] . ' email_link_20130121 email_link 1' . PHP_EOL;
    die(1);
}

require_once realpath(__DIR__ . '/../init.php');

$dbh = Zend_Db_Table::getDefaultAdapter();

$limit = 500;
$sourceTable = $_SERVER['argv'][1];
$targetTable = $_SERVER['argv'][2];
$parse = $_SERVER['argv'][3] == 1 ? true : false;
$lastId = 0;
if (isset($_SERVER['argv'][4])) {
    $lastId = $_SERVER['argv'][4];
}

$fieldString = getFieldString($targetTable);
$insertSql = "INSERT IGNORE {$targetTable} ({$fieldString}) VALUES ";

$sql = "SELECT * FROM {$sourceTable} WHERE id > ? ORDER BY id LIMIT {$limit}";
$stmt = $dbh->prepare($sql);
$totalCount = 0;
try {
    do {
        $stmt->execute(array($lastId));
        $rows = $stmt->fetchAll();
        $count = count($rows);
        $totalCount += $count;
        $lastId = $rows[$count - 1]['id'];
        $valuesString = getValuesString($rows, $parse);
        $dbh->query($insertSql . $valuesString);
        echo "[" . date('H:i:s') . "] --> Total readed: {$totalCount}. Last processed id: {$lastId}" . PHP_EOL;
    } while ($count == $limit);
} catch (Exception $e) {
    echo $e->getMessage() . PHP_EOL;
    echo 'Last processed id = ' . $lastId . PHP_EOL;
}

/**
 * Get field string for insert
 *
 * @param string $tableName
 *
 * @return string
 */
function getFieldString($tableName)
{
    $dbh = Zend_Db_Table::getDefaultAdapter();
    $columns = $dbh->query('SHOW COLUMNS FROM ' . $tableName)->fetchAll();

    $fieldString = '';
    foreach ($columns as $column) {
        if ($column['Field'] == 'id') {
            continue;
        }
        $fieldString .= $column['Field'] . ', ';
    }
    $fieldString = substr($fieldString, 0, -2);

    return $fieldString;
} // getFieldString

/**
 * Construct sql values string by input data
 *
 * @param array $rows
 * @param boolean $parse
 *
 * @return string
 */
function getValuesString($rows, $parse)
{
    $dbh = Zend_Db_Table::getDefaultAdapter();

    $valuesString = '';
    foreach($rows as $row) {
        unset($row['id']);
        if ($parse) {
            unset($row['alert_id']);
            $row['code'] = parseCode($row['code']);
        }
        $quoteValues = array();
        foreach ($row as $field => $value) {
            $quoteValues[] = $dbh->quote($value);
        }
        $valuesString .= '(' . implode(', ', $quoteValues) . '),';
    }
    $valuesString = substr($valuesString, 0, -1);

    return $valuesString;
} // getValuesString

/**
 * Get new code format from old code format
 *
 * @param string $code
 *
 * @return string
 */
function parseCode($code)
{
    list($listingId, $alertId) = explode('-', $code);
    return $listingId;
} // parseCode