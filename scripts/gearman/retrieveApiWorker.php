#!/usr/bin/env php
<?php
/**
 * retrieveApiWorker.php
 *
 * PHP Version 5.3
 *
 * @category Scripts
 * @package  gearman
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Gearman worker for retrieving alert API result
 */

if (count($_SERVER['argv']) < 2) {
    printUsage();
}

$name = $_SERVER['argv'][1];
if (isset($_SERVER['argv'][2])) {
    $memoryLimit = intval($_SERVER['argv'][2]);
    if ($memoryLimit < 128) {
        $memoryLimit = 128;
    }
} else {
    $memoryLimit = 512;
}

$config = include (realpath(dirname(__FILE__)) . '/../init.php');

$worker = new Model_Workers_RetrieveApiResults();
$worker->setMemoryLimit($memoryLimit);
$worker->setName($name);
$worker->run();

function printUsage()
{
    echo 'Usage:' . PHP_EOL;
    echo '    ' . $_SERVER['argv'][0] . '  <process name> [<memory limit>]' . PHP_EOL . PHP_EOL;
    echo 'Example:' . PHP_EOL;
    echo '    ' . $_SERVER['argv'][0] . '  retrieve-0 512' . PHP_EOL;
    die(1);
}
