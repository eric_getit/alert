#!/usr/bin/env php
<?php
/**
 * client.php
 *
 * PHP Version 5.3
 *
 * @category Scripts
 * @package  gearman
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Do the work:
 * - Retrieve alert results from API
 * - Create alert messages
 * - Send messages
 * - Save messages and log operations
 */

$config = include (realpath(dirname(__FILE__)) . '/../init.php');

$options = getopt('', array(
        'help',
        'stat',
        'show-config',
        'send',
        'verbose',
        'dry-run',
        'timezones:',
        'send-hour:',
        'process-name:',
));

array_shift($argv);
$command = array_shift($argv);

switch ($command) {
    case '--help':
        printHelp();
        break;

    case '--stat':
        getStatistics($options);
        break;

    case '--show-config':
        showConfig();
        break;

    case '--send':
        sendMessages($options);
        break;

    default:
        printUsage();
        break;
}

function printUsage()
{
    echo 'Usage:' . PHP_EOL;
    echo '    ' . $_SERVER['argv'][0] . '  --help' . PHP_EOL;
    echo '    ' . $_SERVER['argv'][0] . '  --stat [--process-name=<name>]' . PHP_EOL;
    echo '    ' . $_SERVER['argv'][0] . '  --show-config' . PHP_EOL;
    echo '    ' . $_SERVER['argv'][0] . '  --send [--verbose] [--dry-run] [--timezones=enabled|disabled] ';
    echo '[--send-hour=<number>]' . PHP_EOL;
    die(1);
}

function printHelp()
{
    $usage = <<<USAGE
NAME
       client.php
SYNOPSIS
       client.php --show-config

       client.php --send [--verbose] [--dry-run] [--timezones=enabled|disabled]
       [--sendhour=<number>]

DESCRIPTION
       client.php schedules sending alert messages.

   --help
       Print this page.

   --stat
       Print workers information.

   --show-config
       Print information about current Alerts application configuration.

   --send
       Schedule alerts for sending.

OPTIONS
   --verbose
       Display debug information.

   --dry-run
       Display all statistics but do not actually send messages.

    --timezones=enabled|disabled
       Overwrite the default setting for time zones support.

    --send-hour=<number>
       Hour when we should send messages. By default taken from configuration.

    --process-name=<name>
       Display statistics to only one process.

USAGE;
    echo $usage;
}

function getStatistics($options)
{
    if (isset($options['process-name'])) {
        $name = $options['process-name'];
        $stat = new Model_Workers_Statistics($name);
        $message = $stat->getFormatted();
        echo $message . PHP_EOL;
    } else {
        $supervisor = Model_ConnectionManager::getSupervisorClient();
        $processInfo = $supervisor->getAllProcessInfo();
        foreach ($processInfo as $info) {
            $stat = new Model_Workers_Statistics($info['name']);
            $message = $stat->getFormatted();
            echo $message . PHP_EOL;
        }
    }
}

function showConfig()
{
    if (Model_ConfigDb::getBoolean('useTimezones')) {
        $timezonesEnabled = 'enabled';
    } else {
        $timezonesEnabled = 'disabled';
    }
    $sendTime = Model_ConfigDb::get('sendTime');

    echo 'Alerts sending settings' . PHP_EOL;
    echo '    Send time: ' . $sendTime . PHP_EOL;
    echo '    Time zones support: ' . $timezonesEnabled . PHP_EOL;
}

function sendMessages($options)
{
    $verbose = isset($options['verbose']);
    $dryRun = isset($options['dry-run']);

    if (isset($options['send-hour'])) {
        $sendHour = intval($options['send-hour']);
    } else {
        $sendTime = Model_ConfigDb::get('sendTime');
        list($sendHour, $sendMinute) = explode(':', trim($sendTime));
        $sendHour = intval($sendHour);
    }

    if (isset($options['timezones'])) {
        $timezonesEnabled = $options['timezones'] == 'enabled';
    } else {
        $timezonesEnabled = Model_ConfigDb::getBoolean('useTimezones');
    }
    if ($timezonesEnabled) {
        $curHour = intval(date('H'));
        $timezonesList = Model_Utils::getTimezones($sendHour, $curHour);
    } else {
        $timezonesList = array();
    }

    if ($verbose) {
        $redisConfig = Model_ConnectionManager::getRedisConfig();
        $gearmanConfig = Model_ConnectionManager::getGearmanConfig();
        echo 'Configuration' . PHP_EOL;
        echo '--> Redis host: ' . $redisConfig['host'] . PHP_EOL;
        echo '--> Redis port: ' . $redisConfig['port'] . PHP_EOL;
        echo '--> Gearman host: ' . $gearmanConfig['host'] . PHP_EOL;
        echo '--> Gearman port: ' . $gearmanConfig['port'] . PHP_EOL;
        echo '--> Sending messages sheduled for ' . $sendHour . ' hour' . PHP_EOL;
        echo '--> Time zones support is ' . ($timezonesEnabled ? 'enabled' : 'disabled') . PHP_EOL;
        echo '--> Dry run mode is ' . ($dryRun ? 'enabled' : 'disabled') . PHP_EOL;
    }

    $client = new Model_Workers_AlertProcessClient();
    $client->setProfileMode($verbose);
    $client->setSendHour($sendHour);
    $client->setDryRun($dryRun);
    $client->setTimezonesEnabled($timezonesEnabled);
    $client->setTimezonesList($timezonesList);
    $client->processAlerts();
}
