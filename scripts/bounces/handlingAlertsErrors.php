#!/usr/bin/env php
<?php
/**
 * handlingAlertsErrors.php
 *
 * PHP Version 5.3
 *
 * @category Scripts
 * @package  bounces
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Handling bounces/complaints alerts
 * Usage: php handlingAlertsErrors.php
 */

include realpath(__DIR__ . '/../init.php');

$dbh = Zend_Db_Table::getDefaultAdapter();

$sql = 'select * from trans_email_handling where handling_status = ? order by handling_fourth';
$bind = array();
$bind[] = \GetIt\Queue\Queue::STATUS_NEW;
$rows = $dbh->fetchAll($sql, $bind);
foreach ($rows as $row) {
    $status = NULL;
    if ($row['handling_type'] == \GetIt\Queue\Queue::TYPE_BOUNCE) {
        if ($row['handling_subType'] == \GetIt\Queue\Queue::TYPE_BOUNCE_PERMANENT) {
            $status = Model_Alert::STATUS_SUSPENDED_AS_BOUNCE_PERMANENT;
        } else {
            $status = Model_Alert::STATUS_SUSPENDED_AS_BOUNCE_TRANSIENT;
        }
    } elseif ($row['handling_type'] == \GetIt\Queue\Queue::TYPE_COMPLAINT) {
        $status = Model_Alert::STATUS_SUSPENDED_AS_COMPLAINT;
    }

    if (empty($status)) {
        markAsProcessed($row['handling_id']);
        continue;
    }

    $alert = Model_Alert::getBySesMessageId($row['handling_messageId']);
    if ($alert) {
        $contact = $alert->getContact();
        if ($contact->getEmail() == $row['handling_email']) {
            switch ($status) {
                case Model_Alert::STATUS_SUSPENDED_AS_BOUNCE_PERMANENT:
                    $contactAlerts = $contact->getAllAlerts();
                    foreach ($contactAlerts as $contactAlert) {
                        $contactAlert->markAsBouncePermanent();
                    }
                    break;
                case Model_Alert::STATUS_SUSPENDED_AS_BOUNCE_TRANSIENT:
                    $alert->markAsBounceTransient();
                    break;
                case Model_Alert::STATUS_SUSPENDED_AS_COMPLAINT;
                    $alert->markAsComplaint();
                    break;
            }
        }
    } elseif (!empty($row['handling_email'])) {
        $contact = Model_Contact::getByEmail($row['handling_email']);
        if ($contact) {
            $alerts = $contact->getAllAlerts();
            foreach ($alerts as $alert) {
                switch ($status) {
                    case Model_Alert::STATUS_SUSPENDED_AS_BOUNCE_PERMANENT:
                        $alert->markAsBouncePermanent();
                        break;
                    case Model_Alert::STATUS_SUSPENDED_AS_BOUNCE_TRANSIENT:
                        $alert->markAsBounceTransient();
                        break;
                    case Model_Alert::STATUS_SUSPENDED_AS_COMPLAINT;
                        $alert->markAsComplaint();
                        break;
                }
            }
        }
    }
    markAsProcessed($row['handling_id']);
}

function markAsProcessed ($id) {
    $dbh = Zend_Db_Table::getDefaultAdapter();
    $sql = 'update trans_email_handling set handling_status = ?, handling_updated = ? where handling_id = ?';
    $bind = array();
    $bind[] = \GetIt\Queue\Queue::STATUS_PROCESSED;
    $bind[] = Model_Utils::sqlNow();
    $bind[] = $id;
    $dbh->query($sql, $bind);
}