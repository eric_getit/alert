#!/usr/bin/env php
<?php
/**
 * handlingQueue.php
 *
 * PHP Version 5.3
 *
 * @category Scripts
 * @package  bounces
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Script for getting bounces/complaints message from amazon sqs and store in our database
 * Usage: php handlingQueue.php
 */

include realpath(__DIR__ . '/../init.php');

$bounces = 'ses-bounces';
$complaints = 'ses-complaints';

$queue = new \GetIt\Queue\Queue();
$queue->handling($bounces);
$queue->handling($complaints);
