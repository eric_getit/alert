#!/usr/bin/env php
<?php
/**
 * Build the data for Overview Old report (populate report_general table)
 */
include realpath(__DIR__ . '/../init.php');

Model_Admin_Reports_OverviewOld::build();
