<?php
/**
 * init.php
 *
 * PHP Version 5.3
 *
 * @category Scripts
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Init environment for scripts
 *
 */
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'development'));
// Define path to configs folder
defined('CONFIGS_PATH')
    || define('CONFIGS_PATH', realpath(dirname(__FILE__) . '/../configs'));
defined('VAR_PATH')
    || define('VAR_PATH', realpath(dirname(__FILE__) . '/../var'));

$loader = require_once realpath(__DIR__ . '/../vendor/autoload.php');
$loader->add('GetIt', realpath(APPLICATION_PATH . '/../library/TransEmail/src'));
$loader->add('Ses', realpath(APPLICATION_PATH . '/../library/TransEmail/src'));
$loader->add('Sqs', realpath(APPLICATION_PATH . '/../library/TransEmail/src'));
$loader->add('Zend', realpath(APPLICATION_PATH . '/../library/TransEmail/src'));


$config = include CONFIGS_PATH . '/application.php';
// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    $config
);

$application->bootstrap();

return $config;
