#!/usr/bin/env php
<?php
/**
 * complete_sending.php
 *
 * PHP Version 5.3
 *
 * @category Scripts
 * @package  ec2_control
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Check if we have sent alerts and when done:
 * - reduce DynamoDb table capacity
 * - stop servers
 */

if ($_SERVER['argc'] < 2) {
    echo 'Usage:' . PHP_EOL;
    echo '    ' . $_SERVER['argv'][0] . ' <instance id 1> <instance id 2> ... <instance id N>' . PHP_EOL;
    echo 'Stop servers when done sending alerts.' . PHP_EOL;
    echo 'At least one instance ID should be given as parameters' . PHP_EOL . PHP_EOL;
    echo 'Example:' . PHP_EOL;
    echo '    ' . $_SERVER['argv'][0] . ' i-139ecf72 i-d50859b4' . PHP_EOL;
    die(1);
}

$config = require(realpath(dirname(__FILE__)) . '/../init.php');

if (!Model_Alert::hasScheduledAlerts()) {

    array_shift($_SERVER['argv']);
    $ec2instance = new Model_Amazon_Ec2Instance($config['aws']);
    foreach ($_SERVER['argv'] as $instanceId) {
        $state = $ec2instance->getState($instanceId);
        if ($state != 'running') {
            echo 'Can not stop not running instance ' . $instanceId . '. Current instance state is ' . $state . PHP_EOL;
        }
        echo 'Sending stop command ... ';
        $state = $ec2instance->stop($instanceId);
        echo 'done.' . PHP_EOL;
        echo 'Current instance state is ' . $state . PHP_EOL;
    }
}
