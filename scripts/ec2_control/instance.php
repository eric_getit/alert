#!/usr/bin/env php
<?php
/**
 * instance.php
 *
 * PHP Version 5.3
 *
 * @category Scripts
 * @package  ec2_control
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Start/stop Amazon EC2 instance
 */

if ($_SERVER['argc'] < 3) {
    echo 'Usage:' . PHP_EOL;
    echo '    ' . $_SERVER['argv'][0] . ' <instance id> {start|stop|status}' . PHP_EOL;
    echo 'Start/stop EC2 instance.' . PHP_EOL . PHP_EOL;
    echo 'Example:' . PHP_EOL;
    echo '    ' . $_SERVER['argv'][0] . ' i-139ecf72 state' . PHP_EOL;
    die(1);
}

$config = require(realpath(dirname(__FILE__)) . '/../init_base.php');

$instanceId = $_SERVER['argv'][1];
$action = $_SERVER['argv'][2];

$ec2instance = new Model_Amazon_Ec2Instance($config['aws']);

switch ($action) {

    case 'start':
        $state = $ec2instance->getState($instanceId);
        if ($state != 'stopped') {
            echo 'Can not run not stopped instance. Current instance state is ' . $state . PHP_EOL;
            die(1);
        }
        echo 'Sending start command ... ';
        $state = $ec2instance->start($instanceId);
        echo 'done.' . PHP_EOL;
        echo 'Current instance state is ' . $state . PHP_EOL;
        break;

    case 'stop':
        $state = $ec2instance->getState($instanceId);
        if ($state != 'running') {
            echo 'Can not stop not running instance. Current instance state is ' . $state . PHP_EOL;
            die(1);
        }
        echo 'Sending stop command ... ';
        $state = $ec2instance->stop($instanceId);
        echo 'done.' . PHP_EOL;
        echo 'Current instance state is ' . $state . PHP_EOL;
        break;

    case 'state':
        $state = $ec2instance->getState($instanceId);
        echo 'Instance ' . $instanceId . ' state is ' . $state . PHP_EOL;
        break;

}
