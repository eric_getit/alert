#!/usr/bin/env php
<?php
/**
 * extract_dynamodb.php
 *
 * PHP Version 5.3
 *
 * @category Scripts
 * @package  ec2_control
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Extract data from DynamoDb gateway URL table and insert it into MySQL table.
 */

if ($_SERVER['argc'] < 3) {
    echo 'Usage:' . PHP_EOL;
    echo '    ' . $_SERVER['argv'][0] . ' <dynamo_db_table_name> <target_table_name> [<last_key>]' . PHP_EOL;
    echo 'Extract all data from dynamo db and pass it to MySQL db.' . PHP_EOL;
    echo 'Example:' . PHP_EOL;
    echo '    ' . $_SERVER['argv'][0] . ' gateway_url email_link' . PHP_EOL;
    die(1);
}

if (isset($_SERVER['argv'][3])) {
    $lastKey = $_SERVER['argv'][3];
} else {
    $lastKey = null;
}

$config = require(realpath(dirname(__FILE__)) . '/../init_base.php');

use Aws\DynamoDb\DynamoDbClient;
use Aws\DynamoDb\Enum\Type;

$application = new Zend_Application(
        APPLICATION_ENV,
        $config
);

$application->bootstrap('db');

// Instantiate the DynamoDB client with your AWS credentials
$aws = Aws\Common\Aws::factory($config['aws']);
$client = $aws->get("dynamodb");

$dynamoTableName = $_SERVER['argv'][1] ;
$mysqlTableName = $_SERVER['argv'][2] ;
$params = array(
    "TableName" => $dynamoTableName,
    "Limit" => 500,
);

if ($lastKey) {
    $params['ExclusiveStartKey'] = array(
        'HashKeyElement' => array('S' => $lastKey),
    );
}

$dbh = Zend_Db_Table::getDefaultAdapter();

$fields = array(
    'code', 'url', 'type', 'alert_id'
);

$fieldString = implode(', ', array_merge($fields, array('created')));

// Execute scan operations until the entire table is scanned
$count = 0;
do {
    $response = $client->scan($params);
    $items = $response->get("Items");
    $count = $count + count($items);

    $valuesStrings = array();

    // Do something with the $items
    foreach($items as $item) {
        $values = array();
        foreach ($fields as $field) {
            $values[] = $dbh->quote(current($item[$field]));
        }
        $values[] = 'NOW()';
        $valuesStrings[] = '(' . implode(', ', $values) . ')';
    }
    $valuesString = implode(",\n", $valuesStrings);
    $sql = "INSERT IGNORE INTO {$mysqlTableName}\n({$fieldString})\nVALUES {$valuesString}";
    $dbh->query($sql);
    // Set parameters for next scan
    echo "[" . date('H:i:s') . "] --> Total readed: {$count}. Last key: {$response["LastEvaluatedKey"]['HashKeyElement']['S']}" . PHP_EOL;
    $params["ExclusiveStartKey"] = $response["LastEvaluatedKey"];

} while ($params["ExclusiveStartKey"]);

echo "{$dynamoTableName} table scanned completely. {$count} items found.\n";
