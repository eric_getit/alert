#!/usr/bin/env php
<?php
/**
 * dns_update.php
 *
 * PHP Version 5.3
 *
 * @category Scripts
 * @package  ec2_control
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Update DNS for the server with current server public IP address
 */

if ($_SERVER['argc'] < 4) {
    echo 'Usage:' . PHP_EOL;
    echo '    ' . $_SERVER['argv'][0] . ' <server domain name> <record type> <record value> [<ttl>]' . PHP_EOL;
    echo 'Update DNS zone in Route 53 with server name. Server name should be FQDN ending with dot (.).' . PHP_EOL;
    echo 'Default TTL time is 60 seconds.' . PHP_EOL . PHP_EOL;
    echo 'Example:' . PHP_EOL;
    echo '    ' . $_SERVER['argv'][0] . ' cron.getitcorporate.com. CNAME `curl -s http://169.254.169.254/latest/meta-data/public-hostname` 60' . PHP_EOL;
    die(1);
}

$config = require(realpath(dirname(__FILE__)) . '/../init_base.php');

$dnsName = $_SERVER['argv'][1];
$recordType = $_SERVER['argv'][2];
$recordValue = $_SERVER['argv'][3];
if (isset($_SERVER['argv'][4])) {
    $ttl = intval($_SERVER['argv'][4]);
} else {
    $ttl = 60;
}

$dnsParts = explode('.', $dnsName);
$hostName = array_shift($dnsParts);
$domainName = implode('.', $dnsParts);

$route53 = new Model_Amazon_Route53($config['aws']);
$hostedZoneId = $route53->getHostedZoneId($domainName);

if ($hostedZoneId == null) {
    echo 'Domain name ' . $domainName . ' for the host ' . $dnsName . ' not found in the list of managed zones.' . PHP_EOL;
    die(2);
}

echo 'Altering DNS' . PHP_EOL;
echo '--> Hosted Zone Id: ' . $hostedZoneId . PHP_EOL;
echo '--> Name: ' . $dnsName . PHP_EOL;
echo '--> Record Type: ' . $recordType . PHP_EOL;
echo '--> TTL: ' . $ttl . PHP_EOL;
echo '--> Value: ' . $recordValue . PHP_EOL;
echo 'Sending commands to S3 ... ';
$changeId = $route53->changeRecord($hostedZoneId, $dnsName, $recordType, $ttl, $recordValue);
echo 'done.' . PHP_EOL;
echo 'Waiting for changes to synchronise ... ';

$changeStatus = $route53->getChangeStatus($changeId);
while ($changeStatus != 'INSYNC') {
    sleep(5);
    $changeStatus = $route53->getChangeStatus($changeId);
}
echo 'done. Changes are INSYNC.' . PHP_EOL;
