# Alert System #

Project description goes here.


# Server Dependencies #

* Apache
* MySQL
* Redis
* Gearman
* PHP


# Installation Instructions #

## PHP and Libraries ##

### PHP Installation ##

We need PHP, pear and build tools

```
aptitude install php5 php5-dev php-pear make
```

### Gearman client for PHP ###

Gearman client is not present in Ubuntu distributive and should be installed directly from Pecl.
Latest client version requires gearman server and libraries which are not yet
included in Ubuntu distributive even in 13.04 release. We take library from Gearman PPA
See [Not able to install Gearman on Ubuntu 12.04](http://serverfault.com/questions/487331/not-able-to-install-gearman-on-ubuntu-12-04)

```
aptitude install python-software-properties
add-apt-repository ppa:gearman-developers/ppa
aptitude update
aptitude install libgearman-dev
pecl install gearman
```
You should add "extension=gearman.so" to php.ini or create config in /etc/php5/conf.d/gearman.ini

Check Gearman client version

```
root@soft-rose:/etc/php5/cli/conf.d# php -a
Interactive shell

php > print gearman_version() . PHP_EOL;
1.0.6
php >
```

## Application Configuration ##

Application configuration is defined in two PHP files in `configs` folder:

* `application.php` - for public web application and scripts
* `admin.php` - admin web interface

These filese are create from `application-orig.php` and `admin-orig.php`. Configuration files
are added to git ignore lists and not saved in the repository.

Configuration files should be updated when application is updated.

### Building Configuration Files ###

Configuration files should be build from original files using ant build script.

1. Create or update `build.properties` file in the application root folder. Define values
  for properties used in the configuration.
  
  ```
  prod-db-host=<production DB host>
  prod-db-user=<production DB user name>
  prod-db-pass=<production DB user password>
  prod-db-name=<production DB schema name>
  test-db-host=<test DB host (for unit tests)>
  test-db-user=<test DB user name>
  test-db-pass=<test DB user password>
  test-db-name=<test DB schema name>
  aws-key=<Amazon API access key>
  aws-secret=<Amazon API access secret>
  aws-region=<Amazon region used in API calls>
  redis-host=<Redis server host>
  redis-port=<Redis server port number>
  gearman-host=<Gearman server host>
  gearman-port=<Gearman server port number>
  supervisor-host=<supervisor host name>
  supervisor-port=<supervisor service port name>
  ```
2. Update configration files using ant build script
  ```
  $ ant update-configs
  ```

