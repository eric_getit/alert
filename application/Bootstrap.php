<?php
/**
 * Bootstrap
 *
 * PHP Version 5.3
 *
 * @category Class
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Bootstrap the application, do common initialisation tasks
 */
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    /**
     * Initialise auto loader for application models
     *
     * @return void
     */
    protected function _initAutoload()
    {
        $autoloader = new Zend_Application_Module_Autoloader(array(
            'namespace' => '',
            'basePath' => APPLICATION_PATH
        ));
    } // _initAutoload

    /**
     * Initialise application logging
     *
     * @return void
     */
    protected function _initLogging()
    {
        $this->bootstrap('frontController');
        $this->bootstrap('environment');

        $logger = new Zend_Log();
        if ('development' == $this->getEnvironment() || DEBUG_IP) {
            $writer = new Zend_Log_Writer_Firebug();
            $logger->addWriter($writer);
        } else {
            $writer = new Zend_Log_Writer_Stream(VAR_PATH . '/app.log');
            $logger->addWriter($writer);
            $filter = new Zend_Log_Filter_Priority(Zend_Log::CRIT);
            $logger->addFilter($filter);
        }
        Zend_Registry::set('logger', $logger);
        $this->_logger = $logger;
    } // _initLogging

    /**
     * Init DB profiling for development invironment
     *
     * @return void
     */
    protected function _initDbProfiler()
    {
        $this->bootstrap('logging');
        $this->_logger->info('Bootstrap ' . __METHOD__);
        if ('production' !== $this->getEnvironment() || DEBUG_IP) {
            $this->bootstrap('db');
            $profiler = new Zend_Db_Profiler_Firebug(
                'All DB Queries'
            );
            $profiler->setEnabled(true);
            Zend_Db_Table::getDefaultAdapter()
                ->setProfiler($profiler);
        }
    } // _initDbProfiler

    /**
     * Init Trans Email Library
     *
     * @return void
     */
    protected function _initTransEmail()
    {
        $this->bootstrap('db');
        $adapter = Zend_Db_Table::getDefaultAdapter();
        \GetIt\Email\Email::setPdo($adapter->getConnection());
        $configs = $this->getOption('transemail');
        foreach ($configs['driver'] as $name => $config) {
            if (isset($config['isDefault'])
                && (boolean)$config['isDefault']
            ) {
                \GetIt\Email\Email::setDefaultDriver($config);
            }
        }

        if (!empty($configs['queue'])) {
            \GetIt\Queue\Queue::setPdo($adapter->getConnection());
            foreach ($configs['queue'] as $name => $config) {
                if (isset($config['isDefault'])
                    && (boolean)$config['isDefault']
                ) {
                    \GetIt\Queue\Queue::setDefaultDriver($config);
                }
            }
        }
    } // _initTransEmail

    /**
     * Initialize connections such as redis supervisor etc
     *
     * @return void
     */
    protected function _initConnections()
    {
        $configs = $this->getOption('connections');
        foreach ($configs as $name => $config) {
            $methodName = 'set' . ucfirst($name) . 'Config';
            if (method_exists('Model_ConnectionManager', $methodName)) {
                Model_ConnectionManager::$methodName($config);
            }
        }
    }

    /**
     * Initialise Twig
     *
     * @return void
     */
    protected function _initTwig()
    {
        $config = $this->getOption('twig');
        Model_Page::setTwitOptions($config);
    } // _initTwig

    /**
     * Initialize Enviroment
     *
     * @return void
     */
    protected function _initEnvironment()
    {
        defined('DEBUG_IP') || define("DEBUG_IP", false);

    } // _initEnviroment

    /**
     * Get the client IP address from Amazon LB cluster.
     *
     * @return void
     */
    protected function _initRemoteAddr()
    {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $_SERVER['REMOTE_ADDR'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
    } // _initRemoteAddr
}
