<?php
/**
 * EmailController
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Controller
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Email gateway URLs
 *
 * Redirect user to listing detail page, register gateway URL click.
 */
class EmailController extends Zend_Controller_Action
{
    /**
     * Find gateway URL for the code, redirect to the listing detail page
     * and register gateway URL click.
     *
     * Gateway URL is http://<site domain>.getitcorporate.com/email/<code>
     *
     * Zend Framework recognise this as call to <code> action in email controller.
     * Instead of writing custom route we implement init() method
     * which prevents from actual action being called.
     * This is kind of dirty hack and we might want to have normal implementation
     * here.
     *
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        $linkCode = $this->getRequest()->getParam('action', '');
        $emailLink = new Model_EmailLink();

        $dashPos = strpos($linkCode, '-');
        if ($dashPos > 0) {
            $code = substr($linkCode, 0, $dashPos);
            $alertId = substr($linkCode, $dashPos + 1);

            $linkFound = $emailLink->findLink($code);
            if ($linkFound) {
                $click = new Model_EmailClick();
                $click->addClick($emailLink);

                $jobUrl = $emailLink->getUrl();
                $alert = new Model_Alert($alertId);
                if ($emailLink->getType() ==  Model_Listing::TYPE_ORGANIC) {
                    $jobUrl = $alert->setUtm($jobUrl);
                } else {
                    // Fix site for white labelling
                    $query = parse_url($jobUrl, PHP_URL_QUERY);

                    $params = array();
                    parse_str($query, $params);

                    if (!empty($params['r'])) {
                        $params['r'] = $alert->getSite()->getDomain();
                        $newQuery = http_build_query($params);

                        $host = parse_url($jobUrl, PHP_URL_HOST);
                        $path = parse_url($jobUrl, PHP_URL_PATH);

                        $jobUrl = 'http://' . $host . $path . '?' . $newQuery;
                    }
                }
                $this->redirect($jobUrl);
            }
        }

        $emailLink->setTableName(Model_EmailLink::TABLE_NAME_ARCHIVE);
        if ($emailLink->findLinkInSql($linkCode)) {
            $click = new Model_EmailClick();
            $click->addClick($emailLink);

            $this->redirect($emailLink->getUrl());
        }

        throw new Model_Exception_LinkNotFound();
    } // init
}
