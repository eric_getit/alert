<?php
/**
 * ApiController
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Controller
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * API request processing controller
 *
 * @author Eugene Churmanov
 *
 */
class ApiController extends Zend_Controller_Action
{

    public function init()
    {
        $front = $this->getFrontController();
        $plugin = $front->getPlugin('Zend_Controller_Plugin_ErrorHandler');
        $plugin->setErrorHandlerAction("api");
    }

    /**
     * Api URL: /api/subscribe
     * Params:
     * <ul>
     *     <li>alertType (required) - Search periodic type (DAILY|WEEKLY)</li>
     *     <li>domain (required) - source domain name (i.e. getnichejobs.com)</li>
     *     <li>siteName (optional, required if new domain creation) - Writeable site name (i.e. Get Niche Jobs)</li>
     *     <li>siteNiche (optional, required if new domain creation) - Writeable site niche part (i.e. Niche)</li>
     *     <li>newsletterDomain (optional, required if new domain creation) - Writeable UC domain name (i.e. GetNicheJobs.com)</li>
     *     <li>fromName (optional, required if new domain creation) - Site contact person name (i.e. Casey Wilson)</li>
     *     <li>fromEmail (optional, required if new domain creation) - Site contact email address (i.e. contact@getnichejobs.com)</li>
     *     <li>email (optional, required if new contact creation) - Subscriber email address</li>
     *     <li>query (optional, required if location not passed) - Alert search terms</li>
     *     <li>location (optional, required if query not passed) - Alert search location</li>
     *     <li>contactId (optional) - Contact Unique ID</li>
     *     <li>contactStatus (optional) - Set status for contact</li>
     *     <li>sourceId (optional) - Unique Contact ID in Source system</li>
     *     <li>originalCreatedDate (optional) - Original contact creation date</li>
     *     <li>country (optional) - Subscriber address country</li>
     *     <li>state (optional) - Subscriber address state or region</li>
     *     <li>city (optional) - Subscriber address city</li>
     *     <li>zip (optional) - Subscriber address postal code</li>
     *     <li>address (optional) - Subscriber address (street, house, etc.)</li>
     *     <li>name (optional) - Subscriber name</li>
     *     <li>distance (optional) - search distance</li>
     *     <li>ip (optional) - Subscriber IP</li>
     *     <li>status (optional) - Only option now is: pending</li>
     * </ul>
     * @throws Model_Exception_MissingRequireParam
     * @throws Model_Exception_UnsupportedParamValue
     */
    public function subscribeAction()
    {
        if (APPLICATION_ENV == 'production'
            && !$this->getRequest()->isPost()
        ) {
            throw new Exception("Use HTTP POST for this API method");
        }
        $result = array(
            'success' => true,
        );
        $contact = null;
        $alert = null;
        $params = $this->getRequest()->getParams();

        $errors = Model_Alert::checkAlertParams($params);
        if (!empty($errors)) {
            throw array_pop($errors);
        }

        $dbh = Zend_Db_Table::getDefaultAdapter();
        $dbh->beginTransaction();

        $domain = $params['domain'];
        $site = Model_Site::getByDomain($domain);
        if ($site == null) {
            $site = Model_Site::registry($params);
        }

        if (array_key_exists('contactId', $params)) {
            $contact = Model_Contact::getBySecureId($params['contactId']);
        }

        if ($contact == null && array_key_exists('sourceId', $params)) {
            $contact = Model_Contact::getBySourceCredential($params['sourceId']);
        }
        if ($contact == null && array_key_exists('email', $params)) {
            $contact = Model_Contact::getByEmail($params['email']);
        }
        if ($contact == null) {
            $contact = Model_Contact::registry($params);
        }
        //TODO: Remove when multi-alerts functionality will be ready
        $currentAlerts = $contact->getActiveAlerts();
        if (!empty($currentAlerts)) {
            $params['status'] = 'pending';
        }
        //---
        $alert = $contact->createAlert($params);
        $result['alertUniqueId'] = $alert->getAlertSecureId();
        $result['contactUniqueId'] = $contact->getContactSecureId();

        $dbh->commit();

        $this->getHelper("Json")->suppressExit = true;
        $this->getHelper("Json")->sendJson($result);
    }

    /**
     * Api URL: /api/alerts
     * Params:
     * <ul>
     *     <li>contactId (required) - Contact Unique ID</li>
     *     <li>activeOnly (option) - If passed and not empty - return only listing with ACTIVE status</li>
     * </ul>
     *
     * @throws Model_Exception_MissingRequireParam
     */
    public function alertsAction()
    {
        $result = array(
            'success' => true,
        );
        $contact = null;
        $alert = null;
        $params = $this->getRequest()->getParams();
        if (!array_key_exists('contactId', $params)) {
            throw new Model_Exception_MissingRequireParam('contactId');
        }
        $contact = Model_Contact::getBySecureId($params['contactId']);
        if ($contact == null) {
            throw new Model_Exception_UnknownId();
        }

        if (array_key_exists('activeOnly', $params) && $params['activeOnly']) {
            $alerts = $contact->getActiveAlerts();
        } else {
            $alerts = $contact->getAllAlerts();
        }

        $result['contactUniqueId'] = $contact->getContactSecureId();
        $result['alerts'] = array();
        /* @var $alert Model_Alert */
        foreach ($alerts as $alert) {
            $alertInfo = array(
                    'alertUniqueId' => $alert->getAlertSecureId(),
                    'status' => $alert->getAlertStatus(),
                    'type' => $alert->getAlertType(),
                    'searchTerms' => $alert->getQueryTerms(),
                    'location' => $alert->getLocation(),
                    'lastSend' => $alert->getLastSendDate(),
            );
            $result['alerts'][] = $alertInfo;
        }

        $this->getHelper("Json")->suppressExit = true;
        $this->getHelper("Json")->sendJson($result);
    }

    /**
     * Api URL: /api/alert
     * Params:
     * <ul>
     *     <li>alertId (required) - Alert Unique ID</li>
     * </ul>
     *
     * @throws Model_Exception_MissingRequireParam
     */
    public function alertAction()
    {
        $result = array(
                'success' => true,
        );
        $contact = null;
        $alert = null;
        $params = $this->getRequest()->getParams();
        if (!array_key_exists('alertId', $params)) {
            throw new Model_Exception_MissingRequireParam('alertId');
        }
        $alert = Model_Alert::getBySecureId($params['alertId']);

        if ($alert == null) {
            throw new Model_Exception_UnknownId();
        }

        $contact = new Model_Contact($alert->getContactId());

        $result['contactUniqueId'] = $contact->getContactSecureId();
        $result['alert'] = array(
            'alertUniqueId' => $alert->getAlertSecureId(),
            'status' => $alert->getAlertStatus(),
            'type' => $alert->getAlertType(),
            'searchTerms' => $alert->getQueryTerms(),
            'location' => $alert->getLocation(),
            'lastSend' => $alert->getLastSendDate(),
        );

        $this->getHelper("Json")->suppressExit = true;
        $this->getHelper("Json")->sendJson($result);
    }


    /**
     * Api URL: /api/unsubscribe
     * Params:
     * <ul>
     *     <li>alertId (required) - Alert Unique ID</li>
     * </ul>
     * @throws Model_Exception_MissingRequireParam
     * @throws Model_Exception_UnknownId
     */
    public function unsubscribeAction()
    {
        if (APPLICATION_ENV == 'production'
            && !$this->getRequest()->isPost()
        ) {
            throw new Exception("Use HTTP POST for this API method");
        }
        $result = array(
                'success' => true,
        );
        $contact = null;
        $alert = null;
        $params = $this->getRequest()->getParams();
        if (!array_key_exists('alertId', $params)) {
            throw new Model_Exception_MissingRequireParam('alertId');
        }
        $alert = Model_Alert::getBySecureId($params['alertId']);

        if ($alert == null) {
            throw new Model_Exception_UnknownId();
        }

        $alert->setAlertStatus(Model_Alert::STATUS_UNSUBSCRIBED_API);
        $alert->update();
        Model_AlertOperationHistory::registryOperation(
                $alert,
                Model_AlertOperationHistory::OPERATION_UNSUBSCRIBE_API,
                $_SERVER['REMOTE_ADDR']
            );


        $contact = new Model_Contact($alert->getContactId());

        $result['contactUniqueId'] = $contact->getContactSecureId();
        $result['alert'] = array(
            'alertUniqueId' => $alert->getAlertSecureId(),
            'status' => $alert->getAlertStatus(),
            'type' => $alert->getAlertType(),
            'searchTerms' => $alert->getQueryTerms(),
            'location' => $alert->getLocation(),
            'lastSend' => $alert->getLastSendDate(),
        );

        $this->getHelper("Json")->suppressExit = true;
        $this->getHelper("Json")->sendJson($result);
    }

    /**
     * Api URL: /api/suspend
     * Params:
     * <ul>
     *     <li>email (optional, required if contactId not specified) - Contact Email</li>
     *     <li>contactId (optional, required if email not specified) - Contact Unique ID</li>
     * </ul>
     * @throws Model_Exception_MissingRequireParam
     * @throws Model_Exception_UnknownId
     */
    public function suspendAction()
    {
        $this->verifyPostVerb();

        $result = array(
            'success' => true,
        );
        $contact = null;
        $params = $this->getRequest()->getParams();

        if (!array_key_exists('contactId', $params) && !array_key_exists('email', $params)) {
            throw new Model_Exception_MissingRequireParam('contactId or email');
        }

        if (array_key_exists('contactId', $params)) {
            $contact = Model_Contact::getBySecureId($params['contactId']);
        } else {
            $contact = Model_Contact::getByEmail($params['email']);
        }

        if ($contact == null) {
            throw new Model_Exception_UnknownId();
        }

        $contact->setContactStatus(Model_Contact::STATUS_SUSPEND);
        $contact->update();

        /*
         * This doesn't seem very helpful.
         * Shouldn't the contact being suspended be enough?
         * If not, it should be :)
         */
//        $alerts = $contact->getActiveAlerts();
//        foreach ($alerts as $alert) {
//            $alert->setAlertStatus(Model_Alert::STATUS_SUSPEND);
//            $alert->update();
//        }

        $result['contactUniqueId'] = $contact->getContactSecureId();

        $this->getHelper("Json")->suppressExit = true;
        $this->getHelper("Json")->sendJson($result);
    }


    /**
     * Api URL: /api/restore
     * Params:
     * <ul>
     *     <li>email (optional, required if contactId not specified) - Contact Email</li>
     *     <li>contactId (optional, required if email not specified) - Contact Unique ID</li>
     * </ul>
     * @throws Model_Exception_MissingRequireParam
     * @throws Model_Exception_UnknownId
     */
    public function restoreAction()
    {
        $this->verifyPostVerb();

        $result = array(
            'success' => true,
        );
        $contact = null;
        $params = $this->getRequest()->getParams();

        if (!array_key_exists('contactId', $params) && !array_key_exists('email', $params)) {
            throw new Model_Exception_MissingRequireParam('contactId or email');
        }

        if (array_key_exists('contactId', $params)) {
            $contact = Model_Contact::getBySecureId($params['contactId']);
        } else {
            $contact = Model_Contact::getByEmail($params['email']);
        }

        if ($contact == null) {
            throw new Model_Exception_UnknownId();
        }

        /*
         * Lets only restore the Contact if their status says they were suspended
         * to begin with. I don't want to risk turning back on one that was effected
         * by something else.
         */
        if (Model_Contact::STATUS_SUSPEND == $contact->getContactStatus()) {
            $contact->setContactStatus(Model_Contact::STATUS_ACTIVE);
            $contact->update();
        }

        /*
         * Same as above.
         */
//        $alerts = $contact->getAllAlerts();
//        foreach ($alerts as $alert) {
//            if ($alert->getAlertStatus() == Model_Alert::STATUS_SUSPEND) {
//                $alert->setAlertStatus(Model_Alert::STATUS_ACTIVE);
//                $alert->update();
//            }
//        }

        $result['contactUniqueId'] = $contact->getContactSecureId();

        $this->getHelper("Json")->suppressExit = true;
        $this->getHelper("Json")->sendJson($result);
    }


    /**
     * Api URL: /api/update-alert
     * Params:
     * <ul>
     *     <li>query (option, required if location not passed) - Alert search terms</li>
     *     <li>alertType (required) - Search periodic type (DAILY|WEEKLY)</li>
     *     <li>location (option, required if query not passed) - Alert search location</li>
     *     <li>distance (option) - search distance</li>
     *     <li>ip (option) - Subscriber IP</li>
     * </ul>
     * @throws Model_Exception_MissingRequireParam
     * @throws Model_Exception_UnknownId
     */
    public function updateAlertAction()
    {
        if (APPLICATION_ENV == 'production'
            && !$this->getRequest()->isPost()
        ) {
            throw new Exception("Use HTTP POST for this API method");
        }
        $result = array(
                'success' => true,
        );

        $contact = null;
        $alert = null;
        $params = $this->getRequest()->getParams();
        if (!array_key_exists('alertId', $params)) {
            throw new Model_Exception_MissingRequireParam('alertId');
        }
        $alert = Model_Alert::getBySecureId($params['alertId']);

        if ($alert == null) {
            throw new Model_Exception_UnknownId();
        }

        $contact = new Model_Contact($alert->getContactId());

        $alert->updateAlert($params);
        if (array_key_exists('ip', $params)) {
            $userIp = $params['ip'];
        } else {
            $userIp = $_SERVER['REMOTE_ADDR'];
        }
        Model_AlertOperationHistory::registryOperation(
                $alert,
                Model_AlertOperationHistory::OPERATION_UPDATED_API,
                $userIp
            );

        $result['alert'] = array(
            'alertUniqueId' => $alert->getAlertSecureId(),
            'status' => $alert->getAlertStatus(),
            'type' => $alert->getAlertType(),
            'searchTerms' => $alert->getQueryTerms(),
            'location' => $alert->getLocation(),
            'distance' => $alert->getDistance(),
            'lastSend' => $alert->getLastSendDate(),
        );
        $result['contactUniqueId'] = $contact->getContactSecureId();

        $this->getHelper("Json")->suppressExit = true;
        $this->getHelper("Json")->sendJson($result);
    }

    /**
     * Api URL: /api/exists
     * Params:
     * <ul>
     *     <li>email (required) - Email for contact</li>
     * </ul>
     *
     * @throws Model_Exception_MissingRequireParam
     * @throws Model_Exception_NoRecordForObject
     */
    public function existsAction()
    {
        $result = array(
            'success' => true,
        );

        $params = $this->getRequest()->getParams();

        if (!array_key_exists('email', $params)) {
            throw new Model_Exception_MissingRequireParam('email');
        }

        $contact = Model_Contact::getByEmail($params['email']);

        if (!is_null($contact)) {
            $result['contactUniqueId'] = $contact->getContactSecureId();
        } else {
            throw new Model_Exception_NoRecordForObject();
        }

        $this->getHelper("Json")->suppressExit = true;
        $this->getHelper("Json")->sendJson($result);
    } // existsAction

    /**
     * Api URL: /api/newsletter/new
     * Params:
     * <ul>
     *     <li>id (required) - newsletter id in external system (string - 255)</li>
     *     <li>name (required) - newsletter name for admin control page</li>
     *     <li>domain (required) - source domainname (example.com)</li>
     *     <li>siteName (optional, required if new domain creation) - Writeable site name (i.e. Get Niche Jobs)</li>
     *     <li>siteNiche (optional, required if new domain creation) - Writeable site niche part (i.e. Niche)</li>
     *     <li>newsletterDomain (optional, required if new domain creation) - Writeable UC domain name (i.e. GetNicheJobs.com)</li>
     *     <li>fromName (optional, required if new domain creation) - Site contact person name (i.e. Casey Wilson)</li>
     *     <li>fromEmail (optional, required if new domain creation) - Site contact email address (i.e. contact@getnichejobs.com)</li>
     * </ul>
     *
     * Api URL: /api/newsletter/queue
     * Params:
     * <ul>
     *     <li>id (required) - newsletter id for link queue with newsletter</li>
     *     <li>subject (required) - emails subject</li>
     *     <li>message (required) - newsletter message part (placeholder for template)</li>
     *     <li>sendTime (required) - date ans time for sending emails for this queue</li>
     *     <li>fromName (option) - contact person for emails</li>
     *     <li>fromEmail (option) - email address for emails</li>
     * </ul>
     *
     * Api URL: /api/newsletter/subscribe
     * Params:
     * <ul>
     *     <li>id (required) - newsletter id for link contact with newsletter</li>
     *     <li>email (optional, required if new subscribe creation) - Subscriber email address</li>
     *     <li>contactId (optional, required if email not specified) - Contact Unique ID</li>
     *     <li>originalCreatedDate (optional) - Original contact creation date</li>
     *     <li>country (optional) - Subscriber address country</li>
     *     <li>state (optional) - Subscriber address state or region</li>
     *     <li>city (optional) - Subscriber address city</li>
     *     <li>zip (optional) - Subscriber address postal code</li>
     *     <li>address (optional) - Subscriber address (street, house, etc.)</li>
     *     <li>name (optional) - Subscriber name</li>
     *     <li>ip (optional) - Subscriber IP</li>
     * </ul>
     *
     * @throws Model_Exception_MissingRequireParam
     * @throws Model_Exception_UnknowApiEndpoint
     * @throws Model_Exception_UnknownId
     */
    public function newsletterAction()
    {
        $result = array(
                'success' => true,
        );
        $params = $this->getRequest()->getParams();

        if (strpos($_SERVER['REQUEST_URI'], '/api/newsletter/new') === 0) {
            if (APPLICATION_ENV == 'production'
                    && !$this->getRequest()->isPost()
            ) {
                throw new Exception("Use HTTP POST for this API method");
            }

            $newsletter = Model_Newsletter::registry($params);
            $result['newsletterId'] = $newsletter->getNewsletterId();

        } else if (strpos($_SERVER['REQUEST_URI'], '/api/newsletter/queue') === 0) {
            if (APPLICATION_ENV == 'production'
                    && !$this->getRequest()->isPost()
            ) {
                throw new Exception("Use HTTP POST for this API method");
            }

            if (!array_key_exists('id', $params)) {
                throw new Model_Exception_MissingRequireParam("id");
            }
            $newsletter = Model_Newsletter::getByExternalId($params['id']);
            if ($newsletter == null) {
                throw new Model_Exception_UnknownId();
            }

            $queue = $newsletter->registryQueue($params);
            $result['queueId'] = $queue->getQueueId();

        } else if (strpos($_SERVER['REQUEST_URI'], '/api/newsletter/subscribe') === 0) {
            if (APPLICATION_ENV == 'production'
                    && !$this->getRequest()->isPost()
            ) {
                throw new Exception("Use HTTP POST for this API method");
            }

            if (!array_key_exists('id', $params)) {
                throw new Model_Exception_MissingRequireParam("id");
            }

            $contact = null;
            if (array_key_exists('contactId', $params)) {
                $contact = Model_Contact::getBySecureId($params['contactId']);
            }

            if ($contact == null && array_key_exists('email', $params)) {
                $contact = Model_Contact::getByEmail($params['email']);
            }
            if ($contact == null) {
                $contact = Model_Contact::registry($params);
            }

            $newsletter = Model_Newsletter::getByExternalId($params['id']);
            if ($newsletter == null) {
                throw new Model_Exception_UnknownId();
            }

            $subscription = $contact->subscribeNewsletter($newsletter);
            $result['subscriptionId'] = $subscription->getSubscriptionId();
            $result['subscriptionSecureId'] = $subscription->getSubscriptionSecureId();
            $result['subscriptionStatus'] = $subscription->getSubscriptionStatus();
        } else {
            throw new Model_Exception_UnknowApiEndpoint($_SERVER['REQUEST_URI']);
        }
        $this->getHelper("Json")->suppressExit = true;
        $this->getHelper("Json")->sendJson($result);
    } // newsletterAction

    /**
     * Register gateway URL
     *
     * @throws Exception
     * @throws Model_Exception_MissingRequireParam
     * @throws Model_Exception_UnsupportedParamValue
     *
     * @return void
     */
    public function registryLinkAction()
    {
        $result = array(
                'success' => true,
        );
        $params = $this->getRequest()->getParams();

        if (APPLICATION_ENV == 'production'
                && !$this->getRequest()->isPost()
        ) {
            throw new Exception("Use HTTP POST for this API method");
        }

        if (!array_key_exists('code', $params)) {
            throw new Model_Exception_MissingRequireParam("code");
        }

        if (!array_key_exists('url', $params)) {
            throw new Model_Exception_MissingRequireParam("url");
        }

        if (!array_key_exists('type', $params)) {
            throw new Model_Exception_MissingRequireParam("type");
        }

        $type = strtolower($params['type']);
        if ($type != Model_EmailLink::TYPE_BACKFILL
            && $type != Model_EmailLink::TYPE_ORGANIC
        ) {
            throw new Model_Exception_UnsupportedParamValue(
                'type',
                array(
                    Model_EmailLink::TYPE_BACKFILL,
                    Model_EmailLink::TYPE_ORGANIC,
                )
            );
        }

        $emailLink = new Model_EmailLink();
        $emailLink->setCode($params['code']);
        $emailLink->setType($type);
        $emailLink->setUrl($params['url']);
        $emailLink->insertIntoSql();

        $this->getHelper("Json")->suppressExit = true;
        $this->getHelper("Json")->sendJson($result);
    }//registryLinkAction

    /**
     * Checks if the request is post, and if it isn't will throw and Exception
     *
     * @throws Exception
     */
    protected function verifyPostVerb()
    {
        if (APPLICATION_ENV == 'production' && !$this->getRequest()->isPost()) {
            throw new Exception("Use HTTP POST for this API method");
        }
    }

}