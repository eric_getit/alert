<?php
/**
 * AjaxController
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Controller
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * AJAX request processing controller
 *
 * @author Eugene Churmanov
 *
 */
class AjaxController extends Zend_Controller_Action
{
    public function init()
    {
        $front = $this->getFrontController();
        $plugin = $front->getPlugin('Zend_Controller_Plugin_ErrorHandler');
        $plugin->setErrorHandlerAction("api");
    }


    public function locationAutocompleteAction()
    {
        $result = array();
        $locationSearch = $this->getRequest()->getParam('search');
        $locations = Model_Location::locationAutocomplete($locationSearch);

        foreach ($locations as $location) {
            /* @var $location Model_Location */
            $result[] = array('text' => $location->getCanonical());
        }

        $this->getHelper("Json")->suppressExit = true;
        $this->getHelper("Json")->sendJson($result);
    }

}