<?php
/**
 * PictureController
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Controller
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Controller for count email views // tracking pixel
 *
 */
class PictureController extends Zend_Controller_Action
{
    public function init()
    {
        $secureId = $this->getRequest()->getParam('action', '');
        $message = Model_MessageArchive::getBySecureId($secureId);

        if ($message === null) {
            $redisMessage = Model_Workers_Message::load($secureId);
            if ($redisMessage != null) {
                $message = new Model_MessageArchive($redisMessage->getMessageData());
            }
        }

        if ($message !== null) {
            Model_MessageView::addView($message);
        } else {
            $msg = 'Picture controller : don\'t found message for secureId ' . $secureId;
            error_log($msg);
        }

        $file = realpath(APPLICATION_PATH . '/../public/assets/img/pixel.gif');
        header('Content-Type: image/gif');
        header('Content-Length: ' . filesize($file));
        readfile($file);
        exit;
    }
}
