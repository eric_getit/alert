<?php
/**
 * AlertsController
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Controller
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * API request processing controller
 *
 * @author Eugene Churmanov
 *
 */
class AlertsController extends Zend_Controller_Action
{
    /**
     * Unsubscribe the alert
     *
     * URL: /alerts/unsubscribe
     * PARAMS:
     * - alert Alert secure ID
     */
    public function unsubscribeAction()
    {
        $params = $this->getRequest()->getParams();
        $page = null;
        if (array_key_exists('alert', $params)) {
            $page = new Model_Page_Alerts_UnsubscribeAlert($params);
        }
        if (array_key_exists('contact', $params)) {
            $page = new Model_Page_Alerts_UnsubscribeContact($params);
        }
        if ($page == null) {
            throw new Model_Exception_MissingRequireParam('alert');
        }
        $page->initView();
        $page->render();
    } // unsubscribeAction

    /**
     * Unsubscribe all alerts for the contact
     *
     * URL: /alerts/unsubscribe-all?alert=41ba4cd0
     * PARAMS:
     * - alert Alert secure ID
     */
    public function unsubscribeAllAction()
    {
        $params = $this->getRequest()->getParams();
        $page = null;
        if (array_key_exists('alert', $params)) {
            $page = new Model_Page_Alerts_UnsubscribeContact($params);
        }

        if ($page == null) {
            throw new Model_Exception_MissingRequireParam('alert');
        }
        $page->initView();
        $page->render();
    } // unsubscribeAllAction

    /**
     * Edit the alert
     *
     * URL: /alerts/edit
     * PARAMS:
     * - alert Alert secure ID
     */
    public function editAction()
    {
        $params = $this->getRequest()->getParams();
        $page = new Model_Page_Alerts_Edit($params);
        $page->initView();
        $page->render();
    } // editAction
}
