<?php
/**
 * HealthController
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Controller
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Controller for check server availability
 *
 * @author Eugene Kuznetsov
 *
 */
class HealthController extends Zend_Controller_Action
{
    public function indexAction()
    {
        echo 'It works!';
    }
}