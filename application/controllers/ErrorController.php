<?php
/**
 * ErrorController
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Controller
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

class ErrorController extends Zend_Controller_Action
{
    /**
    * Display error message for API
    *
    * @return void
    */
    public function apiAction()
    {
        // Set 400 response code on error
        // See http://jira.getitcorporate.com/browse/SMS-2350
        $this->getResponse()->setHttpResponseCode(400);

        $errors = $this->_getParam('error_handler');
        $exception = $errors->exception;

        if ($errors->exception instanceof Exception) {
            error_log($errors->exception);
        } elseif (is_array($errors)) {
            error_log(print_r($errors, true));
        }

        if ($errors->exception instanceof Zend_Controller_Action_Exception) {
            $exception = new Model_Exception_UnknowApiEndpoint($_SERVER['REQUEST_URI']);
        }
        $result = array(
            'status'=>'error',
            "message" => $exception->getMessage(),
            'success' => false,
        );

        $this->getHelper("Json")->suppressExit = true;
        $this->getHelper("Json")->sendJson($result);
    } // errorAction

    /**
     * Display error message
     *
     * @return void
     */
    public function errorAction()
    {
        $errors = $this->_getParam('error_handler');
        $params = array();
        if (APPLICATION_ENV == 'development') {
            $params['message'] = $errors->exception->getMessage();
            $params['trace'] = $errors->exception->getTraceAsString();
        } else {
            $this->getResponse()->setHttpResponseCode(404);
            $params['message'] = "Oops! The page you’re looking for has moved or is not available.";
            $params['trace'] = '';
        }
        $page = new Model_Page_Error($params);
        $page->initView();
        $page->render();
    } // errorAction
} // end class