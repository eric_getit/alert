<?php
/**
 * MessageArchiveController
 *
 * PHP Version 5.3
 *
 * @category Controller
 * @package  AdminControllers
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Zend\AdminApplication;

/**
 * Message archive grid page
 */
class MessageArchiveController extends Zend_Controller_Action
{
    /**
     * Message archive page
     *
     * URL: http://alert-admin.getitcorporate.com/message-archive
     * PARAMS:
     * contactId - shows only messages for the given contact
     * page - page number
     */
    public function indexAction()
    {
        $contactId = $this->getRequest()->getParam('contactId', null);
        if (empty($contactId)) {
            $title = 'Message Archive';
            $breadcrumbs = array(
                    'Message Archive' => '',
            );
        } else {
            $contact = new Model_Contact($contactId);
            if ($contact->getContactId() != null) {
                $title = 'Message Archive for ' . $contact->getEmail();
                $breadcrumbs = array(
                        'Message Archive' => '/message-archive',
                        $title => '',
                );
            } else {
                $this->redirect('/message-archive');
            }
        }


        $page = new Model_Admin_Page_MessageArchive();
        $page->setTitle($title);
        $page->setBreadcrumbs($breadcrumbs);

        $app = new AdminApplication($page);
        $app->bootstrap();
        $app->run();
    } // indexAction


    /**
     * Show message details
     *
     * URL: http://alert-admin.getitcorporate.com/message-archive/message-details
     * PARAMS:
     * messageId - ID in the message archive table
     * contactId - used in breadcrumbs
     */
    public function messageDetailsAction()
    {
        $messageId = $this->getRequest()->getParam('messageId', null);
        if (empty($messageId)) {
            throw new Exception('MessageArchiveController::messageDetailsAction() Message ID not specified');
        }
        $contactId = $this->getRequest()->getParam('contactId', null);
        if ($contactId != null) {
            $contact = new Model_Contact($contactId);
            $contactId = $contact->getContactId();
        }

        if ($contactId == null) {
            $breadcrumbs = array(
                    'Message Archive' => '/message-archive',
                    'Message Details' => '',
            );
        } else {
            $breadcrumbs = array(
                    'Message Archive' => '/message-archive',
                    'Message Archive for ' . $contact->getEmail() => '/message-archive?contactId=' . $contactId,
                    'Message Details' => '',
            );
        }

        $page = new Model_Admin_Page_MessageDetails();
        $page->setMessageId($messageId);
        $page->setBreadcrumbs($breadcrumbs);

        $app = new AdminApplication($page);
        $app->bootstrap();
        $app->run();
    } // messageDetailsAction

    /**
     * Message body for iframe on message detail page
     *
     * URL: http://alert-admin.getitcorporate.com/message-archive/message-body
     * PARAMS:
     * messageId - ID in the message archive table
     */
    public function messageBodyAction()
    {
        $messageId = $this->getRequest()->getParam('messageId', null);
        if (empty($messageId)) {
            throw new Exception('MessageArchiveController::messageBodyAction() Message ID not specified');
        }

        $message = new Model_MessageArchive($messageId);
        $this->getResponse()->setBody(preg_replace('~<img(.*)>~siU', '', $message->getBodyHtml()));
    } // messageBodyAction
}
