<?php
/**
 * LogoutController
 *
 * PHP Version 5.3
 *
 * @category Controller
 * @package  AdminControllers
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Logout administrator
 */
class LogoutController extends Zend_Controller_Action
{
    /**
     * Logout administrator
     *
     * URL: http://alert-admin.getitcorporate.com/logout
     */
    public function indexAction()
    {
        Model_AdminUser::logout();
        $this->redirect('/login');
    } // indexAction

}
