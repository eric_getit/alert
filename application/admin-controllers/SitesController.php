<?php
/**
 * SitesController
 *
 * PHP Version 5.3
 *
 * @category Controller
 * @package  AdminControllers
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Zend\AdminApplication;

/**
 * Sites grid page
 */
class SitesController extends Zend_Controller_Action
{
    /**
     * Sites page
     *
     * URL: http://alert-admin.getitcorporate.com/sites
     * PARAMS:
     * search - search keywords
     * page - page number
     */
    public function indexAction()
    {
        $page = new Model_Admin_Page_Site();
        $app = new AdminApplication($page);
        $app->bootstrap();
        $app->run();
    } // indexAction
}
