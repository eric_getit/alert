<?php
/**
 * ConfigurationController
 *
 * PHP Version 5.3
 *
 * @category Controller
 * @package  AdminControllers
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Zend\AdminApplication;

/**
 * Configuration pages
 */
class ConfigurationController extends Zend_Controller_Action
{
    /**
     * Configuration page - section General
     *
     * URL: http://alert-admin.getitcorporate.com/configuration/general
     */
    public function generalAction()
    {
        $this->_showConfiguration('General');
    } // generalAction

    /**
     * Configuration page - section Default UTM
     *
     * URL: http://alert-admin.getitcorporate.com/configuration/default-UTM
     */
    public function defaultUtmAction()
    {
        $this->_showConfiguration('Default UTM');
    } // defaultUtmAction

    /**
     * Configuration page - section Send Time
     *
     * URL: http://alert-admin.getitcorporate.com/configuration/send-time
     */
    public function sendTimeAction()
    {
        $this->_showConfiguration('Send Time');
    } // sendTimeAction

    /**
     * Show confifuration page for the given section
     *
     * @param string $name
     *
     * @return void
     */
    private function _showConfiguration($name)
    {
        $page = new Model_Admin_Page_Config();
        $page->setSectionName($name);

        $app = new AdminApplication($page);
        $app->bootstrap();
        $app->run();
    } // _showConfiguration
}
