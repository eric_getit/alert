<?php
/**
 * EditController
 *
 * PHP Version 5.3
 *
 * @category Controller
 * @package  AdminControllers
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Zend\AdminApplication;

/**
 * Edit object (contact, alert, etc)
 *
 * URL: /edit/<action>?<objectId>=<num>
 *
 * PARAMETERS:
 * objectId - Object ID (primary key in the table)
 */
class EditController extends Zend_Controller_Action
{
    /**
     * Edit alert object
     *
     * @return void
     */
    public function alertAction()
    {
        $id = $this->getRequest()->getParam('alertId',0);
        $object = new Model_Alert($id); // Need check on the item with this id exists?
        $page = new Model_Admin_Page_Edit_Alert($object);
        $page->setCancel('/alerts?contactId=' . $object->getContactId());

        $app = new AdminApplication($page);
        $app->bootstrap();
        $app->run();
    } // alertAction

    /**
     * Edit newsletter object
     *
     * @return void
     */
    public function newsletterAction()
    {
        $id = $this->getRequest()->getParam('newsletterId',0);
        $object = new Model_Newsletter($id); // Need check on the item with this id exists?
        $page = new Model_Admin_Page_Edit_Newsletter($object);

        $app = new AdminApplication($page);
        $app->bootstrap();
        $app->run();
    } // newsletterAction

    /**
     * Edit newsletter subscription object
     *
     * @return void
     */
    public function newsletterSubscriptionAction()
    {
        $id = $this->getRequest()->getParam('subscriptionId',0);
        $object = new Model_Newsletter_Subscription($id); // Need check on the item with this id exists?
        $page = new Model_Admin_Page_Edit_NewsletterSubscription($object);
        $page->setCancel('/newsletter-subscriptions?newsletterId=' . $object->getNewsletterId());

        $app = new AdminApplication($page);
        $app->bootstrap();
        $app->run();
    } // newsletterSubscriptionAction

    /**
     * Edit newsletter queue object
     *
     * @return void
     */
    public function newsletterQueueAction()
    {
        $id = $this->getRequest()->getParam('queueId',0);
        $object = new Model_Newsletter_Queue($id); // Need check on the item with this id exists?
        $page = new Model_Admin_Page_Edit_NewsletterQueue($object);

        $app = new AdminApplication($page);
        $app->bootstrap();
        $app->run();
    } // newsletterQueueAction

    /**
     * Edit site object
     *
     * @return void
     */
    public function siteAction()
    {
        $id = $this->getRequest()->getParam('siteId',0);
        $object = new Model_Site($id); // Need check on the item with this id exists?
        $page = new Model_Admin_Page_Edit_Site($object);

        $app = new AdminApplication($page);
        $app->bootstrap();
        $app->run();
    } // siteAction

    /**
     * Edit template object
     *
     * @return void
     */
    public function templateAction()
    {
        $id = $this->getRequest()->getParam('templateId',0);
        $object = new Model_Template($id); // Need check on the item with this id exists?
        $page = new Model_Admin_Page_Edit_Template($object);

        $app = new AdminApplication($page);
        $app->bootstrap();
        $app->run();
    } // templateAction
}
