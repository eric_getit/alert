<?php
/**
 * ContactDetailsController
 *
 * PHP Version 5.3
 *
 * @category Controller
 * @package  AdminControllers
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Zend\AdminApplication;

/**
 * Contact details page
 */
class ContactDetailsController extends Zend_Controller_Action
{
    /**
     * Contact details page
     *
     * URL: http://alert-admin.getitcorporate.com/contact-details
     * PARAMETER:
     * contactId: contact ID
     */
    public function indexAction()
    {
        $contactId = $this->_request->getParam('contactId', null);
        $contact = new Model_Contact($contactId);

        if ($contact->getContactId() == null) {
            $this->redirect("/contacts");
            return;
        }

        $page = new Model_Admin_Page_ContactDetails();
        $page->setContact($contact);
        $app = new AdminApplication($page);
        $app->bootstrap();
        $app->run();
    } // indexAction
}
