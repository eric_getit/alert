<?php
/**
 * SaveController
 *
 * PHP Version 5.3
 *
 * @category Controller
 * @package  AdminControllers
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Zend\AdminApplication,
    GetIt\Admin\Form\Form;

/**
 * Show admin application errors
 */
class SaveController extends Zend_Controller_Action
{
    /**
     * Save alert object
     *
     * @return void
     */
    public function alertAction()
    {
        $data = $this->getRequest()->getPost();
        $object = new Model_Alert($data['alertId']); // Need check on the item with this id exists?
        $object->fromArray($data);

        $cancel = '/alerts?contactId=' . $object->getContactId();

        $page = new Model_Admin_Page_Edit_Alert($object);
        $page->setCancel($cancel);

        $form = new Form();
        $form->setFields($page->getFields());
        $form->populateFields($object);
        $object->setUpdatedDate(date('Y-m-d H:m:s'));
        if ($form->isValid() && $object->update()) {
            Model_AlertOperationHistory::registryOperation(
                $object,
                Model_AlertOperationHistory::OPERATION_UPDATED_ADMIN,
                $_SERVER['REMOTE_ADDR']
            );
            $this->redirect($cancel);
        }

        $app = new AdminApplication($page);
        $app->bootstrap();
        $app->run();
    } // alertAction

    /**
     * Save newsletter object
     *
     * @return void
     */
    public function newsletterAction()
    {
        $data = $this->getRequest()->getPost();
        $object = new Model_Newsletter($data['newsletterId']); // Need check on the item with this id exists?
        $object->fromArray($data);

        $page = new Model_Admin_Page_Edit_Newsletter($object);

        $form = new Form();
        $form->setFields($page->getFields());
        $form->populateFields($object);
        $object->setUpdatedDate(date('Y-m-d H:m:s'));
        if ($form->isValid() && $object->update()) {
            $this->redirect($page->getCancel());
        }

        $app = new AdminApplication($page);
        $app->bootstrap();
        $app->run();
    } // newsletterAction

    /**
     * Save newsletter subscription object
     *
     * @return void
     */
    public function newsletterSubscriptionAction()
    {
        $data = $this->getRequest()->getPost();
        $object = new Model_Newsletter_Subscription($data['subscriptionId']); // Need check on the item with this id exists?
        $object->fromArray($data);

        $cancel = '/newsletter-subscriptions?newsletterId=' . $object->getNewsletterId();

        $page = new Model_Admin_Page_Edit_NewsletterSubscription($object);
        $page->setCancel($cancel);

        $form = new Form();
        $form->setFields($page->getFields());
        $form->populateFields($object);
        $object->setUpdatedDate(date('Y-m-d H:m:s'));
        if ($form->isValid() && $object->update()) {
            $this->redirect($cancel);
        }

        $app = new AdminApplication($page);
        $app->bootstrap();
        $app->run();
    } // newsletterSubscriptionAction

    /**
     * Save newsletter queue object
     *
     * @return void
     */
    public function newsletterQueueAction()
    {
        $data = $this->getRequest()->getPost();
        $object = new Model_Newsletter_Queue($data['queueId']); // Need check on the item with this id exists?
        $object->fromArray($data);

        $page = new Model_Admin_Page_Edit_NewsletterQueue($object);

        $form = new Form();
        $form->setFields($page->getFields());
        $form->populateFields($object);
        $object->setUpdatedDate(date('Y-m-d H:m:s'));
        if ($form->isValid() && $object->update()) {
            $this->redirect($page->getCancel());
        }

        $app = new AdminApplication($page);
        $app->bootstrap();
        $app->run();
    } // newsletterQueueAction

    /**
     * Save site object
     *
     * @return void
     */
    public function siteAction()
    {
        $data = $this->getRequest()->getPost();
        $object = new Model_Site($data['siteId']); // Need check on the item with this id exists?
        $object->fromArray($data);

        $page = new Model_Admin_Page_Edit_Site($object);

        $form = new Form();
        $form->setFields($page->getFields());
        $form->populateFields($object);
        $object->setUpdatedDate(date('Y-m-d H:m:s'));
        if ($form->isValid() && $object->update()) {
            $this->redirect($page->getCancel());
        }

        $app = new AdminApplication($page);
        $app->bootstrap();
        $app->run();
    } // siteAction

    /**
     * Save template object
     *
     * @return void
     */
    public function templateAction()
    {
        $data = $this->getRequest()->getPost();
        $object = new Model_Template($data['tmplId']); // Need check on the item with this id exists?
        foreach ($data as $name => $value) {
            $method = 'set' . $name;
            if (method_exists($object, $method)) {
                $object->$method($value);
            }
        }

        $page = new Model_Admin_Page_Edit_Template($object);

        $form = new Form();
        $form->setFields($page->getFields());
        $form->populateFields($object);
        $object->setTmplModified(date('Y-m-d H:m:s'));
        if ($form->isValid() && $object->update()) {
            $this->redirect($page->getCancel());
        }

        $app = new AdminApplication($page);
        $app->bootstrap();
        $app->run();
    } // templateAction
}
