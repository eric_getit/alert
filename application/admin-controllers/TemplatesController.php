<?php
/**
 * TemplatesController
 *
 * PHP Version 5.3
 *
 * @category Controller
 * @package  AdminControllers
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Zend\AdminApplication;

/**
 * Templates grid page
 */
class TemplatesController extends Zend_Controller_Action
{
    /**
     * Templates page
     *
     * URL: http://alert-admin.getitcorporate.com/templates
     * PARAMS:
     * page - page number
     */
    public function indexAction()
    {
        $page = new Model_Admin_Page_Template();
        $app = new AdminApplication($page);
        $app->bootstrap();
        $app->run();
    } // indexAction
}
