<?php
/**
 * NewsletterSubscriptionsController
 *
 * PHP Version 5.3
 *
 * @category Controller
 * @package  AdminControllers
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Zend\AdminApplication;

/**
 * Newsletters queue grid page
 */
class NewsletterSubscriptionsController extends Zend_Controller_Action
{
    /**
     * Newsletters queue page
     *
     * URL: http://alert-admin.getitcorporate.com/newsletter-subscriptions
     * PARAMS:
     * newsletterId - newsletter Id
     * contactId - contact Id
     */
    public function indexAction()
    {
        $newsletterId = $this->getRequest()->getParam('newsletterId', 0);
        $contactId = $this->getRequest()->getParam('contactId', 0);

        if ($contactId) {
            $contact = new Model_Contact($contactId);
            if ($contact->getContactId() == $contactId) {
                $title = 'Newsletter Subscriptions for ' . $contact->getEmail();
                $breadcrumbs = array(
                        'Contacts' => '/contacts',
                        $title => '',
                );
            }
        } else {
            $title = 'Newsletter Subscriptions';
            $breadcrumbs = array(
                    'Newsletters' => '/newsletters',
                    $title => '',
            );
        }

        $page = new Model_Admin_Page_NewsletterSubscriptions();
        $page->setBreadcrumbs($breadcrumbs);
        $app = new AdminApplication($page);
        $app->bootstrap();
        $app->run();
    } // indexAction



    /**
     * Newsletter subscription page
     */
    public function newsletterSubscriptionsAction()
    {
        $id = intval($this->_request->getParam('id', 0));
        $page = $this->_request->getParam('page', 1);

        $model = new Model_Grid_NewsletterSubscription();

        $filter = $this->_request->getParams();
        $adapter = new Model_Grid_Paginator($model, $filter);

        $paginator = new Zend_Paginator($adapter);
        $perPage = $this->_request->getParam('perPage', 10);
        $paginator->setItemCountPerPage($perPage);
        $paginator->setCurrentPageNumber($page);

        $viewParams = array();
        $viewParams['filter'] = $filter;
        $viewParams['columns'] = $model->getAvailableColumns();
        $viewParams['paginator'] = $paginator;
        $viewParams['model'] = $model;
        $viewParams['modelName'] = 'NewsletterSubscription';

        $contactId = $this->getRequest()->getParam('contactId', 0);
        if ($contactId) {
            $contact = new Model_Contact($contactId);
            if ($contact) {
                $title = 'Newsletter Subscriptions for ' . $contact->getEmail();
                $breadcrumbs = array(
                        'Contacts' => '/admin/contacts',
                        $title => '',
                );
            }
        } else {
            $title = 'Newsletter Subscriptions';
            $breadcrumbs = array(
                    $title => '',
            );
        }

        $page = new Model_Admin_Page_NewsletterSubscription($viewParams);
        $page->setTitle($title);
        $page->setBreadcrumbs($breadcrumbs);
        $page->initView();
        $page->renderZendResponse();
    } // newslettersubscriptionAction
}
