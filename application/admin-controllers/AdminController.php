<?php
/**
 * AdminController
 *
 * PHP Version 5.3
 *
 * @category  Class
 * @package   Controller
 * @author    Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link      http://www.getit.me/
 */

/**
 * Admin controller for alert mgmt
 *
 * @author
 * @version
 */

class AdminController extends Zend_Controller_Action
{
    public function configsaveAction()
    {
        $result = array('success' => false);

        $data = $this->_request->getParam('data', array());
        if (is_array($data)) {
            foreach ($data as $id => $value) {
                $config = new Model_ConfigDb($id);
                if ($config && $value != NULL) {
                    $config->value = $value;
                    $config->update();
                }
            }
            $result['success'] = true;
        }

        $this->getHelper("Json")->suppressExit = true;
        $this->getHelper("Json")->sendJson($result);
    } // saveAction
}
