<?php
/**
 * ReportsController
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Controller
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Zend\AdminApplication;

/**
 * Admin reports
 */
class ReportsController extends Zend_Controller_Action
{
    /**
     * Report: Overview
     * URL: /reports/overview
     */
    public function overviewAction()
    {
        $page = new Model_Admin_Page_Reports_Overview();
        $app = new AdminApplication($page);
        $app->bootstrap();
        $app->run();
    } // overviewAction

    /**
     * Report: Overview
     * URL: /reports/overview
     */
    public function overviewOldAction()
    {
        $page = new Model_Admin_Page_Reports_OverviewOld();
        $app = new AdminApplication($page);
        $app->bootstrap();
        $app->run();
    } // overviewAction
}
