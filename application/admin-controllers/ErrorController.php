<?php
/**
 * ErrorController
 *
 * PHP Version 5.3
 *
 * @category Controller
 * @package  AdminControllers
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Page\ErrorPage,
    GetIt\Admin\Zend\AdminApplication;

/**
 * Show admin application errors
 */
class ErrorController extends Zend_Controller_Action
{
    /**
     * Display error message
     *
     * @return void
     */
    public function errorAction()
    {
        $errors = $this->_getParam('error_handler');

        $page = new ErrorPage();
        $page->setErrorMessage($errors->exception->getMessage());
        $page->setErrorTrace($errors->exception->getTraceAsString());

        $app = new AdminApplication($page);
        $app->bootstrap();
        $app->run();
    } // errorAction
}
