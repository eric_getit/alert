<?php
/**
 * ContactDetailsController
 *
 * PHP Version 5.3
 *
 * @category Controller
 * @package  AdminControllers
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Zend\AdminApplication;

/**
 * Contact details page
 */
class AlertOperationsHistoryController extends Zend_Controller_Action
{
    /**
     * Alert operations history page
     *
     * URL: http://alert-admin.getitcorporate.com/alert-operations-history
     * PARAMETER:
     * alertId - alert ID
     */
    public function indexAction()
    {
        $alertId = $this->getRequest()->getParam('alertId');
        $alert = new Model_Alert($alertId);

        $page = new Model_Admin_Page_AlertOperationHistory();
        $page->setAlert($alert);
        $app = new AdminApplication($page);
        $app->bootstrap();
        $app->run();
    } // alertOperationsHistoryAction
}
