<?php
/**
 * LoginController
 *
 * PHP Version 5.3
 *
 * @category Controller
 * @package  AdminControllers
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Page\LoginPage,
    GetIt\Admin\Zend\AdminApplication;

/**
 * Login administrator
 */
class LoginController extends Zend_Controller_Action
{
    /**
     * Login administrator
     *
     * URL: http://alert-admin.getitcorporate.com/login
     */
    public function indexAction()
    {
        Model_LoggedUserCookies::authenticateAdmin();
        $viewParams[] = array();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $formData = $this->_request->getPost();
            $user = $request->getParam('user', null);
            $pass = $request->getParam('pass', null);
            $rememberMe = $request->getParam('rememberMe', false);

            try {
                Model_AdminUser::login($user, $pass);
                if ($rememberMe) {
                    Model_LoggedUserCookies::addAdminUser();
                }
                $errorMessage = null;
            } catch (Exception $e) {
                $errorMessage = 'Unable to log in. Please ensure that your user name and password are correct.';
            }
        }

        if (Model_AdminUser::isAuthenticated()) {
            if (substr($_SERVER['REQUEST_URI'], 0, 6) != '/login') {
                $this->redirect($_SERVER['REQUEST_URI']);
            } else {
                $this->redirect('/');
            }
        } else {
            $page = new LoginPage();
            $page->setErrorMessage(null);

            $app = new AdminApplication($page);
            $app->bootstrap();
            $app->run();
        }
    } // indexAction
}
