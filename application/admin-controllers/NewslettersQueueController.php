<?php
/**
 * NewslettersQueueController
 *
 * PHP Version 5.3
 *
 * @category Controller
 * @package  AdminControllers
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Zend\AdminApplication;

/**
 * Newsletters queue grid page
 */
class NewslettersQueueController extends Zend_Controller_Action
{
    /**
     * Newsletters queue page
     *
     * URL: http://alert-admin.getitcorporate.com/newsletters-queue
     * PARAMS:
     * page - page number
     */
    public function indexAction()
    {
        $page = new Model_Admin_Page_NewsletterQueue();
        $app = new AdminApplication($page);
        $app->bootstrap();
        $app->run();
    } // indexAction
}
