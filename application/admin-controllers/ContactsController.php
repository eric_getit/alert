<?php
/**
 * ContactsController
 *
 * PHP Version 5.3
 *
 * @category Controller
 * @package  AdminControllers
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Zend\AdminApplication,
    GetIt\Admin\Form\Form;

/**
 * Contacts grid and contact details page
 */
class ContactsController extends Zend_Controller_Action
{
    /**
     * Contact page
     *
     * URL: http://alert-admin.getitcorporate.com/contacts
     * PARAMS:
     * search - search keywords
     * page - page number
     */
    public function indexAction()
    {
        $page = new Model_Admin_Page_Contacts();
        $app = new AdminApplication($page);
        $app->bootstrap();
        $app->run();
    } // indexAction

    /**
     * Edit contact object
     *
     * URL: http://alert-admin.getitcorporate.com/contacts/edit
     * PARAMS:
     * contactId: Contact ID
     */
    public function editAction()
    {
        $contactId = $this->getRequest()->getParam('contactId', 0);
        $contact = new Model_Contact($contactId);
        if ($contact->getContactId() != $contactId) {
            $this->redirect('/contacts');
            return;
        }

        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost();
            $contact->fromArray($data);
        }

        $page = new Model_Admin_Page_Edit_Contact($contact);

        if ($this->getRequest()->isPost()) {
            $form = new Form();
            $form->setFields($page->getFields());
            $form->populateFields($contact);
            $contact->setUpdatedDate(date('Y-m-d H:m:s'));
            if ($form->isValid() && $contact->update()) {
                $this->redirect($page->getCancel());
                return;
            }
        }

        $app = new AdminApplication($page);
        $app->bootstrap();
        $app->run();
    } // editAction

    /**
     * Unsubscribe contact
     *
     * URL: http://alert-admin.getitcorporate.com/contacts/unsubscribe
     * PARAMS:
     * contactId: ID of the contact which should be unsubscribed
     */
    public function unsubscribeAction()
    {
        $contactId = intval($this->_request->getParam('contactId', null));
        $result = array();
        $result['success'] = false;
        $result['message'] = 'Server Error';

        $contact = new Model_Contact($contactId);
        if ($contact->getContactId() != null) {
            $contact->setContactStatus(Model_Contact::STATUS_UNSUBSCRIBED_ADMIN);
            $contact->update();

            $result['success'] = true;
            $result['message'] = 'Contact unsubscribed.';
        }

        $this->getHelper("Json")->suppressExit = true;
        $this->getHelper("Json")->sendJson($result);
    } // unsubscribeAction

    /**
     * Change contact status to active
     *
     * URL: http://alert-admin.getitcorporate.com/contacts/activate
     * PARAMS:
     * contactId: ID of the contact which should be activated
     */
    public function activateAction()
    {
        $contactId = intval($this->_request->getParam('contactId', null));
        $result = array();
        $result['success'] = false;
        $result['message'] = 'Server Error';

        $contact = new Model_Contact($contactId);
        if ($contact->getContactId() != null) {
            $contact->setContactStatus(Model_Contact::STATUS_ACTIVE);
            $contact->update();

            $result['success'] = true;
            $result['message'] = 'Contact activated.';
        }

        $this->getHelper("Json")->suppressExit = true;
        $this->getHelper("Json")->sendJson($result);
    } // activateAction

    /**
     * Delete contact
     *
     * URL: http://alert-admin.getitcorporate.com/contacts/delete
     * PARAMS:
     * contactId: ID of the contact which should be activated
     */
    public function deleteAction()
    {
        $contactId = intval($this->_request->getParam('contactId', null));
        $result = array();
        $result['success'] = false;
        $result['message'] = 'Server Error';

        $contact = new Model_Contact($contactId);
        if ($contact->getContactId() != null) {
            $contact->delete();
            $result['success'] = true;
            $result['message'] = 'Contact deleted.';
        }

        $this->getHelper("Json")->suppressExit = true;
        $this->getHelper("Json")->sendJson($result);
    } // deleteAction
}
