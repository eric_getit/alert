<?php
/**
 * AlertsController
 *
 * PHP Version 5.3
 *
 * @category Controller
 * @package  AdminControllers
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Zend\AdminApplication;

/**
 * Contacts grid page
 */
class AlertsController extends Zend_Controller_Action
{
    /**
     * Alerts page
     *
     * URL: http://alert-admin.getitcorporate.com/alerts
     * PARAMS:
     * contactId - show alerts for the contact with this ID
     * page - page number
     */
    public function indexAction()
    {
        $contactId = $this->getRequest()->getParam('contactId');
        $contact = new Model_Contact($contactId);

        $page = new Model_Admin_Page_Alerts();
        $page->setContact($contact);
        $app = new AdminApplication($page);
        $app->bootstrap();
        $app->run();
    } // indexAction

    /**
     * Unsubscribe alert
     *
     * URL: http://alert-admin.getitcorporate.com/alerts/unsubscribe
     * PARAMS:
     * alertId: ID of the alert which should be unsubscribed
     */
    public function unsubscribeAction()
    {
        $alertId = intval($this->_request->getParam('alertId', null));
        $result = array();
        $result['success'] = false;
        $result['message'] = 'Server Error';

        $alert = new Model_Alert($alertId);
        if ($alert->getAlertId() != null) {
            $alert->setAlertStatus(Model_Alert::STATUS_UNSUBSCRIBED_ADMIN);
            $alert->update();

            $result['success'] = true;
            $result['message'] = 'Alert unsubscribed.';
        }

        $this->getHelper("Json")->suppressExit = true;
        $this->getHelper("Json")->sendJson($result);
    } // unsubscribeAction

    /**
     * Change alert status to active
     *
     * URL: http://alert-admin.getitcorporate.com/alerts/activate
     * PARAMS:
     * alertId: ID of the alert which should be activated
     */
    public function activateAction()
    {
        $alertId = intval($this->_request->getParam('alertId', null));
        $result = array();
        $result['success'] = false;
        $result['message'] = 'Server Error';

        $alert = new Model_Alert($alertId);
        if ($alert->getContactId() != null) {
            $alert->setAlertStatus(Model_Alert::STATUS_ACTIVE);
            $alert->update();

            $result['success'] = true;
            $result['message'] = 'Alert activated.';
        }

        $this->getHelper("Json")->suppressExit = true;
        $this->getHelper("Json")->sendJson($result);
    } // activateAction

    /**
     * Delete alert
     *
     * URL: http://alert-admin.getitcorporate.com/alerts/delete
     * PARAMS:
     * alertId: ID of the contact which should be activated
     */
    public function deleteAction()
    {
        $alertId = intval($this->_request->getParam('alertId', null));
        $result = array();
        $result['success'] = false;
        $result['message'] = 'Server Error';

        $alert = new Model_Alert($alertId);
        if ($alert->getContactId() != null) {
            $alert->delete();
            $result['success'] = true;
            $result['message'] = 'Alert deleted.';
        }

        $this->getHelper("Json")->suppressExit = true;
        $this->getHelper("Json")->sendJson($result);
    } // deleteAction
}
