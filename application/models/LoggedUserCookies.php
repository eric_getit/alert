<?php
/**
 * Model_LoggedUserCookies
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Class for auto user sign in
 *
 */
class Model_LoggedUserCookies extends Model_AbstractModel
{
    /**
     * ID (primary key)
     *
     * @var integer
     */
    protected $_id;

    /**
     * Date and time when cookie was created
     *
     * @var string
     */
    protected $_date;

    /**
     * User email
     *
     * @var string
     */
    protected $_email;

    /**
     * Site reference
     *
     * @var integer
     */
    protected $_siteId;

    /**
     * User reference
     *
     * @var integer
     */
    protected $_userId;

    /**
     * Secret Word
     *
     * @var string
     */
    protected $_secretWord;

    /**
     * Data base fields
     *
     * @var array
     */
    protected $_tableFields = array(
        'id' => '_id',
        'date' => '_date',
        'email' => '_email',
        'site_id' => '_siteId',
        'user_id' => '_userId',
        'secret_word' => '_secretWord'
    );

    /**
     * Data base table name for the model
     *
     * @var string
     */
    protected $_tableName = 'logged_user_cookies';

    /**
     * Authenticate admin if cookie isset
     *
     * @return void
     */
    public static function authenticateAdmin()
    {
        self::_clearExpired();

        if (!Model_AdminUser::isAuthenticated()) {

            $adminRow = self::_findAdmin();
            if (!empty($adminRow)) {
                Model_AdminUser::login($adminRow['email'], $adminRow['passwd'], false);
            }
        }
    } // authenticateAdmin

    /**
     * Remove admin from list of authenticated users
     *
     * @param string $email
     *
     * @return void
     */
    public static function removeAdmin($email)
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $dbh->delete('logged_user_cookies', 'email = "' . $email . '" AND site_id = 0');
        // Note: in order to delete cookie we just set expire time in the past
        setcookie('AL', 'AL', 1, '/');
    } // removeAdmin

    /**
     * Add user identity in database
     *
     * @return void
     */
    public static function addAdminUser()
    {
        if (self::_findAdmin()) {
            return;
        }
        $identity = Model_AdminUser::getUserInfo();
        if ($identity) {
            $model = new Model_LoggedUserCookies();
            $token = Model_Utils::generateUniqueToken();
            $model->_date = date('Y-m-d H:i:s');
            $model->_siteId = 0;
            $model->_email = $identity->email;
            $model->_secretWord = $token;
            $model->insert();
            setcookie('AL', $token, time() + (86400 * 14), '/');
        }
    } // addAdminUser

    /**
     * Find row from the table for the admin user authenticated with cookie
     *
     * @return array | null
     */
    private static function _findAdmin()
    {
        $adminRow = null;

        $name = 'AL';
        if (!empty($_COOKIE[$name])) {
            $model = new Model_LoggedUserCookies();
            $userRows = $model->selectAll(array(
                    'siteId' => 0,
                    'secret_word' => $_COOKIE[$name],
            ));
            if (count($userRows) > 0) {
                $admin = new Model_AdminUser();
                $adminRows = $admin->selectAll(array('email' => $userRows[0]['email']));

                if (count($adminRows) > 0) {
                    $adminRow = $adminRows[0];
                }
            }
        }
        return $adminRow;
    } // _findAdmin

    /**
     * Delete expired user identities
     *
     * @return void
     */
    private static function _clearExpired()
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $where = 'date < "' . Model_Utils::sqlNow() . '" - interval 2 week';
        $dbh->delete('logged_user_cookies', $where);
    } // _clearExpired
}
