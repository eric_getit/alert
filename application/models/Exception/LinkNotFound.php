<?php
/**
 * Model_Exception_LinkNotFound
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Exception
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Exception to the situation when we do not find links to external source
 *
 * @author Eugene Kuznetsov
 *
 */
class Model_Exception_LinkNotFound extends Exception
{
    protected $message = 'Link not found.';
}
