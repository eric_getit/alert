<?php
/**
 * Model_Exception_DuplicateUniqueId
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Exception
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Exception for operation expected saved recod for object
 *
 * @author Eugene Churmanov
 *
 */
class Model_Exception_DuplicateUniqueId extends Exception
{
    protected $message = 'Duplicate Unique ID for ';

    public function __construct($objectType, $recordId)
    {
        $this->message .= $objectType . " by record " . $recordId;
    }
}
