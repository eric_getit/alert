<?php
/**
 * Model_Exception_UnknowApiEndpoint
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Exception
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 *
 * Exception occurred when provided an invalid api url
 *
 */
class Model_Exception_UnknowApiEndpoint extends Exception
{
    public function __construct($uri)
    {
        $this->message = 'Unknow API endpoint: ' . $uri;
    }
}
