<?php
/**
 * Model_Exception_NoRecordForObject
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Exception
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Exception for operation expected saved recod for object
 *
 * @author Eugene Churmanov
 *
 */
class Model_Exception_NoRecordForObject extends Exception
{
    protected $message = 'Record for object not found';
}
