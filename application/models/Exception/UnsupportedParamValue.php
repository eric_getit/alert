<?php
/**
 * Model_Exception_UnsupportedParamValue
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Exception
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 *
 * Exception occurred when subscribe to new alert with bad data.
 *
 */
class Model_Exception_UnsupportedParamValue extends Exception
{
    protected $message = 'Unsupported value for ';

    public function __construct($paramName, $value = array())
    {
        if (empty($value)) {
            $this->message .= "\"$paramName\"";
        } else {
            $this->message .= "\"$paramName\".";
            $this->message .= "Suported values: " . implode(', ', $value);
        }
    }
}
