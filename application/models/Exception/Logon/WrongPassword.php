<?php
/**
 * Model_Exception_Logon_WrongPassword
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Exception\Logon
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 *
 * Exception related to the situation of incorrect login to the Alert Admin
 *
 */
class Model_Exception_Logon_WrongPassword extends Exception
{
}
