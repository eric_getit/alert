<?php
/**
 * Model_Exception_MissingRequireParam
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Exception
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 *
 * Exception occurred when subscribe to new alert with bad data.
 *
 */
class Model_Exception_MissingRequireParam extends Exception
{
    protected $message = 'Missing Required Parameter ';

    public function __construct($paramName)
    {
        $this->message .= "\"$paramName\"";
    }
}
