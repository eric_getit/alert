<?php
/**
 * Model_Workers_UpdateAlert
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Workers
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */
class Model_Workers_UpdateAlert extends Model_Workers_WorkerAbstract
{
    /**
     * Task name for Gearman server
     *
     * @var string
     */
    protected $_processName = Model_Workers_GearmanCore::TASK_UPDATE_ALERT;

    /**
     * Persist changes to the DB
     *
     * @param Model_Workers_Message $message
     *
     * @return void
     */
    public function handling($message)
    {
        $messageId = $message->getMessageId();

        $alert = $message->getAlert();
        switch ($message->getStatus()) {
            case Model_Workers_Message::STATUS_API_NO_RESULTS:
                $alert->updateLastProcessDate();
                $alert->updateNextProcessDate();
                $alert->update();
                break;

            case Model_Workers_Message::STATUS_SEND_SUCCESSFUL:
            case Model_Workers_Message::STATUS_SEND_FAIL:
                $messageData = $message->getMessageData();
                $alert->setLastProcessDate($messageData['sendDate']);
                $alert->setLastSendDate($messageData['sendDate']);
                $alert->updateNextProcessDate();
                $alert->update();

                $codes = $message->getGatewayCodes();
                foreach ($codes as $code) {
                    $emailLink = new Model_EmailLink();
                    if ($emailLink->findLinkInRedis($code)) {
                        $emailLink->insertIntoSql();
                        $emailLink->deleteFromRedis();
                    }
                }

                Model_MessageArchive::save($messageData);

                break;

            default:
                $this->errorMessage('Application error: Unexpected status ' . $message->getStatus() . ' for message ' . $messageId);
                return;
        }

        $message->delete();
        $redis = Model_ConnectionManager::getRedisClient();
        if ($redis->exists($alert->getAlertId())) {
            $redis->del($alert->getAlertId());
        }
    } // handling
}
