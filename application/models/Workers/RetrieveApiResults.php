<?php
/**
 * Model_Workers_RetrieveApiResults
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Workers
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Retrieve API results from for the alert
 */
class Model_Workers_RetrieveApiResults extends Model_Workers_WorkerAbstract
{
    /**
     * Task name for Gearman server
     *
     * @var string
     */
    protected $_processName = Model_Workers_GearmanCore::TASK_RETRIEVE_API;

    /**
     * Function for handling messages
     *
     * @param Model_Workers_Message $message
     *
     * @return void
     */
    public function handling($message)
    {
        $messageId = $message->getMessageId();

        $whitelabelingEnabled = Model_ConfigDb::getBoolean('whitelabeling');

        $alert = $message->getAlert();
        $site = $alert->getSite();
        $siteDomain = $site->getDomain();

        $response = $alert->getListings();
        $gatewayUrls = array();
        $gatewayCodes = array();

        $apiResult = array();
        $apiResult['listings'] = $response->getListings();
        $apiResult['count'] = count($apiResult['listings']);
        $apiResult['organicCount'] = 0;
        $apiResult['backfillCount'] = 0;


        /* @var $listing Model_Listing */
        foreach ($apiResult['listings'] as $listing) {

            $externalUrl = $listing->getExternalUrl();

            if ($listing->getType() == Model_Listing::TYPE_BACKFILL &&  $whitelabelingEnabled) {
                $query = parse_url($externalUrl, PHP_URL_QUERY);
                if (!empty($query)) {
                    $externalUrl .= '&r=' . $siteDomain;
                } else {
                    $externalUrl .= '?r=' . $siteDomain;
                }
            }

            $listingId =  $listing->getId();
            $listingType = $listing->getType();

            $gatewayUrl = new Model_EmailLink();
            $gatewayUrl->setUrl($externalUrl);
            $gatewayUrl->setType($listingType);
            $gatewayUrl->setCode($listingId);
            $gatewayUrl->setCreated(Model_Utils::sqlNow());

            $gatewayUrls[] = $gatewayUrl;
            $gatewayCodes[] = $gatewayUrl->getCode();

            $listing->setExternalUrl($gatewayUrl->getExternalLink($alert));

            if ($listingType == Model_Listing::TYPE_ORGANIC) {
                $apiResult['organicCount']++;
            } else {
                $apiResult['backfillCount']++;
            }
        }

        if (count($gatewayUrls) > 0) {
            Model_EmailLink::multipleInsertIntoRedis($gatewayUrls);
            $message->setGatewayCodes($gatewayCodes);
        }

        $message->setApiResult($apiResult);

        $resultsCount = $apiResult['count'];
        if ($resultsCount > 0) {
            $message->setStatus(Model_Workers_Message::STATUS_API_HAS_RESULTS);
        } else {
            $message->setStatus(Model_Workers_Message::STATUS_API_NO_RESULTS);
        }

        $message->save();

        $gearmanClient = Model_ConnectionManager::getGearmanClient();
        if ($resultsCount > 0) {
            $gearmanClient->doBackground(Model_Workers_GearmanCore::TASK_MESSAGES_PREPARE, $messageId);
        } else {
            $gearmanClient->doBackground(Model_Workers_GearmanCore::TASK_UPDATE_ALERT, $messageId);
        }
    } // handling
}
