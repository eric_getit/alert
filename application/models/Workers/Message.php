<?php
/**
 * Model_Workers_Message
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Workers
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */
class Model_Workers_Message
{
    /**
     * Newly created message
     *
     * @var string
     */
    const STATUS_CREATED = 'STATUS_CREATED';

    /**
     * No results retrieved from API
     *
     * @var string
     */
    const STATUS_API_NO_RESULTS = 'STATUS_API_NO_RESULTS';

    /**
     * API returned some results
     *
     * @var string
     */
    const STATUS_API_HAS_RESULTS = 'STATUS_API_HAS_RESULTS';

    /**
     * Message ready for sending
     *
     * @var string
     */
    const STATUS_MESSAGE_READY = 'STATUS_MESSAGE_READY';

    /**
     * Failed to send message
     *
     * @var string
     */
    const STATUS_SEND_FAIL = 'STATUS_SEND_FAIL';

    /**
     * Sent message successfully
     *
     * @var string
     */
    const STATUS_SEND_SUCCESSFUL = 'STATUS_SEND_SUCCESSFUL';

    /**
     * Message ID used to identify message in Redis
     *
     * @var string
     */
    private $_messageId;

    /**
     * Message status
     *
     * @var string
     */
    private $_status;

    /**
     * Processing alert object
     *
     * @var Model_Alert
     */
    private $_alert;

    /**
     * Processing alert's contact object
     *
     * @var Model_Contact
     */
    private $_contact;

    /**
     * Data from API for alert
     *
     * @var array
     */
    private $_apiResult;

    /**
     * Array with alert prepared message data
     *
     * @var array
     */
    private $_messageData;

    /**
     * SES message unique ID
     *
     * @var string
     */
    private $_sesMessageId;

    /**
     * TRUE if send was successful
     *
     * @var bool
     */
    private $_sendStatus;

    /**
     * Send date getted from SES result object
     *
     * @var string
     */
    private $_sesSendDate;

    /**
     * SES result message if send was failed
     *
     * @var string
     */
    private $_sesMessage;

    /**
     * List of gateway urls for message
     *
     * @var array
     */
    private $_gatewayCodes = array();

    /**
     * Fields in Redis
     * Pair classField => dbField
     *
     * @var array
     */
    protected $_tableFields = array(
                '_messageId' => 'messageId',
                '_status' => 'status',
                '_alert' => 'alert',
                '_contact' => 'contact',
                '_apiResult' => 'apiResult',
                '_messageData' => 'messageData',
                '_sesMessageId' => 'sesMessageId',
                '_sendStatus' => 'sendStatus',
                '_sesSendDate' => 'sesSendDate',
                '_sesMessage' => 'sesMessage',
                '_gatewayCodes' => 'gatewayCodes',
    );

    protected $_objFields = array(
                'alert', 'contact', 'apiResult', 'messageData', 'gatewayCodes');

    /**
     * List of changed fields
     *
     * @var array
     */
    protected $_changedFields = array();

    /**
     * Get message ID
     *
     * @return string
     */
    public function getMessageId()
    {
        return $this->_messageId;
    } // getMessageId

    /**
     * Set message ID
     *
     * @param string $messageId
     *
     * @return Model_Workers_Message
     */
    public function setMessageId($messageId)
    {
        $this->_messageId = $messageId;
        $this->_changedFields[] = '_messageId';
        return $this;
    } // setMessageId

    /**
     * Get message status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->_status;
    } // getStatus

    /**
     * Set message status
     *
     * @param string $status
     *
     * @return Model_Workers_Message
     */
    public function setStatus($status)
    {
        $this->_status = $status;
        $this->_changedFields[] = '_status';
        return $this;
    } // setStatus

    /**
     * Get alert
     *
     * @return Model_Alert
     */
    public function getAlert()
    {
        return $this->_alert;
    } // getAlert

    /**
     * Set alert
     *
     * @param Model_Alert $alert
     *
     * @return Model_Workers_Message
     */
    public function setAlert($alert)
    {
        $this->_alert = $alert;
        $this->_changedFields[] = '_alert';
        return $this;
    } // setAlert

    /**
     * Get contact
     *
     * @return Model_Contact
     */
    public function getContact()
    {
        return $this->_contact;
    } // getContact

    /**
     * Set contact
     *
     * @param Model_Contact $contact
     *
     * @return Model_Workers_Message
     */
    public function setContact($contact)
    {
        $this->_contact = $contact;
        $this->_changedFields[] = '_contact';
        return $this;
    } // setContact

    /**
     * Get API result
     *
     * @return array
     */
    public function getApiResult()
    {
        return $this->_apiResult;
    } // getApiResult

    /**
     * Set API result
     *
     * @param array $apiResult
     *
     * @return Model_Workers_Message
     */
    public function setApiResult($apiResult)
    {
        $this->_apiResult = $apiResult;
        $this->_changedFields[] = '_apiResult';
        return $this;
    } // setApiResult

    /**
     * Get message data
     *
     * @return array
     */
    public function getMessageData()
    {
        return $this->_messageData;
    } // getMessageData

    /**
     * Set message data
     *
     * @param array $messageData
     *
     * @return Model_Workers_Message
     */
    public function setMessageData($messageData)
    {
        $this->_messageData = $messageData;
        $this->_changedFields[] = '_messageData';
        return $this;
    } // setMessageData

    /**
     * Get gateway codes
     *
     * @return array
     */
    public function getGatewayCodes()
    {
        return $this->_gatewayCodes;
    } // getGatewayCodes

    /**
     * Set gateway codes
     *
     * @param array $gatewayCodes
     *
     * @return Model_Workers_Message
     */
    public function setGatewayCodes($gatewayCodes)
    {
        $this->_gatewayCodes = $gatewayCodes;
        $this->_changedFields[] = '_gatewayCodes';
        return $this;
    } // setGatewayCodes

    /**
     * Save message to Redis server
     *
     * @return void
     */
    public function save()
    {
        $redis = Model_ConnectionManager::getRedisClient();
        $data = array();
        foreach ($this->_changedFields as $classField) {
            $dbField = $this->_tableFields[$classField];
            if (in_array($dbField, $this->_objFields)) {
                $data[$dbField] = serialize($this->$classField);
            } else {
                $data[$dbField] = $this->$classField;
            }

        }
        $redis->hmset($this->_messageId, $data);
    } // saveMessage

    /**
     * Load message from Redis server
     *
     * @param integer $messageId
     *
     * @return Model_Workers_Message
     */
    public static function load($messageId)
    {
        $redis = Model_ConnectionManager::getRedisClient();
        if ($redis->exists($messageId)) {
            $data = $redis->hgetall($messageId);
            $message = new self();
            $message->fromArray($data);
        } else {
            $message = null;
        }
        return $message;
    } // loadMessage

    /**
     * Delete message from Redis server
     *
     * @param string $messageId
     *
     * @return void
     */
    public function delete()
    {
        $redis = Model_ConnectionManager::getRedisClient();
        if ($redis->exists($this->_messageId)) {
            $redis->del($this->_messageId);
        }
    } // delete

    /**
     * Populate message from associative array
     *
     * @param array $data
     *
     * @return void
     */
    public function fromArray($data)
    {
        foreach ($this->_tableFields as $classField => $dbField) {
            if (isset($data[$dbField])) {
                if (in_array($dbField, $this->_objFields)) {
                    $this->$classField = unserialize($data[$dbField]);
                } else {
                    $this->$classField = $data[$dbField];
                }
            }
        }
    } // fromArray
}
