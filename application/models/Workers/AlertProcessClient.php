<?php
/**
 * Model_Workers_AlertProcessClient
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Workers
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Read list of alerts and schedule tasks to send alerts.
 *
 * Usage:

    $client = new Model_Workers_AlertProcessClient();

    $client->setTimezonesEnabled(true);
    $client->setTimezonesList(array('EST+0', 'EST+1'));
    $client->setSendHour(4);

 *
 */
class Model_Workers_AlertProcessClient extends Model_Workers_GearmanCore
{
    /**
     * Number of alerts to read from database in batch mode
     *
     * @var integer
     */
    const READING_BATCH_SIZE = 50000;

    /**
     * Day hour when we send alerts
     *
     * @var integer
     */
    protected $_sendHour;

    /**
     * Do we use time zone information to send alerts
     *
     * @var boolean
     */
    protected $_timezonesEnabled;

    /**
     * Dry run mode
     *
     * @var boolean
     */
    protected $_dryRun;

    /**
     * List of time zones if we process alerts in time zones enabled mode.
     * For example array('EST+17', 'EST-7')
     *
     * @var array
     */
    protected $_timezonesList;

    /**
     * Set time zone support enabled
     *
     * @param boolean $timezonesEnabled
     *
     * @return Model_Workers_AlertProcessClient
     */
    public function setTimezonesEnabled($timezonesEnabled)
    {
        $this->_timezonesEnabled = $timezonesEnabled;
        return $this;
    } // setTimezonesEnabled

    /**
     * Set dry run mode
     *
     * @param boolean $dryRun
     *
     * @return Model_Workers_AlertProcessClient
     */
    public function setDryRun($dryRun)
    {
        $this->_dryRun = $dryRun;
        return $this;
    } // setDryRun

    /**
     * Set time zones list
     *
     * @param array $timezonesList
     *
     * @return Model_Workers_AlertProcessClient
     */
    public function setTimezonesList($timezonesList)
    {
        $this->_timezonesList = $timezonesList;
        return $this;
    } // setTimezonesList

    /**
     * Set hour for which we are sending alerts
     *
     * @param integer $sendHour
     *
     * @return Model_Workers_AlertProcessClient
     */
    public function setSendHour($sendHour)
    {
        $this->_sendHour = $sendHour;
        return $this;
    } // setSendHour

    /**
     * Schedule tasks to retrieve alert results from API
     *
     * @return void
     */
    public function processAlerts()
    {
        if ($this->_timezonesEnabled) {
            if (count($this->_timezonesList) == 0) {
                $this->warningMessage('List of time zones is empty. Nothing to do now, exiting.');
                return;
            }
        } else {
            $curHour = intval(date('H'));
            if ($curHour != $this->_sendHour) {
                $this->warningMessage('Current time is ' . date('H:i') .
                     '. We should send alerts starting at ' . $this->_sendHour .
                     ':00. Nothing to do now, exiting.');
                return;
            }
        }

        $dbh = Zend_Db_Table::getDefaultAdapter();
        $dbh->beginTransaction();

        $this->profileMessage('Reading alert statistics ... ');
        $alertsCount = $this->_readStatistics();
        $this->profileMessage('Active alerts: ' . number_format($alertsCount));

        if (!$this->_dryRun && $alertsCount > 0) {

            $this->profileMessage('Begin: Schedule alerts.');

            $start = 0;
            while ($start < $alertsCount) {

                $this->_loadAlerts($start, self::READING_BATCH_SIZE);

                $start += self::READING_BATCH_SIZE;
            }

            $this->profileMessage('End: Schedule alerts.');
        }

        $dbh->rollBack();
    } // processAlerts

    /**
     * Get number of active alerts
     *
     * @return number
     */
    private function _readStatistics()
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $sql = <<<SQL
SELECT count(*) as cnt
FROM alerts a
INNER JOIN contacts c USING (contactId)
WHERE a.nextProcessDate <= ?
AND a.alertStatus = "ACTIVE"
AND a.isLocationValid
AND c.contactStatus = "ACTIVE"
AND (
        (a.queryTerms IS NOT NULL AND a.queryTerms != "")
    OR  (a.location IS NOT NULL AND a.location != "")
)
SQL;
        $sql .= $this->_getTimezoneSql();

        $bind = array();
        $bind[] = Model_Utils::sqlNow();

        $count = intval($dbh->fetchOne($sql, $bind));
        return $count;
    } // _readStatistics

    /**
     * Process alerts for the given range and schedule tasks to the Gearman server.
     *
     * @param integer $startId Alert range start
     * @param integer $endId Alert range end
     *
     * @return void
     */
    protected function _loadAlerts($start, $limit)
    {
        $this->profileMessage('Reading alerts in the range [' . number_format($start)  . ' - ' . number_format($start + $limit - 1) . ']');

        $dbh = Zend_Db_Table::getDefaultAdapter();
        $timezoneSql = $this->_getTimezoneSql();
        $sql = <<<SQL
SELECT a.alertId as a_alertId,
    a.alertSecureId as a_alertSecureId,
    a.alertType as a_alertType,
    a.alertStatus as a_alertStatus,
    a.contactId as a_contactId,
    a.siteId as a_siteId,
    a.location as a_location,
    a.queryTerms as a_queryTerms,
    a.distance as a_distance,
    a.ip as a_ip,
    a.lastSendDate as a_lastSendDate,
    a.lastProcessDate as a_lastProcessDate,
    a.nextProcessDate as a_nextProcessDate,
    a.createdDate as a_createdDate,
    a.updatedDate as a_updatedDate,
    a.utmSource as a_utmSource,
    a.utmMedium as a_utmMedium,
    a.utmTerm as a_utmTerm,
    a.utmContent as a_utmContent,
    a.utmCampaign as a_utmCampaign,
    a.bouncesCount as a_bouncesCount,
    a.complaintsCount as a_complaintsCount,
    a.lastBounceDate as a_lastBounceDate,
    a.lastComplaintDate as a_lastComplaintDate,
    a.sesMessageId as a_sesMessageId,
    a.isLocationValid as a_isLocationValid,
    c.contactId as c_contactId,
    c.contactSecureId as c_contactSecureId,
    c.contactSourceId as c_contactSourceId,
    c.contactStatus as c_contactStatus,
    c.name as c_name,
    c.country as c_country,
    c.state as c_state,
    c.city as c_city,
    c.zip as c_zip,
    c.address as c_address,
    c.email as c_email,
    c.createdDate as c_createdDate,
    c.updatedDate as c_updatedDate,
    c.originalCreatedDate as c_originalCreatedDate,
    c.sendTimezone as c_sendTimezone,
    c.userTimezone as c_userTimezone
FROM alerts a
INNER JOIN contacts c USING (contactId)
WHERE a.nextProcessDate <= ?
AND a.alertStatus = "ACTIVE"
AND a.isLocationValid
AND c.contactStatus = "ACTIVE"
AND (
        (a.queryTerms IS NOT NULL AND a.queryTerms != "")
    OR  (a.location IS NOT NULL AND a.location != "")
)
$timezoneSql
LIMIT $start, $limit
SQL;

        $bind = array();
        $bind[] = Model_Utils::sqlNow();

        $stmt = $dbh->query($sql, $bind);

        $redis = Model_ConnectionManager::getRedisClient();
        $gearmanClient = Model_ConnectionManager::getGearmanClient();

        $this->profileMessage('Adding tasks to Gearman');

        $row = $stmt->fetch(Zend_Db::FETCH_ASSOC);

        while ($row) {

            $alertId = $row['a_alertId'];
            // TODO: we might want to prefix key but I have not put the changes
            // because this should be fixed in other places too.
            // $redisKey = self::REDIS_KEY_PREFIX . $alertId;
            $redisKey = $alertId;
            if ($redis->exists($redisKey)) {
                $this->errorMessage('Skipping already scheduled alert ' . $alertId);
            } else {

                $contact = $this->_createContact($row);
                $alert = $this->_createAlert($row);

                $messageId = Model_Utils::guid();
                $message = new Model_Workers_Message();
                $message->setMessageId($messageId);
                $message->setContact($contact);
                $message->setAlert($alert);
                $message->setStatus(Model_Workers_Message::STATUS_CREATED);
                $message->save();

                $gearmanClient->doBackground(
                        Model_Workers_GearmanCore::TASK_RETRIEVE_API,
                        $message->getMessageId()
                );

                if ($gearmanClient->returnCode() == GEARMAN_SUCCESS) {
                    $redis->set($redisKey, $messageId);
                } else {
                    $message->delete();
                    $this->errorMessage('Failed to add task to Gearman server');
                    break;
                }
            }

            $row = $stmt->fetch(Zend_Db::FETCH_ASSOC);
        }
    } // _loadAlerts

    /**
     * Create alert object from row data
     *
     * @param array $row
     *
     * @return Model_Alert
     */
    private function _createAlert($row)
    {
        $data = array();
        $data['alertId'] = $row['a_alertId'];
        $data['alertSecureId'] = $row['a_alertSecureId'];
        $data['alertType'] = $row['a_alertType'];
        $data['alertStatus'] = $row['a_alertStatus'];
        $data['contactId'] = $row['a_contactId'];
        $data['siteId'] = $row['a_siteId'];
        $data['location'] = $row['a_location'];
        $data['queryTerms'] = $row['a_queryTerms'];
        $data['distance'] = $row['a_distance'];
        $data['ip'] = $row['a_ip'];
        $data['lastSendDate'] = $row['a_lastSendDate'];
        $data['lastProcessDate'] = $row['a_lastProcessDate'];
        $data['nextProcessDate'] = $row['a_nextProcessDate'];
        $data['createdDate'] = $row['a_createdDate'];
        $data['updatedDate'] = $row['a_updatedDate'];
        $data['utmSource'] = $row['a_utmSource'];
        $data['utmMedium'] = $row['a_utmMedium'];
        $data['utmTerm'] = $row['a_utmTerm'];
        $data['utmContent'] = $row['a_utmContent'];
        $data['utmCampaign'] = $row['a_utmCampaign'];
        $data['bouncesCount'] = $row['a_bouncesCount'];
        $data['complaintsCount'] = $row['a_complaintsCount'];
        $data['lastBounceDate'] = $row['a_lastBounceDate'];
        $data['lastComplaintDate'] = $row['a_lastComplaintDate'];
        $data['sesMessageId'] = $row['a_sesMessageId'];
        $data['isLocationValid'] = $row['a_isLocationValid'];
        $alert = new Model_Alert($data);
        return $alert;
    } // _createAlert

    /**
     * Create contact object from row data
     *
     * @param array $row
     *
     * @return Model_Contact
     */
    private function _createContact($row)
    {
        $data = array();
        $data['contactId'] = $row['c_contactId'];
        $data['contactSecureId'] = $row['c_contactSecureId'];
        $data['contactSourceId'] = $row['c_contactSourceId'];
        $data['contactStatus'] = $row['c_contactStatus'];
        $data['name'] = $row['c_name'];
        $data['country'] = $row['c_country'];
        $data['state'] = $row['c_state'];
        $data['city'] = $row['c_city'];
        $data['zip'] = $row['c_zip'];
        $data['address'] = $row['c_address'];
        $data['email'] = $row['c_email'];
        $data['createdDate'] = $row['c_createdDate'];
        $data['updatedDate'] = $row['c_updatedDate'];
        $data['originalCreatedDate'] = $row['c_originalCreatedDate'];
        $data['sendTimezone'] = $row['c_sendTimezone'];
        $data['userTimezone'] = $row['c_userTimezone'];
        $contact = new Model_Contact($data);
        return $contact;
    } // _createContact

    /**
     * Get SQL timezone filter
     *
     * @return string
     */
    private function _getTimezoneSql()
    {
        if ($this->_timezonesEnabled) {

            $timizones = array();
            foreach ($this->_timezonesList as $timezone) {
                $timizones[] = 'c.sendTimezone = "' . $timezone .'"';
            }
            $sql = ' AND (' . implode(' OR ', $timizones) . ')';
        } else {
            $sql = '';
        }
        return $sql;
    } // _getTimezoneSql
}
