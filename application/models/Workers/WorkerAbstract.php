<?php
/**
 * Model_Workers_WorkerAbstract
 *
 * PHP Version 5.3
 *
 * @category Abstraction
 * @package  Model\Workers
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */
abstract class Model_Workers_WorkerAbstract extends Model_Workers_GearmanCore implements Model_Workers_WorkerInterface
{
    /**
     * Time to wait for new Gearman task in GearmanWorker::work() method
     *
     * @var integer
     */
    const GEARMAN_TIMEOUT = 5000;

    /**
     * Worker function name to register with Gearman server
     *
     * @var string
     */
    protected $_processName;

    /**
     * Unique name for the process in supervisor
     * Need for save in redis // when we have multi-threading
     *
     * @var string
     */
    protected $_name;

    /**
     * We have received SIGTERM and should stop ASAP
     *
     * @var bool
     */
    protected $_stopOnSigterm;

    /**
     * Set unique process name for worker
     *
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->_name = $name;
        return $this;
    } // setName

    /**
     * Do the work. Contains general logic for all workers
     *
     * @return void
     */
    public function run()
    {
        declare(ticks = 1);
        pcntl_signal(SIGTERM, array($this, 'handleSignals'));
        pcntl_signal(SIGQUIT, array($this, 'handleSignals'));
        pcntl_signal(SIGINT,  array($this, 'handleSignals'));
        pcntl_signal_dispatch();

        $stat = new Model_Workers_Statistics($this->_name);
        $stat->init();

        $this->init();

        $worker = Model_ConnectionManager::getGearmanWorker();
        $worker->addFunction($this->_processName, array($this, 'baseHandling'));
        $worker->setTimeout(Model_Workers_WorkerAbstract::GEARMAN_TIMEOUT);

        while (!$this->_stopOnSigterm && ($worker->work() || GEARMAN_TIMEOUT == $worker->returnCode())) {
            if (GEARMAN_TIMEOUT == $worker->returnCode()) {
                $this->handleTimeout();
                $echo = @$worker->echo(1);
                if ($echo == false) {
                    $this->errorMessage('Gearman error: lost connection to Gearman server');
                    break;
                }
            } elseif (GEARMAN_SUCCESS != $worker->returnCode()) {
                $this->errorMessage('Gearman error: ' . $worker->error());
                break;
            }

            $memoryLimit = $this->getMemoryLimit();
            if ($memoryLimit != null) {
                $currentMemory = memory_get_usage(true)/1024/1024;
                if ($currentMemory > $memoryLimit) {
                    $this->handleTimeout();
                    $msg = 'Stopping on memory limit reached. Memory used ' . number_format($currentMemory) . ' Mb';
                    $this->warningMessage($msg);
                    break;
                }
            }
        }
    } // run

    /**
     * Common job handling logic
     *
     * @param GearmanJob $job
     *
     * @return void
     */
    public function baseHandling($job)
    {
        $start = microtime(true);

        $messageId = $job->workload();
        $message = Model_Workers_Message::load($messageId);

        if ($message == null) {
             $msg = 'Application error: message not found for id ' . $messageId;
             $this->errorMessage($msg);
        } else {
            $alert = $message->getAlert();

            if ($alert == null) {
                $msg = 'Application error: alert not defined for message with id ' . $messageId;
                $this->errorMessage($msg);
            } else {
                $this->handling($message);
            }
        }

        $job->sendComplete($messageId);

        $end = microtime(true);
        $time =  $end - $start;
        $stat = new Model_Workers_Statistics($this->_name);
        $stat->increaseCount();
        $stat->increaseExecTime($time);
        $stat->updateMemory();
    } // baseHandling

    /**
     * Initialise worker before start handling jobs from Gearman.
     * This method is called from self::run()
     *
     * @return void
     */
    public function init()
    {
    } // init

    /**
     * Specific worker implementation
     *
     * @param Model_Workers_Message $message
     *
     * @return void
     */
    abstract public function handling($message);

    /**
     * Do something useful when we don't have any jobs
     *
     * @return void
     */
    public function handleTimeout()
    {
    } // handleTimeout

    public function handleSignals($signo)
    {
        switch ($signo) {
            case SIGTERM:
            case SIGINT:
            case SIGQUIT:
                $this->_stopOnSigterm = true;
                $this->warningMessage('Stopping on signal ' . $signo);
                break;

            default:
                // handle all other signals
                break;
        }
    } // handleSignals
}
