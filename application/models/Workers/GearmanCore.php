<?php
/**
 * Model_Workers_GearmanCore
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Workers
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */
class Model_Workers_GearmanCore
{
    /**
     * Task to prepare alert message bodies
     *
     * @var string
     */
    const TASK_MESSAGES_PREPARE = 'prepareMessage-v3';

    /**
     * Task to send messages
     *
     * @var string
     */
    const TASK_MESSAGES_SEND = 'sendMessages-v3';

    /**
     * Task to retrieve results from API
     *
     * @var string
     */
    const TASK_RETRIEVE_API = 'retrieveApi-v3';

    /**
     * Task to update data in the DB
     *
     * @var string
     */
    const TASK_UPDATE_ALERT = 'updateAlert-v3';

    /**
     * Script memory limit in megabytes. Script exits correctly when the limit is reached.
     *
     * @var integer
     */
    protected $_memoryLimit;

    /**
     * Enable profiling output
     *
     * @var boolean
     */
    protected $_profileMode;

    /**
     * Set worker memory usage limit in megabytes
     *
     * @param integer $memoryLimit
     *
     * @return self
     */
    public function setMemoryLimit($memoryLimit)
    {
        $this->_memoryLimit = $memoryLimit;
        return $this;
    } // setMemoryLimit

    /**
     * Get script memory limit
     *
     * @return number
     */
    public function getMemoryLimit()
    {
        return $this->_memoryLimit;
    } // getMemoryLimit

    /**
     * Set profile mode
     *
     * @param boolean $profileMode
     *
     * @return self
     */
    public function setProfileMode($profileMode)
    {
        $this->_profileMode = $profileMode;
        return $this;
    } // setProfileMode

    /**
     * Get profile mode
     *
     * @return boolean
     */
    public function isProfileMode()
    {
        return $this->_profileMode;
    } // isProfileMode

    /**
     * Output profiling information
     *
     * @param string $msg
     *
     * @return void
     */
    public function profileMessage($msg)
    {
        if ($this->isProfileMode()) {
            $this->message('PROFILE', $msg);
        }
    } // profileMessage

    /**
     * Log error message
     *
     * @param string $msg
     *
     * @return void
     */
    public function errorMessage($msg)
    {
        $this->message('ERROR', $msg);
    } // errorMessage

    /**
     * Output warning message
     *
     * @param string $msg
     *
     * @return void
     */
    public function warningMessage($msg)
    {
        $this->message('WARNING', $msg);
    } // warningMessage

    /**
     * Output message
     *
     * @param string $level
     * @param string $msg
     *
     * @return void
     */
    public function message($level, $msg)
    {
        echo date('Y-m-d H:i:s') . ' - ' . strtoupper($level) . ' --> ' . $msg . PHP_EOL;
    } // message
}
