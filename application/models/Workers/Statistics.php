<?php
/**
 * Model_Workers_Statistics
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Workers
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */
class Model_Workers_Statistics
{
    /**
     * Time when worker start
     *
     * @var string
     */
    private $_start;

    /**
     * Number of jobs that worker execute
     *
     * @var integer
     */
    private $_count;

    /**
     * The total time of all jobs execution
     *
     * @var string
     */
    private $_execTime;

    /**
     * Memory usage
     *
     * @var string
     */
    private $_memory;

    /**
     * Statistics name
     *
     * @var string
     *
     * @return self
     */
    private $_name;

    public function __construct($name)
    {
        $this->_name = $name;
    }

    /**
     * Statistics initialisation
     *
     * @return void
     */
    public function init()
    {
        $data = $this->get();
        if (count($data) == 0 || !isset($data['start']) || time() - $data['start'] > 24*60*60) {
            $data = array();
            $data['start'] = time();
            $data['count'] = 0;
            $data['execTime'] = 0;
            $data['memory'] = memory_get_usage(true);

            $redis = Model_ConnectionManager::getRedisClient();
            $redis->hmset($this->_name, $data);
        }
    } // init

    /**
     * Increase counter by one
     */
    public function increaseCount()
    {
        $redis = Model_ConnectionManager::getRedisClient();
        $redis->hincrby($this->_name, 'count', 1);
    } // increaseCount

    /**
     * Increase execution time
     *
     * @param string $time
     */
    public function increaseExecTime($time)
    {
        //important!needs redis version >= 2.6
        $redis = Model_ConnectionManager::getRedisClient();
        $redis->hincrbyfloat($this->_name, 'execTime', $time);
    } // increaseExecTime

    /**
     * Update memory usage
     */
    public function updateMemory()
    {
        $redis = Model_ConnectionManager::getRedisClient();
        $redis->hset($this->_name, 'memory', memory_get_usage(true));
    } // updateMemory

    /**
     * Get Statistics
     */
    public function get()
    {
        $redis = Model_ConnectionManager::getRedisClient();
        $stat = $redis->hgetall($this->_name);

        return $stat;
    } // get

    /**
     * Get formatted message
     */
    public function getFormatted()
    {
        $msg = 'Name: ' . $this->_name . PHP_EOL;
        $stat = $this->get();
        if (count($stat) > 0) {
            $del = $stat['count'] ? $stat['count'] : 1;
            $msg .= 'Start Time: ' . date('Y-m-d H:i:s', $stat['start']) . PHP_EOL;
            $msg .= 'Executed Jobs: ' . number_format($stat['count']) . PHP_EOL;
            $msg .= 'Average Time: ' . number_format($stat['execTime']/$del, 7) . 'sec' . PHP_EOL;
            $msg .= 'Memory Usage: ' . number_format($stat['memory']/1024/1024, 2) . 'MB' . PHP_EOL;
        }

        return $msg;
    } // getFormatted
}
