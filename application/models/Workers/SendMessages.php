<?php
/**
 * Model_Workers_SendMessages
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Workers
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Send message via Amazon SES
 */
class Model_Workers_SendMessages extends Model_Workers_WorkerAbstract
{
    /**
     * Batch size for SES send
     *
     * @var integer
     */
    const MAX_PACK_SIZE = 250;

    /**
     * Task name for Gearman server
     *
     * @var string
     */
    protected $_processName = Model_Workers_GearmanCore::TASK_MESSAGES_SEND;

    /**
     * Name for save messages queue in Redis
     *
     * @var string
     */
    protected $_messageQueueName;

    /**
     * SES client
     *
     * @var Aws\Ses\SesClient
     */
    private $_ses;

    /**
     * Message queue
     *
     * @var array
     */
    private $_messageQueue;

    /**
     * Set unique process name for worker
     *
     * @param string $name
     *
     * @return Model_Workers_SendMessages
     */
    public function setName($name)
    {
        parent::setName($name);
        $this->_messageQueueName = $this->_name . '_queue';
        return $this;
    } // setName

    /**
     * Initialise worker before start handling jobs from Gearman.
     * This method is called from self::run()
     *
     * @return void
     */
    public function init()
    {
        $this->_ses = Aws\Ses\SesClient::factory(array(
                'key' => 'AKIAIMXT52XARSLVCOYQ',
                'secret' => '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',
                'region' => 'us-east-1',
        ));

        $this->_messageQueue = $this->_redisGetMessages();
        $this->profileMessage('--> Restore from Redis ' . $this->_getMessagesCount() . ' messages');
    } // init

    /**
     * Send messages
     *
     * @see Model_Workers_WorkerAbstract::handling()
     *
     * @param Model_Workers_Message $message
     *
     * @return void
     */
    public function handling($message)
    {
        if ($message !== null) {
            $this->_addMessage($message);
        }

        $count = $this->_getMessagesCount();

        if ($count >= self::MAX_PACK_SIZE) {
            $this->_sendMessages();
        }
    } // handling

    /**
     * Send messages in the queue if we have any
     *
     * @return void
     */
    public function handleTimeout()
    {
        $this->_messageQueue = $this->_redisGetMessages();
        $count = $this->_getMessagesCount();
        if ($count > 0) {
            $this->_sendMessages();
        }
    } // handleTimeout

    /**
     * Load messages from Redis
     *
     * @return void
     */
    private function _redisGetMessages()
    {
        $messages = array();
        $redis = Model_ConnectionManager::getRedisClient();
        $count = $redis->llen($this->_messageQueueName);

        if ($count > 0) {
            $messageList = $redis->lrange($this->_messageQueueName, 0, $count);
            foreach ($messageList as $item) {
                $message = unserialize($item);
                $messageId = $message->getMessageId();
                $messages[$messageId] = $message;
            }
        }
        return $messages;
    } // _redisGetMessages

    /**
     * Get number of messages in the queue for sending
     *
     * @return number
     */
    private function _getMessagesCount()
    {
        return count($this->_messageQueue);
    } // _getMessagesCount

    /**
     * Get all messages for sending
     *
     * @return array
     */
    private function _getMessages()
    {
        return $this->_messageQueue;
    } // _getMessages

    /**
     * Add message to the queue for sending
     *
     * @param Model_Workers_Message $message
     *
     * @return void
     */
    private function _addMessage($message)
    {
        $messageId = $message->getMessageId();
        $this->_messageQueue[$messageId] = $message;

        $redis = Model_ConnectionManager::getRedisClient();
        $redis->rpush($this->_messageQueueName, serialize($message));
    } // _addMessage

    /**
     * Clear messages queue
     *
     * @return void
     */
    private function _clearMessages()
    {
        $this->_messageQueue = array();

        $redis = Model_ConnectionManager::getRedisClient();
        $redis->del($this->_messageQueueName);
    } // _clearMessages

    /**
     * Send messages queued for sending
     *
     * @return void
     */
    private function _sendMessages()
    {
        $messages = $this->_getMessages();

        $commands = array();
        foreach ($messages as $message) {
            $commands[] = $this->_createSesCommand($message);
        }

        $gearmanClient = Model_ConnectionManager::getGearmanClient();

        try {
            $successfulCommands = $this->_ses->execute($commands);
            $this->_clearMessages();
        } catch (Guzzle\Service\Exception\CommandTransferException $sesError) {
            $this->profileMessage('--> Messages sent with errors.');
            $successfulCommands = $sesError->getSuccessfulCommands();
            $failedCommands = $sesError->getFailedCommands();
            $this->_clearMessages();

            $successedMessages = array();
            foreach ($successfulCommands as $command) {
                $successedMessages[] = $command->get('messageId');
            }

            $sesMessage = $sesError->getMessage();
            $sesMessages = explode("\n", $sesMessage);
            $m = 0;
            foreach ($failedCommands as $command) {
                $sesMessage = $sesMessages[$m];
                $messageId = $command->get('messageId');
                $message = $messages[$messageId];

                if ($sesMessage == 'Maximum sending rate exceeded.') {
                    if (array_search($messageId, $successedMessages) === false) {
                        $gearmanClient->doBackground(Model_Workers_GearmanCore::TASK_MESSAGES_SEND, $messageId);
                    }
                } else {
                    $this->_updateMessage($message, false, null);
                    $gearmanClient->doBackground(Model_Workers_GearmanCore::TASK_UPDATE_ALERT, $messageId);
                }
                $m++;
            }
        }
        foreach ($successfulCommands as $command) {
            $messageId = $command->get('messageId');
            $result = $command->getResult();
            $sesMessageId = $result->get('MessageId');

            $message = $messages[$messageId];
            $this->_updateMessage($message, true, $sesMessageId);
            $gearmanClient->doBackground(Model_Workers_GearmanCore::TASK_UPDATE_ALERT, $messageId);
        }

        $this->_clearMessages();
    } // _sendMessages

    /**
     * Create SES command to send given message
     *
     * @param Model_Workers_Message $message
     *
     * @return CommandInterface
     */
    private function _createSesCommand($message)
    {
        $messageId = $message->getMessageId();
        $messageData = $message->getMessageData();
        $data = array(
                'Source' => $messageData['fromAddress'],
                'Destination' => array(
                        'ToAddresses' => array($messageData['toAddress']),
                ),
                'Message' => array(
                        'Subject' => array(
                                'Data' => $messageData['subject'],
                                'Charset' => 'UTF-8'
                        ),
                        'Body' => array(
                                'Text' => array(
                                        'Data' => $messageData['bodyText'],
                                        'Charset' => 'UTF-8'
                                ),
                                'Html' => array(
                                        'Data' => $messageData['bodyHtml'],
                                        'Charset' => 'UTF-8'
                                )
                        )
                ),
                'ReturnPath' => $messageData['fromEmail'],
        );

        $command = $this->_ses->getCommand('SendEmail', $data);
        $command->add('messageId', $messageId);
        return $command;
    } // _createSesCommand

    /**
     * Update message
     *
     * @param Model_Workers_Message $message
     * @param boolean $success
     * @param string $sesMessageId
     *
     * @return void
     */
    private function _updateMessage($message, $success, $sesMessageId)
    {
        $messageData = $message->getMessageData();
        $messageData['sendDate'] = Model_Utils::sqlNow();
        if ($success) {
            $status = Model_Workers_Message::STATUS_SEND_SUCCESSFUL;
            $messageData['sesMessageId'] = $sesMessageId;
            $messageData['status'] = Model_MessageArchive::STATUS_SEND_SUCCESS;
        } else {
            $status = Model_Workers_Message::STATUS_SEND_FAIL;
            $messageData['status'] = Model_MessageArchive::STATUS_SEND_FAIL;
        }
        $message->setStatus($status);
        $message->setMessageData($messageData);
        $message->save();
    } // _updateMessage
}
