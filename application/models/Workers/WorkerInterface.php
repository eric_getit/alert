<?php
/**
 * Model_Workers_WorkerInterface
 *
 * PHP Version 5.3
 *
 * @category Interface
 * @package  Model\Workers
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */
interface Model_Workers_WorkerInterface
{
    /**
     * Do the work :P
     *
     * @return void
     */
    public function run();
}
