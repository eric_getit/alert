<?php
/**
 * Model_Workers_PrepareMessages
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Workers
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */
class Model_Workers_PrepareMessages extends Model_Workers_WorkerAbstract
{
    /**
     * Task name for Gearman server
     *
     * @var string
     */
    protected $_processName = Model_Workers_GearmanCore::TASK_MESSAGES_PREPARE;

    /**
     * Prepare message for sending
     *
     * @param Model_Workers_Message $message
     *
     * @return void
     */
    public function handling($message)
    {
        $messageId = $message->getMessageId();

        $alert = $message->getAlert();
        try {
            $apiResults = $message->getApiResult();
            $messageData = $alert->prepareMessage($apiResults, $messageId);
            $message->setMessageData($messageData);
            $message->setStatus(Model_Workers_Message::STATUS_MESSAGE_READY);
            $message->save();

            $gearmanClient = Model_ConnectionManager::getGearmanClient();
            $gearmanClient->doBackground(Model_Workers_GearmanCore::TASK_MESSAGES_SEND, $messageId);
        } catch (Exception $e) {
            $msg = 'Fail to prepare message body for the alert ' . $alert->getAlertId()
                 . ' Error message ' . $e->getMessage();
            $this->errorMessage($msg);

            $apiResult = array();
            $apiResult['listings'] = array();
            $apiResult['count'] = 0;
            $apiResult['organicCount'] = 0;
            $apiResult['backfillCount'] = 0;

            $message->setApiResult($apiResult);
            $message->setMessageData(null);
            $message->setStatus(Model_Workers_Message::STATUS_API_NO_RESULTS);
            $message->save();

            $gearmanClient = Model_ConnectionManager::getGearmanClient();
            $gearmanClient->doBackground(Model_Workers_GearmanCore::TASK_UPDATE_ALERT, $messageId);
        }
    } // handling
}
