<?php
/**
 * Model_Login
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Plugin to check if user is authenticated to access admin pages
 */
class Model_Login extends Zend_Controller_Plugin_Abstract
{
    /**
     * Called before an action is dispatched by Zend_Controller_Dispatcher.
     *
     * This callback allows for proxy or filter behavior.  By altering the
     * request and resetting its dispatched flag (via
     * {@link Zend_Controller_Request_Abstract::setDispatched() setDispatched(false)}),
     * the current action may be skipped.
     *
     * @param  Zend_Controller_Request_Abstract $request
     *
     * @return void
     */
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        Model_LoggedUserCookies::authenticateAdmin();
        if (!Model_AdminUser::isAuthenticated()) {
            $params = $this->getRequest()->getParams();
            if ($params['controller'] != 'login' || $params['action'] != 'index') {
                $this->getRequest()->setModuleName('default')
                    ->setControllerName('login')
                    ->setActionName('index');
            }
        }
    } // preDispatch
}
