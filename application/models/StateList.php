<?php
/**
 * Model_StateList
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Working with states names
 */
class Model_StateList
{
    /**
     * An associative array with the codes and the names of the states
     *
     * @var array
     */
    protected static $_stateMap = array(
        'AL' => 'Alabama',
        'IL' => 'Illinois',
        'MT' => 'Montana',
        'RI' => 'Rhode Island',
        'AK' => 'Alaska',
        'IN' => 'Indiana',
        'NE' => 'Nebraska',
        'SC' => 'South Carolina',
        'AZ' => 'Arizona',
        'IA' => 'Iowa',
        'NV' => 'Nevada',
        'SD' => 'South Dakota',
        'AR' => 'Arkansas',
        'KS' => 'Kansas',
        'NH' => 'New Hampshire',
        'TN' => 'Tennessee',
        'CA' => 'California',
        'KY' => 'Kentucky',
        'NJ' => 'New Jersey',
        'TX' => 'Texas',
        'CO' => 'Colorado',
        'LA' => 'Louisiana',
        'NM' => 'New Mexico',
        'UT' => 'Utah',
        'CT' => 'Connecticut',
        'ME' => 'Maine',
        'NY' => 'New York',
        'VT' => 'Vermont',
        'DE' => 'Delaware',
        'MD' => 'Maryland',
        'NC' => 'North Carolina',
        'VA' => 'Virginia',
        'DC' => 'District Of Columbia',
        'MA' => 'Massachusetts',
        'ND' => 'North Dakota',
        'WA' => 'Washington',
        'FL' => 'Florida',
        'MI' => 'Michigan',
        'OH' => 'Ohio',
        'WV' => 'West Virginia',
        'GA' => 'Georgia',
        'MN' => 'Minnesota',
        'OK' => 'Oklahoma',
        'WI' => 'Wisconsin',
        'HI' => 'Hawaii',
        'MS' => 'Mississippi',
        'OR' => 'Oregon',
        'WY' => 'Wyoming',
        'ID' => 'Idaho',
        'MO' => 'Missouri',
        'PA' => 'Pennsylvania',
        'PR' => 'Puerto Rico',
        'VI' => 'Virgin Islands',
        'GU' => 'Guam',
        'MH' => 'Marshall Islands',
    );


    /**
     * Check is the given $name is correct state code or name and
     * return state code.
     * Return false if given name is incorrect state
     *
     * @param string $name State name or code
     *
     * @return string | false
     */
    public static function normalize($name)
    {
        $code = strtoupper($name);
        if (isset(self::$_stateMap[$code])) {
            return $code;
        }

        return self::getCode($name);
    } // normalize


    /**
     * Return state two-symbol code by name or some anther code
     *
     * @param string $name State name
     *
     * @return string | false
     */
    public static function getCode($name)
    {
        $tmp_array = self::$_stateMap;
        foreach ($tmp_array as $key => $value) {
           $tmp_array[$key] = strtolower($value);
        }

        $name = strtolower($name);
        $result = array_search($name, $tmp_array);
        return $result;
    } // getCode


    /**
     * Return state name by code
     *
     * @param string $code State Code
     *
     * @return string | null
     */
    public static function getName($code)
    {
        $code = strtoupper($code);
        if (isset(self::$_stateMap[$code])) {
            $result = self::$_stateMap[$code];
        } else {
            $result = null;
        }
        return $result;
    } // getName


    /**
     * Get all state list
     *
     * @return array
     */
    public static function getAll()
    {
        $tmp_array = self::$_stateMap;
        $result = asort($tmp_array, SORT_STRING);
        return $tmp_array;
    } // getAll
}
