<?php
/**
 * Model_Admin_Page_ContactDetail
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Page\Admin
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Page\Page,
    GetIt\Admin\Grid\Column,
    GetIt\Admin\Grid\Grid;

/**
 * Manage contact Detail
 * @author Eugene Churmanov <eugene@soft-rose.com>
 */
class Model_Admin_Page_ContactDetails extends Page
{
    /**
     * Template file
     *
     * @var string
     */
    protected $_twigTemplateName = '/admin/contact-details.html.twig';

    /**
     * Contact object - show details for this contact
     *
     * @var Model_Contact
     */
    protected $_contact;

    /**
     * Get contact object for the detail page
     *
     * @return Model_Contact
     */
    public function getContact()
    {
        return $this->_contact;
    } // getContact

    /**
     * Set contact object for the detail page
     *
     * @param Model_Contact $contact
     *
     * @return Model_Admin_Page_ContactDetails
     */
    public function setContact($contact)
    {
        $this->_contact = $contact;

        $title = 'Contact Details for ' . $contact->getEmail();
        $this->setTitle($title);

        $breadcrumbs = array(
                'Contacts' => '/contacts',
                $title => '',
        );
        $this->setBreadcrumbs($breadcrumbs);

        return $this;
    } // setContact

    /**
     * Get admin page breadcrumbs
     *
     * @return array
     */
    public function getBreadcrumbs()
    {
        if (empty($this->_breadcrumbs)) {
            $this->_breadcrumbs = array(
                    'Contacts' => '/contacts',
                    $this->getTitle() => '',
            );
        }

        return $this->_breadcrumbs;
    } // getBreadcrumbs

    /**
     * Initialize Twig template
     *
     * @see \GetIt\Admin\Page\Page::_initTwigContext()
     *
     * @return void
     */
    protected function _initTwigContext()
    {
        parent::_initTwigContext();

        $contact = $this->getContact();

        switch ($contact->getContactStatus()) {
            case Model_Contact::STATUS_ACTIVE:
                $contactStatusClass = 'text-success';
                break;

            case Model_Contact::STATUS_DEACTIVE:
                $contactStatusClass = 'text-error';
                break;

            case Model_Contact::STATUS_SUSPEND:
                $contactStatusClass = 'text-info';
                break;

            default:
                $contactStatusClass = 'text-inwarningfo';
                break;
        }
        $this->_twigContext['contact'] = $contact;
        $this->_twigContext['contactStatusClass'] = $contactStatusClass;

        $alertsColumns = $this->_getAlertsColumns();
        $alertsStore = new Model_Admin_Store_Alerts();

        $alertsGrid = new Grid();
        $alertsGrid->setPagingDisabled(true);
        $alertsGrid->setParams($this->getParams());
        $alertsGrid->setColumns($alertsColumns);
        $alertsGrid->setStore($alertsStore);
        $this->_twigContext['alertsGrid'] = $alertsGrid->render();

        $newslettersColumns = $this->_getNewslettersColumns();
        $newslettersStore = new Model_Admin_Store_NewsletterSubscriptions();
        $newslettersStore->setFilters(array('contactId' => $contact->getContactId()));

        $newslettersGrid = new Grid();
        $newslettersGrid->setPagingDisabled(true);
        $newslettersGrid->setParams($this->getParams());
        $newslettersGrid->setColumns($newslettersColumns);
        $newslettersGrid->setStore($newslettersStore);
        $this->_twigContext['newslettersGrid'] = $newslettersGrid->render();


        $operationsColumns = $this->_getOperationsColumns();
        $operationsStore = new Model_Admin_Store_AlertsOperationsHistory();

        $operationsGrid = new Grid();
        $operationsGrid->setPagingDisabled(true);
        $operationsGrid->setParams($this->getParams());
        $operationsGrid->setColumns($operationsColumns);
        $operationsGrid->setStore($operationsStore);
        $this->_twigContext['operationsGrid'] = $operationsGrid->render();

        // TODO : Put user last activity date here
        $this->_twigContext['lastActivity'] = '';
    } // _initTwigContext

    /**
     * Get list of columns for the alert grid
     *
     * @return array
     */
    private function _getAlertsColumns()
    {
        $columns = array();

        $column = new Column();
        $column->setProperty('alertStatus');
        $column->setCaption('Status');
        $column->setType(Column::TYPE_ENUM);
        $column->setSortable(false);
        $column->setEnumMap(array(
                Model_Alert::STATUS_ACTIVE => 'Active',
                Model_Alert::STATUS_DEACTIVE => 'Deactivated',
                Model_Alert::STATUS_PENDING => 'Pending Multi Alert',
                Model_Alert::STATUS_ADM_PENDING => 'Admin Pending',
                Model_Alert::STATUS_SUSPENDED_AS_BOUNCE_PERMANENT => 'Permanent Bounce',
                Model_Alert::STATUS_SUSPENDED_AS_BOUNCE_TRANSIENT => 'Transient Bounce',
                Model_Alert::STATUS_SUSPENDED_AS_COMPLAINT => 'SPAM Complaint',
                Model_Alert::STATUS_UNSUBSCRIBED_ADMIN => 'Unsubscribed by admin',
                Model_Alert::STATUS_UNSUBSCRIBED_API => 'Unsubscribed via API',
                Model_Alert::STATUS_UNSUBSCRIBED_USER => 'Unsubscribed',
        ));
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('siteDomain');
        $column->setCaption('Site');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('alertType');
        $column->setCaption('Type');
        $column->setType(Column::TYPE_ENUM);
        $column->setSortable(false);
        $column->setEnumMap(array(
                'DAILY' => 'Daily',
                'WEEKLY' => 'Weekly'
        ));
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('queryDisplay');
        $column->setCaption('Query');
        $column->setType(Column::TYPE_RAW);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('createdDate');
        $column->setCaption('Created');
        $column->setType(Column::TYPE_DATE);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('lastSendDate');
        $column->setCaption('Last Sent');
        $column->setType(Column::TYPE_DATE);
        $column->setSortable(false);
        $columns[] = $column;

        return $columns;
    } // _getAlertsColumns

    /**
     * Get list of columns for the newsletters grid
     *
     * @return array
     */
    private function _getNewslettersColumns()
    {
        $columns = array();

        $column = new \GetIt\Admin\Grid\Column();
        $column->setProperty('SubscriptionStatus');
        $column->setSortingProperty('subscriptionStatus');
        $column->setCaption('Status');
        $column->setType(Column::TYPE_ENUM);
        $column->setSortable(false);
        $column->setEnumMap(array(
            Model_Newsletter_Subscription::STATUS_ACTIVE => 'Active',
            Model_Newsletter_Subscription::STATUS_DEACTIVE => 'Unsubscribed',
        ));
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('NewsletterName');
        $column->setSortingProperty('newsletterName');
        $column->setCaption('Newsletter');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new \GetIt\Admin\Grid\Column();
        $column->setProperty('Domain');
        $column->setSortingProperty('domain');
        $column->setCaption('Domain');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new \GetIt\Admin\Grid\Column();
        $column->setProperty('CreatedDate');
        $column->setSortingProperty('createdDate');
        $column->setCaption('Created');
        $column->setType(Column::TYPE_DATE);
        $column->setSortable(false);
        $columns[] = $column;

        return $columns;
    } // _getNewslettersColumns

    /**
     * Get list of columns for the alerts operations history list
     *
     * @return array
     */
    private function _getOperationsColumns()
    {
        $columns = array();

        $column = new \GetIt\Admin\Grid\Column();
        $column->setProperty('operation');
        $column->setCaption('Operation');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(false);
        $column->setEnumMap(array(
                Model_Newsletter_Subscription::STATUS_ACTIVE => 'Active',
                Model_Newsletter_Subscription::STATUS_DEACTIVE => 'Unsubscribed',
        ));
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('ip');
        $column->setCaption('IP Address');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new \GetIt\Admin\Grid\Column();
        $column->setProperty('createdDate');
        $column->setCaption('Date');
        $column->setType(Column::TYPE_DATE);
        $column->setSortable(false);
        $columns[] = $column;

        return $columns;
    } // _getOperationsColumns

}
