<?php
/**
 * Model_Admin_Page_MessageArchive
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Page\Admin
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Grid\Column,
    GetIt\Admin\Grid\Grid,
    GetIt\Admin\Page\GridPage;

/**
 * Manage messages archive
 * @author Eugene Kuaznetsov <eugene@soft-rose.com>
 */
class Model_Admin_Page_MessageArchive extends GridPage
{
    /**
     * Page title
     * @var  string
     */
    protected $_title = 'Message Archive';

    /**
     * Get list of columns for the grid
     *
     * @return array:GetIt\Admin\Grid\Item
     */
    protected function _getColumns()
    {
        $columns = array();

        $column = new Column();
        $column->setProperty('ContactId');
        $column->setSortingProperty('contactId');
        $column->setCaption('Contact Id');
        $column->setType(Column::TYPE_INT);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new \GetIt\Admin\Grid\Column();
        $column->setProperty('AlertId');
        $column->setSortingProperty('alertId');
        $column->setCaption('Alert Id');
        $column->setType(Column::TYPE_INT);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new \GetIt\Admin\Grid\Column();
        $column->setProperty('SendDate');
        $column->setSortingProperty('sendDate');
        $column->setCaption('Send Date');
        $column->setType(Column::TYPE_DATE);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new \GetIt\Admin\Grid\Column();
        $column->setProperty('FromEmail');
        $column->setSortingProperty('fromEmail');
        $column->setCaption('Sender Email');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new \GetIt\Admin\Grid\Column();
        $column->setProperty('FromName');
        $column->setSortingProperty('fromName');
        $column->setCaption('Sender Name');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new \GetIt\Admin\Grid\Column();
        $column->setProperty('ToEmail');
        $column->setSortingProperty('toEmail');
        $column->setCaption('Recipient Email');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new \GetIt\Admin\Grid\Column();
        $column->setProperty('Subject');
        $column->setSortingProperty('subject');
        $column->setCaption('Subject');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(true);
        $columns[] = $column;

        return $columns;
    } // getColumns

    protected function _getStore()
    {
        $store = new Model_Admin_Store_MessageArchive();
        $store->setSorters(array('property' => 'sendDate', 'direction' => 'DESC'));
        return $store;
    } // getStore

    /**
     * Apply custom grid settings
     *
     * @param Grid $grid
     *
     * @return void
     */
    protected function _initGrid($grid)
    {
        $grid->setDateRangeFilterEnabled(true);
        if ($grid->getParam('interval') == null &&
            $grid->getParam('from') == null &&
            $grid->getParam('to') == null) {
            $grid->setParam('interval', 'day');
        }
    } // _initGrid
}
