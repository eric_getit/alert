<?php
/**
 * Model_Admin_Page_Config
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Page\Admin
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Grid\Column,
    GetIt\Admin\Grid\Grid,
    GetIt\Admin\Page\GridPage;

/**
 * Manage contacts
 */
class Model_Admin_Page_Contacts extends GridPage
{
    /**
     * Page title
     *
     * @var string
     */
    protected $_title = 'Contacts';

    /**
     * Get columns for the grid
     *
     * @return multitype:Column
     */
    protected function _getColumns()
    {
        $columns = array();

        $column = new Column();
        $column->setProperty('ContactDetailsLink');
        $column->setSortingProperty('email');
        $column->setCaption('Email');
        $column->setType(Column::TYPE_RAW);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('ContactStatus');
        $column->setCaption('Status');
        $column->setType(Column::TYPE_ENUM);
        $column->setSortable(true);
        $column->setEnumMap(array(
                Model_Contact::STATUS_ACTIVE => 'Active',
                Model_Contact::STATUS_DEACTIVE => 'Deactivated',
                Model_Contact::STATUS_UNSUBSCRIBED_ADMIN => 'Unsubscribed by admin',
                Model_Contact::STATUS_UNSUBSCRIBED_API => 'Unsubscribed via API',
                Model_Contact::STATUS_UNSUBSCRIBED_USER => 'Unsubscribed',
                Model_Contact::STATUS_SUSPEND => 'Suspended',
        ));
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('AlertsLink');
        $column->setSortingProperty('alertsCount');
        $column->setCaption('Alerts');
        $column->setType(Column::TYPE_RAW);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('NewslettersLink');
        $column->setSortingProperty('newslettersCount');
        $column->setCaption('Newsletters');
        $column->setType(Column::TYPE_RAW);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('Name');
        $column->setCaption('Name');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('Address');
        $column->setCaption('Address');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('City');
        $column->setCaption('City');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('State');
        $column->setCaption('State');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('Country');
        $column->setCaption('Country');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('Zip');
        $column->setCaption('Zip');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('CreatedDate');
        $column->setCaption('Created');
        $column->setType(Column::TYPE_DATE);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('Actions');
        $column->setCaption('Actions');
        $column->setType(Column::TYPE_ACTIONS);
        $column->setSortable(false);
        $columns[] = $column;

        return $columns;
    } // getColumns

    /**
     * Get store for the grid
     *
     * @return GetIt\Admin\Grid\Store
     */
    protected function _getStore()
    {
        $store = new Model_Admin_Store_Contacts();
        $store->setSorters(array('property' => 'CreatedDate', 'direction' => 'DESC'));
        return $store;
    } // getStore

    /**
     * Apply custom grid settings
     *
     * @param Grid $grid
     *
     * @return void
     */
    protected function _initGrid($grid)
    {
        $grid->setSearchEnabled(true);
    } // _initGrid
}
