<?php
/**
 * Model_Admin_Page_Edit_Contact
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Admin\Page\Edit
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Form\Field,
    GetIt\Admin\Page\EditPage;

/**
 * Edit newsletter
 */
class Model_Admin_Page_Edit_Contact extends EditPage
{
    /**
     * Page title
     *
     * @var  string
     */
    protected $_title = 'Edit Contact';

    /**
     * Action on submit
     *
     * @var string
     */
    protected $_action = '/contacts/edit';

    /**
     * Action on cancel
     *
     * @var string
     */
    protected $_cancel = '/contacts';

    /**
     * Get list of fields for the edit
     *
     * @return array:GetIt\Admin\Form\Field
     */
    public function getFields()
    {
        $fields = array();

        $field = new Field();
        $field->setProperty('email');
        $field->setCaption('Email');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $field->setParams(array('allowBlank' => false));
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('name');
        $field->setCaption('Name');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('address');
        $field->setCaption('Address');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('city');
        $field->setCaption('City');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('state');
        $field->setCaption('State');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('country');
        $field->setCaption('Country');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('zip');
        $field->setCaption('Postalcode');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('contactId');
        $field->setDataType(Field::DATA_TYPE_INT);
        $field->setFieldType(Field::FIELD_TYPE_HIDDEN);
        $fields[] = $field;

        return $fields;
    } // getFields
}
