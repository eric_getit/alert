<?php
/**
 * Model_Admin_Page_Edit_Alert
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Admin\Page\Edit
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Form\Field,
    GetIt\Admin\Page\EditPage;

/**
 * Edit newsletter
 */
class Model_Admin_Page_Edit_Alert extends EditPage
{
    /**
     * Page title
     *
     * @var  string
     */
    protected $_title = 'Edit Alert';

    /**
     * Action on submit
     *
     * @var string
     */
    protected $_action = '/save/alert';

    /**
     * List of sites
     */
    static protected $_sites = null;

    /**
     * Get list of fields for the edit
     *
     * @return array:GetIt\Admin\Form\Field
     */
    public function getFields()
    {
        if (empty(self::$_sites)) {
            $dbh = Zend_Db_Table::getDefaultAdapter();
            $sql = 'select siteId, domain from sites';
            self::$_sites = $dbh->fetchPairs($sql);
        }

        $fields = array();

        $field = new Field();
        $field->setProperty('siteId');
        $field->setCaption('Site');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_ENUM);
        $field->setEnumMap(self::$_sites);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('alertType');
        $field->setCaption('Type');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_ENUM);
        $field->setEnumMap(array(
                    Model_Alert::TYPE_DAILY => 'Daily',
                    Model_Alert::TYPE_WEEKLY => 'Weekly'
        ));
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('alertStatus');
        $field->setCaption('Status');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_ENUM);
        $field->setEnumMap(array(
                    Model_Alert::STATUS_ACTIVE => 'Active',
                    Model_Alert::STATUS_DEACTIVE => 'Deactivated',
                    Model_Alert::STATUS_PENDING => 'Pending Multi Alert',
                    Model_Alert::STATUS_ADM_PENDING => 'Admin Pending',
                    Model_Alert::STATUS_SUSPENDED_AS_BOUNCE_PERMANENT => 'Permanent Bounce',
                    Model_Alert::STATUS_SUSPENDED_AS_BOUNCE_TRANSIENT => 'Transient Bounce',
                    Model_Alert::STATUS_SUSPENDED_AS_COMPLAINT => 'SPAM Complaint',
                    Model_Alert::STATUS_UNSUBSCRIBED_ADMIN => 'Unsubscribed by admin',
                    Model_Alert::STATUS_UNSUBSCRIBED_API => 'Unsubscribed via API',
                    Model_Alert::STATUS_UNSUBSCRIBED_USER => 'Unsubscribed',
        ));
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('queryTerms');
        $field->setCaption('Query');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('location');
        $field->setCaption('Location');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('isLocationValid');
        $field->setCaption('Location Valid');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_ENUM);
        $field->setEnumMap(array(
                    '1' => 'Yes',
                    '0' => 'No',
        ));
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('distance');
        $field->setCaption('Distance');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_ENUM);
        $field->setEnumMap(array(
                    '0' => 'exact',
                    '5' => 'within 5 miles',
                    '10' => 'within 10 miles',
                    '15' => 'within 15 miles',
                    '25' => 'within 25 miles',
                    '50' => 'within 50 miles',
                    '100' => 'within 100 miles',
        ));
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('utmSource');
        $field->setCaption('Utm Source');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('utmMedium');
        $field->setCaption('Utm Medium');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('utmTerm');
        $field->setCaption('Utm Term');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('utmContent');
        $field->setCaption('Utm Content');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('utmCampaign');
        $field->setCaption('Utm Campaign');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('alertId');
        $field->setDataType(Field::DATA_TYPE_INT);
        $field->setFieldType(Field::FIELD_TYPE_HIDDEN);
        $fields[] = $field;

        return $fields;
    } // getFields
}
