<?php
/**
 * Model_Admin_Page_Edit_Site
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Admin\Page\Edit
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Form\Field,
    GetIt\Admin\Page\EditPage;

/**
 * Edit newsletter
 */
class Model_Admin_Page_Edit_Site extends EditPage
{
    /**
     * Page title
     *
     * @var  string
     */
    protected $_title = 'Edit Site';

    /**
     * Action on submit
     *
     * @var string
     */
    protected $_action = '/save/site';

    /**
     * Action on cancel
     *
     * @var string
     */
    protected $_cancel = '/sites';

    /**
     * Get list of fields for the edit
     *
     * @return array:GetIt\Admin\Form\Field
     */
    public function getFields()
    {
        $fields = array();

        $field = new Field();
        $field->setProperty('domain');
        $field->setCaption('Domain');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $field->setParams(array('allowBlank' => false));
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('siteName');
        $field->setCaption('Site Name');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('fromName');
        $field->setCaption('From Name');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('fromEmail');
        $field->setCaption('From Email');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('siteId');
        $field->setDataType(Field::DATA_TYPE_INT);
        $field->setFieldType(Field::FIELD_TYPE_HIDDEN);
        $fields[] = $field;

        return $fields;
    } // getFields
}
