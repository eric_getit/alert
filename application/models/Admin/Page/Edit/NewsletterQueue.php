<?php
/**
 * Model_Admin_Page_Edit_NewsletterQueue
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Admin\Page\Edit
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Form\Field,
    GetIt\Admin\Page\EditPage;

/**
 * Edit newsletter
 */
class Model_Admin_Page_Edit_NewsletterQueue extends EditPage
{
    /**
     * Page title
     *
     * @var  string
     */
    protected $_title = 'Edit Newsletter Queue';

    /**
     * Action on submit
     *
     * @var string
     */
    protected $_action = '/save/newsletter-queue';

    /**
     * Action on cancel
     *
     * @var string
     */
    protected $_cancel = '/newsletters-queue';

    /**
     * Get list of fields for the edit
     *
     * @return array:GetIt\Admin\Form\Field
     */
    public function getFields()
    {
        $fields = array();

        $field = new Field();
        $field->setProperty('queueStatus');
        $field->setCaption('Status');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_ENUM);
        $field->setEnumMap(array(
                    Model_Newsletter_Queue::STATUS_NEW => 'New',
                    Model_Newsletter_Queue::STATUS_PREPARED => 'Prepared',
        ));
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('subject');
        $field->setCaption('Subject');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $field->setParams(array('allowBlank' => false));
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('message');
        $field->setCaption('Message');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXTAREA);
        $field->setParams(array('allowBlank' => false));
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('fromName');
        $field->setCaption('From Name');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('fromEmail');
        $field->setCaption('From Email');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('queueId');
        $field->setDataType(Field::DATA_TYPE_INT);
        $field->setFieldType(Field::FIELD_TYPE_HIDDEN);
        $fields[] = $field;

        return $fields;
    } // getFields
}
