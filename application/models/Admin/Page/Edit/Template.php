<?php
/**
 * Model_Admin_Page_Edit_Template
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Admin\Page\Edit
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Form\Field,
    GetIt\Admin\Page\EditPage;

/**
 * Edit newsletter
 */
class Model_Admin_Page_Edit_Template extends EditPage
{
    /**
     * Page title
     *
     * @var  string
     */
    protected $_title = 'Edit Template';

    /**
     * Action on submit
     *
     * @var string
     */
    protected $_action = '/save/template';

    /**
     * Action on cancel
     *
     * @var string
     */
    protected $_cancel = '/templates';

    /**
     * Get list of fields for the edit
     *
     * @return array:GetIt\Admin\Form\Field
     */
    public function getFields()
    {
        $fields = array();

        $field = new Field();
        $field->setProperty('tmplBcc');
        $field->setCaption('BCC');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('tmplBody');
        $field->setCaption('Body');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXTAREA);
        $field->setStyle('width: 55%; height: 415px;');
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('tmplBodyHtml');
        $field->setCaption('Html Body');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXTAREA);
        $field->setStyle('width: 55%; height: 415px;');
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('tmplSubject');
        $field->setCaption('Subject Template');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('tmplfromName');
        $field->setCaption('Sender Name');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('tmplfromEmail');
        $field->setCaption('Sender Email');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('tmplReplyToName');
        $field->setCaption('Reply To Name');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('tmplReplyToEmail');
        $field->setCaption('Reply To Email');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('tmplId');
        $field->setDataType(Field::DATA_TYPE_INT);
        $field->setFieldType(Field::FIELD_TYPE_HIDDEN);
        $fields[] = $field;

        return $fields;
    } // getFields
}
