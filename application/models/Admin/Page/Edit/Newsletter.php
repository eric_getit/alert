<?php
/**
 * Model_Admin_Page_Edit_Newsletter
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Admin\Page\Edit
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Form\Field,
    GetIt\Admin\Page\EditPage;

/**
 * Edit newsletter
 */
class Model_Admin_Page_Edit_Newsletter extends EditPage
{
    /**
     * Page title
     *
     * @var  string
     */
    protected $_title = 'Edit Newsletter';

    /**
     * Action on submit
     *
     * @var string
     */
    protected $_action = '/save/newsletter';

    /**
     * Action on cancel
     *
     * @var string
     */
    protected $_cancel = '/newsletters';

    /**
     * Get list of fields for the edit
     *
     * @return array:GetIt\Admin\Form\Field
     */
    public function getFields()
    {
        $fields = array();

        $field = new Field();
        $field->setProperty('newsletterName');
        $field->setCaption('Name');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('domain');
        $field->setCaption('Domain');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('siteName');
        $field->setCaption('Site Name');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('fromName');
        $field->setCaption('From Name');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('fromEmail');
        $field->setCaption('From Email');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_TEXT);
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('newsletterId');
        $field->setDataType(Field::DATA_TYPE_INT);
        $field->setFieldType(Field::FIELD_TYPE_HIDDEN);
        $fields[] = $field;

        return $fields;
    } // getFields
}
