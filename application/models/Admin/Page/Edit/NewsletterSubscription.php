<?php
/**
 * Model_Admin_Page_Edit_NewsletterSubscription
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Admin\Page\Edit
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Form\Field,
    GetIt\Admin\Page\EditPage;

/**
 * Edit newsletter
 */
class Model_Admin_Page_Edit_NewsletterSubscription extends EditPage
{
    /**
     * Page title
     *
     * @var  string
     */
    protected $_title = 'Edit Newsletter Subscription';

    /**
     * Action on submit
     *
     * @var string
     */
    protected $_action = '/save/newsletter-subscription';

    /**
     * Get list of fields for the edit
     *
     * @return array:GetIt\Admin\Form\Field
     */
    public function getFields()
    {
        $fields = array();

        $field = new Field();
        $field->setProperty('subscriptionStatus');
        $field->setCaption('Status');
        $field->setDataType(Field::DATA_TYPE_STRING);
        $field->setFieldType(Field::FIELD_TYPE_ENUM);
        $field->setEnumMap(array(
                    Model_Newsletter_Subscription::STATUS_ACTIVE => 'Active',
                    Model_Newsletter_Subscription::STATUS_DEACTIVE => 'Deactivated',
        ));
        $fields[] = $field;

        $field = new Field();
        $field->setProperty('subscriptionId');
        $field->setDataType(Field::DATA_TYPE_INT);
        $field->setFieldType(Field::FIELD_TYPE_HIDDEN);
        $fields[] = $field;

        return $fields;
    } // getFields
}
