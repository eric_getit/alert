<?php
/**
 * Model_Admin_Page_Template
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Admin\Page
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Grid\Column,
    GetIt\Admin\Page\GridPage;

/**
 * Admin page to manage templates
 */
class Model_Admin_Page_Template extends GridPage
{
    /**
     * Page title
     *
     * @var  string
     */
    protected $_title = 'Templates';

    /**
     * Get list of columns for the grid
     *
     * @return array:GetIt\Admin\Grid\Item
     */
    protected function _getColumns()
    {
        $columns = array();

        $column = new Column();
        $column->setProperty('TmplSubject');
        $column->setSortingProperty('tmpl_subject');
        $column->setCaption('Template Subject');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new \GetIt\Admin\Grid\Column();
        $column->setProperty('TmplFromName');
        $column->setSortingProperty('tmpl_fromName');
        $column->setCaption('Sender Name');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new \GetIt\Admin\Grid\Column();
        $column->setProperty('TmplFromEmail');
        $column->setSortingProperty('tmpl_fromEmail');
        $column->setCaption('Sender Email');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('Actions');
        $column->setCaption('Actions');
        $column->setType(Column::TYPE_ACTIONS);
        $column->setSortable(false);
        $columns[] = $column;

        return $columns;
    } // getColumns

    protected function _getStore()
    {
        $store = new Model_Admin_Store_Template();
        return $store;
    } // getStore
}
