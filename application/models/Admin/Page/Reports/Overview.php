<?php
/**
 * Model_Admin_Page_Reports_Overview
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Page\Admin\Reports
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Grid\Column,
    GetIt\Admin\Grid\Grid,
    GetIt\Admin\Page\GridPage;


/**
 * Page model for overview report.
 */
class Model_Admin_Page_Reports_Overview extends GridPage
{
    /**
     * Page title
     * @var  string
     */
    protected $_title = 'Overview';

    /**
     * Get columns for the grid
     *
     * @return multitype:Column
     */
    protected function _getColumns()
    {
        $columns = array();

        $column = new Column();
        $column->setProperty('reportDate');
        $column->setCaption('Date');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('sentEmails');
        $column->setCaption('Emails Sent');
        $column->setComment('Number of email messages sent that day.');
        $column->setType(Column::TYPE_INT);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('newAlerts');
        $column->setCaption('New Alerts');
        $column->setComment('Number of new alerts registered that day.');
        $column->setType(Column::TYPE_INT);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('totalAlerts');
        $column->setCaption('Total Alerts');
        $column->setComment('Number of alerts at the end of the day.');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(false);
        $columns[] = $column;


        $column = new Column();
        $column->setProperty('activeAlerts');
        $column->setCaption('Active Alerts');
        $column->setComment('Number of active (eligible for sending) alerts at the end of the day.');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('newSubscribers');
        $column->setCaption('New Subscribers');
        $column->setComment('Number of new subscribers registered that day.');
        $column->setType(Column::TYPE_INT);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('activeSubscribers');
        $column->setCaption('Total Subscribers');
        $column->setComment('Number of active subscribers at the end of the day.');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('organicLinks');
        $column->setCaption('Organic Links');
        $column->setComment('Number of organic links in all emails send that day.');
        $column->setType(Column::TYPE_INT);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('backfillLinks');
        $column->setCaption('Backfill Links');
        $column->setComment('Number of backfill links in all emails sent that day.');
        $column->setType(Column::TYPE_INT);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('totalLinks');
        $column->setCaption('Total Links');
        $column->setComment('Total number of links in all emails sent that day.');
        $column->setType(Column::TYPE_INT);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('organicClicks');
        $column->setCaption('Organic Clicks');
        $column->setComment('Number of clicks on organic URLs done that day.');
        $column->setType(Column::TYPE_INT);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('backfillClicks');
        $column->setCaption('Backfill Clicks');
        $column->setComment('Number of click on backfill URLs done that day.');
        $column->setType(Column::TYPE_INT);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('totalClicks');
        $column->setCaption('Total Clicks');
        $column->setComment('Total number of clicks done that day.');
        $column->setType(Column::TYPE_INT);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('messageViews');
        $column->setCaption('Message Views');
        $column->setComment('Number of email messages viewed that day.');
        $column->setType(Column::TYPE_INT);
        $column->setSortable(false);
        $columns[] = $column;

        return $columns;
    } // getColumns

    /**
     * Get store for the grid
     *
     * @return GetIt\Admin\Grid\Store
     */
    protected function _getStore()
    {
        $store = new Model_Admin_Store_OverviewReport();
        return $store;
    } // getStore

    /**
     * Apply custom grid settings
     *
     * @param Grid $grid
     *
     * @return void
     */
    protected function _initGrid($grid)
    {
        $grid->setDateRangeFilterEnabled(true);
    } // _initGrid
}
