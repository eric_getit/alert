<?php
/**
 * Model_Admin_Page_Reports_OverviewOld
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Page\Admin\Reports
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Grid\Column,
    GetIt\Admin\Grid\Grid,
    GetIt\Admin\Page\GridPage;

/**
 * Old overview report
 */
class Model_Admin_Page_Reports_OverviewOld extends GridPage
{
    /**
     * Page title
     * @var  string
     */
    protected $_title = 'Overview Old';

    /**
     * Get report data
     *
     * @return array
     */
    public function getReportData()
    {
        $filter = $this->getParams();
        if ($filter['from']) {
            $from = $filter['from']->format("Y-m-d H:i:s");
        } else {
            $from = NULL;
        }
        if ($filter['to']) {
            $to = $filter['to']->format("Y-m-d H:i:s");
        } else {
            $to = NULL;
        }
        return Model_Admin_Reports_OverviewOld::run($from, $to);
    } // getReportData
    /**
     * Get columns for the grid
     *
     * @return multitype:Column
     */
    protected function _getColumns()
    {
        $columns = array();

        $column = new Column();
        $column->setProperty('reportDate');
        $column->setCaption('Date');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('sentEmails');
        $column->setCaption('Emails Sent');
        $column->setType(Column::TYPE_INT);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('newAlerts');
        $column->setCaption('New Alerts');
        $column->setComment('Includes Active, Pending, Unsubscribes, etc.');
        $column->setType(Column::TYPE_INT);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('activeAlerts');
        $column->setCaption('Total Alerts');
        $column->setType(Column::TYPE_INT);
        $column->setSortable(false);
        $columns[] = $column;


        $column = new Column();
        $column->setProperty('newSubscribers');
        $column->setCaption('New Subscribers');
        $column->setType(Column::TYPE_INT);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('activeSubscribers');
        $column->setCaption('Total Subscribers');
        $column->setType(Column::TYPE_INT);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('organicLinks');
        $column->setCaption('Organic Links');
        $column->setType(Column::TYPE_INT);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('backfillLinks');
        $column->setCaption('Backfill Links');
        $column->setType(Column::TYPE_INT);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('totalLinks');
        $column->setCaption('Total Links');
        $column->setType(Column::TYPE_INT);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('organicClicks');
        $column->setCaption('Organic Clicks');
        $column->setType(Column::TYPE_INT);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('backfillClicks');
        $column->setCaption('Backfill Clicks');
        $column->setType(Column::TYPE_INT);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('totalClicks');
        $column->setCaption('Total Clicks');
        $column->setType(Column::TYPE_INT);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('messageViews');
        $column->setCaption('Message Views');
        $column->setType(Column::TYPE_INT);
        $column->setSortable(false);
        $columns[] = $column;

        return $columns;
    } // getColumns

    /**
     * Get store for the grid
     *
     * @return GetIt\Admin\Grid\Store
     */
    protected function _getStore()
    {
        $store = new Model_Admin_Store_OverviewOldReport();
        return $store;
    } // getStore

    /**
     * Apply custom grid settings
     *
     * @param Grid $grid
     *
     * @return void
     */
    protected function _initGrid($grid)
    {
        $grid->setDateRangeFilterEnabled(true);
    } // _initGrid
}