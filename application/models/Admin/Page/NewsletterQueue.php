<?php
/**
 * Model_Admin_Page_NewsletterQueue
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Page\Admin
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Grid\Column,
    GetIt\Admin\Page\GridPage;

/**
 * Manage newsletters sending queue
 * @author Eugene Kuaznetsov <eugene@soft-rose.com>
 */
class Model_Admin_Page_NewsletterQueue extends GridPage
{
    /**
     * Page title
     * @var  string
     */
    protected $_title = 'Newsletters Queue';

    /**
     * Get list of columns for the grid
     *
     * @return array:GetIt\Admin\Grid\Item
     */
    protected function _getColumns()
    {
        $columns = array();

        $column = new Column();
        $column->setProperty('QueueStatus');
        $column->setSortingProperty('queueStatus');
        $column->setCaption('Status');
        $column->setType(Column::TYPE_ENUM);
        $column->setSortable(true);
        $column->setEnumMap(array(
                    Model_Newsletter_Queue::STATUS_NEW => 'New',
                    Model_Newsletter_Queue::STATUS_PREPARED => 'Prepared',
        ));
        $columns[] = $column;

        $column = new \GetIt\Admin\Grid\Column();
        $column->setProperty('Subject');
        $column->setSortingProperty('subject');
        $column->setCaption('Subject');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new \GetIt\Admin\Grid\Column();
        $column->setProperty('Message');
        $column->setSortingProperty('message');
        $column->setCaption('Message');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new \GetIt\Admin\Grid\Column();
        $column->setProperty('FromName');
        $column->setSortingProperty('fromName');
        $column->setCaption('From Name');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new \GetIt\Admin\Grid\Column();
        $column->setProperty('FromEmail');
        $column->setSortingProperty('fromEmail');
        $column->setCaption('From Email');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new \GetIt\Admin\Grid\Column();
        $column->setProperty('SendTime');
        $column->setSortingProperty('sendTime');
        $column->setCaption('Send Time');
        $column->setType(Column::TYPE_DATE);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new \GetIt\Admin\Grid\Column();
        $column->setProperty('CreatedDate');
        $column->setSortingProperty('createdDate');
        $column->setCaption('Created');
        $column->setType(Column::TYPE_DATE);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new \GetIt\Admin\Grid\Column();
        $column->setProperty('UpdatedDate');
        $column->setSortingProperty('updatedDate');
        $column->setCaption('Updated');
        $column->setType(Column::TYPE_DATE);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('Actions');
        $column->setCaption('Actions');
        $column->setType(Column::TYPE_ACTIONS);
        $column->setSortable(false);
        $columns[] = $column;

        return $columns;
    } // getColumns

    protected function _getStore()
    {
        $store = new Model_Admin_Store_NewsletterQueue();
        $store->setSorters(array('property' => 'createdDate', 'direction' => 'DESC'));
        return $store;
    } // getStore
}
