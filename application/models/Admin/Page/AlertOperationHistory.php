<?php
/**
 * Model_Admin_Page_AlertOperationHistory
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Page\Admin
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Grid\Column,
    GetIt\Admin\Grid\Grid,
    GetIt\Admin\Grid\DropDownFilter,
    GetIt\Admin\Page\GridPage;
/**
 * Alert operations
 */
class Model_Admin_Page_AlertOperationHistory extends GridPage
{
    /**
     * Page title
     * @var string
     */
    protected $_title = 'Alert Operations History';

    /**
     * Show history for this alert
     *
     * @var Model_Alert
     */
    protected $_alert;

    /**
     * Get alert
     *
     * @return Model_Alert
     */
    public function getAlert()
    {
        return $this->_alert;
    } // getAlert

    /**
     * Set alert
     *
     * @param Model_Alert $alert
     *
     * @return Model_Admin_Page_AlertOperationHistory
     */
    public function setAlert($alert)
    {
        $this->_alert = $alert;

        $contactId = $alert->getContactId();
        $breadcrumbs = array(
                'Contacts' => '/contacts',
                'Alerts' => '/alerts?contactId=' . $contactId,
                'Alert Operations History' => '',
        );
        $this->setBreadcrumbs($breadcrumbs);

        return $this;
    } // setAlert

    /**
     * Get list of columns for the grid
     *
     * @return array:GetIt\Admin\Grid\Item
     */
    protected function _getColumns()
    {
        $columns = array();

        $column = new \GetIt\Admin\Grid\Column();
        $column->setProperty('operation');
        $column->setCaption('Operation');
        $column->setType(Column::TYPE_ENUM);
        $column->setSortable(false);
        $column->setEnumMap(array(
                Model_AlertOperationHistory::OPERATION_CREATE => 'Created',
                Model_AlertOperationHistory::OPERATION_UNSUBSCRIBE_USER => 'Unsubscribed by User',
                Model_AlertOperationHistory::OPERATION_UNSUBSCRIBE_API => 'Unsubscribed by API',
                Model_AlertOperationHistory::OPERATION_UNSUBSCRIBE_ADMIN => 'Unsubscribed by Admin',
                Model_AlertOperationHistory::OPERATION_SUSPEND_API => 'Suspended by API',
                Model_AlertOperationHistory::OPERATION_SUSPEND_ADMIN => 'Suspended by Admin',
                Model_AlertOperationHistory::OPERATION_UPDATED_USER => 'Updated by User',
                Model_AlertOperationHistory::OPERATION_UPDATED_API => 'Updated by API',
                Model_AlertOperationHistory::OPERATION_UPDATED_ADMIN => 'Updated by Admin',
                Model_AlertOperationHistory::OPERATION_RESTORED_USER => 'Restored by User',
                Model_AlertOperationHistory::OPERATION_RESTORED_API => 'Restored by API',
                Model_AlertOperationHistory::OPERATION_RESTORED_ADMIN => 'Restored by Admin',
        ));
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('ip');
        $column->setCaption('IP Address');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(false);
        $columns[] = $column;

        $column = new \GetIt\Admin\Grid\Column();
        $column->setProperty('createdDate');
        $column->setCaption('Date');
        $column->setType(Column::TYPE_DATE);
        $column->setSortable(false);
        $columns[] = $column;
        return $columns;
    } // getColumns

    protected function _getStore()
    {
        $store = new Model_Admin_Store_AlertsOperationsHistory();
        return $store;
    } // getStore

    /**
     * Apply custom grid settings
     *
     * @param Grid $grid
     *
     * @return void
     */
    protected function _initGrid($grid)
    {
        $dropDownFilters = array();

        $optionsFilter = new DropDownFilter($grid->getStore());
        $optionsFilter->setLabel('Operation');
        $optionsFilter->setProperty('operation');
        $optionsFilter->setDropDownList(array(
                '' => 'All Operations',
                Model_AlertOperationHistory::OPERATION_CREATE => 'Created',
                Model_AlertOperationHistory::OPERATION_UNSUBSCRIBE_USER => 'Unsubscribed by User',
                Model_AlertOperationHistory::OPERATION_UNSUBSCRIBE_API => 'Unsubscribed by API',
                Model_AlertOperationHistory::OPERATION_UNSUBSCRIBE_ADMIN => 'Unsubscribed by Admin',
                Model_AlertOperationHistory::OPERATION_SUSPEND_API => 'Suspended by API',
                Model_AlertOperationHistory::OPERATION_SUSPEND_ADMIN => 'Suspended by Admin',
                Model_AlertOperationHistory::OPERATION_UPDATED_USER => 'Updated by User',
                Model_AlertOperationHistory::OPERATION_UPDATED_API => 'Updated by API',
                Model_AlertOperationHistory::OPERATION_UPDATED_ADMIN => 'Updated by Admin',
                Model_AlertOperationHistory::OPERATION_RESTORED_USER => 'Restored by User',
                Model_AlertOperationHistory::OPERATION_RESTORED_API => 'Restored by API',
                Model_AlertOperationHistory::OPERATION_RESTORED_ADMIN => 'Restored by Admin',
        ));
        $dropDownFilters[] = $optionsFilter;

        $grid->setDropDownFilters($dropDownFilters);
    } // _initGrid
}
