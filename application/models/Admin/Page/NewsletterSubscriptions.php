<?php
/**
 * Model_Admin_Page_NewsletterSubscriptions
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Admin\Page
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Grid\Column,
    GetIt\Admin\Page\GridPage;

/**
 *
 * @author Eugene Kuanetsov
 *
 */
class Model_Admin_Page_NewsletterSubscriptions extends GridPage
{
    /**
     *
     * @var  string
     */
    protected $_title = 'Newsletter Subscriptions';

    /**
     * Get list of columns for the grid
     *
     * @return array:GetIt\Admin\Grid\Item
     */
    protected function _getColumns()
    {
        $columns = array();

        $column = new Column();
        $column->setProperty('NewsletterName');
        $column->setSortingProperty('newsletterName');
        $column->setCaption('Newsletter');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new \GetIt\Admin\Grid\Column();
        $column->setProperty('Domain');
        $column->setSortingProperty('domain');
        $column->setCaption('Domain');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new \GetIt\Admin\Grid\Column();
        $column->setProperty('SubscriptionStatus');
        $column->setSortingProperty('subscriptionStatus');
        $column->setCaption('Status');
        $column->setType(Column::TYPE_ENUM);
        $column->setSortable(true);
        $column->setEnumMap(array(
            Model_Newsletter_Subscription::STATUS_ACTIVE => 'Active',
            Model_Newsletter_Subscription::STATUS_DEACTIVE => 'Deactivated',
        ));
        $columns[] = $column;

        $column = new \GetIt\Admin\Grid\Column();
        $column->setProperty('ContactEmail');
        $column->setSortingProperty('contactEmail');
        $column->setCaption('Contact');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new \GetIt\Admin\Grid\Column();
        $column->setProperty('CreatedDate');
        $column->setSortingProperty('createdDate');
        $column->setCaption('Created');
        $column->setType(Column::TYPE_DATE);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new \GetIt\Admin\Grid\Column();
        $column->setProperty('UpdatedDate');
        $column->setSortingProperty('updatedDate');
        $column->setCaption('Updated');
        $column->setType(Column::TYPE_DATE);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('Actions');
        $column->setCaption('Actions');
        $column->setType(Column::TYPE_ACTIONS);
        $column->setSortable(false);
        $columns[] = $column;

        return $columns;
    } // getColumns

    protected function _getStore()
    {
        $store = new Model_Admin_Store_NewsletterSubscriptions();
        return $store;
    } // getStore

}