<?php
/**
 * Model_Admin_Page_MessageDetails
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Page\Admin
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Page\Page;

/**
 * Message details page
 */
class Model_Admin_Page_MessageDetails extends Page
{
    /**
     * Page template
     *
     * @var string
     */
    protected $_twigTemplateName = 'admin/messageDetails.html.twig';

    /**
     * Page title
     *
     * @var  string
     */
    protected $_title = 'Message Details';

    /**
     * Message Id
     *
     * @var integet
     */
    protected $_messageId;

    /**
     * Set message Id
     *
     * @param integer $messageId
     *
     * @return Model_Admin_Page_MessageDetails
     */
    public function setMessageId($messageId)
    {
        $this->_messageId = $messageId;
        return $this;
    } // setMessageId

    /**
     * Initialise view
     *
     * @see Model_Page::initView()
     *
     * @return void
     */
    protected function _initTwigContext()
    {
        parent::_initTwigContext();

        $message = new Model_MessageArchive($this->_messageId);
        $details = $message->getMessageDetailData();
        $bodyIframe = $details['Body HTML'];
        unset($details['Body HTML']);

        $this->_twigContext['message'] = $details;
        $this->_twigContext['bodyIframe'] = $bodyIframe;
    } // _initTwigContext
}
