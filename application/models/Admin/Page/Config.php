<?php
/**
 * Model_Admin_Page_Config
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Page\Admin
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Page\Page;


/**
 * Show/modify configuration
 */
class Model_Admin_Page_Config extends Page
{
    /**
     * Template file name
     *
     * @var string
     */
    protected $_twigTemplateName = 'admin/config.html.twig';

    /**
     * Configuration section name
     *
     * @var string
     */
    protected $_sectionName;

    /**
     * Get configuration section name
     *
     * @return string
     */
    public function getSectionName()
    {
        return $this->_sectionName;
    } // getSectionName

    /**
     * Set section name
     *
     * @param string $sectionName
     *
     * @return Model_Admin_Page_Config
     */
    public function setSectionName($sectionName)
    {
        $this->_sectionName = $sectionName;
        $this->setTitle($sectionName);
        $breadcrumbs = array(
                'Configuration' => '',
                $sectionName => '',
        );
        $this->setBreadcrumbs($breadcrumbs);

        return $this;
    } // setSectionName


    /**
     * Initialise view
     *
     * @see Model_Page::initView()
     *
     * @return void
     */
    protected function _initTwigContext()
    {
        parent::_initTwigContext();

        $configSection = $this->getSectionName();
        $configItems = $this->_loadConfigItems($configSection);

        $this->_twigContext['configSection'] = $configSection;
        $this->_twigContext['configItems'] = $configItems;
    } // _initTwigContext

    private function _loadConfigItems($configSection)
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $select = $dbh->select();
        $select->from('config_db');
        $where = 'displayGroupId = (SELECT id FROM config_group WHERE displayName = ?)';
        $select->where($where, $configSection);
        $rows = $select->query()->fetchAll(Zend_Db::FETCH_ASSOC);

        //Zend_Debug::dump($rows); die();

        $result = array();
        foreach ($rows as $row) {
            $result[] = new Model_ConfigDb($row);
        }

        return $result;
    } // _loadConfigItems
}
