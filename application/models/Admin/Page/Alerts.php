<?php
/**
 * Model_Admin_Page_Alerts
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Page\Admin
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */
use GetIt\Admin\Grid\Column,
    GetIt\Admin\Page\GridPage;

/**
 * Alerts grid page
 */
class Model_Admin_Page_Alerts extends GridPage
{
    /**
     * Show alerts for this contact
     *
     * @var Model_Contact
     */
    protected $_contact;

    /**
     * Get contact
     *
     * @return Model_Contact
     */
    public function getContact()
    {
        return $this->_contact;
    } // getContact

    /**
     * Set contact and update title and breadcrumbs
     *
     * @param Model_Contact $contact
     *
     * @return Model_Admin_Page_Alerts
     */
    public function setContact($contact)
    {
        $this->_contact = $contact;
        $title = 'Alerts for ' . $contact->getEmail();
        $this->setTitle($title);

        $breadcrumbs = array(
                'Contacts' => '/contacts',
                $title => '',
        );
        $this->setBreadcrumbs($breadcrumbs);

        return $this;
    } // setContact

    /**
     * Get columns for the grid
     *
     * @return multitype:Column
     */
    protected function _getColumns()
    {
        $columns = array();

        $column = new Column();
        $column->setProperty('alertStatus');
        $column->setCaption('Status');
        $column->setType(Column::TYPE_ENUM);
        $column->setSortable(true);
        $column->setEnumMap(array(
            Model_Alert::STATUS_ACTIVE => 'Active',
            Model_Alert::STATUS_DEACTIVE => 'Deactivated',
            Model_Alert::STATUS_PENDING => 'Pending Multi Alert',
            Model_Alert::STATUS_ADM_PENDING => 'Admin Pending',
            Model_Alert::STATUS_SUSPENDED_AS_BOUNCE_PERMANENT => 'Permanent Bounce',
            Model_Alert::STATUS_SUSPENDED_AS_BOUNCE_TRANSIENT => 'Transient Bounce',
            Model_Alert::STATUS_SUSPENDED_AS_COMPLAINT => 'SPAM Complaint',
            Model_Alert::STATUS_UNSUBSCRIBED_ADMIN => 'Unsubscribed by admin',
            Model_Alert::STATUS_UNSUBSCRIBED_API => 'Unsubscribed via API',
            Model_Alert::STATUS_UNSUBSCRIBED_USER => 'Unsubscribed',
        ));
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('siteDomain');
        $column->setCaption('Site');
        $column->setType(Column::TYPE_STRING);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('alertType');
        $column->setCaption('Type');
        $column->setType(Column::TYPE_ENUM);
        $column->setSortable(true);
        $column->setEnumMap(array(
            'DAILY' => 'Daily',
            'WEEKLY' => 'Weekly'
        ));
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('queryTerms');
        $column->setCaption('Query');
        $column->setType(Column::TYPE_RAW);
        $column->setSortable(true);
        $columns[] = $column;
        
        $column = new Column();
        $column->setProperty('location');
        $column->setCaption('Location');
        $column->setType(Column::TYPE_RAW);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('isLocationValid');
        $column->setCaption('Location Valid');
        $column->setType(Column::TYPE_BOOL);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('lastProcessDate');
        $column->setCaption('Processed');
        $column->setType(Column::TYPE_DATE);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('lastSendDate');
        $column->setCaption('Sent');
        $column->setType(Column::TYPE_DATE);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('createdDate');
        $column->setCaption('Created');
        $column->setType(Column::TYPE_DATE);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('updatedDate');
        $column->setCaption('Edited');
        $column->setType(Column::TYPE_DATE);
        $column->setSortable(true);
        $columns[] = $column;

        $column = new Column();
        $column->setProperty('Actions');
        $column->setCaption('Actions');
        $column->setType(Column::TYPE_ACTIONS);
        $column->setSortable(false);
        $columns[] = $column;

        return $columns;
    } // getColumns

    /**
     * Get store for the grid
     *
     * @return GetIt\Admin\Grid\Store
     */
    protected function _getStore()
    {
        $contact = $this->getContact();

        $store = new Model_Admin_Store_Alerts();
        $store->setFilters(array('contactId' => $contact->getContactId()));
        $store->setSorters(array('property' => 'createdDate', 'direction' => 'DESC'));
        return $store;
    } // getStore
}
