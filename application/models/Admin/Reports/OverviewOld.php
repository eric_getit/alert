<?php
/**
 * Model_Admin_Reports_OverviewOld
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Page\Admin\Reports
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Generate data for old overview report
 */
class Model_Admin_Reports_OverviewOld extends Model_AbstractModel
{
    /**
     * Primary key field
     *
     * @var int
     */
    protected $_reportGeneralId;

    /**
     * Report date
     *
     * @var string
     */
    protected $_reportDate;

    /**
     * Number of emails sent
     *
     * @var int
     */
    protected $_sentEmails = 0;

    /**
     * Number of new alerts
     *
     * @var int
     */
    protected $_newAlerts = 0;

    /**
     * Number of active alerts
     *
     * @var int
     */
    protected $_activeAlerts = 0;

    /**
     * Number of new subscriber
     *
     * @var int
     */
    protected $_newSubscribers = 0;

    /**
     * Number of active subscribers
     *
     * @var int
     */
    protected $_activeSubscribers = 0;

    /**
     * Number of organic links
     *
     * @var int
     */
    protected $_organicLinks = 0;

    /**
     * Number of backfill links
     *
     * @var int
     */
    protected $_backfillLinks = 0;

    /**
     * Number of times HTML message was shown (based on transparent GIF loads)
     *
     * @var int
     */
    protected $_messageViews = 0;

    /**
     * Number of clicks on organic links
     *
     * @var int
     */
    protected $_organicClicks = 0;

    /**
     * Number of backfill link clicks
     *
     * @var int
     */
    protected $_backfillClicks = 0;

    /**
     * Data base table name for the model
     *
     * @var string
     */
    protected $_tableName = 'report_general';

    /**
     * Primary key field name
     *
     * @var string
     */
    protected $_primaryKey = 'reportGeneralId';

    /**
     * Data base fields
     *
     * @var array
     */
    protected $_tableFields = array(
            'reportGeneralId' => '_reportGeneralId',
            'reportDate' => '_reportDate',
            'sentEmails' => '_sentEmails',
            'newAlerts' => '_newAlerts',
            'activeAlerts' => '_activeAlerts',
            'newSubscribers' => '_newSubscribers',
            'activeSubscribers' => '_activeSubscribers',
            'organicLinks' => '_organicLinks',
            'backfillLinks' => '_backfillLinks',
            'organicClicks' => '_organicClicks',
            'backfillClicks' => '_backfillClicks',
            'messageViews' => '_messageViews',
            );

    /**
     * @return the $_reportGeneralId
     */
    public function getReportGeneralId()
    {
        return $this->_reportGeneralId;
    } // getReportGeneralId

    /**
     * @return the $_reportDate
     */
    public function getReportDate()
    {
        return $this->_reportDate;
    } // getReportDate

    /**
     * Get start date for the row
     * @return string
     */
    public function getReportDateFrom()
    {
        return $this->_reportDate . ' 00:00:00';
    } // getReportDateFrom

    /**
     * Get end date for the report row
     * @return string
     */
    public function getReportDateTo()
    {
        return $this->_reportDate . ' 23:59:59';
    } // getReportDateTo

    /**
     * @return the $_sentEmails
     */
    public function getSentEmails()
    {
        return $this->_sentEmails;
    } // getSentEmails

    /**
     * @return the $_newAlerts
     */
    public function getNewAlerts()
    {
        return $this->_newAlerts;
    } // getNewAlerts

    /**
     * @return the $_activeAlerts
     */
    public function getActiveAlerts()
    {
        return $this->_activeAlerts;
    } // getActiveAlerts

    /**
     * @return the $_newSubscribers
     */
    public function getNewSubscribers()
    {
        return $this->_newSubscribers;
    } // getNewSubscribers

    /**
     * @return the $_activeSubscribers
     */
    public function getActiveSubscribers()
    {
        return $this->_activeSubscribers;
    } // getActiveSubscribers

    /**
     * @return the $_organicLinks
     */
    public function getOrganicLinks()
    {
        return $this->_organicLinks;
    } // getOrganicLinks

    /**
     * @return the $_backfillLinks
     */
    public function getBackfillLinks()
    {
        return $this->_backfillLinks;
    } // getBackfillLinks

    /**
     * Get number of messages views based on transparent GIF image loads
     * @return int
     */
    public function getMessageViews()
    {
        return $this->_messageViews;
    } // getMessageViews

    /**
     * @return the $_organicClicks
     */
    public function getOrganicClicks()
    {
        return $this->_organicClicks;
    } // getOrganicClicks

    /**
     * @return the $_backfillClicks
     */
    public function getBackfillClicks()
    {
        return $this->_backfillClicks;
    } // getBackfillClicks

    /**
     * @param string $_reportDate
     */
    public function setReportDate($reportDate)
    {
        $this->_reportDate = $reportDate;
    } // setReportDate

    /**
     * @param number $_sentEmails
     */
    public function setSentEmails($sentEmails)
    {
        $this->_sentEmails = $sentEmails;
    } // setSentEmails

    /**
     * @param number $_newAlerts
     */
    public function setNewAlerts($newAlerts)
    {
        $this->_newAlerts = $newAlerts;
    } // setNewAlerts

    /**
     * @param number $_activeAlerts
     */
    public function setActiveAlerts($activeAlerts)
    {
        $this->_activeAlerts = $activeAlerts;
    } // setActiveAlerts

    /**
     * @param number $_newSubscribers
     */
    public function setNewSubscribers($newSubscribers)
    {
        $this->_newSubscribers = $newSubscribers;
    } // setNewSubscribers

    /**
     * @param number $_activeSubscribers
     */
    public function setActiveSubscribers($activeSubscribers)
    {
        $this->_activeSubscribers = $activeSubscribers;
    } // setActiveSubscribers

    /**
     * @param number $_organicLinks
     */
    public function setOrganicLinks($organicLinks)
    {
        $this->_organicLinks = $organicLinks;
    } // setOrganicLinks

    /**
     * @param number $_backfillLinks
     */
    public function setBackfillLinks($backfillLinks)
    {
        $this->_backfillLinks = $backfillLinks;
    } // setBackfillLinks

    /**
     * Set number of message views
     * @param int $messageViews
     */
    public function setMessageViews($messageViews)
    {
        $this->_messageViews = $messageViews;
    } // setMessageViews

    /**
     * @param number $_organicClicks
     */
    public function setOrganicClicks($organicClicks)
    {
        $this->_organicClicks = $organicClicks;
    } // setOrganicClicks

    /**
     * @param number $_backfillClicks
     */
    public function setBackfillClicks($backfillClicks)
    {
        $this->_backfillClicks = $backfillClicks;
    } // setBackfillClicks

    public function calculateSentEmails()
    {
        $sql = 'select count(*) cnt
                from message_archive
                where status = "send_success"
                and sendDate between ? and ?';
        $this->setSentEmails($this->_getSqlResultsForDateRange($sql));
    } // calculateSentEmails

    /**
     * Calculate number of new alerts
     * @return void
     */
    public function calculateNewAlerts()
    {
        $sql = 'select count(*) cnt
                from alerts
                where createdDate between ? and ?';
        $this->setNewAlerts($this->_getSqlResultsForDateRange($sql));
    } // calculateNewAlerts

    /**
     * Calculate number of active alerts
     * @return void
     */
    public function calculateActiveAlerts()
    {
        $sql = 'select count(*) cnt
                from alerts
                where createdDate <= ?
                and alertStatus = "ACTIVE"';
        $this->setActiveAlerts($this->_getSqlResultsForDate($sql));
    } // calculateActiveAlerts

    /**
     * Calculate number of new subscribers
     */
    public function calculateNewSubscribers()
    {
        // NOTE Newly added contact should have at least one alert
        // to be counted as new subscriber
        $sql = 'select count(distinct contactId) cnt
                from contacts
                inner join alerts using (contactId)
                where contacts.createdDate between ? and ?';
        $this->setNewSubscribers($this->_getSqlResultsForDateRange($sql));
    } // calculateNewSubscribers

    /**
     * Calculate number of active subscribers
     * @return void
     */
    public function calculateActiveSubscibers()
    {
        $sql = 'select count(distinct contactId)
                from alerts
                where createdDate <= ?
                and alerts.alertStatus = "ACTIVE"';
        $this->setActiveSubscribers($this->_getSqlResultsForDate($sql));
    } // calculateActiveSubscibers

    /**
     * Caclulate organic and backfill links and message views
     * @return void
     */
    public function calculateLinks()
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $sql = 'select sum(organicLinks) cntOrganic, sum(backfillLinks) cntBackfill
                from message_archive
                where preparedDate between ? and ?';
        $bind = array();
        $bind[] = $this->getReportDateFrom();
        $bind[] = $this->getReportDateTo();
        $counts = $dbh->fetchRow($sql, $bind);
        if ($counts) {
            $this->setOrganicLinks($counts['cntOrganic']);
            $this->setBackfillLinks($counts['cntBackfill']);
        } else {
            $this->setOrganicLinks(0);
            $this->setBackfillLinks(0);
        }
    } // calculateOrganicLinks

    /**
     * Calculate messages view from the history table
     */
    public function calculateMessageViews()
    {

        $sql = 'select count(*) cnt
                from message_views
                where createdDate between ? and ?';
        $this->setMessageViews($this->_getSqlResultsForDateRange($sql));
    } // calculateMessageViews

    /**
     * Caclulate organic and backfill clicks
     * @return void
     */
    public function calculateClicks()
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $sql = 'select type, count(*) cnt
                from email_click
                where email_click.created between ? and ?
                group by type';
        $bind = array();
        $bind[] = $this->getReportDateFrom();
        $bind[] = $this->getReportDateTo();
        $clicks = $dbh->fetchPairs($sql, $bind);
        if (!empty($clicks['organic'])) {
            $this->setOrganicClicks($clicks['organic']);
        }
        if (!empty($clicks['backfill'])) {
            $this->setBackfillClicks($clicks['backfill']);
        }
    } // calculateClicks

    /**
     * Calculate all report numbers
     * @return void
     */
    public function calculateAll()
    {
        $this->calculateSentEmails();
        $this->calculateNewAlerts();
        $this->calculateActiveAlerts();
        $this->calculateNewSubscribers();
        $this->calculateActiveSubscibers();
        $this->calculateLinks();
        $this->calculateMessageViews();
        $this->calculateClicks();
    } // calculateAll

    /**
     * Populate report table with data for missing days
     */
    public static function build()
    {
        $fromDate = self::_getStartDate();

        $toDate = new DateTime();
        $toDate->setTime(0, 0, 0);
        $dayInterval = new DateInterval('P1D');
        $toDate->add($dayInterval);

        $reportRows = self::_initReport($fromDate, $toDate);
        foreach ($reportRows as $row) {
            /* @var $row Model_Admin_Reports_OverviewOld */
            $dbh = Zend_Db_Table::getDefaultAdapter();
            $sql = 'select reportGeneralId from report_general where reportDate = ?';
            $bind = array();
            $bind[] = $row->getReportDate();
            $checks = $dbh->fetchAll($sql, $bind);
            if (empty($checks)) {
                $row->insert();
            } else {
                $row->_reportGeneralId = $checks[0]['reportGeneralId'];
                $row->update();
            }
        }
    } // build

    /**
     * Run report and return results
     *
     * @return array
     */
    public static function run($from, $to)
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();

        $bind = array();
        $where = '';

        if ($from && $to) {
            $bind[] = $from;
            $bind[] = $to;
            $where = 'where reportDate between ? and ?';
        }
        $sql = 'select * from report_general ' . $where . ' order by reportDate desc';

        $report = array();
        $report['body'] = $dbh->fetchAll($sql, $bind);

        foreach ($report['body'] as &$row) {
            $row['totalLinks'] = $row['organicLinks'] + $row['backfillLinks'];
            $row['totalClicks'] = $row['organicClicks'] + $row['backfillClicks'];
        }

        return $report;
    } // run

    /**
     * Execute SQL with 2 parameters - from and to dates
     * @param string $sql
     * @return string
     */
    private function _getSqlResultsForDateRange($sql)
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $bind = array();
        $bind[] = $this->getReportDateFrom();
        $bind[] = $this->getReportDateTo();
        $result = $dbh->fetchOne($sql, $bind);
        return $result;
    } // _getSqlResultsForDateRange

    /**
     * Execute SQL with 1 parameters - to date
     * @param string $sql
     * @return string
     */
    private function _getSqlResultsForDate($sql)
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $bind = array();
        $bind[] = $this->getReportDateTo();
        $result = $dbh->fetchOne($sql, $bind);
        return $result;

    } // _getSqlResultsForDate

    /**
     * Get the first date for which we have not generated data
     * @throws Exception
     * @return Model_Admin_Reports_General
     */
    private static function _getStartDate()
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $sql = 'select max(reportDate) - interval 2 day from report_general';
        $newestDate = $dbh->fetchOne($sql);

        if (empty($newestDate)) {
            $sql = 'select min(date(createdDate)) from contacts';
            $newestDate = $dbh->fetchOne($sql);
        }

        $newestDate = new DateTime($newestDate);
        return $newestDate;
    } // _getStartDate

    private static function _initReport($fromDate, $toDate)
    {
        $dayInterval = new DateInterval('P1D');
        $report = array();

        while ($fromDate <= $toDate) {
            $day = $fromDate->format('Y-m-d');

            $row = new Model_Admin_Reports_OverviewOld();
            $row->setReportDate($day);
            $row->calculateAll();
            $report[] = $row;

            $fromDate->add($dayInterval);
        }

        return $report;
    } // _initReport
}
