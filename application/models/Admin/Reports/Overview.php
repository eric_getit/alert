<?php
/**
 * Model_Admin_Reports_Overview
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Reports
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Overview report
 *
 * @see http://jira.getitcorporate.com/browse/ALT-39
 */
class Model_Admin_Reports_Overview extends Model_Admin_Reports_TimeZoneReport
{
    /**
     * Data base table name for the model
     *
     * @var string
     */
    protected static $_tableName = 'report_overview';

    /**
     * Field by which we determine whether to insert or update the record
     *
     * @var string
     */
    protected static $_uniqueField = 'reportDate';

    /**
     * Get total alerts
     *
     * @return the integer
     */
    public function getTotalAlerts()
    {
        return $this->_rowData['totalAlerts'];
    }

    /**
     * @param number $_totalAlerts
     */
    public function setTotalAlerts($_totalAlerts)
    {
        $this->_rowData['totalAlerts'] = $_totalAlerts;
    }

    /**
     * @return the $_reportDate
     */
    public function getReportDate()
    {
        return $this->_rowData['reportDate'];
    } // getReportDate

    /**
     * @return the $_sentEmails
     */
    public function getSentEmails()
    {
        return $this->_rowData['sentEmails'];
    } // getSentEmails

    /**
     * @return the $_newAlerts
     */
    public function getNewAlerts()
    {
        return $this->_rowData['newAlerts'];
    } // getNewAlerts

    /**
     * @return the $_activeAlerts
     */
    public function getActiveAlerts()
    {
        return $this->_rowData['activeAlerts'];
    } // getActiveAlerts

    /**
     * @return the $_newSubscribers
     */
    public function getNewSubscribers()
    {
        return $this->_rowData['newSubscribers'];
    } // getNewSubscribers

    /**
     * @return the $_activeSubscribers
     */
    public function getActiveSubscribers()
    {
        return $this->_rowData['activeSubscribers'];
    } // getActiveSubscribers

    /**
     * @return the $_organicLinks
     */
    public function getOrganicLinks()
    {
        return $this->_rowData['organicLinks'];
    } // getOrganicLinks

    /**
     * @return the $_backfillLinks
     */
    public function getBackfillLinks()
    {
        return $this->_rowData['backfillLinks'];
    } // getBackfillLinks

    /**
     * Get number of messages views based on transparent GIF image loads
     * @return int
     */
    public function getMessageViews()
    {
        return $this->_rowData['messageViews'];
    } // getMessageViews

    /**
     * @return the $_organicClicks
     */
    public function getOrganicClicks()
    {
        return $this->_rowData['organicClicks'];
    } // getOrganicClicks

    /**
     * @return the $_backfillClicks
     */
    public function getBackfillClicks()
    {
        return $this->_rowData['backfillClicks'];
    } // getBackfillClicks

    /**
     * @param string $_reportDate
     */
    public function setReportDate($reportDate)
    {
        $this->_rowData['reportDate'] = $reportDate;
    } // setReportDate

    /**
     * @param number $_sentEmails
     */
    public function setSentEmails($sentEmails)
    {
        $this->_rowData['sentEmails'] = $sentEmails;
    } // setSentEmails

    /**
     * @param number $_newAlerts
     */
    public function setNewAlerts($newAlerts)
    {
        $this->_rowData['newAlerts'] = $newAlerts;
    } // setNewAlerts

    /**
     * @param number $_activeAlerts
     */
    public function setActiveAlerts($activeAlerts)
    {
        $this->_rowData['activeAlerts'] = $activeAlerts;
    } // setActiveAlerts

    /**
     * @param number $_newSubscribers
     */
    public function setNewSubscribers($newSubscribers)
    {
        $this->_rowData['newSubscribers'] = $newSubscribers;
    } // setNewSubscribers

    /**
     * @param number $_activeSubscribers
     */
    public function setActiveSubscribers($activeSubscribers)
    {
        $this->_rowData['activeSubscribers'] = $activeSubscribers;
    } // setActiveSubscribers

    /**
     * @param number $_organicLinks
     */
    public function setOrganicLinks($organicLinks)
    {
        $this->_rowData['organicLinks'] = $organicLinks;
    } // setOrganicLinks

    /**
     * @param number $_backfillLinks
     */
    public function setBackfillLinks($backfillLinks)
    {
        $this->_rowData['backfillLinks'] = $backfillLinks;
    } // setBackfillLinks

    /**
     * Set number of message views
     * @param int $messageViews
     */
    public function setMessageViews($messageViews)
    {
        $this->_rowData['messageViews'] = $messageViews;
    } // setMessageViews

    /**
     * @param number $_organicClicks
     */
    public function setOrganicClicks($organicClicks)
    {
        $this->_rowData['organicClicks'] = $organicClicks;
    } // setOrganicClicks

    /**
     * @param number $_backfillClicks
     */
    public function setBackfillClicks($backfillClicks)
    {
        $this->_rowData['backfillClicks'] = $backfillClicks;
    } // setBackfillClicks

    /**
     * Run report and return results
     *
     * @return array
     */
    public function run()
    {
        $convertTz = 'date(reportDate)';
        $convertTzJoin = 'r.reportDate';
        $where = '';
        $bind = array();

        if ($this->getStartDate() && $this->getEndDate()) {
            if (self::SQL_TIME_ZONE == $this->getStartDate()->getTimezone()->getName()) {
                $convertTz = 'date(reportDate)';
                $convertTzJoin = 'r.reportDate';
            } else {
                $convertTz = 'date(convert_tz(reportDate, "' . self::SQL_TIME_ZONE . '", '
                           . '"' . $this->getStartDate()->getTimezone()->getName() . '"))';
                $convertTzJoin = 'convert_tz(r.reportDate, "' . self::SQL_TIME_ZONE . '", '
                               . '"' . $this->getStartDate()->getTimezone()->getName() . '")';
            }
            $where = 'where reportDate between ? and ?';
            $bind[] = $this->getSqlStartDate();
            $bind[] = $this->getSqlEndDate();
        }

        $tableName = static::$_tableName;
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $sql = <<<SQL
select x.*, r.activeAlerts, r.activeSubscribers, r.totalAlerts
from (
    select {$convertTz} reportDate,
    sum(sentEmails) as sentEmails,
    sum(newAlerts) as newAlerts,
    sum(newSubscribers) as newSubscribers,
    sum(organicLinks) as organicLinks,
    sum(backfillLinks) as backfillLinks,
    sum(organicClicks) as organicClicks,
    sum(backfillClicks) as backfillClicks,
    sum(messageViews) as messageViews
    from {$tableName} {$where}
    group by {$convertTz}
) x
left join {$tableName} r on {$convertTzJoin} = x.reportDate + interval 23 hour
order by x.reportDate desc
SQL;

        $reportBody = $dbh->fetchAll($sql, $bind);

        $total = array(
                'sentEmails' => 0,
                'newAlerts' => 0,
                'newSubscribers' => 0,
                'organicLinks' => 0,
                'backfillLinks' => 0,
                'organicClicks' => 0,
                'backfillClicks' => 0,
                'messageViews' => 0,
                'totalClicks' => 0,
                'totalLinks' => 0
        );

        foreach ($reportBody as &$row) {
            $row['totalLinks'] = $row['organicLinks'] + $row['backfillLinks'];
            $row['totalClicks'] = $row['organicClicks'] + $row['backfillClicks'];

            foreach ($total as $totalName => &$totalRow) {
                $totalRow += $row[$totalName];
            }

            $reportDate = new DateTime($row['reportDate']);
            $row['reportDate'] = $reportDate->format('Y-m-d D');
            $row['activeAlerts'] = $row['activeAlerts'] == null ? 'N/A' : number_format($row['activeAlerts']);
            $row['activeSubscribers'] = $row['activeSubscribers'] == null ? 'N/A' : number_format($row['activeSubscribers']);
            $row['totalAlerts'] = $row['totalAlerts'] == null ? 'N/A' : number_format($row['totalAlerts']);
        }

        $rowsCount = count($reportBody);
        if ($rowsCount == 0) {
            $rowsCount = 42;
            // @see http://en.wikipedia.org/wiki/Answer_to_The_Ultimate_Question_of_Life,_the_Universe,_and_Everything#Answer_to_the_Ultimate_Question_of_Life.2C_the_Universe.2C_and_Everything_.2842.29
        }
        $average = array();
        $average['_type'] = 'footer';
        $average['reportDate'] = 'Average';
        $average['sentEmails'] = $total['sentEmails']/$rowsCount;
        $average['newAlerts'] = $total['newAlerts']/$rowsCount;
        $average['newSubscribers'] = $total['newSubscribers']/$rowsCount;
        $average['organicLinks'] = $total['organicLinks']/$rowsCount;
        $average['backfillLinks'] = $total['backfillLinks']/$rowsCount;
        $average['organicClicks'] = $total['organicClicks']/$rowsCount;
        $average['backfillClicks'] = $total['backfillClicks']/$rowsCount;
        $average['messageViews'] = $total['messageViews']/$rowsCount;
        $average['totalLinks'] = $total['totalLinks']/$rowsCount;
        $average['totalClicks'] = $total['totalClicks']/$rowsCount;

        $total['_type'] = 'footer';
        $total['reportDate'] = 'Total';

        $report = array();
        $report['body'] = $reportBody;
        $report['footer'] = array();
        $report['footer'][] = $average;
        $report['footer'][] = $total;

        return $report;
    } // run

    /**
     * Get last report data and time
     *
     * @return DateTime
     */
    public static function getLastAvailableData()
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();

        $sql = 'SELECT MAX(reportDate) FROM ' . static::$_tableName;
        $date = $dbh->fetchOne($sql);

        if (!$date) {
            $sql = 'SELECT MIN(DATE(createdDate)) - INTERVAL 1 HOUR FROM contacts';
            $date = $dbh->fetchOne($sql);
        }
        return new DateTime($date, new DateTimeZone(Model_Admin_Reports_TimeZoneReport::SQL_TIME_ZONE));
    } // getLastAvailableData

    /**
     * Calculate report data for the data range from source DB tables
     *
     * @return void
     */
    public function calculate()
    {
        $this->setReportDate($this->getSqlStartDate());

        $this->_calculateEmailsSendAndLinks();

        $this->_calculateNewAlerts();
        $this->_calculateTotalAlerts();
        $this->_calculateActiveAlerts();

        $this->_calculateNewSubscribers();
        $this->_calculateTotalSubscibers();

        $this->_calculateMessageViews();
        $this->_calculateClicks();

        $this->save();
    } // calculate

    /**
     * Calculate number of sent emails and links based on archive data
     *
     * @return void
     */
    private function _calculateEmailsSendAndLinks()
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $sql = 'select count(*) cnt,
                sum(organicLinks) cntOrganic, sum(backfillLinks) cntBackfill
                from message_archive
                where status = "send_success"
                and sendDate between ? and ?';

        $bind = array();
        $bind[] = $this->getSqlStartDate();
        $bind[] = $this->getSqlEndDate();
        $counts = $dbh->fetchRow($sql, $bind);

        if ($counts) {
            $this->setSentEmails($counts['cnt']);
            $this->setOrganicLinks(intval($counts['cntOrganic']));
            $this->setBackfillLinks(intval($counts['cntBackfill']));
        } else {
            $this->setSentEmails(0);
            $this->setOrganicLinks(0);
            $this->setBackfillLinks(0);
        }
    } // _calculateEmailsSendAndLinks

    /**
     * Calculate number of new alerts
     * - all alerts created that day
     *
     * @return void
     */
    private function _calculateNewAlerts()
    {
        $sql = 'select count(*) cnt
                from alerts
                where createdDate between ? and ?';
        $this->setNewAlerts($this->getSqlResultForDateRange($sql));
    } // _calculateNewAlerts

    /**
     * Calculate number of total alerts
     * - all alerts created to date
     *
     * @return void
     */
    private function _calculateTotalAlerts()
    {
        $sql = 'select count(*) cnt
                from alerts
                where alerts.createdDate <= ?';
        $this->setTotalAlerts($this->getSqlResultsForEndDate($sql));
    } // _calculateTotalAlerts

    /**
     * Calculate number of active alerts eligible for sending messages
     * - all alerts eligible to be included in an email
     *
     * @return void
     */
    private function _calculateActiveAlerts()
    {
        $sql = 'select count(*) cnt
                from alerts
                join contacts using (contactId)
                where alerts.createdDate <= ?
                and alerts.alertStatus = "ACTIVE"
                and contacts.contactStatus = "ACTIVE"
                and alerts.isLocationValid
                and ((location != "" and location is not null) or (queryTerms != "" and queryTerms is not null))';
        $this->setActiveAlerts($this->getSqlResultsForEndDate($sql));
    } // _calculateActiveAlerts


    /**
     * Calculate number of new subscribers
     * - all subscribers created that day
     *
     * @return void
     */
    private function _calculateNewSubscribers()
    {
        $sql = 'select count(*)
                from contacts
                where contacts.createdDate between ? and ?';
        $this->setNewSubscribers($this->getSqlResultForDateRange($sql));
    } // _calculateNewSubscribers

    /**
     * Calculate total number of subscribers
     * - All subscribers except spam, hard bounce, unsubscribe.
     * These are the only actions that should change someone from being eligible
     * to receive an alert to not. In other words transient should not effect
     * someone's subscriber status, or unknown etc.
     *
     * @return void
     */
    private function _calculateTotalSubscibers()
    {
        $sql = 'select count(*)
                from contacts
                where createdDate <= ?
                and contactStatus = "ACTIVE"';
        $this->setActiveSubscribers($this->getSqlResultsForEndDate($sql));
    } // _calculateTotalSubscibers

    /**
     * Calculate messages view from the history table
     *
     * @return void
     */
    private function _calculateMessageViews()
    {
        $sql = 'select count(*) cnt
                from message_views
                where createdDate between ? and ?';
        $this->setMessageViews($this->getSqlResultForDateRange($sql));
    } // _calculateMessageViews

    /**
     * Calculate organic and backfill clicks
     *
     * @return void
     */
    private function _calculateClicks()
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $sql = 'select type, count(*) cnt
                from email_click
                where email_click.created between ? and ?
                group by type';
        $bind = array();
        $bind[] = $this->getSqlStartDate();
        $bind[] = $this->getSqlEndDate();
        $clicks = $dbh->fetchPairs($sql, $bind);

        if (!empty($clicks['organic'])) {
            $this->setOrganicClicks($clicks['organic']);
        } else {
            $this->setOrganicClicks(0);
        }
        if (!empty($clicks['backfill'])) {
            $this->setBackfillClicks($clicks['backfill']);
        } else {
            $this->setBackfillClicks(0);
        }
    } // _calculateClicks
}
