<?php
/**
 * Model_Admin_Reports_TimeZoneReport
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Reports
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Common functionality for reports.
 */
abstract class Model_Admin_Reports_TimeZoneReport extends Model_Admin_Reports_ReportStorage
{
    /**
     * Time zone name for the data in the data base
     *
     * @var string
     */
    const SQL_TIME_ZONE = 'US/Eastern';

    /**
     * Report start date and time
     *
     * @var DateTime
     */
    private $_startDate;

    /**
     * Report end date and time
     *
     * @var DateTime
     */
    private $_endDate;

    /**
     * Run report and return results
     *
     * @return array
     */
    abstract public function run();

    /**
     * Calculate report data for the data range from source DB tables
     *
     * @return void
     */
    abstract public function calculate();

    /**
     * Get last report data and time
     *
     * @return DateTime
     */
    public static function getLastAvailableData()
    {
        return null;
    } // getLastAvailableData

    /**
     * Update aggregated report data
     *
     * @return void
     */
    public static function update()
    {
        $startDate = static::getLastAvailableData();
        $interval = new DateInterval('PT3H');
        $startDate->sub($interval);

        $endDate = new DateTime('now', new DateTimeZone(self::SQL_TIME_ZONE));
        $endDate->setTime(intval($endDate->format('H')), 59, 59);


        $almostHourInterval = new DateInterval('PT59M59S');
        $hourInterval = new DateInterval('PT1H');
        while ($startDate < $endDate) {

            $nextHour = clone $startDate;
            $nextHour->add($almostHourInterval);

            $report = new static();
            $report->setStartDate($startDate);
            $report->setEndDate($nextHour);
            $report->calculate();

            $startDate->add($hourInterval);
        }
    } // update

    /**
     * Get start time
     *
     * @return DateTime
     */
    public function getStartDate()
    {
        return $this->_startDate;
    } // getStartDate

    /**
     * Set start time. Report time zone is taken from start time
     *
     * @param DateTime $startDate
     *
     * @return void
     */
    public function setStartDate($startDate)
    {
        $this->_startDate = $startDate;
    } // setStartDate

    /**
     * Get end time
     *
     * @return DateTime
     */
    public function getEndDate()
    {
        return $this->_endDate;
    } // getEndDate

    /**
     * Set end time
     *
     * @param DateTime $endDate
     *
     * @return void
     */
    public function setEndDate($endDate)
    {
        $this->_endDate = $endDate;
    } // setEndDate

    /**
     * Get start date and time in MySQL server time zone and datetime format.
     *
     * @return string
     */
    public function getSqlStartDate()
    {
        return $this->getSqlDateTime($this->_startDate);
    } // getSqlStartDate

    /**
     * Get end date and time in MySQL server time zone and datetime format.
     *
     * @return string
     */
    public function getSqlEndDate()
    {
        return $this->getSqlDateTime($this->_endDate);
    } // getSqlEndDate

    /**
     * Execute SQL with start and end dates parameters and retrieve single column result.
     *
     * @param string $sql
     *
     * @return string
     */
    public function getSqlResultForDateRange($sql)
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $bind = array();
        $bind[] = $this->getSqlStartDate();
        $bind[] = $this->getSqlEndDate();
        $result = $dbh->fetchOne($sql, $bind);
        return $result;
    } // getSqlResultForDateRange

    /**
     * Execute SQL with end date parameters and retrieve single column result.
     *
     * @param string $sql
     *
     * @return string
     */
    public function getSqlResultsForEndDate($sql)
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $bind = array();
        $bind[] = $this->getSqlEndDate();
        $result = $dbh->fetchOne($sql, $bind);
        return $result;
    } // getSqlResultsForEndDate

    /**
     * Get date in MySQL server time zone and datetime format
     *
     * @param DateTime $date
     *
     * @return sdtring
     */
    public function getSqlDateTime($date)
    {
        $date = clone $date;
        $sqlTimeZone = new DateTimeZone(Model_Admin_Reports_TimeZoneReport::SQL_TIME_ZONE);
        $date->setTimezone($sqlTimeZone);
        return $date->format('Y-m-d H:i:s');
    } // getSqlDateTime
}
