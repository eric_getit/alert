<?php
/**
 * Model_Admin_Reports_ReportStorage
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Reports
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Common base functionality to save report date in aggregate table
 */
class Model_Admin_Reports_ReportStorage
{
    /**
     * Database table name
     *
     * @var string
     */
    protected static $_tableName;

    /**
     * Field by which we determine whether to insert or update the record
     *
     * @var string
     */
    protected static $_uniqueField;

    /**
     * Data to persist in the data base
     *
     * @var array
     */
    protected $_rowData = array();

    /**
     * Insert or update row with $this->_rowData into the table called $this->_tableName
     *
     *  @return void
     */
    protected function save()
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $select = $dbh->select();
        $select->from(static::$_tableName);
        $select->where(static::$_uniqueField . ' = ?', $this->_rowData[static::$_uniqueField]);
        $row = $select->query()->fetch();

        if ($row === false) {
            $dbh->insert(static::$_tableName, $this->_rowData);
        } else {
            $dbh->update(
                static::$_tableName,
                $this->_rowData,
                static::$_uniqueField . '="' . $this->_rowData[static::$_uniqueField] . '"'
            );
        }

        $debug = array();
        foreach ($this->_rowData as $key => $value) {
            $debug[] = $key . ' => ' . $value;
        }
        echo implode(' : ', $debug) . PHP_EOL;
    } // insertRow
}
