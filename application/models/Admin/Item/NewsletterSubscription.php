<?php
/**
 * Model_Admin_Item_NewsletterSubscription
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Admin\Item
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Grid\Item;

/**
 * Grid item for the newsletters subscription.
 */
class Model_Admin_Item_NewsletterSubscription extends Item
{
    /**
     * Get contact email
     *
     * @return string
     */
    public function getContactEmail()
    {
        $result = $this->_object->getContact()->getEmail();

        return $result;
    } // getContactEmail

    /**
     * Get site domain
     *
     * @return string
     */
    public function getDomain()
    {
        $result = $this->_object->getNewsletter()->getDomain();

        return $result;
    } // getDomain

    /**
     * Get newsletter name
     *
     * @return string
     */
    public function getNewsletterName()
    {
        $result = $this->_object->getNewsletter()->getNewsletterName();

        return $result;
    } // getNewsletterName

    /**
     * Get actions for the alert
     *
     * @return array
     */
    public function getActions()
    {
        $actions = array();
        $actions[] = '<a href="/edit/newsletter-subscription?subscriptionId=' . $this->get('SubscriptionId') . '">Edit</a>';
        return $actions;
    } // getActions
}
