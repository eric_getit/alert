<?php
/**
 * Model_Admin_Item_MessageArchive
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Admin\Item
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Grid\Item;

/**
 * Grid item for the message archive.
 */
class Model_Admin_Item_MessageArchive extends Item
{
    /**
     * Get actions for the alert
     *
     * @return array
     */
    public function getActions()
    {
        $actions = array();
        return $actions;
    } // getActions

    /**
     * Get to email Link
     *
     * @return string
     */
    public function getToEmail()
    {
        $contactId = Zend_Controller_Front::getInstance()->getRequest()->getParam('contactId', null);
        $toEmail = $this->_object->getToEmail();
        $curContactId = $this->_object->getContactId();
        if ($contactId == $curContactId) {
            $link = $toEmail;
        } else {
            $link = '<a href="/message-archive?contactId=' . $curContactId . '">' .$toEmail . '</a>';
        }

        return $link;
    } // getToEmail

    /**
     * Get subject link
     *
     * @return string
     */
    public function getSubject()
    {
        $contactId = Zend_Controller_Front::getInstance()->getRequest()->getParam('contactId', null);
        $link = '<a href="/message-archive/message-details?messageId=' . $this->_object->getMessageId();
        if ($contactId != null) {
            $link .= '&contactId=' . $contactId;
        }
        $link .= '">' . $this->_object->getSubject() . '</a>';

        return $link;
    } // getToEmail
}
