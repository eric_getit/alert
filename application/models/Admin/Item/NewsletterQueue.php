<?php
/**
 * Model_Admin_Item_NewsletterQueue
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Admin\Item
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Grid\Item;

/**
 * Grid item for the newsletters queue.
 */
class Model_Admin_Item_NewsletterQueue extends Item
{
    /**
     * Get actions for the alert
     *
     * @return array
     */
    public function getActions()
    {
        $actions = array();
        $actions[] = '<a href="/edit/newsletter-queue?queueId=' . $this->get('QueueId') . '">Edit</a>';
        return $actions;
    } // getActions
}
