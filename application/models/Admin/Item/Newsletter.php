<?php
/**
 * Model_Admin_Item_Newsletter
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Admin\Item
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Grid\Item;

/**
 * Grid item for the newsletter.
 */
class Model_Admin_Item_Newsletter extends Item
{
    /**
     * Get URL to the page with alerts for the contact if it has any
     *
     * @return string
     */
    public function getSubscription()
    {
        $newsletterId = $this->get('NewsletterId');
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $sql = 'SELECT count(*) FROM newsletter_subscriptions WHERE newsletterId = ?';
        $bind = array();
        $bind[] = $newsletterId;
        $newslettersCount = intval($dbh->fetchOne($sql, $bind));
        if ($newslettersCount > 0) {
            $result = '<a href="/newsletter-subscriptions?newsletterId='
                    . $newsletterId . '">'
                    . number_format($newslettersCount) . '</a>';
        } else {
            $result = '0';
        }
        return $result;
    } // getSubscription

    /**
     * Get actions for the alert
     *
     * @return array
     */
    public function getActions()
    {
        $actions = array();
        $actions[] = '<a href="/edit/newsletter?newsletterId=' . $this->get('NewsletterId') . '">Edit</a>';
        return $actions;
    } // getActions
}
