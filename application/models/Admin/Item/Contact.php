<?php
/**
 * Model_Admin_Item_Contact
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Grid
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Grid\Item;

/**
 * Grid item for the contact.
 */
class Model_Admin_Item_Contact extends Item
{
    /**
     * Number of alerts subscribed by the contact
     *
     * @var number
     */
    protected $_alertsCount;

    /**
     * Number of newsletters subscribed by the contact
     *
     * @var number
     */
    protected $_newslettersCount;

    /**
     * Get alerts count
     *
     * @return number
     */
    public function getAlertsCount()
    {
        return $this->_alertsCount;
    } // getAlertsCount

    /**
     * Set alerts count
     *
     * @param number $alertsCount
     *
     * @return Model_Admin_Item_Contact
     */
    public function setAlertsCount($alertsCount)
    {
        $this->_alertsCount = $alertsCount;
        return $this;
    } // setAlertsCount

    /**
     * Get newsletters count
     *
     * @return number
     */
    public function getNewslettersCount()
    {
        return $this->_newslettersCount;
    } // getNewslettersCount

    /**
     * Set newsletters count
     *
     * @param number $newslettersCount
     *
     * @return Model_Admin_Item_Contact
     */
    public function setNewslettersCount($newslettersCount)
    {
        $this->_newslettersCount = $newslettersCount;
        return $this;
    } // setNewslettersCount

    /**
     * Get URL to the page with alerts for the contact if it has any
     *
     * @return string
     */
    public function getAlertsLink()
    {
        $alertsCount = $this->getAlertsCount();
        if ($alertsCount > 0) {
            $result = '<a href="/alerts?contactId='
                    . $this->get('contactId') . '">'
                    . number_format($alertsCount) . '</a>';
        } else {
            $result = '0';
        }
        return $result;
    } // getAlerts

    /**
     * Get URL to the page with newsletters for the contact if it has any subscribed
     *
     * @return string
     */
    public function getNewslettersLink()
    {
        $newslettersCount = $this->getNewslettersCount();
        if ($newslettersCount > 0) {
            $result = '<a href="/newsletter-subscriptions?contactId='
                    . $this->get('contactId') . '">'
                    . number_format($newslettersCount) . '</a>';
        } else {
            $result = '0';
        }
        return $result;
    } // getNewsletters

    /**
     * Get link to contacts details page
     *
     * @return string
     */
    public function getContactDetailsLink()
    {
        $result = '<a href="/contact-details?contactId=' . $this->get('contactId')
                . '">' . $this->get('email') . '</a>';
        return $result;
    } // getEmailLink

    /**
     * Get actions for the contact
     *
     * @return array
     */
    public function getActions()
    {
        $actions = array();
        $actions[] = '<a href="/contacts/edit?contactId=' . $this->get('contactId') . '">Edit</a>';
        if ($this->get('contactStatus') == Model_Contact::STATUS_ACTIVE) {
            $actions[] = '<a href="javascript:void(0);" '
                       . 'onclick="action(\'/contacts/unsubscribe?contactId='
                       . $this->get('contactId') . '\');">Unsubscribe</a>';
        } elseif (in_array(
                $this->get('contactStatus'),
                array(
                        Model_Contact::STATUS_UNSUBSCRIBED_USER,
                        Model_Contact::STATUS_UNSUBSCRIBED_ADMIN,
                        Model_Contact::STATUS_UNSUBSCRIBED_API
                )
        )) {
            $actions[] = '<a href="javascript:void(0);" '
                       . 'onclick="action(\'/contacts/activate?contactId='
                       . $this->get('contactId') . '\');">Activate</a>';
        }
        $actions[] = '<a href="javascript:void(0);" '
                       . 'onclick="action(\'/contacts/delete?contactId='
                       . $this->get('contactId') . '\', '
                       . '\'Are you sure you want to delete contact?\n'
                       . 'This operation cannot be undone.\');">Delete</a>';
        return $actions;
    } // getActions
}
