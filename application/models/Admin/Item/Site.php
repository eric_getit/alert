<?php
/**
 * Model_Admin_Item_Alert
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Admin\Item
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Grid\Item;

/**
 * Grid item for the site.
 */
class Model_Admin_Item_Site extends Item
{
    /**
     * Get actions for the site
     *
     * @return array
     */
    public function getActions()
    {
        $actions = array();
        $actions[] = '<a href="/edit/site?siteId=' . $this->get('SiteId') . '">Edit</a>';
        return $actions;
    } // getActions
}
