<?php
/**
 * Model_Admin_Item_Template
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Admin\Item
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Grid\Item;

/**
 * Grid item for the template.
 */
class Model_Admin_Item_Template extends Item
{
    /**
     * Get actions for the template
     *
     * @return array
     */
    public function getActions()
    {
        $actions = array();
        $actions[] = '<a href="/edit/template?templateId=' . $this->get('TmplId') . '">Edit</a>';
        return $actions;
    } // getActions
}
