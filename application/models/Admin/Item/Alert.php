<?php
/**
 * Model_Admin_Item_Alert
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Grid
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Grid\Item;

/**
 * Grid item for the alert.
 */
class Model_Admin_Item_Alert extends Item
{
    /**
     * Alerts site domain name
     *
     * @var string
     */
    protected $_siteDomain;

    /**
     * Get alert's site domain name
     *
     * @return string
     */
    public function getSiteDomain()
    {
        return $this->_siteDomain;
    } // getSiteDomain

    /**
     * Set alert's site domain name
     *
     * @param string $siteDomain
     *
     * @return void
     */
    public function setSiteDomain($siteDomain)
    {
        $this->_siteDomain = $siteDomain;
    } // setSiteDomain

    /**
     * Get actions for the alert
     *
     * @return array
     */
    public function getActions()
    {
        $alertId = $this->get('AlertId');
        $actions = array();

        $actions[] = '<a href="/edit/alert?alertId=' . $alertId . '">Edit</a>';

        if ($this->get('AlertStatus') == Model_Alert::STATUS_ACTIVE) {

            $actions[] = '<a href="javascript:void(0);" '
                    . 'onclick="action(\'/alerts/unsubscribe?alertId='
                            . $alertId . '\');">Unsubscribe</a>';
        } elseif (in_array(
                $this->get('AlertStatus'),
                array(
                        Model_Alert::STATUS_UNSUBSCRIBED_USER,
                        Model_Alert::STATUS_UNSUBSCRIBED_ADMIN,
                        Model_Alert::STATUS_UNSUBSCRIBED_API
                )
        )) {
            $actions[] = '<a href="javascript:void(0);" '
                    . 'onclick="action(\'/alerts/activate?alertId='
                            . $alertId . '\');">Activate</a>';

        }

        $actions[] = '<a href="javascript:void(0);" '
                   . 'onclick="action(\'/alerts/delete?alertId='
                   . $alertId . '\', '
                   . '\'Are you sure you want to delete the alert?\n'
                   . 'This operation cannot be undone.\');">Delete</a>';

        $actions[] = '<a href="/alert-operations-history?alertId='
                   . $alertId . '">History</a>';
        return $actions;
    } // getActions
}
