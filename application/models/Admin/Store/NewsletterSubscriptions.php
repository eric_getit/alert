<?php
/**
 * Model_Admin_Store_NewsletterSubscriptions
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Admin\Store
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Zend\TableStore;

/**
 * Store to hold items for contacts grid
 */
class Model_Admin_Store_NewsletterSubscriptions extends TableStore
{
    /**
     * List of allowed filters
     *
     * @var array
     */
    protected $_allowedFilters = array('contactId', 'newsletterId');

    /**
     * Data base table name for the model.
     *
     * @var string
     */
    protected $_tableName = 'newsletter_subscriptions';

    /**
     * Get Zend_Db_Select to fetch row data
     *
     * @return \Zend_Db_Select
    */
    protected function _getSelectResults()
    {
        $dbh = \Zend_Db_Table::getDefaultAdapter();
        $select = $dbh->select();
        $select->from($this->getTableName(), '*');
        $select->limitPage($this->getCurrentpage(), $this->getPagesize());

        foreach ($this->_filters as $name => $value) {
            $select->where($name . ' = ?', $value);
        }

        $sorters = $this->getSorters();
        if (!empty($sorters)) {
            if ($sorters['property'] == 'newsletterName') {
                $select->columns('(SELECT newsletterName FROM newsletter WHERE newsletter.newsletterId = newsletter_subscriptions.newsletterId) as newsletterName');
            }
            if ($sorters['property'] == 'domain') {
                $select->columns('(SELECT domain FROM newsletter WHERE newsletter.newsletterId = newsletter_subscriptions.newsletterId) as domain');
            }
            if ($sorters['property'] == 'contactEmail') {
                $select->columns('(SELECT email FROM contacts WHERE contacts.contactId = newsletter_subscriptions.contactId) as contactEmail');
            }
            $order = $sorters['property'] . ' ' . $sorters['direction'];
            $select->order($order);
        }

        return $select;
    } // _getSelectResults

    /**
     * Create item instance from SQL table row
     *
     * @param array $rowData
     *
     * @return Item
     */
    protected function _getItem($rowData)
    {
        $subscrtiption = new Model_Newsletter_Subscription($rowData);
        $item = new Model_Admin_Item_NewsletterSubscription($subscrtiption);
        return $item;
    } // _getItem
}
