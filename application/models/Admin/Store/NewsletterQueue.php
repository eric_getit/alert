<?php
/**
 * Model_Admin_Store_NewsletterQueue
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Admin\Store
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Zend\TableStore;

/**
 * Store to hold items for contacts grid
 */
class Model_Admin_Store_NewsletterQueue extends TableStore
{
    /**
     * Data base table name for the model.
     *
     * @var string
     */
    protected $_tableName = 'newsletter_queue';

    /**
     * Create item instance from SQL table row
     *
     * @param array $rowData
     *
     * @return Item
     */
    protected function _getItem($rowData)
    {
        $queue = new Model_Newsletter_Queue($rowData);
        $item = new Model_Admin_Item_NewsletterQueue($queue);
        return $item;
    } // _getItem
}
