<?php
/**
 * Model_Admin_Store_Contacts
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Grid
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Zend\CoreStore;

/**
 * Store to hold items for contacts grid
 */
class Model_Admin_Store_Contacts extends CoreStore
{
    /**
     * Get Zend_Db_Select to count results
     *
     * @return \Zend_Db_Select
     */
    protected function _getSelectCount()
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $select = $dbh->select();
        $select->from('contacts', 'count(*)');
        $this->_applySearch($select);
        return $select;
    } // _getSelectCount

    /**
     * Get Zend_Db_Select to fetch row data
     *
     * @return \Zend_Db_Select
    */
    protected function _getSelectResults()
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $select = $dbh->select();
        $select->from('contacts');
        $select->columns(array(
                'alertsCount' => '(SELECT count(*) FROM alerts WHERE contacts.contactId = alerts.contactId)',
                'newslettersCount' => '(SELECT count(*) FROM newsletter_subscriptions WHERE contacts.contactId = newsletter_subscriptions.contactId)',
        ));

        $this->_applySearch($select);
        $sorters = $this->getSorters();
        if (!empty($sorters)) {
            $select->order($sorters['property'] . ' ' . $sorters['direction']);
        }

        $select->limitPage($this->getCurrentpage(), $this->getPagesize());

        return $select;
    } // _getSelectResults

    /**
     * Create item instance from SQL table row
     *
     * @param array $rowData
     *
     * @return Item
    */
    protected function _getItem($rowData)
    {
        $contact = new Model_Contact($rowData);
        $item = new Model_Admin_Item_Contact($contact);
        $item->setAlertsCount($rowData['alertsCount']);
        $item->setNewslettersCount($rowData['newslettersCount']);
        $item->setStore($this);
        return $item;
    } // _getItem

    /**
     * Apply search query to the select statement
     *
     * @param \Zend_Db_Select  $select
     *
     * @return void
     */
    private function _applySearch($select)
    {
        $searchQuery = $this->getSearchQuery();

        if (!empty($searchQuery)) {
            // NOTE: Prepare string for wild card search
            $searchQuery = str_replace(' ', '%', $searchQuery);
            $searchQuery = '%' . $searchQuery . '%';
            $searchQuery = str_replace('%%', '%', $searchQuery);

            $select->orWhere('contacts.email like ?', $searchQuery);
            $select->orWhere('contacts.name like ?', $searchQuery);
        }
    } // _applySearch
}
