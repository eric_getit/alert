<?php
/**
 * Model_Admin_Store_OverviewOldReport
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Admin\Store
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Grid\DateRangeStore,
    GetIt\Admin\Grid\Item;

/**
 * Old overview report data
 */
class Model_Admin_Store_OverviewOldReport extends DateRangeStore
{
    /**
     * Load items using filters and sorting,
     * populates $_items and $_totalCount properties
     *
     * @return void
     */
    public function load()
    {

        if ($this->getBeginDate() != null) {
            $from = $this->getBeginDate()->format("Y-m-d H:i:s");
        } else {
            $from = NULL;
        }
        if ($this->getEndDate()) {
            $to = $this->getEndDate()->format("Y-m-d H:i:s");
        } else {
            $to = NULL;
        }
        $data = Model_Admin_Reports_OverviewOld::run($from, $to);

        $this->_totalCount = count($data['body']);

        $body = array_slice(
                $data['body'],
                ($this->getCurrentPage()-1)*$this->getPageSize(),
                $this->getPageSize()
        );

        $this->_items = array();
        foreach ($body as $row) {
            $item = new Item($row);
            $this->_items[] = $item;
        }
    } // load
}
