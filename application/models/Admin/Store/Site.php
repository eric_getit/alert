<?php
/**
 * Model_Admin_Store_Site
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Admin\Store
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Zend\TableStore;

/**
 * Store to hold items for contacts grid
 */
class Model_Admin_Store_Site extends TableStore
{
    /**
     * Data base table name for the model.
     *
     * @var string
     */
    protected $_tableName = 'sites';

    /**
     * Create item instance from SQL table row
     *
     * @param array $rowData
     *
     * @return Item
     */
    protected function _getItem($rowData)
    {
        $site = new Model_Site($rowData);
        $item = new Model_Admin_Item_Site($site);
        return $item;
    } // _getItem
}
