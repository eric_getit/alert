<?php
/**
 * Model_Admin_Store_MessageArchive
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Admin\Store
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Zend\CoreDateRangeStore;

/**
 * Store to hold items for contacts grid
 */
class Model_Admin_Store_MessageArchive extends CoreDateRangeStore
{
    /**
     * List of allowed filters
     *
     * @var array
     */
    protected $_allowedFilters = array('contactId');

    /**
     * Data base table name for the model.
     *
     * @var string
     */
    protected $_tableName = 'message_archive';

    /**
     * Get Zend_Db_Select to count results
     *
     * @return \Zend_Db_Select
     */
    protected function _getSelectCount()
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $select = $dbh->select();
        $select->from('message_archive', 'count(*)');
        $this->_applyDateRange($select);
        return $select;
    } // _getSelectCount

    /**
     * Get Zend_Db_Select to fetch row data
     *
     * @return \Zend_Db_Select
    */
    protected function _getSelectResults()
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $select = $dbh->select();
        $select->from('message_archive');

        $this->_applyDateRange($select);

        $sorters = $this->getSorters();
        if (!empty($sorters)) {
            $select->order($sorters['property'] . ' ' . $sorters['direction']);
        }

        $select->limitPage($this->getCurrentpage(), $this->getPagesize());

        return $select;
    } // _getSelectResults

    /**
     * Create item instance from SQL table row
     *
     * @param array $rowData
     *
     * @return Item
     */
    protected function _getItem($rowData)
    {
        $archive = new Model_MessageArchive($rowData);
        $item = new Model_Admin_Item_MessageArchive($archive);
        return $item;
    } // _getItem

    private function _applyDateRange($select)
    {
        $beginDate = $this->getBeginDate();
        if ($beginDate != null) {
            $select->where('sendDate >= ?', $beginDate->format('Y-m-d H:i:s'));
        }

        $endDate = $this->getEndDate();
        if ($endDate != null) {
            $select->where('sendDate <= ?', $endDate->format('Y-m-d H:i:s'));
        }

        $contactId = $this->getFilter('contactId');
        if ($contactId != null) {
            $select->where('contactId = ?', $contactId);
        }
    } // _applyDateRange
}
