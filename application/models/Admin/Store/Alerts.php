<?php
/**
 * Model_Admin_Store_Contacts
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Grid
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Zend\CoreStore;

/**
 * Store to hold items for contacts grid
 */
class Model_Admin_Store_Alerts extends CoreStore
{
    /**
     * List of allowed filters
     *
     * @var array
     */
    protected $_allowedFilters = array('contactId');

    /**
     * Get Zend_Db_Select to count results
     *
     * @return \Zend_Db_Select
     */
    protected function _getSelectCount()
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $select = $dbh->select();
        $select->from('alerts', 'count(*)');
        $this->_applyContactFilter($select);
        return $select;
    } // _getSelectCount

    /**
     * Get Zend_Db_Select to fetch row data
     *
     * @return \Zend_Db_Select
    */
    protected function _getSelectResults()
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $select = $dbh->select();
        $select->from('alerts');
        $select->joinInner(
                'sites',
                'alerts.siteId = sites.siteId',
                array('siteDomain' => 'domain')
        );
        $this->_applyContactFilter($select);

        $sorters = $this->getSorters();
        if (!empty($sorters)) {
            $select->order($sorters['property'] . ' ' . $sorters['direction']);
        }

        $select->limitPage($this->getCurrentpage(), $this->getPagesize());

        return $select;
    } // _getSelectResults

    /**
     * Create item instance from SQL table row
     *
     * @param array $rowData
     *
     * @return Item
    */
    protected function _getItem($rowData)
    {
        $alert = new Model_Alert($rowData);
        $item = new Model_Admin_Item_Alert($alert);
        $item->setSiteDomain($rowData['siteDomain']);
        $item->setStore($this);
        return $item;
    } // _getItem

    /**
     * Apply filter by contactId
     *
     * @param Zend_Db_Select  $select
     *
     * @return void
     */
    private function _applyContactFilter($select)
    {
        $filters = $this->getFilters();
        if (isset($filters['contactId'])) {
            $contactId = $filters['contactId'];
            $select->where('contactId = ?', $contactId);
        }
    } // _applyContactFilter
}
