<?php
/**
 * Model_Admin_Store_Newsletter
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Admin\Store
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Zend\TableStore;

/**
 * Store to hold items for contacts grid
 */
class Model_Admin_Store_Newsletter extends TableStore
{
    /**
     * Data base table name for the model.
     *
     * @var string
     */
    protected $_tableName = 'newsletter';

    /**
     * Get Zend_Db_Select to fetch row data
     *
     * @return \Zend_Db_Select
    */
    protected function _getSelectResults()
    {
        $dbh = \Zend_Db_Table::getDefaultAdapter();
        $select = $dbh->select();
        $select->from($this->getTableName(), '*');
        $select->limitPage($this->getCurrentpage(), $this->getPagesize());

        $sorters = $this->getSorters();
        if (!empty($sorters)) {
            if ($sorters['property'] == 'subscription') {
                $select->columns('(SELECT count(*) FROM newsletter_subscriptions WHERE newsletter.newsletterId = newsletter_subscriptions.newsletterId) as subscription');
            }
            $order = $sorters['property'] . ' ' . $sorters['direction'];
            $select->order($order);
        }

        return $select;
    } // _getSelectResults

    /**
     * Create item instance from SQL table row
     *
     * @param array $rowData
     *
     * @return Item
     */
    protected function _getItem($rowData)
    {
        $newsletter = new Model_Newsletter($rowData);
        $item = new Model_Admin_Item_Newsletter($newsletter);
        return $item;
    } // _getItem
}
