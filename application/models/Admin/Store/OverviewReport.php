<?php
/**
 * Model_Admin_Store_OverviewReport
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Admin\Store
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Grid\DateRangeStore,
    GetIt\Admin\Grid\Item;

/**
 * Overview report data
 */
class Model_Admin_Store_OverviewReport extends DateRangeStore
{
    /**
     * Load items using filters and sorting,
     * populates $_items and $_totalCount properties
     *
     * @return void
     */
    public function load()
    {
        $report = new Model_Admin_Reports_Overview();
        $report->setStartDate($this->getBeginDate());
        $report->setEndDate($this->getEndDate());
        $data = $report->run();

        $this->_totalCount = count($data['body']);

        $body = array_slice(
                $data['body'],
                ($this->getCurrentPage()-1)*$this->getPageSize(),
                $this->getPageSize()
        );

        $this->_items = array();
        foreach ($body as $row) {
            $item = new Item($row);
            $this->_items[] = $item;
        }

        $this->_summaryItems = array();
        foreach ($data['footer'] as $summaryRow) {
            $summaryItem = new Item($summaryRow);
            $this->_summaryItems[] = $summaryItem;
        }
    } // load
}
