<?php
/**
 * Model_Admin_Store_Contacts
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Grid
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

use GetIt\Admin\Zend\CoreStore,
    GetIt\Admin\Grid\Item;

/**
 * Store to hold items for contacts grid
 */
class Model_Admin_Store_AlertsOperationsHistory extends CoreStore
{
    /**
     * List of allowed filters
     *
     * @var array
     */
    protected $_allowedFilters = array('operation', 'alertId', 'contactId', 'alertIds');

    /**
     * Get Zend_Db_Select to count results
     *
     * @return \Zend_Db_Select
     */
    protected function _getSelectCount()
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $select = $dbh->select();
        $select->from('alert_operation_history', 'count(*)');
        $this->_applyFilters($select);
        return $select;
    } // _getSelectCount

    /**
     * Get Zend_Db_Select to fetch row data
     *
     * @return \Zend_Db_Select
    */
    protected function _getSelectResults()
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $select = $dbh->select();
        $select->from('alert_operation_history');
        $this->_applyFilters($select);

        $sorters = $this->getSorters();
        if (!empty($sorters)) {
            $select->order($sorters['property'] . ' ' . $sorters['direction']);
        }

        $select->limitPage($this->getCurrentpage(), $this->getPagesize());

        return $select;
    } // _getSelectResults

    /**
     * Create item instance from SQL table row
     *
     * @param array $rowData
     *
     * @return Item
    */
    protected function _getItem($rowData)
    {
        $operation = new Model_AlertOperationHistory($rowData);
        $item = new Item($operation);
        $item->setStore($this);
        return $item;
    } // _getItem

    /**
     * Apply filter by alert or contact ids
     *
     * @param Zend_Db_Select  $select
     *
     * @return void
     */
    private function _applyFilters($select)
    {
        $filters = $this->getFilters();
        if (isset($filters['alertIds'])) {
            $alertIds = $filters['alertIds'];
            $select->where('alertId in (?)', $alertIds);
        } else if (isset($filters['alertId'])) {
            $alertId = $filters['alertId'];
            $select->where('alertId = ?', $alertId);
        } else if (isset($filters['contactId'])) {
            $contactId = $filters['contactId'];
            $subSelect = Zend_Db_Table::getDefaultAdapter()->select();
            $subSelect->from('alerts', 'alertId')
                ->where('contactId = ?', $contactId);
            $rows = $subSelect->query()->fetchAll();
            $alertsId = array();
            foreach ($rows as $row) {
                $alertIds[] = $row['alertId'];
            }
            $this->_filters['alertIds'] = $alertIds;
            $select->where('alertId in (?)', $alertIds);
        }
        if (isset($filters['operation'])) {
            $select->where('operation = ?', $filters['operation']);
        }
    } // _applyAlertFilter
}
