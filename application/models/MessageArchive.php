<?php
/**
 * Model_MessageArchive
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */
class Model_MessageArchive extends Model_AbstractModel
{
    /**
     * Message prepared for sending
     *
     * @var string
     */
    const STATUS_PREPARED = 'prepared';

    /**
     * Message sent successfully
     *
     * @var string
     */
    const STATUS_SEND_SUCCESS = 'send_success';

    /**
     * Message sent fail
     *
     * @var string
     */
    const STATUS_SEND_FAIL = 'send_fail';

    /**
     * Primary key
     *
     * @var integer
     */
    protected $_messageId;

    /**
     * Alert reference (foreign key)
     *
     * @var integer
     */
    protected $_alertId;

    /**
     * Contact reference (foreign key)
     *
     * @var integer
     */
    protected $_contactId;

    /**
     * Sent date and time
     *
     * @var string
     */
    protected $_sendDate;

    /**
     * Prepared date and time
     *
     * @var string
     */
    protected $_preparedDate;

    /**
     * Count organic links in the message
     *
     * @var integer
     */
    protected $_organicLinks;

    /**
     * Count backfill links in the message
     *
     * @var integer
     */
    protected $_backfillLinks;

    /**
     * Email from header address
     *
     * @var string
     */
    protected $_fromEmail;

    /**
     * Email from header name
     *
     * @var string
     */
    protected $_fromName;

    /**
     * Email to header address
     *
     * @var string
     */
    protected $_toEmail;

    /**
     * Email to header name
     *
     * @var string
     */
    protected $_toName;

    /**
     * Email subject
     *
     * @var string
     */
    protected $_subject;

    /**
     * Email bcc
     *
     * @var string
     */
    protected $_bcc;

    /**
     * Email status
     *
     * @var string
     */
    protected $_status;

    /**
     * Message ID used in references
     *
     * @var string
     */
    protected $_secureId;

    /**
     * Message body object
     *
     * @var Model_MessageBody
     */
    protected $_messageBody;

    /**
     * Data base table name for the model
     *
     * @var string
     */
    protected $_tableName = 'message_archive';

    /**
     * Primary key field name
     *
     * @var string
     */
    protected $_primaryKey = 'messageId';

    /**
     * Data base fields
     *
     * @var array
     */
    protected $_tableFields = array(
            'messageId' => '_messageId',
            'alertId' => '_alertId',
            'contactId' => '_contactId',
            'sendDate' => '_sendDate',
            'organicLinks' => '_organicLinks',
            'backfillLinks' => '_backfillLinks',
            'fromEmail' => '_fromEmail',
            'fromName' => '_fromName',
            'toEmail' => '_toEmail',
            'toName' => '_toName',
            'subject' => '_subject',
            'bcc' => '_bcc',
            'status' => '_status',
            'preparedDate' => '_preparedDate',
            'secureId' => '_secureId',
            );

    /**
     * Save message date to message_archive and message_bodies tables
     *
     * @param array $messageData
     *
     * @return void
     */
    public static function save($messageData)
    {
        $messageArchive = new Model_MessageArchive($messageData);
        $messageArchive->insert();

        $bodyData = array();
        $bodyData['messageId'] = $messageArchive->getMessageId();
        $bodyData['preparedDate'] = $messageArchive->getPreparedDate();
        $bodyData['bodyText'] = $messageData['bodyText'];
        $bodyData['bodyHtml'] = $messageData['bodyHtml'];

        $messageBody = new Model_MessageBody($bodyData);
        $messageBody->insert();
    } // save

    /**
     * Set number of organic links
     *
     * @param integer $organicLinks Link count
     *
     * @return Model_MessageArchive
     */
    public function setOrganicLinks($organicLinks)
    {
        $this->_organicLinks = $organicLinks;
        return $this;
    } // setOrganicLinks

    /**
     * Set number of backfill links
     *
     * @param integer $backfillLinks Link count
     *
     * @return Model_MessageArchive
     */
    public function setBackfillLinks($backfillLinks)
    {
        $this->_backfillLinks = $backfillLinks;
        return $this;
    } // setBackfillLinks

    /**
     * Set some newsletter references to the message
     *
     * @param Model_Newsletter $newsletter Newsletter Object
     *
     * @return Model_MessageArchive
     */
    public function setNewsletter($newsletter)
    {
        $this->_status = self::STATUS_PREPARED;
        // TODO Do something useful here ;)
        return $this;
    } // setNewsletter

    /**
     * Set parameters based on newsletter queue
     *
     * @param Model_Newsletter_Queue $newsletterQueue Newsletter Queue Object
     *
     * @return Model_MessageArchive
     */
    public function setNewsletterQueue($newsletterQueue)
    {
        // TODO Do something useful here ;)s
        return $this;
    } // setNewsletterQueue

    /**
     * Populate message archive row from template data
     *
     * @param \GetIt\Email\Template\SmsTemplate $template
     *
     * @return Model_MessageArchive
     */
    public function setTemplate($template)
    {
        // NOTE : We know that we have already inserted
        // the row and know it's primary key
        $now = new DateTime();
        $this->_fromEmail = $template->getFromEmail();
        $this->_fromName = $template->getFromName();
        $this->_toEmail = $template->getToEmail();
        $this->_toName = $template->getToName();
        $this->_subject = $template->getSubject();
        $this->_bcc = implode(',', $template->getBcc());
        $this->_preparedDate = $now->format('Y-m-d H:i:s');
        $this->update();

        $bodyData = array();
        $bodyData['messageId'] = $this->getMessageId();
        $bodyData['preparedDate'] = $this->getPreparedDate();
        $bodyData['bodyText'] = $template->getBodyText();
        $bodyData['bodyHtml'] = $template->getBodyHtml();

        $messageBody = new Model_MessageBody($bodyData);
        $messageBody->insert();

        return $this;
    } // setTemplate

    /**
     * Get messages details to show on the admin message details page
     *
     * @return array
     */
    public function getMessageDetailData()
    {
        $details = array();
        $details['Send Date'] = $this->_sendDate;
        $details['Sender'] = trim($this->_fromName . ' <' . $this->_fromName . '>');
        $details['Recepient'] = trim($this->_toName . ' <' . $this->_toEmail . '>');
        $details['Subject'] = $this->_subject;
        $details['Body Text'] = $this->getBodyText();

        $body = '<iframe height="500" width="100%" '
              . 'src="/message-archive/message-body?messageId=' . $this->getMessageId()
              . '"></iframe>';
        $details['Body HTML'] = $body;

        return $details;
    } // getMessageDetailData

    /**
     * Generate Secure Id for message based on alert ID and prepare date
     *
     * @return void
     *
     * @throws Model_Exception_NoRecordForObject
     */
    public function generateSecureId()
    {
        if ($this->_alertId == null || $this->_preparedDate == null) {
            throw new Model_Exception_NoRecordForObject();
        }

        $this->_secureId = md5($this->_alertId . $this->_preparedDate);
    } // generateSecureId

    /**
     * Fetch by SecureId
     *
     * @param string $secureId SecureId
     *
     * @return Model_MessageArchive | null
     */
    public static function getBySecureId($secureId)
    {
        $message = new Model_MessageArchive();
        $dbh = Zend_Db_Table::getDefaultAdapter();

        $select = $dbh->select();
        $select->from($message->_tableName, '*')
            ->where('secureId = ?', $secureId);

        $row = $select->query()->fetch();

        if ($row) {
            $message->fromArray($row);
        } else {
            $message = null;
        }

        return $message;
    } // getBySecureId

    /**
     * Get message ID
     *
     * @return integer $_messageId
     */
    public function getMessageId()
    {
        return $this->_messageId;
    } // getMessageId

    /**
     * Get related alert ID
     *
     * @return integer $_alertId
     */
    public function getAlertId()
    {
        return $this->_alertId;
    } // getAlertId

    /**
     * Get related contact ID
     *
     * @return integer $_contactId
     */
    public function getContactId()
    {
        return $this->_contactId;
    } // getContactId

    /**
     * Get message prepared date
     *
     * @return string
     */
    public function getPreparedDate()
    {
        return $this->_preparedDate;
    } // getPreparedDate

    /**
     * Get send date and time
     *
     * @return string $_sendDate
     */
    public function getSendDate()
    {
        return $this->_sendDate;
    } // getSendDate

    /**
     * Get number of organic links
     *
     * @return integer
     */
    public function getOrganicLinks()
    {
        return $this->_organicLinks;
    } // getOrganicLinks

    /**
     * Get number of backfill links
     *
     * @return integer
     */
    public function getBackfillLinks()
    {
        return $this->_backfillLinks;
    } // getBackfillLinks

    /**
     * Get sender email
     *
     * @return string $_fromEmail
     */
    public function getFromEmail()
    {
        return $this->_fromEmail;
    } // getFromEmail

    /**
     * Get sender name
     *
     * @return string $_fromName
     */
    public function getFromName()
    {
        return $this->_fromName;
    } // getFromName

    /**
     * Get recipient email
     *
     * @return string $_toEmail
     */
    public function getToEmail()
    {
        return $this->_toEmail;
    } // getToEmail

    /**
     * Get recipient name
     *
     * @return string $_toName
     */
    public function getToName()
    {
        return $this->_toName;
    } // getToName

    /**
     * Get message subject
     *
     * @return string $_subject
     */
    public function getSubject()
    {
        return $this->_subject;
    } // getSubject

    /**
     * Get message body object for the message archive record
     *
     * @return Model_MessageBody
     */
    public function getMessageBody()
    {
        if ($this->_messageBody == null) {
            $this->_messageBody = new Model_MessageBody($this);
        }
        return $this->_messageBody;
    } // getMessageBody

    /**
     * Get text body
     *
     * @return string $_bodyText
     */
    public function getBodyText()
    {
        $messageBody = $this->getMessageBody();
        return $messageBody->getBodyText();
    } // getBodyText

    /**
     * Get HTML body
     *
     * @return string $_bodyHtml
     */
    public function getBodyHtml()
    {
        $messageBody = $this->getMessageBody();
        return $messageBody->getBodyHtml();
    } // getBodyHtml

    /**
     * Get secure id
     *
     * @return string $_secureId
     */
    public function getSecureId()
    {
        return $this->_secureId;
    } // getSecureId
}
