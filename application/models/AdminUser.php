<?php
/**
 * Model_AdminUser
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Class Model_AdminUser
 */
class Model_AdminUser extends Model_AbstractModel
{

    /**
     * Space name in session for storage of the information on the user.
     *
     * @var string
     */
    const ADMIN_NAMESPACE = 'admin';

    /**
     * Primary Key
     *
     * @var integer
     */
    public $id;

    /**
     * Login Name
     *
     * @var string
     */
    public $email;

    /**
     * User password in MD5 hash
     *
     * @var string
     */
    public $passwd;

    /**
     * First Name
     *
     * @var string
     */
    public $firstName;

    /**
     * Last Name
     *
     * @var string
     */
    public $lastName;

    /**
     * Status
     *
     * @var string
     */
    public $status;

    /**
     * Comment ID
     *
     * @var integer
     */
    public $adminComment;

    /**
     * Role
     *
     * @var integer
     */
    public $roleId;

    /**
     * Data base table name for the model
     *
     * @var string
     */
    protected $_tableName = 'admin_user';

    /**
     * Data base fields
     * @var array
     */
    protected $_tableFields = array(
        'id' => 'id',
        'email' => 'email',
        'status' => 'status',
        'firstName' => 'firstName',
        'lastName' => 'lastName',
        'passwd' => 'passwd',
        'roleId' => 'roleId',
        'adminComment' => 'adminComment',
    );

    /**
     * Returns the information on the user, stored in session.
     * If the user isn't authorized, returns NULL.
     *
     * @return Model_AdminUser
     */
    public static function getUserInfo()
    {
        $userNamespace = new Zend_Session_Namespace(self::ADMIN_NAMESPACE);
        if (isset($userNamespace->admin)) {
            return $userNamespace->admin;
        }
        return null;
    } // getUserInfo

    /**
     * Checks, whether the user is authorized now
     * return true if user authenticated
     *
     * @return boolean
     */
    public static function isAuthenticated()
    {
        $userInfo = self::getUserInfo();
        return $userInfo != null;

    } // isAuthenticated

    /**
     * Authorise user in session
     *
     * @param string  $login User
     * @param string  $pass  Pass
     * @param boolean $md5   Use MD5
     *
     * @throws Model_Exception_Logon_AdminNotFound
     * @throws Model_Exception_Logon_WrongPassword
     *
     * @return Model_AdminUser
     */
    public static function login($login, $pass, $md5 = true)
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();

        $admins = $dbh->select()
            ->from('admin_user')
            ->where('email = ?', array($login))
            ->query(Zend_Db::FETCH_ASSOC)->fetchAll();

        if (!$admins) {
            throw new Model_Exception_Logon_AdminNotFound();
        }

        $adminRow = $admins[0];

        if ($md5) {
            $pass = md5($pass);
        }
        if ($pass != $adminRow['passwd']) {
            throw new Model_Exception_Logon_WrongPassword();
        }

        $admin = new Model_AdminUser($adminRow['id']);

        $namespace = new Zend_Session_Namespace(self::ADMIN_NAMESPACE);
        $namespace->admin = $admin;

        return $admin;
    } // login

    /**
     * Log out admin user
     *
     * @return void
     */
    public static function logout()
    {
        $admin = self::getUserInfo();
        if ($admin != null) {
            Model_LoggedUserCookies::removeAdmin($admin->email);
            $namespace = new Zend_Session_Namespace(self::ADMIN_NAMESPACE);
            $namespace->unsetAll();
        }
    } // logout

}
