<?php
/**
 * Model_Newsletter
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Class for Newsletter
 */
class Model_Newsletter extends Model_AbstractModel
{
    /**
     * Primary key
     *
     * @var integer
     */
    protected $_newsletterId;

    /**
     * External Id (unique key)
     *
     * @var string
     */
    protected $_externalId;

    /**
     * Newsletter Name (foreign key)
     *
     * @var string
     */
    protected $_newsletterName;

    /**
     * Site reference (foreign key)
     *
     * @var siteId
     */
    protected $_siteId;

    /**
     * Site Object
     *
     * @var Model_Site
     */
    protected $_site;

    /**
     * Domain
     *
     * @var string
     */
    protected $_domain;

    /**
     * Site Name
     *
     * @var string
     */
    protected $_siteName;

    /**
     * Email from header name
     *
     * @var string
     */
    protected $_fromName;

    /**
     * Email from header address
     *
     * @var string
     */
    protected $_fromEmail;

    /**
     * Created date and time
     *
     * @var string
     */
    protected $_createdDate;

    /**
     * Updated date and time
     *
     * @var string
     */
    protected $_updatedDate;

    /**
     * Data base table name for the model
     *
     * @var string
     */
    protected $_tableName = 'newsletter';

    /**
     * Primary key field name
     *
     * @var string
     */
    protected $_primaryKey = 'newsletterId';

    /**
     * Data base fields
     *
     * @var array
     */
    protected $_tableFields = array(
        'newsletterId' => '_newsletterId',
        'externalId' => '_externalId',
        'newsletterName' => '_newsletterName',
        'siteId' => '_siteId',
        'domain' => '_domain',
        'siteName' => '_siteName',
        'fromName' => '_fromName',
        'fromEmail' => '_fromEmail',
        'createdDate' => '_createdDate',
        'updatedDate' => '_updatedDate',
    );

    /**
     * Registry newsletter
     *
     * @param array $params
     *
     * @return Model_Newsletter
     *
     * @throws Model_Exception_MissingRequireParam
     */
    public static function registry(array $params)
    {
        $reqFields = array('id', 'name', 'domain');
        foreach ($reqFields as $field) {
            if (empty($params[$field])) {
                throw new Model_Exception_MissingRequireParam($field);
            }
        }

        $domain = Model_Site::normalizeDomain($params['domain']);
        $site = Model_Site::getByDomain($domain);

        if ($site == null) {
            $site = Model_Site::registry($params);
        }

        $newsletter = new Model_Newsletter();
        $newsletter->setExternalId($params['id']);
        $newsletter->setNewsletterName($params['name']);
        $newsletter->setDomain($domain);
        $newsletter->setSiteId($site->getSiteId());

        if (array_key_exists('fromEmail', $params)) {
            $newsletter->setFromEmail($params['fromEmail']);
        } else {
            $newsletter->setFromEmail($site->getFromEmail());
        }

        if (array_key_exists('fromName', $params)) {
            $newsletter->setFromName($params['fromName']);
        } else {
            $newsletter->setFromName($site->getFromName());
        }

        if (array_key_exists('siteName', $params)) {
            $newsletter->setSiteName($params['siteName']);
        } else {
            $newsletter->setSiteName($site->getSiteName());
        }

        $newsletter->insert();

        return $newsletter;
    } // registry

    /**
     * Registry queue for newsletter
     *
     * @param array $params
     *
     * @return Model_Queue
     *
     * @throws Model_Exception_MissingRequireParam
     */
    public function registryQueue(array $params)
    {
        $reqFields = array('subject', 'message', 'sendTime');
        foreach ($reqFields as $field) {
            if (empty($params[$field])) {
                throw new Model_Exception_MissingRequireParam($field);
            }
        }

        $queue = new Model_Newsletter_Queue();
        $queue->setSubject($params['subject']);
        $queue->setMessage($params['message']);
        $queue->setSendTime($params['sendTime']);
        $queue->setQueueStatus(Model_Newsletter_Queue::STATUS_NEW);
        $queue->setNewsletterId($this->_newsletterId);

        if (!empty($params['fromEmail'])) {
            $queue->setFromEmail($params['fromEmail']);
        } else {
            $queue->setFromEmail($this->_fromEmail);
        }
        if (!empty($params['fromName'])) {
            $queue->setFromName($params['fromName']);
        } else {
            $queue->setFromName($this->_fromName);
        }

        $queue->insert();

        return $queue;
    } // registryQueue

    /**
     * Insert object into DB
     *
     * @return void
     */
    public function insert()
    {
        $date = date('Y-m-d H:i:s');
        $this->setCreatedDate($date);
        $this->setUpdatedDate($date);
        parent::insert();
    } // insert

    /**
     * Get Site Id
     *
     * @return integer
     */
    public function getSiteId()
    {
        return $this->_siteId;
    } // getSiteId

    /**
     * Get Site
     *
     * @return Model_Site
     */
    public function getSite()
    {
        if ($this->_site == null && $this->_siteId != null) {
            $this->_site = new Model_Site($this->_siteId);
        }
        return $this->_site;
    } // getSite

    /**
     * Set Site Id
     *
     * @param integer $siteId
     *
     * @return void
     */
    public function setSiteId($siteId)
    {
        $this->_siteId = $siteId;
    } // setSiteId

    /**
     * Update object in DB
     *
     * @return boolean
     */
    public function update()
    {
        $date = date('Y-m-d H:i:s');
        $this->setUpdatedDate($date);
        return parent::update();
    } // update

    /**
     * Get Newsletter Id
     *
     * @return integer
     */
    public function getNewsletterId()
    {
        return $this->_newsletterId;
    } // getNewsletterId

    /**
     * Get External Id
     *
     * @return string
     */
    public function getExternalId()
    {
        return $this->_externalId;
    } // getExternalId

    /**
     * Get Newsletter Name
     *
     * @return string
     */
    public function getNewsletterName()
    {
        return $this->_newsletterName;
    } // getNewsletterName

    /**
     * Get Domain
     *
     * @return string
     */
    public function getDomain()
    {
        return $this->_domain;
    } // getDomain

    /**
     * Get Site Name
     *
     * @return string
     */
    public function getSiteName()
    {
        return $this->_siteName;
    } // getSiteName

    /**
     * Get From Name
     *
     * @return string
     */
    public function getFromName()
    {
        return $this->_fromName;
    } // getFromName

    /**
     * Get From Email
     *
     * @return string
     */
    public function getFromEmail()
    {
        return $this->_fromEmail;
    } // getFromEmail

    /**
     * Get Created Date
     *
     * @return string
     */
    public function getCreatedDate()
    {
        return $this->_createdDate;
    } // getCreatedDate

    /**
     * Get Updated Date
     *
     * @return string
     */
    public function getUpdatedDate()
    {
        return $this->_updatedDate;
    } // getUpdatedDate

    /**
     * Set External Id
     *
     * @param string $externalId
     *
     * @return Model_Newsletter
     */
    public function setExternalId($externalId)
    {
        $this->_externalId = $externalId;
        return $this;
    } // setExternalId

    /**
     * Set Newsletter Name
     *
     * @param string $newsletterName
     *
     * @return Model_Newsletter
     */
    public function setNewsletterName($newsletterName)
    {
        $this->_newsletterName = $newsletterName;
        return $this;
    } // setNewsletterName

    /**
     * Set Domain
     *
     * @param string $domain
     *
     * @return Model_Newsletter
     */
    public function setDomain($domain)
    {
        $this->_domain = $domain;
        return $this;
    } // setDomain

    /**
     * Set Site Name
     *
     * @param string $siteName
     *
     * @return Model_Newsletter
     */
    public function setSiteName($siteName)
    {
        $this->_siteName = $siteName;
        return $this;
    } // setSiteName

    /**
     * Set From Name
     *
     * @param string $fromName
     *
     * @return Model_Newsletter
     */
    public function setFromName($fromName)
    {
        $this->_fromName = $fromName;
        return $this;
    } // setFromName

    /**
     * Set From Email
     *
     * @param string $fromEmail
     *
     * @return Model_Newsletter
     */
    public function setFromEmail($fromEmail)
    {
        $this->_fromEmail = $fromEmail;
        return $this;
    } // setFromEmail

    /**
     * Set Created Date
     *
     * @param string $createdDate
     *
     * @return Model_Newsletter
     */
    public function setCreatedDate($createdDate)
    {
        $this->_createdDate = $createdDate;
        return $this;
    } // setCreatedDate

    /**
     * Set Updated Date
     *
     * @param string $updatedDate
     *
     * @return Model_Newsletter
     */
    public function setUpdatedDate($updatedDate)
    {
        $this->_updatedDate = $updatedDate;
        return $this;
    } // setUpdatedDate

    /**
     * Get newsletter by external Id
     *
     * @param string $externalId
     *
     * @return Model_Newsletter | null
     */
    public static function getByExternalId($externalId)
    {
        $result = null;
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $select = $dbh->select();
        $o = new Model_Newsletter();
        $select->from($o->_tableName, '*')
            ->where('externalId = ?', $externalId);
        $row = $select->query()->fetch();
        if ($row) {
            $result = new Model_Newsletter($row);
        }
        return $result;
    } // getByExternalId
}
