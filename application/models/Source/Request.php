<?php
/**
 * Model_Source_Request
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Source
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * API request parameters
 */
class Model_Source_Request
{
    /**
     * Sort by date
     *
     * @var string
     */
    const SORT_DATE = 'date';

    /**
     * Sort by relevance
     *
     * @var string
     */
    const SORT_RELEVANCE = 'rel';

    /**
     * Search query
     *
     * @var string
     */
    private $_query;

    /**
     * Search location
     *
     * @var string
     */
    private $_location;

    /**
     * User location
     *
     * @var string
     */
    private $_userLocation;

    /**
     * Range start
     *
     * @var int
     */
    private $_start;

    /**
     * Number of listings to retrieve
     *
     * @var integer
     */
    private $_limit;

    /**
     * Sort order (API specific)
     *
     * @var string
     */
    private $_sort;

    /**
     * Search proximity in miles
     *
     * @var integer
     */
    private $_proximity;

    /**
     * Search time period in days
     *
     * @var integer
     */
    private $_period;

    /**
     * Site name for organic searches
     *
     * @var string;
     */
    private $_site;

    /**
     * Get search query
     *
     * @return string
     */
    public function getQuery()
    {
        return $this->_query;
    } // getQuery

    /**
     * Set search query
     *
     * @param string $query
     *
     * @return Model_Source_Request
     */
    public function setQuery($query)
    {
        $this->_query = $query;
        return $this;
    } // setQuery

    /**
     * Get search location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->_location;
    } // getLocation

    /**
     * Set search location
     *
     * @param string $location
     *
     * @return Model_Source_Request
     */
    public function setLocation($location)
    {
        $this->_location = $location;
        return $this;
    } // setLocation

    /**
     * Get user location
     *
     * @return string
     */
    public function getUserLocation()
    {
        return $this->_userLocation;
    } // getUserLocation

    /**
     * Set user location
     *
     * @param string $userLocation
     *
     * @return Model_Source_Request
     */
    public function setUserLocation($userLocation)
    {
        $this->_userLocation = $userLocation;
        return $this;
    } // setUserLocation

    /**
     * Get range start
     *
     * @return integer
     */
    public function getStart()
    {
        return $this->_start;
    } // getStart

    /**
     * Set range start
     *
     * @param integer $start
     *
     * @return Model_Source_Request
     */
    public function setStart($start)
    {
        $this->_start = $start;
        return $this;
    } // setStart

    /**
     * Get limit
     *
     * @return integer
     */
    public function getLimit()
    {
        return $this->_limit;
    } // getLimit

    /**
     * Set limit
     *
     * @param integer $limit
     *
     * @return Model_Source_Request
     */
    public function setLimit($limit)
    {
        $this->_limit = $limit;
        return $this;
    } // setLimit

    /**
     * Get sort
     *
     * @return string
     */
    public function getSort()
    {
        return $this->_sort;
    } // getSort

    /**
     * Set sort
     *
     * @param string $sort
     *
     * @return Model_Source_Request
     */
    public function setSort($sort)
    {
        $this->_sort = $sort;
        return $this;
    } // setSort

    /**
     * Get proximity
     *
     * @return integer
     */
    public function getProximity()
    {
        return $this->_proximity;
    } // getProximity

    /**
     * Set proximity
     *
     * @param integer $proximity
     *
     * @return Model_Source_Request
     */
    public function setProximity($proximity)
    {
        $this->_proximity = $proximity;
        return $this;
    } // setProximity

    /**
     * Get period
     *
     * @return integer
     */
    public function getPeriod()
    {
        return $this->_period;
    } // getPeriod

    /**
     * Set period
     *
     * @param number $period
     *
     * @return Model_Source_Request
     */
    public function setPeriod($period)
    {
        $this->_period = $period;
        return $this;
    } // setPeriod

    /**
     * Get site for organic searched
     *
     * @return string
     */
    public function getSite()
    {
        return $this->_site;
    } // getSite

    /**
     * Set site for organic searches
     *
     * @param string $site
     *
     * @return Model_Source_Request
     */
    public function setSite($site)
    {
        $this->_site = $site;
        return $this;
    } // setSite
}
