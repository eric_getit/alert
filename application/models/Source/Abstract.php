<?php
/**
 * Model_Source_Abstract
 *
 * PHP Version 5.3
 *
 * @category Abstraction
 * @package  Model\Source
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Abstract class for data provider classes
 */
abstract class Model_Source_Abstract implements Model_Source_Interface
{
    /**
     * Timeout to read data from external source
     *
     * @var integer
     */
    const TIMEOUT = 60;

    /**
     * API requet
     *
     * @var Model_Source_Request
     */
    protected $_request;

    /**
     * Extarnal API record ID
     *
     * @var integer
     */
    protected $_sourceId;

    /**
     * External Source name
     *
     * @var string
     */
    protected $_sourceName;

    /**
     * Base Source URL for prepare query
     *
     * @var string
     */
    protected $_sourceUrl;

    /**
     * Source access key
     *
     * @var string
     */
    protected $_sourceKey;

    /**
     * Build provider class for given parameters
     *
     * @return self
     */
    public function __construct($params)
    {
        $this->_sourceId = $params['sourceId'];
        $this->_sourceName = $params['sourceName'];
        $this->_sourceUrl = $params['sourceUrl'];
        $this->_sourceKey = $params['sourceKey'];
    } // __construct

    /**
     * Get response object for current external source request
     *
     * @return Model_Source_Response
     */
    public function getRequest()
    {
        return $this->_request;
    } // getRequest

    /**
     * Set request used to query external data source
     *
     * @see Model_Source_Interface::setRequest()
     *
     * @param Model_Source_Request $request
     *
     * @return self
     */
    public function setRequest($request)
    {
        $this->_request = $request;
        return $this;
    } // setRequest

    /**
     * Does HTTP request to external Source with given parameters
     *
     * @param string $url
     * @param array $config
     *
     * @return string
     */
    protected function _httpRequest($url, $config)
    {
        $client = new Zend_Http_Client($url, $config);
        $body = $client->request()->getBody();

        return $body;
    } // _httpRequest
}
