<?php
/**
 * Model_Source_Response
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Source
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * External API response
 */
class Model_Source_Response
{
    /**
     * If the request was successful
     *
     * @var boolean
     */
    private $_success;

    /**
     * Error message if any
     *
     * @var string
     */
    private $_errorMessage;

    /**
     * Total number of results for given query(all)
     *
     * @var int
     */
    private $_totalCount;

    /**
     * Listings returned from external source
     *
     * @var array
     */
    private $_listings;

    /**
     * Get response result
     *
     * @return bool
     */
    public function getSuccess()
    {
        return $this->_success;
    } // getSuccess

    /**
     * Get error message
     *
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->_errorMessage;
    } // getErrorMessage

    /**
     * Get total results count
     *
     * @return integer
     */
    public function getTotalCount()
    {
        return $this->_totalCount;
    } // getTotalCount

    /**
     * Get listings found
     *
     * @return array
     */
    public function getListings()
    {
        return $this->_listings;
    } // getListings

    /**
     * Set Success
     *
     * @param boolean $success
     *
     * @return Model_Source_Response
     */
    public function setSuccess($success)
    {
        $this->_success = $success;
        return $this;
    } // setSuccess

    /**
     * Set Error Message
     *
     * @param string $errorMessage
     *
     * @return Model_Source_Response
     */
    public function setErrorMessage($errorMessage)
    {
        $this->_errorMessage = $errorMessage;
        return $this;
    } // setErrorMessage

    /**
     * Set Total Count
     *
     * @param number $totalCount
     *
     * @return Model_Source_Response
     */
    public function setTotalCount($totalCount)
    {
        $this->_totalCount = $totalCount;
        return $this;
    } // setTotalCount

    /**
     * Set Listings
     *
     * @param array $listings
     *
     * @return Model_Source_Response
     */
    public function setListings($listings)
    {
        $this->_listings = $listings;
        return $this;
    } // setListings
}