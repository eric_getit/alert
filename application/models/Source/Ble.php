<?php
/**
 * Model_Source_Ble
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Source
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Listing API implementation for BLE API.
 */
class Model_Source_Ble extends Model_Source_Abstract
{
    /**
     * Query external data source and return results
     *
     * @return Model_Source_Response
     */
    public function getResponse()
    {
        $response = new Model_Source_Response();
        try {
            $limit = $this->getRequest()->getLimit();

            $organicUrl = $this->_getSourceUrl(Model_Listing::TYPE_ORGANIC, $limit);
            $organicResultsBody = $this->_httpRequest($organicUrl, null);
            $organicResults = unserialize($organicResultsBody);

            if ($organicResults['count'] == 0) {
                $totalCount = 0;
                $listings = array();
            } else {
                $totalCount = $organicResults['count'];
                $limit -= count($organicResults['rows']);
                $listings = array();
                foreach ($organicResults['rows'] as $item) {
                    $listings[] = $this->_parseItem($item, Model_Listing::TYPE_ORGANIC);
                }
            }

            if ($limit > 0) {
                $backfillUrl = $this->_getSourceUrl(Model_Listing::TYPE_BACKFILL, $limit);
                $backfillResultsBody = $this->_httpRequest($backfillUrl, null);
                $backfillResults = unserialize($backfillResultsBody);

                if ($backfillResults['count'] > 0) {
                    $totalCount += $organicResults['count'];
                    foreach ($backfillResults['rows'] as $item) {
                        $listings[] = $this->_parseItem($item, Model_Listing::TYPE_BACKFILL);
                    }
                }
            }

            $response->setSuccess(true);
            $response->setTotalCount($totalCount);
            $response->setListings($listings);

        } catch (Exception $e) {
            $response->setSuccess(false);
            $response->setErrorMessage($e->getMessage());
        }

        return $response;
    } // getResponse

    /**
     * Get external source HTTP request url
     *
     * @param string $type
     * @param integer $limit
     *
     * @return string
     */
    protected function _getSourceUrl($type, $limit)
    {
        $request = $this->getRequest();

        $url = $this->_sourceUrl;
        $url .= '?format=PHP';
        $url .= '&key=' . $this->_sourceKey;

        if ($request->getQuery()) {
            $url .= '&q=' . urlencode(Model_Utils::escapeString($request->getQuery()));
        }
        if ($request->getLocation()) {
            $url .= '&location=' . urlencode($request->getLocation());
        }
        if ($request->getUserLocation()) {
            $url .= '&userLocation=' . urlencode($request->getUserLocation());
        }
        if ($request->getPeriod()) {
            $url .= '&period=' . $request->getPeriod();
        }
        if ($request->getSort() == Model_Source_Request::SORT_DATE) {
            $url .= '&order=date';
        } elseif ($request->getSort() == Model_Source_Request::SORT_RELEVANCE) {
            $url .= '&order=rel';
        }
        if ($type == Model_Listing::TYPE_ORGANIC) {
            $url .= '&type=organic';
            $url .= '&site=' . $request->getSite();
        } elseif ($type == Model_Listing::TYPE_BACKFILL) {
            $url .= '&type=external';
        }

        $url .= '&radius=' . $request->getProximity();
        $url .= '&offset=' . $request->getStart();
        $url .= '&limit=' . $limit;

        return $url;
    } // _getSourceUrl

    /**
     * Create item value from API results
     *
     * @param array $item
     * @param string $type
     *
     * @return Model_Listing
     */
    private function _parseItem($item, $type)
    {
        $listing = new Model_Listing();
        $listing->setSource($this->_sourceName);
        $listing->setId($item['id']);
        $listing->setDate($item['date']);
        $listing->setTitle($item['title']);
        $listing->setExternalUrl($item['externalUrl']);
        $listing->setExternalSource($item['source']);
        $listing->setLocation($this->_createLocation($item));
        $listing->setCompany($item['company']);
        $listing->setType($type);
        return $listing;
    } //_parseEntry

    /**
     * Create location for the given item
     *
     * @param array $item
     *
     * @return string
     */
    private function _createLocation($item)
    {
        $location = '';
        if (isset($item['locationCity'])) {
            $location = $item['locationCity'];
        }
        if (isset($item['locationState'])) {
            if ($location == '') {
                $location = Model_StateList::getName($item['locationState']);
            } else {
                $location .= ', ' . $item['locationState'];
            }
        }
        return $location;
    } // _createLocation
}
