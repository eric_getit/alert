<?php
/**
 * Model_Source_Interface
 *
 * PHP Version 5.3
 *
 * @category Interface
 * @package  Model\Source
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * External data source interface
 */
interface Model_Source_Interface
{
    /**
     * Set request used to query external data source
     *
     * @param Model_Source_Request $request
     */
    public function setRequest($request);

    /**
     * Get request used to query external data source
     *
     * @return Model_Source_Request
     */
    public function getRequest();

    /**
     * Query external data source and return results
     *
     * @return Model_Source_Response
     */
    public function getResponse();
}
