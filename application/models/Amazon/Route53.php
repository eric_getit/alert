<?php
/**
 * Model_Amazon_Route53
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Amazon
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Manage Amazon Route 53 information.
 *
 * @see http://docs.aws.amazon.com/Route53/latest/APIReference/Welcome.html
 */
class Model_Amazon_Route53
{
    /**
     * Route 53 API client
     *
     * @var \Aws\Route53\Route53Client
     */
    private $_client;

    /**
     * Build Route53 API client
     *
     * @param array $awsConfig
     *
     * @return self
     */
    public function __construct($awsConfig)
    {
        $this->_client = \Aws\Route53\Route53Client::factory($awsConfig);
    } // __construct

    /**
     * Get list of all hosted zones
     * TODO : The list is restricted by 100 items now
     *
     * @see http://docs.aws.amazon.com/Route53/latest/APIReference/API_ListResourceRecordSets.html
     *
     * @return array
     */
    public function listHostedZones()
    {
        return $this->_client->ListHostedZones()->get('HostedZones');
    } // listHostedZones

    /**
     * Get hosted zone Id by zone name
     *
     * @param string $name
     * @return string
     */
    public function getHostedZoneId($name)
    {
        $zoneId = null;
        $zones = $this->listHostedZones();
        foreach ($zones as $zone) {
            if ($zone['Name'] == $name) {
                $zoneId = str_replace('/hostedzone/', '', $zone['Id']);
            }
        }
        return $zoneId;
    } // getHostedZoneId

    /**
     * Change information in DNS for given Hosted Zone and resource name.
     * Return change Id.
     *
     * @param string $zoneId Hosted Zone Id
     * @param string $name Record name
     * @param string $type New value for record type
     * @param string $ttl New value for TTL
     * @param string $value New record value
     *
     * @throws Exception
     *
     * @return string
     */
    public function changeRecord($zoneId, $name, $type, $ttl, $value)
    {
        $changes = array();

        $queryResult = $this->_client->listResourceRecordSets(array(
            'HostedZoneId' => $zoneId,
            'StartRecordName' => $name,
            'MaxItems' => 1,
        ));
        $records = $queryResult->get('ResourceRecordSets');
        if (count($records) == 1) {
            $record = $records[0];
            if ($record['Name'] == $name) {
                $deleteChange = array(
                        'Action' => 'DELETE',
                        'ResourceRecordSet' => $record,
                );
                $changes[] = $deleteChange;
            }
        }

        $createChange = array(
                'Action' => 'CREATE',
                'ResourceRecordSet' => array(
                        'Name' => $name,
                        'Type' => $type,
                        'TTL' => $ttl,
                        'ResourceRecords' => array(array('Value' => $value)),
                ),
        );
        $changes[] = $createChange;

        $changeResult = $this->_client->changeResourceRecordSets(array(
                'HostedZoneId' => $zoneId,
                'ChangeBatch' => array (
                        'Changes' => $changes,
                ),
        ));
        $changeInfo = $changeResult->get('ChangeInfo');
        return $changeInfo['Id'];
    } // changeRecord

    /**
     * Get change status
     *
     * @param string $changeId
     *
     * @return string
     */
    public function getChangeStatus($changeId)
    {
        $getResults = $this->_client->getChange(array(
                'Id' => $changeId,
        ));
        $changeInfo = $getResults->get('ChangeInfo');
        return $changeInfo['Status'];
    } // getChangeStatus
}
