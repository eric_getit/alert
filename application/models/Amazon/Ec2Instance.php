<?php
/**
 * Model_Amazon_Ec2Instance
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Amazon
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Manage Amazon Ec2 instances
 *
 * @see http://aws.amazon.com/documentation/ec2/
 */
class Model_Amazon_Ec2Instance
{
    /**
     * EC2 API client
     *
     * @var Aws/Ec2/Ec2Client
     */
    private $_client;

    /**
     * Build EC2 instance manager
     *
     * @param array $awsConfig
     *
     * @return self
     */
    public function __construct($awsConfig)
    {
        $this->_client = \Aws\Ec2\Ec2Client::factory($awsConfig);
    } // __construct

    /**
     * Start instance
     *
     * @param string $instanceId Amazon EC2 instance Id
     *
     * @return string Current instance state
     */
    public function start($instanceId)
    {
        $result = $this->_client->startInstances(array(
                'InstanceIds' => array($instanceId),
        ));
        $startInstances = $result->get('StartingInstances');
        return $startInstances[0]['CurrentState']['Name'];
    } // start

    /**
     * Stop instance
     *
     * @param string $instanceId Amazon EC2 instance Id
     *
     * @return string Current instance state
     */
    public function stop($instanceId)
    {
        $result = $this->_client->stopInstances(array(
                'InstanceIds' => array($instanceId),
        ));

        $stoppingInstances = $result->get('StoppingInstances');
        return $stoppingInstances[0]['CurrentState']['Name'];
    } // stop

    /**
     * Get instance state
     *
     * @param string $instanceId Amazon EC2 instance Id
     *
     * @return string Current instance state
     */
    public function getState($instanceId)
    {
        $result = $this->_client->describeInstances(array(
                'InstanceIds' => array($instanceId),
        ));
        $reservations = $result->get('Reservations');
        $state = $reservations[0]['Instances'][0]['State']['Name'];
        return $state;
    } // getState
}
