<?php
/**
 * Model_Amazon_InstanceMetadata
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Amazon
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Retrieve Amazon Elastic Compute Cloud instance metadata
 * See http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AESDG-chapter-instancedata.html
 */
class Model_Amazon_InstanceMetadata
{
    /**
     * Instance AMI id
     *
     * @var string
     */
    const AMIID = 'amiid';

    /**
     * Instance hostname
     *
     * @var string
     */
    const HOSTNAME = 'hostname';

    /**
     * Instance id
     *
     * @var string
     */
    const INSTANCE_ID = 'instance-id';

    /**
     * Instance type
     *
     * @var string
     */
    const INSTANCE_TYPE = 'instance-type';

    /**
     * Instance local hostname
     *
     * @var string
     */
    const LOCAL_HOSTNAME = 'local-hostname';

    /**
     * Instance local IPv4 address
     *
     * @var string
     */
    const LOCAL_IP = 'local-ipv4';

    /**
     * Instance public hostname
     *
     * @var string
     */
    const PUBLIC_HOSTNAME = 'public-hostname';

    /**
     * Instance public IPv4 address
     *
     * @var string
     */
    const PUBLIC_IP = 'public-ipv4';

    /**
     * Instamce security group
     *
     * @var string
     */
    const SECURITY_GROUPS = 'security-groups';


    /**
     * Get metadata item for the running instance.
     * Works only on Amazon EC2 instances.
     *
     * @param string $item
     *
     * @return string
     */
    public static function get($item)
    {
        $curl = curl_init('http://169.254.169.254/latest/meta-data/' . $item);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $value = curl_exec($curl);
        return $value;
    } // get
}
