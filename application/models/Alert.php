<?php
/**
 * Model_Alert
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */
class Model_Alert extends Model_AbstractModel
{
    /**
     * Default proximity for location search
     *
     * @var int
     */
    const DEFAULT_PROXIMITY = 25;

    /**
     * Daily alerts
     *
     * @var string
     */
    const TYPE_DAILY = 'DAILY';

    /**
     * Weekly alerts
     *
     * @var string
     */
    const TYPE_WEEKLY = 'WEEKLY';

    /**
     * Alert is active
     *
     * @var string
     */
    const STATUS_ACTIVE = 'ACTIVE';

    /**
     * Alert is suspended
     *
     * @var string
     */
    const STATUS_SUSPEND = 'SUSPEND';

    /**
     * Alert is unsubscribed by the user
     *
     * @var string
     */
    const STATUS_UNSUBSCRIBED_USER = 'UNSUBSCRIBED_USER';

    /**
     * Alert is unsubscribed by admin
     *
     * @var string
     */
    const STATUS_UNSUBSCRIBED_ADMIN = 'UNSUBSCRIBED_ADMIN';

    /**
     * Alert is unsubscribed using API
     *
     * @var string
     */
    const STATUS_UNSUBSCRIBED_API = 'UNSUBSCRIBED_API';

    /**
     * Alert is deactivated (inactive)
     *
     * @var string
     */
    const STATUS_DEACTIVE = 'DEACTIVE';

    /**
     * Alert is pending (inactive)
     *
     * It means that we've created an alert for an email
     * that already exist in the system. We don't currently support
     * multiple alerts in one message, and when we do these can
     * be moved out of this status, and into active.
     *
     * I'm doing this to store the multiple alerts I'm in the process of
     * adding into the system in the next week.
     *
     * @var string
     */
    const STATUS_PENDING = 'PENDING';

    /**
     * Alert is admin pending (inactive)
     *
     * I've added this status to allow me to change some alerts globally,
     * while knowing which were the original.
     *
     * @var string
     */
    const STATUS_ADM_PENDING = 'ADM_PENDING';

    /**
     * Alert email has been bounced (PERMANENT)
     *
     * @var string
     */
    const STATUS_SUSPENDED_AS_BOUNCE_PERMANENT = 'BOUNCE_PERMANENT';

    /**
     * Alert email has been bounced (TRANSIENT)
     *
     * @var string
     */
    const STATUS_SUSPENDED_AS_BOUNCE_TRANSIENT = 'BOUNCE_TRANSIENT';

    /**
     * We have received complain for the alert email message
     *
     * @var string
     */
    const STATUS_SUSPENDED_AS_COMPLAINT = 'COMPLAINT';

    /**
     * How many times in a row we should get bounces to stop sending the alert
     *
     * @var integer
     */
    const BOUNCE_LIMIT = 5;

    /**
     * How many erros with get in a row to stop sending the alert
     *
     * @var integer
     */
    const FAILURE_LIMIT = 4;

    /**
     * Default distance
     *
     * @var integer
     */
    const DEFAULT_DISTANCE = 25;

    /**
     * Alert ID (primary key)
     *
     * @var integer
     */
    protected $_alertId;

    /**
     * Alert ID used in references
     *
     * @var string
     */
    protected $_alertSecureId;

    /**
     * Alert type (daily or weekly)
     *
     * @var string
     */
    protected $_alertType;

    /**
     * Contact reference
     *
     * @var integer
     */
    protected $_contactId;

    /**
     * Contact model for the alert.
     *
     * @var Model_Contact
     */
    protected $_contact;

    /**
     * Site reference
     *
     * @var integer
     */
    protected $_siteId;

    /**
     * Site model for the alert.
     *
     * @var Model_Site
     */
    protected $_site;

    /**
     * Alert search location
     *
     * @var string
     */
    protected $_location;

    /**
     * Alert search keywords
     *
     * @var string
     */
    protected $_queryTerms;

    /**
     * Distance for search
     *
     * @var integer
     */
    protected $_distance;

    /**
     * Subscriber IP address
     *
     * @var string
     */
    protected $_ip;

    /**
     * Alert creation date and time
     *
     * @var string
     */
    protected $_createdDate;

    /**
     * Alert last modification date and time
     *
     * @var string
     */
    protected $_updatedDate;

    /**
     * Last date and time when we tried to send search results email for the alert
     *
     * @var string
     */
    protected $_lastProcessDate;

    /**
     * Last date and time when we have sent search results email for the alert
     *
     * @var string
     */
    protected $_lastSendDate;

    /**
     * Next time we plan to process the alert (check search results)
     *
     * @var string
     */
    protected $_nextProcessDate;

    /**
     * Alert status
     *
     * @var string
     */
    protected $_alertStatus;

    /**
     * Alert UTM source parameter
     *
     * @var string
     */
    protected $_utmSource;

    /**
     * Alert UTM medium parameter
     *
     * @var string
     */
    protected $_utmMedium;

    /**
     * Alert UTM term parameter
     *
     * @var string
     */
    protected $_utmTerm;

    /**
     * Alert UTM content parameter
     *
     * @var string
     */
    protected $_utmContent;

    /**
     * Alert UTM campaign parameter
     *
     * @var string
     */
    protected $_utmCampaign;

    /**
     * Number of recent bounces happened in a row
     *
     * @var integer
     */
    protected $_bouncesCount;

    /**
     * Number of complaints received
     *
     * @var integer
     */
    protected $_complaintsCount;

    /**
     * Last date and time when the has has been bounced
     *
     * @var string
     */
    protected $_lastBounceDate;

    /**
     * Last date and time when we have received complain for the alert
     *
     * @var string
     */
    protected $_lastComplaintDate;

    /**
     * ID for the last sent message via SES
     *
     * @var string
     */
    protected $_sesMessageId;

    /**
     * Valid location or no
     *
     * @var boolean
     */
    protected $_isLocationValid;

    /**
     * Data base table name for the model
     *
     * @var string
     */
    protected $_tableName = 'alerts';

    /**
     * Primary key field name
     *
     * @var string
     */
    protected $_primaryKey = 'alertId';

    /**
     * Data base fields
     *
     * @var array
     */
    protected $_tableFields = array(
        'alertId' => '_alertId',
        'alertSecureId' => '_alertSecureId',
        'alertType' => '_alertType',
        'alertStatus' => '_alertStatus',
        'contactId' => '_contactId',
        'siteId' => '_siteId',
        'location' => '_location',
        'queryTerms' => '_queryTerms',
        'distance' => '_distance',
        'ip' => '_ip',
        'lastSendDate' => '_lastSendDate',
        'lastProcessDate' => '_lastProcessDate',
        'nextProcessDate' => '_nextProcessDate',
        'createdDate' => '_createdDate',
        'updatedDate' => '_updatedDate',
        'utmSource' => '_utmSource',
        'utmMedium' => '_utmMedium',
        'utmTerm' => '_utmTerm',
        'utmContent' => '_utmContent',
        'utmCampaign' => '_utmCampaign',
        'bouncesCount' => '_bouncesCount',
        'complaintsCount' => '_complaintsCount',
        'lastBounceDate' => '_lastBounceDate',
        'lastComplaintDate' => '_lastComplaintDate',
        'sesMessageId' => '_sesMessageId',
        'isLocationValid' => '_isLocationValid',
    );

    /**
     * Get site ID for the alert
     *
     * @return integer
     */
    public function getSiteId()
    {
        return $this->_siteId;
    } // getSiteId

    /**
     * Get site object for the alert
     *
     * @return Model_Site
     */
    public function getSite()
    {
        if ($this->_site == null && !empty($this->_siteId)) {
            $this->_site = new Model_Site($this->_siteId);
        }
        return $this->_site;
    } // getSite

    /**
     * Set Site ID
     *
     * @param integer $siteId Site ID
     *
     * @return Model_Alert
     */
    public function setSiteId($siteId)
    {
        $this->_siteId = $siteId;
        return $this;
    } // setSiteId

    /**
     * Get Distance
     *
     * @return integer
     */
    public function getDistance()
    {
        return $this->_distance;
    } // getDistance

    /**
     * Set Distance
     *
     * @param integer $distance Distance
     *
     * @return Model_Alert
     */
    public function setDistance($distance)
    {
        $this->_distance = $distance;
        return $this;
    } // setDistance

    /**
     * Get number of listings to send in one email message
     *
     * @return integer
     */
    public function getListingsPerEmailCount()
    {
        // TODO: We might want to get this from config
        return 10;
    } // getListPerEmailCount

    /**
     * SES Message ID
     *
     * @return string
     */
    public function getSesMessageId()
    {
        return $this->_sesMessageId;
    } // getSesMessageId

    /**
     * Set SES Message ID
     *
     * @param string $sesMessageId SES Message ID
     *
     * @return Model_Alert
     */
    public function setSesMessageId($sesMessageId)
    {
        $this->_sesMessageId = $sesMessageId;
        return $this;
    }

    /**
     * Get Bounce Count
     *
     * @return integer
     */
    public function getBouncesCount()
    {
        return $this->_bouncesCount;
    } // getBouncesCount

    /**
     * Get Complaint Count
     *
     * @return integer
     */
    public function getComplaintsCount()
    {
        return $this->_complaintsCount;
    } // getComplaintsCount

    /**
     * Get Last Bounce Date
     *
     * @return string
     */
    public function getLastBounceDate()
    {
        return $this->_lastBounceDate;
    } // getLastBounceDate

    /**
     * Get Last Complaint Date
     *
     * @return string
     */
    public function getLastComplaintDate()
    {
        return $this->_lastComplaintDate;
    } // getLastComplaintDate

    /**
     * Set Bounce Count
     *
     * @param integer $bouncesCount Bounce Count
     *
     * @return Model_Alert
     */
    public function setBouncesCount($bouncesCount)
    {
        $this->_bouncesCount = $bouncesCount;
        return $this;
    } // setBouncesCount

    /**
     * Set Complaint Count
     *
     * @param integer $complaintsCount Complaint Count
     *
     * @return Model_Alert
     */
    public function setComplaintsCount($complaintsCount)
    {
        $this->_complaintsCount = $complaintsCount;
        return $this;
    } // setComplaintsCount

    /**
     * Set Last Bounce Date
     *
     * @param string $lastBounceDate Last Bounce Date
     *
     * @return Model_Alert
     */
    public function setLastBounceDate($lastBounceDate)
    {
        $this->_lastBounceDate = $lastBounceDate;
        return $this;
    } // setLastBounceDate

    /**
     * Set Last Complaint Date
     *
     * @param string $lastComplaintDate Last Complaint Date
     *
     * @return Model_Alert
     */
    public function setLastComplaintDate($lastComplaintDate)
    {
        $this->_lastComplaintDate = $lastComplaintDate;
        return $this;
    } // setLastComplaintDate

    /**
     * Get UTM Source
     *
     * @return string
     */
    public function getUtmSource()
    {
        return $this->_utmSource;
    } // getUtmSource

    /**
     * Get UTM Medium
     *
     * @return string
     */
    public function getUtmMedium()
    {
        return $this->_utmMedium;
    } // getUtmMedium

    /**
     * Get UTM Term
     *
     * @return string
     */
    public function getUtmTerm()
    {
        return $this->_utmTerm;
    } // getUtmTerm

    /**
     * Get UTM Content
     *
     * @return string
     */
    public function getUtmContent()
    {
        return $this->_utmContent;
    } // getUtmContent

    /**
     * Get UTM Campaign
     *
     * @return string
     */
    public function getUtmCampaign()
    {
        return $this->_utmCampaign;
    } // getUtmCampaign

    /**
     * Set UTM Source
     *
     * @param string $utmSource UTM Source
     *
     * @return Model_Alert
     */
    public function setUtmSource($utmSource)
    {
        $this->_utmSource = $utmSource;
        return $this;
    } // setUtmSource

    /**
     * Set UTM Medium
     *
     * @param string $utmMedium UTM Medium
     *
     * @return Model_Alert
     */
    public function setUtmMedium($utmMedium)
    {
        $this->_utmMedium = $utmMedium;
        return $this;
    } // setUtmMedium

    /**
     * Set UTM Term
     *
     * @param string $utmTerm UTM Term
     *
     * @return Model_Alert
     */
    public function setUtmTerm($utmTerm)
    {
        $this->_utmTerm = $utmTerm;
        return $this;
    } // setUtmTerm

    /**
     * Set UTM Content
     *
     * @param string $utmContent UTM Content
     *
     * @return Model_Alert
     */
    public function setUtmContent($utmContent)
    {
        $this->_utmContent = $utmContent;
        return $this;
    } // setUtmContent

    /**
     * Set UTM Campaign
     *
     * @param string $utmCampaign UTM Campaign
     *
     * @return Model_Alert
     */
    public function setUtmCampaign($utmCampaign)
    {
        $this->_utmCampaign = $utmCampaign;
        return $this;
    } // setUtmCampaign

    /**
     * Get Alert Status
     *
     * @return string
     */
    public function getAlertStatus()
    {
        return $this->_alertStatus;
    } // getAlertStatus

    /**
     * Set Alert Status
     *
     * @param string $alertStatus Alert Status
     *
     * @return Model_Alert
     */
    public function setAlertStatus($alertStatus)
    {
        $this->_alertStatus = $alertStatus;
        return $this;
    } // setAlertStatus

    /**
     * Get Alert ID
     *
     * @return integer
     */
    public function getAlertId()
    {
        return $this->_alertId;
    } // getAlertId

    /**
     * Get Alert Secure ID
     *
     * @return string
     */
    public function getAlertSecureId()
    {
        return $this->_alertSecureId;
    } // getAlertSecureId

    /**
     * Get Alert Type
     *
     * @return string
     */
    public function getAlertType()
    {
        return $this->_alertType;
    } // getAlertType

    /**
     * Get Contact ID
     *
     * @return integer
     */
    public function getContactId()
    {
        return $this->_contactId;
    } // getContactId

    /**
     * Get Contact Model
     *
     * @return Model_Contact
     */
    public function getContact()
    {
        if (!empty($this->_contactId) && empty($this->_contact)) {
            $this->_contact = new Model_Contact($this->_contactId);
        }
        return $this->_contact;
    } // getContact

    /**
     * Get Location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->_location;
    } // getLocation

    /**
     * Get Query Terms
     *
     * @return string
     */
    public function getQueryTerms()
    {
        return $this->_queryTerms;
    } // getQueryTerms

    /**
     * Get IP
     *
     * @return string
     */
    public function getIp()
    {
        return $this->_ip;
    } // getIp

    /**
     * Get Created Date
     *
     * @return string
     */
    public function getCreatedDate()
    {
        return $this->_createdDate;
    } // getCreatedDate

    /**
     * Get Last Process Date
     *
     * @return string
     */
    public function getLastProcessDate()
    {
        return $this->_lastProcessDate;
    } // getLastProcessDate

    /**
     * Get Last Send Date
     *
     * @return string
     */
    public function getLastSendDate()
    {
        return $this->_lastSendDate;
    } // getLastSendDate

    /**
     * Get Next Process Date
     *
     * @return string
     */
    public function getNextProcessDate()
    {
        return $this->_nextProcessDate;
    } // getNextProcessDate

    /**
     * Get Update Date
     *
     * @return string
     */
    public function getUpdatedDate()
    {
        return $this->_updatedDate;
    } // getUpdatedDate

    /**
     * Set Alert Secure ID
     *
     * @param string $alertSecureId Alert Secure ID
     *
     * @return Model_Alert
     */
    public function setAlertSecureId($alertSecureId)
    {
        $this->_alertSecureId = $alertSecureId;
        return $this;
    } // setAlertSecureId

    /**
     * Set Alert Type
     *
     * @param string $alertType Alert Type
     *
     * @return Model_Alert
     */
    public function setAlertType($alertType)
    {
        $this->_alertType = $alertType;
        return $this;
    } // setAlertType

    /**
     * Set contact ID
     *
     * @param integer $contactId Contact ID
     *
     * @return Model_Alert
     */
    public function setContactId($contactId)
    {
        $this->_contactId = $contactId;
        $this->_contact = null;
        // Note: we are lazy and do contact initialization
        // $this->_contact = new Model_Contact($this->_contactId);
        // only when contact is requested by getter method
        return $this;
    } // setContactId

    /**
     * Set Location
     *
     * @param string $_location Location
     *
     * @return Model_Alert
     */
    public function setLocation($_location)
    {
        $result = $_location;
        $location = new Model_Location($_location);

        if (!$location->isEmpty()) {
            $result = $location->getCanonical(false);
        } else if ($location->getValid()) {
            $result = '';
        }
        if (!$location->getValid()) {
            $this->setIsLocationValid(0);
        }
        $this->_location = $result;
        return $this;
    } // setLocation

    /**
     * Set Query Terms
     *
     * @param string $queryTerms Query Terms
     *
     * @return Model_Alert
     */
    public function setQueryTerms($queryTerms)
    {
        $this->_queryTerms = $queryTerms;
        return $this;
    } // setQueryTerms

    /**
     * Set IP
     *
     * @param string $ip IP
     *
     * @return Model_Alert
     */
    public function setIp($ip)
    {
        $this->_ip = $ip;
        return $this;
    } // $ip

    /**
     * Set Created Date
     *
     * @param string $createdDate Created Date
     *
     * @return Model_Alert
     */
    public function setCreatedDate($createdDate)
    {
        $this->_createdDate = $createdDate;
        return $this;
    } // setCreatedDate

    /**
     * Set Updated Date
     *
     * @param string $updatedDate Updated Date
     *
     * @return Model_Alert
     */
    public function setUpdatedDate($updatedDate)
    {
        $this->_updatedDate = $updatedDate;
        return $this;
    } // setUpdatedDate

    /**
     * Set Last Process Date
     *
     * @param string $lastProcessDate Last Process Date
     *
     * @return Model_Alert
     */
    public function setLastProcessDate($lastProcessDate)
    {
        $this->_lastProcessDate = $lastProcessDate;
        return $this;
    } // setLastProcessDate

    /**
     * Set Last Send Date
     *
     * @param string $lastSendDate Last Send Date
     *
     * @return Model_Alert
     */
    public function setLastSendDate($lastSendDate)
    {
        $this->_lastSendDate = $lastSendDate;
        return $this;
    } // setLastSendDate

    /**
     * Set Next Process Date
     *
     * @param string $nextProcessDate Next Process Date
     *
     * @return Model_Alert
     */
    public function setNextProcessDate($nextProcessDate)
    {
        $this->_nextProcessDate = $nextProcessDate;
        return $this;
    } // setNextProcessDate

    /**
     * Fetch/Get source model for the alert
     *
     * @return Model_Source_Abstract
     *
     * @throws Exception
     */
    public function getSource()
    {
        // TODO : Right now we take the first model configured in the sources table
        // What would be the logic here?

        $dbh = Zend_Db_Table::getDefaultAdapter();
        $sql = 'SELECT * FROM sources LIMIT 1';
        $row = $dbh->fetchRow($sql, array(), Zend_Db::FETCH_ASSOC);

        if (empty($row)) {
            throw new Exception(
                'Model_Alert::getSource Failed to load source definition.'
            );
        }

        $model = 'Model_Source_' . $row['sourceClass'];
        if (class_exists($model)
            && is_subclass_of($model, 'Model_Source_Abstract')
        ) {
            /* @var $source Model_Source_Abstract */
            $source = new $model($row);
        } else {
            throw new Exception(
                'Model_Alert::getSource Failed to load source class ' . $model
            );
        }

        return $source;
    } // getSource

    /**
     * Check if we have any scheduled alerts
     *
     * @return boolean
     */
    public static function hasScheduledAlerts()
    {
        $sql = 'SELECT COUNT(*) FROM alerts
                INNER JOIN contacts USING (contactId)
                WHERE alertStatus = "ACTIVE"
                AND contacts.contactStatus = "ACTIVE"
                AND isLocationValid
                AND (
                        (queryTerms IS NOT NULL AND queryTerms != "")
                    OR  (location IS NOT NULL AND location != "")
                )
                AND nextProcessDate <= ?';
        $bind = array();
        $bind[] = Model_Utils::sqlNow();
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $alertsCount = $dbh->fetchOne($sql, $bind);
        return $alertsCount > 0;
    } // hasScheduledAlerts

    /**

     * Generate and set next process date
     *
     * @return void
     */
    public function updateNextProcessDate()
    {
        if ($this->_alertType == Model_Alert::TYPE_DAILY ) {
            $interval = 1;
        } else {
            $interval = 7;
        }

        $nextProcessDay = new DateTime();
        $nextProcessDay->add(new DateInterval('P' . $interval . 'D'));

        $this->_nextProcessDate = $nextProcessDay->format('Y-m-d');
    } // updateNextProcessDate

    /**
     * Update last_process_date with current date and time
     *
     * @return void
     */
    public function updateLastProcessDate()
    {
        $date = new DateTime();
        $this->setLastProcessDate($date->format('Y-m-d H:i:s'));
    } // updateLastProcessDate

    /**
     * Update last send date with current date and time
     *
     * @return void
     */
    public function updateLastSendDate()
    {
        $date = new DateTime();
        $this->setLastSendDate($date->format('Y-m-d H:i:s'));
    } // updateLastSendDate

    /**
     * Insert object into DB
     *
     * @return void
     */
    public function insert()
    {
        $dt = new DateTime();
        $date = $dt->format('Y-m-d H:i:s');
        $this->setCreatedDate($date);
        $this->setUpdatedDate($date);
        if ($this->getNextProcessDate() == null) {
            $dt->add(new DateInterval('P1D'));
            $dt->setTime(5, 0, 0);
            $this->setNextProcessDate($dt->format('Y-m-d H:i:s'));
        }
        return parent::insert();
    }

    /**
     * Fetch Message based on SES Message ID
     *
     * @param string $sesMessageId SES Message ID
     *
     * @return Model_Alert | null
     */
    public static function getBySesMessageId($sesMessageId)
    {
        $alert = null;
        if (!empty($sesMessageId)) {
            $alert = new Model_Alert();
            $dbh = Zend_Db_Table::getDefaultAdapter();
            $select = $dbh->select();
            $select->from($alert->_tableName, '*')
                ->where('sesMessageId = ?', $sesMessageId);
            $row = $select->query()->fetch();
            if ($row) {
                $alert->fromArray($row);
            } else {
                $alert = null;
            }
        }
        return $alert;
    } // getBySesMessageId

    /**
     * Fetch Alert by Secure ID
     *
     * @param string $secureId Secure ID
     *
     * @return Model_Alert | null
     */
    public static function getBySecureId($secureId)
    {
        $alert = new Model_Alert();
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $select = $dbh->select();
        $select->from($alert->_tableName, '*')
            ->where('alertSecureId = ?', $secureId);

        $row = $select->query()->fetch();
        if ($row) {
            $alert->fromArray($row);
        } else {
            $alert = null;
        }

        return $alert;
    } // getBySecureId

    /**
     * Generate the Secure ID for the row
     *
     * @return string
     */
    public function generateSecureId()
    {
        $this->_alertSecureId = Model_Utils::guid();
        return $this->_alertSecureId;
    } // generateSecureId

    /**
     * Update alert using API request parameters
     *
     * @param array $params
     *
     * @return void
     */
    public function updateAlert($params)
    {
        if (array_key_exists('query', $params)) {
            $this->setQueryTerms($params['query']);
        }
        if (array_key_exists('location', $params)) {
            $this->setLocation($params['location']);
        }
        if (array_key_exists('distance', $params)) {
            $this->setDistance($params['distance']);
        }
        if (array_key_exists('alertType', $params)) {
            $this->setAlertType($params['alertType']);
        }
        if (array_key_exists('status', $params)) {
            $this->setAlertStatus($params['status']);
        }
        $this->update();
    } // updateAlert

    /**
     * Get canonical query for alert
     *
     * @param boolean $many
     *
     * @return string
     */
    public function getCanonicalQuery($many = false)
    {
        $query = '';
        $keyword = $many ? 'Jobs' : 'Job';

        if ($this->_queryTerms && $this->_location) {
            $query = $this->getCamelCaseQuery() . ' ' . $keyword . ' in ' . $this->_location;
        } elseif ($this->_queryTerms) {
            $query = $this->getCamelCaseQuery() . ' ' . $keyword;
        } elseif ($this->_location) {
            $query = $keyword . ' in ' . $this->_location;
        }

        return $query;
    } // getCanonicalQuery

    /**
     * Get camelCase from query terms
     *
     * @return string
     */
    public function getCamelCaseQuery()
    {
        return ucwords($this->_queryTerms);
    } // getCamelCaseQuery

    /**
     * Get url for edit alert
     *
     * @return string
     */
    public function getEditUrl()
    {
        $baseUrl = $this->getSite()->getBaseUrl();

        return $baseUrl . '/alerts/edit?alert=' . $this->getAlertSecureId();
    } // getEditUrl

    /**
     * Get url for unsubscribe from alert
     *
     * @return string
     */
    public function getUnsubscribeUrl()
    {
        $baseUrl = $this->getSite()->getBaseUrl();

        return $baseUrl . '/alerts/unsubscribe?alert=' . $this->getAlertSecureId();
    } // getEditUrl

    /**
     * Get url for unsubscribe from all alert
     *
     * @return string
     */
    public function getUnsubscribeAllUrl()
    {
        $baseUrl = $this->getSite()->getBaseUrl();

        return $baseUrl . '/alerts/unsubscribe-all?alert=' . $this->getAlertSecureId();
    } // getEditUrl

    /**
     * Get listings from external sources
     *
     * @see AlertTest::testGetListings
     *
     * @return Model_Source_Response
     */
    public function getListings()
    {
        $request = $this->getRequest();
        $source = $this->getSource();
        $source->setRequest($request);
        $response = $source->getResponse();

        $listings = $response->getListings();
        foreach ($listings as $listing) {
            /* @var $listing Model_Listing */
            if ($listing->getType() == Model_Listing::TYPE_ORGANIC) {
                $urlWithUtms = $this->setUtm($listing->getExternalUrl());
                $listing->setExternalUrl($urlWithUtms);
            }
        }
        return $response;
    } // getListings

    /**
     * Construct API search request for the alert
     *
     * @see AlertTest::testGetRequest
     *
     * @return Model_Source_Request
     */
    public function getRequest()
    {
        $domain = $this->getSite()->getDomain();

        $request = new Model_Source_Request();
        $request->setSort(Model_Source_Request::SORT_DATE);
        $request->setStart(0);
        $request->setQuery($this->_queryTerms);
        $request->setSite($domain);
        $request->setLimit($this->getListingsPerEmailCount());

        $location = new Model_Location($this->_location);
        if (!$location->isEmpty()) {
            $alertLocation = $location->getCanonical(false);
            $request->setLocation($alertLocation);
            if ($this->_distance !== null) {
                $request->setProximity($this->_distance);
            } else {
                $request->setProximity(self::DEFAULT_PROXIMITY);
            }
        } else {
            $userLocation = $this->getContact()->getLocation();
            if ($userLocation != null) {
                $request->setUserLocation($userLocation);
            }
        }

        if ($this->_alertType == Model_Alert::TYPE_DAILY) {
            $request->setPeriod(1);
        } elseif ($this->_alertType == Model_Alert::TYPE_WEEKLY) {
            $request->setPeriod(7);
        }

        return $request;
    } // getRequest

    /**
     * Set utm codes for url
     *
     * @todo I don't like all the if's - was going to adjust it more, but didn't
     *
     * @param string $url
     *
     * @return string
     */
    public function setUtm($url)
    {
        /* Default values */
        $utmSource = Model_ConfigDb::get('utmSource');
        $utmMedium = Model_ConfigDb::get('utmMedium');
        $utmCampaign = Model_ConfigDb::get('utmCampaign');
        $utmTerm = Model_ConfigDb::get('utmTerm');
        $utmContent = Model_ConfigDb::get('utmContent');

        if ($this->_utmSource) {
            $utmSource = $this->_utmSource;
        }
        if ($this->_utmMedium) {
            $utmMedium = $this->_utmMedium;
        }
        if ($this->_utmCampaign) {
            $utmCampaign = $this->_utmCampaign;
        }
        if ($this->_utmTerm) {
            $utmTerm = $this->_utmTerm;
        }
        if ($this->_utmContent) {
            $utmContent = $this->_utmContent;
        }

        $query = parse_url($url, PHP_URL_QUERY);
        $params = array();
        parse_str($query, $params);

        if (!empty($utmSource)) {
            $params['utm_source'] = $utmSource;
        }

        if (!empty($utmMedium)) {
            $params['utm_medium'] = $utmMedium;
        }

        if (!empty($utmCampaign)) {
            $params['utm_campaign'] = $utmCampaign;
        }

        if (!empty($utmTerm)) {
            $params['utm_term'] = $utmTerm;
        }

        if (!empty($utmContent)) {
            $params['utm_content'] = $utmContent;
        }

        $newQuery = http_build_query($params);

        $host = parse_url($url, PHP_URL_HOST);
        $path = parse_url($url, PHP_URL_PATH);

        return 'http://' . $host . $path . '?' . $newQuery;
    } // setUtm

    /**
     * Add redirect parametr if turn on whitelabeling
     *
     * @see http://jira.getitcorporate.com/browse/ALT-51
     *
     * @param string $url
     * @param integer $id
     *
     * @return string
     */
    public function addRedirectParameter($url, $id)
    {
        $turnOn = Model_ConfigDb::getBoolean('whitelabeling');

        if ($turnOn) {
            $site = $this->getSite();
            $redirectUrl = $site->getDomain();
            $query = parse_url($url, PHP_URL_QUERY);
            if (!empty($query)) {
                $url .= '&r=' . $redirectUrl;
            } else {
                $url .= '?r=' . $redirectUrl;
            }
        }

        return $url;
    } // addRedirectParameter

    /**
     * Mark Alert as complaint
     *
     * @return void
     */
    public function markAsComplaint()
    {
        $this->setLastComplaintDate(date('Y-m-d h:i:s'));
        $count = (int)$this->getComplaintsCount() + 1;
        $this->setComplaintsCount($count);
        $this->setAlertStatus(Model_Alert::STATUS_SUSPENDED_AS_COMPLAINT);
        $this->update();
    } // markAsComplaint

    /**
     * Mark Alert as Bounce Permanent
     *
     * @return void
     */
    public function markAsBouncePermanent()
    {
        $this->setLastBounceDate(date('Y-m-d h:i:s'));
        $count = (int)$this->getBouncesCount() + 1;
        $this->setBouncesCount($count);
        $this->setAlertStatus(Model_Alert::STATUS_SUSPENDED_AS_BOUNCE_PERMANENT);
        $this->update();
    } // markAsBouncePermanent

    /**
     * Mark Alert as Bounce Transient
     *
     * @return void
     */
    public function markAsBounceTransient()
    {
        $this->setLastBounceDate(date('Y-m-d h:i:s'));
        $count = (int)$this->getBouncesCount() + 1;
        $this->setBouncesCount($count);
        if ($count >= Model_Alert::BOUNCE_LIMIT) {
            $this->setAlertStatus(Model_Alert::STATUS_SUSPENDED_AS_BOUNCE_TRANSIENT);
        }
        $this->update();
    } // markAsBounceTransient

    /**
     * Check given alert parameters and return array with Exceptions
     *
     * @param array $params Parameters
     *
     * @return array
     *
     * @throws Model_Exception_UnsupportedParamValue
     * @throws Model_Exception_MissingRequireParam
     */
    public static function checkAlertParams($params)
    {
        $result = array();
        if (!array_key_exists('alertType', $params)) {
            $result[] = new Model_Exception_MissingRequireParam('alertType');
        } else {
            $alertType = trim(strtoupper($params['alertType']));
            if ($alertType != Model_Alert::TYPE_DAILY
                && $alertType != Model_Alert::TYPE_WEEKLY
            ) {
                $result[] = new Model_Exception_UnsupportedParamValue(
                    'alertType',
                    array(
                        Model_Alert::TYPE_DAILY,
                        Model_Alert::TYPE_WEEKLY,
                    )
                );
            }
        }

        if (!array_key_exists('query', $params)
            && !array_key_exists('location', $params)
        ) {

            $result[] = new Model_Exception_MissingRequireParam('query or location');
        }

        if (!array_key_exists('domain', $params)) {
            $result[] = new Model_Exception_MissingRequireParam('domain');
        }

        array_merge($result, Model_Site::checkRegistryParams($params));

        return $result;
    } // checkAlertParams

    /**
     * Prepare emails message for the alert using given results from API
     *
     * @param array $apiResults
     * @param string $messageSecureId
     *
     * @return array
     */
    public function prepareMessage($apiResults, $messageSecureId)
    {
        $count = count($apiResults['listings']);
        $now = new DateTime();
        $contact = $this->getContact();
        $site = $this->getSite();

        $placeholders = array();
        $placeholders['COUNT'] = $count;
        $placeholders['CANONICAL_QUERY'] = $this->getCanonicalQuery($count > 1);
        $placeholders['DATE'] = $now->format('m/j/y');
        $placeholders['LISTINGS'] = $this->_prepareListings($apiResults['listings']);
        $placeholders['EDIT_ALERT_URL'] = $this->getEditUrl();
        $placeholders['UNSUBSCRIBE_ALERT_URL'] = $this->getUnsubscribeUrl();
        $placeholders['VIEW_ALL_ALERTS_URL'] = $this->getUnsubscribeAllUrl();
        $placeholders['FIRST_MESSAGE'] = $this->_isFirstMessage();
        $placeholders['SITE_NICHE'] = $site->getSiteNiche();
        $placeholders['IMG_SRC'] = $site->getBaseUrl() . '/picture/' . $messageSecureId . '/logo.gif';
        // TODO : Validate placeholders
        $this->_validatePlaceholders($placeholders);

        $template = new \GetIt\Email\Template\SmsTemplate('email alert');
        $template->addMerges($placeholders);

        // Hardcoding this for now, since none of the emails I pass in will be verified.
        $fromEmail = 'contact@getitllc.com';
        $fromPerson = $site->getFromName() ? $site->getFromName() : $site->getSiteName();
        $template->setFrom($fromEmail, $fromPerson);
        $template->setTo($contact->getEmail(), $contact->getName());

        $result = array();
        $result['messageSecureId'] = $messageSecureId;
        $result['alertId'] = $this->getAlertId();
        $result['contactId'] = $this->getContactId();
        $result['organicLinks'] = $apiResults['organicCount'];
        $result['backfillLinks'] = $apiResults['backfillCount'];
        $result['fromEmail'] = $template->getFromEmail();
        $result['fromName'] = $template->getFromName();
        $result['fromAddress'] = $template->getFrom();
        $result['toEmail'] = $template->getToEmail();
        $result['toName'] = $template->getToName();
        $result['toAddress'] = $template->getTo();
        $result['subject'] = $template->getSubject();
        $result['bcc'] = NULL;
        $result['preparedDate'] = Model_Utils::sqlNow();
        $result['secureId'] = $messageSecureId;
        $result['bodyText'] = $template->getBodyText();
        $result['bodyHtml'] = $template->getBodyHtml();
        return $result;
    } // _prepareSqsMessage

    /**
     * Validate placeholders before sending email
     *
     * @param array $placeholders
     *
     * @throws Exception
     */
    private function _validatePlaceholders($placeholders)
    {
        $require = array(
            'COUNT' => 'Empty listing set.',
            'CANONICAL_QUERY' => 'Empty canonical query.',
            'DATE' => 'Empty date.',
            'LISTINGS' => 'Empty listing set.',
            'EDIT_ALERT_URL' => 'Empty edit alert url.',
            'UNSUBSCRIBE_ALERT_URL' => 'Empty unsubscribe alert url.',
            'VIEW_ALL_ALERTS_URL' => 'Empty view all alerts url.',
            'SITE_NICHE' => 'Empty site niche name.',
            'IMG_SRC' => 'Empty image link.'
        );
        foreach ($require as $name => $message) {
            if (empty($placeholders[$name])) {
                throw new Exception($message);
            }
        }
        $checkUrls = array(
            'EDIT_ALERT_URL' => 'Bad edit alert url.',
            'UNSUBSCRIBE_ALERT_URL' => 'Bad unsubscribe alert url.',
            'VIEW_ALL_ALERTS_URL' => 'Bad view all alerts url.',
            'IMG_SRC' => 'Bad image link.'
        );
        foreach ($checkUrls as $name => $message) {
            if(filter_var($placeholders[$name], FILTER_VALIDATE_URL) === false) {
                throw new Exception($message);
            }
            if (strpos($placeholders[$name], 'http://') === false) {
                throw new Exception($message);
            }
        }
        foreach ($placeholders['LISTINGS'] as $listing) {
            if(filter_var($listing['url'], FILTER_VALIDATE_URL) === false) {
                throw new Exception('Bad listing URL.');
            }
            if (strpos($listing['url'], 'http://') === false) {
                throw new Exception('Bad listing URL.');
            }
            if (empty($listing['title'])) {
                throw new Exception('Empty listing title.');
            }
            if (empty($listing['date'])) {
                throw new Exception('Empty listing date.');
            }
        }
    } // _validatePlaceholders

    /**
     * Prepare template for send
     *
     * @param array $row
     *
     * @return \GetIt\Email\Template\SmsTemplate Template
     */
    public static function prepareTemplate($row)
    {
        $template = new \GetIt\Email\Template\SmsTemplate('email alert');

        $template->setBodyHtml($row['bodyHtml']);
        $template->setBodyText($row['bodyText']);
        $template->setSubject($row['subject']);
        $template->setFrom($row['fromEmail'], $row['fromName']);
        if (!empty($row['bcc'])) {
            $template->setBcc(explode(',', $row['bcc']));
        }
        $template->setTo($row['toEmail'], $row['toName']);

        return $template;
    } // prepareTemplate

    /**
     * Build HTML table with listings
     *
     * @param array $listings
     *
     * @return array
     */
    protected function _prepareListings($listings)
    {
        $result = array();

        /* @var Model_Listing $listing */
        foreach ($listings as $listing) {
            $data = array();
            $data['title'] = Model_Utils::shortenText($listing->getTitle(), 40);
            $data['url'] = $listing->getExternalUrl();
            $data['location'] = $listing->getLocation();
            $date = new DateTime($listing->getDate());
            $data['date'] = $date->format('F jS');
            $result[] = $data;
        }

        return $result;
    } // _prepareListings

    /**
     * If this is the first message being sent, than we need to
     * add a custom message to the top of the broadcast.
     *
     * @return boolean
     */
    private function _isFirstMessage()
    {
        return $this->_lastSendDate == null;
    } // _isFirstMessage

    /**
     * Get Is Location Valid
     *
     * @return boolean
     */
    public function getIsLocationValid()
    {
        return $this->_isLocationValid;
    } // getIsLocationValid

    /**
     * Set Is Location Valid
     *
     * @param boolean $isLocationValid Is Location Valid
     *
     * @return Model_Alert
     */
    public function setIsLocationValid($isLocationValid)
    {
        $this->_isLocationValid = $isLocationValid;
        return $this;
    } // setIsLocationValid
}
