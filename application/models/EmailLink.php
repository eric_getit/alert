<?php
/**
 * Model_EmailLink
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Gateway URL
 *
 * In search alert email messages we put gateway URLs for all job URLs.
 * Gateway URL looks like
 * http://<sitename>.getitcorporate.com/email/<url_code>
 *
 */
class Model_EmailLink extends Model_AbstractModel
{
    /**
     * Prefix for Redis keys
     *
     * @var string
     */
    const REDIS_KEY_PREFIX = 'email-link-';

    /**
     * Table name with code in format src32
     *
     * @var string
     */
    const TABLE_NAME_ARCHIVE = 'email_links_archive';

    /**
     * Table name with code in format 'listingId'
     *
     * @var string
     */
    const TABLE_NAME_LAST = 'email_links';

    /**
     * Unique URL code used in gateway URL
     *
     * @var string
     */
    protected $_code;

    /**
     * External URL value
     *
     * @var string
     */
    protected $_url;

    /**
     * URL type
     *
     * @var string
     */
    protected $_type;

    /**
     * Date and time when URL has been created
     *
     * @var string
     */
    protected $_created;

    /**
     * Default data base table name for the model
     *
     * @var string
     */
    protected $_tableName = Model_EmailLink::TABLE_NAME_LAST;

    /**
     * Primary key field name
     *
     * @var string
     */
    protected $_primaryKey = 'id';

    /**
     * Data base fields
     *
     * @var array
     */
    protected $_tableFields = array(
        'code' => '_code',
        'url' => '_url',
        'type' => '_type',
        'created' => '_created'
    );

    /**
     * Check if link with given code exists in table
     *
     * @param string $code
     *
     * @return boolean
     */
    public function findLink($code)
    {
        $result = $this->findLinkInSql($code);

        if ($result == false) {
            $result = $this->findLinkInRedis($code);
        }

        return $result;
    } // findLink

    /**
     * Check if link with given code exists in MySQL table
     *
     * @param string $code
     *
     * @return boolean
     */
    public function findLinkInSql($code)
    {
        $result = false;

        $dbh = Zend_Db_Table::getDefaultAdapter();
        $tableName = $this->getTableName();

        $select = $dbh->select();
        $select->from($tableName, '*');
        $select->where('code = ?', $code);
        $row = $select->query()->fetch();
        if ($row !== false) {
            $this->fromArray($row);
            $result = true;
        }

        return $result;
    } // findLinkInSql

    /**
     * Check if link with given code exists in redis
     *
     * @param string $code
     *
     * @return boolean
     */
    public function findLinkInRedis($code)
    {
        $result = false;
        $redis = Model_ConnectionManager::getRedisClient();
        $redisKey = self::REDIS_KEY_PREFIX . $code;

        if ($redis->exists($redisKey)) {
            $data = $redis->hgetall($redisKey);
            $this->fromArray($data);
            /*
             * Between lines 156 and 157
             * another process can deletes the record
             */
            $result = $this->getCode() && $this->getUrl();
        }

        return $result;
    } // findLinkInRedis

    /**
     * Delete email link from Redis
     *
     * @return void
     */
    public function deleteFromRedis()
    {
        $redis = Model_ConnectionManager::getRedisClient();
        if ($redis->exists($this->getRedisKey())) {
            $redis->del($this->getRedisKey());
        }
    } // deleteFromRedis

    /**
     * Generate unique link for display in email
     *
     * @param Model_Alert $alert
     *
     * @return string
     */
    public function getExternalLink(Model_Alert $alert)
    {
        $domainName = $alert->getSite()->getBaseUrl();
        $code = $this->getCode();
        $alertId = $alert->getAlertId();

        return $domainName . '/email/' . $code . '-' . $alertId;
    } // getExternalLink

    /**
     * Multiple insert in redis
     *
     * @param array $emailLinks
     *
     * @return void
     */
    public static function multipleInsertIntoRedis($emailLinks)
    {
        $redis = Model_ConnectionManager::getRedisClient();
        /* @var $emailLink Model_EmailLink */
        foreach ($emailLinks as $emailLink) {
            $values = $emailLink->toArray();
            $redis->hmset($emailLink->getRedisKey(), $values);
        }
    } // multipleInsertIntoRedis

    /**
     * Insert object into DB
     *
     * @return void
     */
    public function insertIntoSql()
    {
        if (!$this->getCode() || !$this->getUrl()) {
            return;
        }
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $tableName = $this->getTableName();
        $data = $this->toArray();

        $fieldString = '';
        $quoteValues = array();
        foreach ($data as $field => $value) {
            $quoteValues[] = $dbh->quote($value);
            $fieldString .= $field . ', ';
        }
        $fieldString = '(' . substr($fieldString, 0, -2) . ')';
        $valuesString = '(' . implode(', ', $quoteValues) . ')';

        $sql = "INSERT IGNORE INTO {$tableName} {$fieldString} VALUES {$valuesString}";
        $dbh->query($sql);
    } // insert

    /**
     * Get Code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->_code;
    } // getCode

    /**
     * Get key used for saving link into Redis
     *
     * @return string
     */
    public function getRedisKey()
    {
        return self::REDIS_KEY_PREFIX . $this->getCode();
    } // getRedisKey

    /**
     * Get Url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->_url;
    } // getUrl

    /**
     * Get Type
     *
     * @return string
     */
    public function getType()
    {
        return $this->_type;
    } // getType

    /**
     * Get created date
     *
     * @return string
     */
    public function getCreated()
    {
        return $this->_created;
    } // getCreated

    /**
     * Get table name
     *
     * @return string
     */
    public function getTableName()
    {
        return $this->_tableName;
    } // getTableName

    /**
     * Set Code
     *
     * @param string $code
     *
     * @return Model_EmailLink
     */
    public function setCode($code)
    {
        $this->_code = $code;
        return $this;
    } // setCode

    /**
     * Set Url
     *
     * @param string $url
     *
     * @return Model_EmailLink
     */
    public function setUrl($url)
    {
        $this->_url = $url;
        return $this;
    } // setUrl

    /**
     * Set Type
     *
     * @param string $type
     *
     * @return Model_EmailLink
     */
    public function setType($type)
    {
        $this->_type = $type;
        return $this;
    } // setType

    /**
     * Set created date
     *
     * @param string $created
     *
     * @return Model_EmailLink
     */
    public function setCreated($created)
    {
        $this->_created = $created;
        return $this;
    } // setCreated

    /**
     * Set table name
     *
     * @param string $tableName
     *
     * @return Model_EmailLink
     */
    public function setTableName($tableName)
    {
        $this->_tableName = $tableName;
        return $this;
    } // setTableName
}
