<?php
/**
 * Model_MessageBody
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Hold message bodies
 */
class Model_MessageBody extends Model_AbstractModel
{
    /**
     * Primary and foreign key
     *
     * @var integer
     */
    protected $_messageId;

    /**
     * Email body pure (before apply merges) text
     *
     * @var string
     */
    protected $_bodyText;

    /**
     * Email body pure (before apply merges) html
     *
     * @var string
     */
    protected $_bodyHtml;

    /**
     * Data base table name for the model
     *
     * @var string
     */
    protected $_tableName = 'message_bodies';

    /**
     * Primary key field name
     *
     * @var string
     */
    protected $_primaryKey = 'messageId';

    /**
     * Data base fields
     *
     * @var array
     */
    protected $_tableFields = array(
            'messageId' => '_messageId',
            'bodyText' => '_bodyText',
            'bodyHtml' => '_bodyHtml',
            );

    /**
     * Create body messages from the row data or load
     * messages for MessageArchive object
     *
     * @param array | Model_MessageArchives $data Data from DB table or record ID
     *
     * @return Model_MessageBody
     *
     * @throws Exception
     */
    public function __construct($data = null)
    {
        if (is_array($data)) {
            if (empty($data['messageId'])) {
                throw new Exception(
                    'Model_MessageBody::__construct() Failed to build message ' .
                    'bodies object, messageId parameter is missing.'
                );
            }
            if (empty($data['preparedDate'])) {
                throw new Exception(
                    'Model_MessageBody::__construct() Failed to build message ' .
                    'bodies object, prepared date parameter is missing.'
                );
            }

            $this->_tableName = $this->_getTableName($data['preparedDate']);

            $this->fromArray($data);
        } elseif ($data instanceof Model_MessageArchive) {
            /* @var $data Model_MessageArchive */
            $this->_tableName = $this->_getTableName($data->getPreparedDate());
            $this->find($data->getMessageId());
        }
    } // __construct

    /**
     * Get message ID - foreign key to message_archive table
     *
     * @return integer
     */
    public function getMessageId()
    {
        return $this->_messageId;
    } // getMessageId

    /**
     * Get text body
     *
     * @return string $_bodyText
     */
    public function getBodyText()
    {
        return $this->_bodyText;
    } // getBodyText

    /**
     * Get HTML body
     *
     * @return string $_bodyHtml
     */
    public function getBodyHtml()
    {
        return $this->_bodyHtml;
    } // getBodyHtml

    /**
     * Insert object into DB
     *
     * @return void
     */
    public function insert()
    {
        $bind = $this->toArray();
        $this->_insert($this->_tableName, $bind);
    } // insert

    /**
     * Get the table name based prepared date
     *
     * @param string $preparedDate Prepared Date
     *
     * @return string
     */
    private function _getTableName($preparedDate)
    {
        $preparedDate = new DateTime($preparedDate);
        // ISO-8601 numeric representation of the day of the
        // week (added in PHP 5.1.0)
        // 1 (for Monday) through 7 (for Sunday)
        $weekDay = $preparedDate->format('N');
        $delta = intval($weekDay) - 1;
        if ($delta > 0) {
            $interval = new DateInterval('P' . $delta . 'D');
            $preparedDate->sub($interval);
        }
        $tableName = 'message_bodies_' . $preparedDate->format('Ymd');

        return $tableName;
    } // _getTableName
}
