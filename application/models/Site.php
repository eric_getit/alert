<?php
/**
 * Model_Site
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Class for Site
 */
class Model_Site extends Model_AbstractModel
{
    /**
     * Primary key
     *
     * @var integer
     */
    protected $_siteId;

    /**
     * Site domain name (getnichejobs.com)
     *
     * @var string
     */
    protected $_domain;

    /**
     * Site print name (Get Niche Jobs)
     *
     * @var string
     */
    protected $_siteName;

    /**
     * Site UC domain name (GetNicheJobs.com)
     *
     * @var string
     */
    protected $_newsletterDomain;

    /**
     * Site niche (Niche)
     *
     * @var string
     */
    protected $_siteNiche;

    /**
     * Site contact person name (Casey Wilson)
     *
     * @var string
     */
    protected $_fromName;

    /**
     * Site contacnt email (contact@getnichejobs.com)
     *
     * @var string
     */
    protected $_fromEmail;

    /**
     * Record Creation data
     *
     * @var string
     */
    protected $_createdDate;

    /**
     * Record change date
     *
     * @var string
     */
    protected $_updatedDate;

    /**
     * Primary key field name
     *
     * @var string
     */
    protected $_primaryKey = 'siteId';

    /**
     * Data base table name for the model
     *
     * @var string
     */
    protected $_tableName = 'sites';

    /**
     * Data base fields
     *
     * @var array
     */
    protected $_tableFields = array(
        'siteId' => '_siteId',
        'domain' => '_domain',
        'siteName' => '_siteName',
        'newsletterDomain' => '_newsletterDomain',
        'siteNiche' => '_siteNiche',
        'fromName' => '_fromName',
        'fromEmail' => '_fromEmail',
        'createdDate' => '_createdDate',
        'updatedDate' => '_updatedDate',
    );


    /**
     * Get Site Id
     *
     * @return integer
     */
    public function getSiteId()
    {
        return $this->_siteId;
    } // getSiteId

    /**
     * Get Domain
     *
     * @return string
     */
    public function getDomain()
    {
        return $this->_domain;
    } // getDomain

    /**
     * Get Site Name
     *
     * @return string
     */
    public function getSiteName()
    {
        return $this->_siteName;
    } // getSiteName

    /**
     * Get Newsletter Domain
     *
     * @return string
     */
    public function getNewsletterDomain()
    {
        return $this->_newsletterDomain;
    } // getNewsletterDomain

    /**
     * Get Site Niche
     *
     * @return string
     */
    public function getSiteNiche()
    {
        return $this->_siteNiche;
    } // getSiteNiche

    /**
     * Get From Name
     *
     * @return string
     */
    public function getFromName()
    {
        return $this->_fromName;
    } // getFromName

    /**
     * Get From Email
     *
     * @return string
     */
    public function getFromEmail()
    {
        return $this->_fromEmail;
    } // getFromEmail

    /**
     * Get Created Date
     *
     * @return string
     */
    public function getCreatedDate()
    {
        return $this->_createdDate;
    } // getCreatedDate

    /**
     * Get Updated Date
     *
     * @return string
     */
    public function getUpdatedDate()
    {
        return $this->_updatedDate;
    } // getUpdatedDate

    /**
     * Set Domain
     *
     * @param string $domain
     *
     * @return Model_Site
     */
    public function setDomain($domain)
    {
        $this->_domain = Model_Site::normalizeDomain($domain);
        return $this;
    } // setDomain

    /**
     * Set Site Name
     *
     * @param string $siteName
     *
     * @return Model_Site
     */
    public function setSiteName($siteName)
    {
        $this->_siteName = $siteName;
        return $this;
    } // setSiteName

    /**
     * Set Newsletter Domain
     *
     * @param string $newsletterDomain
     *
     * @return Model_Site
     */
    public function setNewsletterDomain($newsletterDomain)
    {
        $this->_newsletterDomain = $newsletterDomain;
        return $this;
    } // setNewsletterDomain

    /**
     * Set Site Niche
     *
     * @param string $siteNiche
     *
     * @return Model_Site
     */
    public function setSiteNiche($siteNiche)
    {
        $this->_siteNiche = $siteNiche;
        return $this;
    } // setSiteNiche

    /**
     * Set From Name
     *
     * @param string $fromName
     *
     * @return Model_Site
     */
    public function setFromName($fromName)
    {
        $this->_fromName = $fromName;
        return $this;
    } // setFromName

    /**
     * Set From Email
     *
     * @param string $fromEmail
     *
     * @return Model_Site
     */
    public function setFromEmail($fromEmail)
    {
        $this->_fromEmail = $fromEmail;
        return $this;
    } // setFromEmail

    /**
     * Set Created Date
     *
     * @param string $createdDate
     *
     * @return Model_Site
     */
    public function setCreatedDate($createdDate)
    {
        $this->_createdDate = $createdDate;
        return $this;
    } // setCreatedDate

    /**
     * Set Updated Date
     *
     * @param string $updatedDate
     *
     * @return Model_Site
     */
    public function setUpdatedDate($updatedDate)
    {
        $this->_updatedDate = $updatedDate;
        return $this;
    } // setUpdatedDate

    /**
     * Populate object from associative array
     *
     * @param array $data Array from DB table
     *
     * @return void
     */
    public function fromArray($data)
    {
        if (array_key_exists('domain', $data)) {
            $data['domain'] = Model_Site::normalizeDomain($data['domain']);
        }
        parent::fromArray($data);
    } // fromArray

    /**
     * Get site by domain
     *
     * @param string $domain Site domain name
     *
     * @return Model_Site | null
     */
    public static function getByDomain($domain)
    {
        $domain = Model_Site::normalizeDomain($domain);
        $result = new Model_Site();
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $select = $dbh->select();
        $select->from($result->_tableName, '*')
            ->where('domain = ?', $domain)
            ->limit(1);
        $row = $select->query()->fetch();
        if ($row) {
            $result->fromArray($row);
        } else {
            $result = null;
        }
        return $result;
    } // getByDomain

    /**
     * Normalize domain name
     *
     * @param string $domain
     *
     * @return string
     */
    public static function normalizeDomain($domain)
    {
        $domain = strtolower($domain);
        $domain = str_replace("http://", "", $domain);
        $parts = explode('.', $domain);
        if ($parts[0] == 'www') {
            array_shift($parts);
        }
        return implode('.', $parts);
    } //normalizeDomain

    /**
     * Check params given when registry new site
     * Return array with errors
     *
     * @param array $params
     *
     * @return array
     */
    public static function checkRegistryParams(array $params = array())
    {
        $errors = array();
        $fields = array(
                'domain', 'siteName', 'fromEmail', 'fromName', 'siteNiche', 'newsletterDomain'
        );
        foreach ($fields as $field) {
            if (!array_key_exists($field, $params)) {
                $errors[] = new Model_Exception_MissingRequireParam($field);
            }
        }
        if (!empty($errors) && array_key_exists('domain', $params)) {
            $site = Model_Site::getByDomain($params['domain']);
            if ($site) {
                $errors = array();
            }
        }

        return $errors;
    } // checkRegistryAvaliable

    /**
     * Registry site
     *
     * @param array $params
     *
     * @return Model_Site
     *
     * @throws Model_Exception_MissingRequireParam
     */
    public static function registry(array $params = array())
    {
        $result = new Model_Site();
        $data = array();
        $fields = array(
            'domain', 'siteName', 'fromEmail', 'fromName', 'siteNiche', 'newsletterDomain'
        );
        foreach ($fields as $field) {
            if (!array_key_exists($field, $params)) {
                throw new Model_Exception_MissingRequireParam($field);
            }
            $data[$field] = $params[$field];
        }
        $check = Model_Site::getByDomain($data['domain']);
        if ($check != null) {
            $result = $check;
        } else {
            $result->fromArray($data);
            $result->insert();
        }

        return $result;
    } // registry

    /**
     * Get site base url
     *
     * @return string
     */
    public function getBaseUrl()
    {
        /** Don't know how do it proper **/
        $site = $this->_domain;
        $parts = explode('.', $site);
        return 'http://' . $parts[0] . '.getitcorporate.com';
    } // getBaseUrl

    /**
     * Insert object into DB
     *
     * @return void
     */
    public function insert()
    {
        $date = date('Y-m-d H:i:s');
        $this->setCreatedDate($date);
        $this->setUpdatedDate($date);
        parent::insert();
    } // insert

    /**
     * Update object in DB
     *
     * @return boolean
     */
    public function update()
    {
        $date = date('Y-m-d H:i:s');
        $this->setUpdatedDate($date);
        return parent::update();
    } // update
}
