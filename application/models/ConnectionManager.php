<?php
/**
 * Model_ConnectionManager
 *
 * PHP Version 5.4
 *
 * @category Class
 * @package  Model
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */
class Model_ConnectionManager
{
    /**
     * Redis client configuration
     * Expected :
     * array(
     *  'scheme' => 'tcp',
     *  'host' => '@redis-host@',
     *  'port' => '@redis-port@'
     * )
     *
     * @var array
     */
    protected static $_redisConfig;

    /**
     * Gearman client configuration
     * Expected :
     * array(
     *  'host' => '@gearman-host@',
     *  'port' => '@gearman-port@',
     * )
     *
     * @var array
     */
    protected static $_gearmanConfig;

    /**
     * Supervisor configuration
     * Expected :
     * array(
     *  'host' => '@supervisor-host@',
     *  'port' => '@supervisor-port@',
     * )
     *
     * @var array
     */
    protected static $_supervisorConfig;

    /**
     * Redis client
     *
     * @var \Predis\Client
     */
    protected static $_redisClient;

    /**
     * Gearman client
     *
     * @var \GearmanClient
     */
    protected static $_gearmanClient;

    /**
     * Gearman worker
     *
     * @var \GearmanWorker
     */
    protected static $_gearmanWorker;

    /**
     * Supervisor client
     *
     * @var \SupervisorClient\SupervisorClient
     */
    protected static $_supervisorClient;

    /**
     * Set redis client configuration
     *
     * @param array $config
     *
     * @return void
     */
    public static function setRedisConfig($config)
    {
        self::$_redisConfig = $config;
    } // setRedisConfig

    /**
     * Set gearman client configuration
     *
     * @param array $config
     *
     * @return void
     */
    public static function setGearmanConfig($config)
    {
        self::$_gearmanConfig = $config;
    } // setGearmanConfig

    /**
     * Set supervisor client configuration
     *
     * @param array $config
     *
     * @return void
     */
    public static function setSupervisorConfig($config)
    {
        self::$_supervisorConfig = $config;
    } // setSupervisorConfig

    /**
     * Set redis client
     *
     * @var \Predis\Client
     *
     * @return void
     */
    public static function setRedisClient($client)
    {
        self::$_redisClient = $client;
    } // setRedisClient

    /**
     * Set gearman client
     *
     * @var \GearmanClient
     *
     * @return void
     */
    public static function setGearmanClient($client)
    {
        self::$_gearmanClient = $client;
    } // setGearmanClient

    /**
     * Set gearman worker
     *
     * @var \GearmanWorker
     *
     * @return void
     */
    public static function setGearmanWorker($worker)
    {
        self::$_gearmanWorker = $worker;
    } // setGearmanWorker

    /**
     * Set supervisor client
     *
     * @var \SupervisorClient\SupervisorClient
     *
     * @return void
     */
    public static function setSupervisorClient($client)
    {
        self::$_supervisorClient = $client;
    } // setSupervisorClient

    /**
     * Get redis configuration
     *
     * @return array
     */
    public static function getRedisConfig()
    {
        return self::$_redisConfig;
    } // getRedisConfig

    /**
     * Get gearman configuration
     *
     * @return array
     */
    public static function getGearmanConfig()
    {
        return self::$_gearmanConfig;
    } // getGearmanConfig

    /**
     * Get supervisor configuration
     *
     * @return array
     */
    public static function getSupervisorConfig()
    {
        return self::$_supervisorConfig;
    } // getSupervisorConfig

    /**
     * Get redis client
     *
     * @return \Predis\Client
     */
    public static function getRedisClient()
    {
        if (self::$_redisClient == null) {
            $redisClient = new \Predis\Client(self::$_redisConfig);
            self::$_redisClient = $redisClient;
        }

        return self::$_redisClient;
    } // getRedisClient

    /**
     * Get gearman client
     *
     * @return \GearmanClient
     */
    public static function getGearmanClient()
    {
        if (self::$_gearmanClient == null) {
            $gearmanClient = new \GearmanClient();
            $gearmanClient->addServer(self::$_gearmanConfig['host'], self::$_gearmanConfig['port']);
            self::$_gearmanClient = $gearmanClient;
        }

        return self::$_gearmanClient;
    } // getGearmanClient

    /**
     * Get gearman worker
     *
     * @return \GearmanWorker
     */
    public static function getGearmanWorker()
    {
        if (self::$_gearmanWorker == null) {
            $gearmanWorker = new \GearmanWorker();
            $gearmanWorker->addServer(self::$_gearmanConfig['host'], self::$_gearmanConfig['port']);
            self::$_gearmanWorker = $gearmanWorker;
        }

        return self::$_gearmanWorker;
    } // getGearmanWorker

    /**
     * Get supervisor client
     *
     * @return \SupervisorClient\SupervisorClient
     */
    public static function getSupervisorClient()
    {
        if (self::$_supervisorClient == null) {
            $supervisorClient = new \SupervisorClient\SupervisorClient(self::$_supervisorConfig['host'], self::$_supervisorConfig['port']);
            self::$_supervisorClient = $supervisorClient;
        }

        return self::$_supervisorClient;
    } // getSupervisorClient
}
