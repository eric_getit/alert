<?php
/**
 * Model_Contact
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */
class Model_Contact extends Model_AbstractModel
{
    /**
     * Status for active contacts
     *
     * @var string
     */
    const STATUS_ACTIVE = 'ACTIVE';

    /**
     * Status for contacts that were unsubscribed by the user
     *
     * @var string
     */
    const STATUS_UNSUBSCRIBED_USER = 'UNSUBSCRIBED_USER';

    /**
     * Status for contacts that were unsubscribed by admin
     *
     * @var string
     */
    const STATUS_UNSUBSCRIBED_ADMIN = 'UNSUBSCRIBED_ADMIN';

    /**
     * Status for contacts that were unsubscribed using API
     *
     * @var string
     */
    const STATUS_UNSUBSCRIBED_API = 'UNSUBSCRIBED_API';

    /**
     * Status for contacts that were suspended
     * @var string
     */
    const STATUS_SUSPEND = 'SUSPEND';

    /**
     * Status for contacts that were deactivated (inactive)
     *
     * @var string
     */
    const STATUS_DEACTIVE = 'DEACTIVE';

    /**
     *
     * Short form of default timezone
     *
     * @var string
     */
    const TIMEZONE_PREFIX = 'EST';

    /**
     * Contact ID (primary key)
     *
     * @var integer
     */
    protected $_contactId;

    /**
     * Contact Secure Id
     *
     * @var string
     */
    protected $_contactSecureId;

    /**
     * Contact Source Id
     *
     * @var string
     */
    protected $_contactSourceId;

    /**
     * Contact Status
     *
     * @var string
     */
    protected $_contactStatus;

    /**
     * Name
     *
     * @var string
     */
    protected $_name;

    /**
     * Country
     *
     * @var string
     */
    protected $_country;

    /**
     * State
     *
     * @var string
     */
    protected $_state;

    /**
     * City
     *
     * @var string
     */
    protected $_city;

    /**
     * Postalcode
     *
     * @var string
     */
    protected $_zip;

    /**
     * Address
     *
     * @var string
     */
    protected $_address;

    /**
     * Email address
     *
     * @var string
     */
    protected $_email;

    /**
     * Date and time when contact was created
     *
     * @var string
     */
    protected $_createdDate;

    /**
     * Date and time when contact was updated
     *
     * @var string
     */
    protected $_updatedDate;

    /**
     * Timezone that we have defined for the contact
     * Used to send alerts
     *
     * @var string
     */
    protected $_sendTimezone;

    /**
     * Timezone that the user has established itself
     *
     * @var string
     */
    protected $_userTimezone;

    /**
     * Date and time from original application when the contact has been registered
     *
     * @var string
     */
    protected $_originalCreatedDate;

    /**
     * Data base table name for the model
     *
     * @var string
     */
    protected $_tableName = 'contacts';

    /**
     * Primary key field name
     *
     * @var string
     */
    protected $_primaryKey = 'contactId';

    /**
     * Data base fields
     *
     * @var array
     */
    protected $_tableFields = array(
            'contactId' => '_contactId',
            'contactSecureId' => '_contactSecureId',
            'contactSourceId' => '_contactSourceId',
            'contactStatus' => '_contactStatus',
            'name' => '_name',
            'country' => '_country',
            'state' => '_state',
            'city' => '_city',
            'zip' => '_zip',
            'address' => '_address',
            'email' => '_email',
            'createdDate' => '_createdDate',
            'updatedDate' => '_updatedDate',
            'originalCreatedDate' => '_originalCreatedDate',
            'sendTimezone' => '_sendTimezone',
            'userTimezone' => '_userTimezone',
            );

    /**
     * Get Contact Status
     *
     * @return string
     */
    public function getContactStatus()
    {
        return $this->_contactStatus;
    } // getContactStatus

    /**
     * Set Contact Status
     *
     * @param string $contactStatus
     *
     * @return Model_Contact
     */
    public function setContactStatus($contactStatus)
    {
        $this->_contactStatus = $contactStatus;
        return $this;
    } // setContactStatus

    /**
     * Get Country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->_country;
    } // getCountry

    /**
     * Get State
     *
     * @return string
     */
    public function getState()
    {
        return $this->_state;
    } // getState

    /**
     * Get City
     *
     * @return string
     */
    public function getCity()
    {
        return $this->_city;
    } // getCity

    /**
     * Get Postalcode
     *
     * @return string
     */
    public function getZip()
    {
        return $this->_zip;
    } // getZip

    /**
     * Set Country
     *
     * @param string $country
     *
     * @return Model_Contact
     */
    public function setCountry($country)
    {
        $this->_country = $country;
        return $this;
    } // setCountry

    /**
     * Set State
     *
     * @param string $state
     *
     * @return Model_Contact
     */
    public function setState($state)
    {
        $this->_state = $state;
        return $this;
    } // setState

    /**
     * Set City
     *
     * @param string $city
     *
     * @return Model_Contact
     */
    public function setCity($city)
    {
        $this->_city = $city;
        return $this;
    } // setCity

    /**
     * Set Postalcode
     *
     * @param string $zip
     *
     * @return Model_Contact
     */
    public function setZip($zip)
    {
        $this->_zip = $zip;
        return $this;
    } // setZip

    /**
     * Get Contact Id
     *
     * @return integer
     */
    public function getContactId()
    {
        return $this->_contactId;
    } // getContactId

    /**
     * Get Contact Secure Id
     *
     * @return string
     */
    public function getContactSecureId()
    {
        return $this->_contactSecureId;
    } // getContactSecureId

    /**
     * Get Contact Source Id
     *
     * @return string
     */
    public function getContactSourceId()
    {
        return $this->_contactSourceId;
    } // getContactSourceId

    /**
     * Get Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    } // getName

    /**
     * Get Address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->_address;
    } // getAddress

    /**
     * Get Email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->_email;
    } // getEmail

    /**
     * Get Created Date
     *
     * @return string
     */
    public function getCreatedDate()
    {
        return $this->_createdDate;
    } // getCreatedDate

    /**
     * Get Updated Date
     *
     * @return string
     */
    public function getUpdatedDate()
    {
        return $this->_updatedDate;
    } // getUpdatedDate

    /**
     * Get original created date when the contact has been registered in the original application
     *
     * @return string
     */
    public function getOriginalCreatedDate()
    {
        return $this->_originalCreatedDate;
    } // getOriginalCreatedDate

    /**
     * Get Send Timezone
     *
     * @return string
     */
    public function getSendTimezone()
    {
        return $this->_sendTimezone;
    } // getSendTimezone

    /**
     * Get User Timezone
     *
     * @return string
     */
    public function getUserTimezone()
    {
        return $this->_userTimezone;
    } // getUserTimezone

    /**
     * Set Contact Secure Id
     *
     * @param string $contactSecureId
     *
     * @return Model_Contact
     */
    public function setContactSecureId($contactSecureId)
    {
        $this->_contactSecureId = $contactSecureId;
        return $this;
    } // setContactSecureId

    /**
     * Set Contact SourceId
     *
     * @param string $contactSourceId
     *
     * @return Model_Contact
     */
    public function setContactSourceId($contactSourceId)
    {
        $this->_contactSourceId = $contactSourceId;
        return $this;
    } // setContactSourceId

    /**
     * Set Name
     *
     * @param string $name
     *
     * @return Model_Contact
     */
    public function setName($name)
    {
        $this->_name = $name;
        return $this;
    } // setName

    /**
     * Set Address
     *
     * @param string $address
     *
     * @return Model_Contact
     */
    public function setAddress($address)
    {
        $this->_address = $address;
        return $this;
    } // $address

    /**
     * Set Email
     *
     * @param string $email
     *
     * @return Model_Contact
     */
    public function setEmail($email)
    {
        $this->_email = $email;
        return $this;
    } // setEmail

    /**
     * Set Created Date
     *
     * @param string $createdDate
     *
     * @return Model_Contact
     */
    public function setCreatedDate($createdDate)
    {
        $this->_createdDate = $createdDate;
        return $this;
    } // setCreatedDate

    /**
     * Set Updated Date
     *
     * @param string $updatedDate
     *
     * @return Model_Contact
     */
    public function setUpdatedDate($updatedDate)
    {
        $this->_updatedDate = $updatedDate;
        return $this;
    } // setUpdatedDate

    /**
     * Set original created date
     *
     * @param string $originalCreatedDate
     *
     * @return Model_Contact
     */
    public function setOriginalCreatedDate($originalCreatedDate)
    {
        $date = new DateTime($originalCreatedDate);
        $this->_originalCreatedDate = $date->format('Y-m-d H:i:s');
        return $this;
    } // setOriginalCreatedDate

    /**
     * Set Send Timezone
     *
     * @param string $sendTimezone
     *
     * @return Model_Contact
     */
    public function setSendTimezone($sendTimezone)
    {
        $this->_sendTimezone = $sendTimezone;
        return $this;
    } // setSendTimezone

    /**
     * Set User Timezone
     *
     * @param string $userTimezone
     *
     * @return Model_Contact
     */
    public function setUserTimezone($userTimezone)
    {
        $this->_userTimezone = $userTimezone;
        return $this;
    } // setUserTimezone

    /**
     * Insert object into DB
     *
     * @return void
     */
    public function insert()
    {
        $date = date('Y-m-d H:i:s');
        $this->setCreatedDate($date);
        $this->setUpdatedDate($date);
        parent::insert();
    }

    /**
     * Update object in DB
     *
     * @return boolean
     */
    public function update()
    {
        $date = date('Y-m-d H:i:s');
        $this->setUpdatedDate($date);
        return parent::update();
    }

    /**
     * Get contact by email
     *
     * @param string $email
     *
     * @return Model_Contact | null
     */
    public static function getByEmail($email)
    {
        $contact = new Model_Contact();
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $select = $dbh->select();
        $select->from($contact->_tableName, '*')
            ->where('email = ?', $email)
            ->order('contactId')
            ->limit(1);
        $row = $select->query()->fetch();
        if ($row) {
            $contact->fromArray($row);
        } else {
            $contact = null;
        }
        return $contact;
    } // getByEmail

    /**
     * Get contact by secure Id
     *
     * @param string $secureId
     *
     * @return Model_Contact | null
     */
    public static function getBySecureId($secureId)
    {
        $contact = new Model_Contact();
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $select = $dbh->select();
        $select->from($contact->_tableName, '*')
            ->where('contactSecureId = ?', $secureId);
        $row = $select->query()->fetch();
        if ($row) {
            $contact->fromArray($row);
        } else {
            $contact = null;
        }
        return $contact;
    } // getBySecureId

    /**
     * Generate secure Id for contact
     *
     * @return string
     */
    public function generateSecureId()
    {
        $this->_contactSecureId = Model_Utils::guid();
        return $this->_contactSecureId;
    } // generateSecureId

    /**
     * Registry contact
     *
     * @param array $params
     *
     * @return Model_Contact
     *
     * @throws Model_Exception_MissingRequireParam
     */
    public static function registry(array $params)
    {
        $reqFields = array('email');
        foreach ($reqFields as $field) {
            if (!array_key_exists($field, $params)) {
                throw new Model_Exception_MissingRequireParam($field);
            }
        }

        $contact = new Model_Contact();
        $contact->generateSecureId();
        $contact->setContactStatus(Model_Contact::STATUS_ACTIVE);
        $contact->setEmail($params['email']);
        $contact->setSendTimezone(Model_Contact::TIMEZONE_PREFIX  . '+0');
        if (array_key_exists('name', $params)) {
            $contact->setName($params['name']);
        }
        if (array_key_exists('sourceId', $params)) {
            $contact->setContactSourceId($params['sourceId']);
        }
        if (array_key_exists('country', $params)) {
            $contact->setCountry($params['country']);
        }
        if (array_key_exists('state', $params)) {
            $contact->setState($params['state']);
        }
        if (array_key_exists('city', $params)) {
            $contact->setCity($params['city']);
        }
        if (array_key_exists('zip', $params)) {
            $contact->setZip($params['zip']);
        }
        if (array_key_exists('address', $params)) {
            $contact->setAddress($params['address']);
        }
        if (array_key_exists('originalCreatedDate', $params)) {
            $contact->setOriginalCreatedDate($params['originalCreatedDate']);
        }
        if (array_key_exists('contactStatus', $params)) {
            $status = strtoupper($params['contactStatus']);
            switch ($params['contactStatus']) {
                case Model_Contact::STATUS_ACTIVE:
                case Model_Contact::STATUS_DEACTIVE:
                case Model_Contact::STATUS_SUSPEND:
                case Model_Contact::STATUS_UNSUBSCRIBED_ADMIN:
                case Model_Contact::STATUS_UNSUBSCRIBED_API:
                case Model_Contact::STATUS_UNSUBSCRIBED_USER:
                        $contact->setContactStatus($status);
                    break;
            }
        }
        $contact->insert();

        return $contact;
    } // registry

    /**
     * Get contact by source credential
     *
     * @param string $sourceId
     *
     * @return Model_Contact | null
     */
    public static function getBySourceCredential($sourceId)
    {
        $contact = new Model_Contact();
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $select = $dbh->select();
        $select->from($contact->_tableName, '*')
            ->where('contactSourceId = ?', $sourceId);
        $row = $select->query()->fetch();
        if ($row) {
            $contact->fromArray($row);
        } else {
            $contact = null;
        }
        return $contact;
    } // getBySourceCredential

    /**
     * Get active alerts for contact
     *
     * @return array
     */
    public function getActiveAlerts()
    {
        $select = Zend_Db_Table::getDefaultAdapter()->select();
        $select->from('alerts', '*')
            ->where('contactId = ?', $this->_contactId)
            ->where('alertStatus = ?', Model_Alert::STATUS_ACTIVE);
        $rows = $select->query()->fetchAll();
        $result = array();

        foreach ($rows as $row) {
            $result[] = new Model_Alert($row);
        }

        return $result;
    } // getActiveAlerts

    /**
     * Get all alerts for contact
     *
     * @return array
     */
    public function getAllAlerts()
    {
        $select = Zend_Db_Table::getDefaultAdapter()->select();
        $select->from('alerts', '*')
            ->where('contactId = ?', $this->_contactId);
        $rows = $select->query()->fetchAll();
        $result = array();

        foreach ($rows as $row) {
            $result[] = new Model_Alert($row);
        }

        return $result;
    } // getActiveAlerts


    /**
     * Get all newsletter subscriptions for contact
     *
     * @return array
     */
    public function getAllNewsletterSubscriptions()
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $sql = "SELECT newsletterId FROM newsletter_subscriptions WHERE contactId = :contactId";
        $rows = $dbh->query($sql, array('contactId' => $this->getContactId()))->fetchAll();
        $result = array();

        foreach ($rows as $row) {
            $result[] = new Model_Newsletter_Subscription($row);
        }

        return $result;
    } // getAllNewsletterSubscriptions

    /**
     * Get active newsletter subscriptions for contact
     *
     * @return array
     */
    public function getActiveNewsletterSubscriptions()
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $sql = "SELECT newsletterId FROM newsletter_subscriptions WHERE contactId = :contactId AND subscriptionStatus = 'ACTIVE'";
        $rows = $dbh->query($sql, array('contactId' => $this->getContactId()))->fetchAll();
        $result = array();

        foreach ($rows as $row) {
            $result[] = new Model_Newsletter_Subscription($row);
        }

        return $result;
    } // getActiveNewsletterSubscriptions


    /**
     * Get all contacts
     *
     * @return array
     */
    public static function getAllContacts()
    {
        $select = Zend_Db_Table::getDefaultAdapter()->select();
        $select->from('contacts', '*');
        $rows = $select->query()->fetchAll();
        $result = array();

        foreach ($rows as $row) {
            $result[] = new Model_Contact($row);
        }

        return $result;
    } // getAllContacts

    /**
     * Get a certain number of contacts
     *
     * @param integer | null
     *
     * @return array
     */
    public static function getContacts($limit = null)
    {
        if (is_null($limit)) {
            return self::getAllContacts();
        }

        $select = Zend_Db_Table::getDefaultAdapter()->select();
        $select->from('contacts', '*');

        if ($limit) {
            $select->limit($limit);
        }

        $rows = $select->query()->fetchAll();
        $result = array();

        foreach ($rows as $row) {
            $result[] = new Model_Contact($row);
        }

        return $result;
    } // getContacts

    /**
     * Create alert for contact
     *
     * @param array $params
     *
     * @return Model_Alert
     *
     * @throws Model_Exception_UnsupportedParamValue
     * @throws Model_Exception_MissingRequireParam
     */
    public function createAlert(array $params)
    {
        $errors = Model_Alert::checkAlertParams($params);
        if (!empty($errors)) {
            throw array_pop($errors);
        }

        $site = Model_Site::getByDomain($params['domain']);

        $alertType = trim(strtoupper($params['alertType']));
        $alert = new Model_Alert();
        $alert->setAlertStatus(Model_Alert::STATUS_ACTIVE);
        $alert->setContactId($this->_contactId);
        $alert->setAlertType($alertType);
        $alert->setSiteId($site->getSiteId());
        $alert->generateSecureId();
        $alert->setIsLocationValid(1);

        if (array_key_exists('query', $params)) {
            $alert->setQueryTerms($params['query']);
        }
        if (array_key_exists('location', $params)) {
            $alert->setLocation($params['location']);
        }
        if (array_key_exists('distance', $params)) {
            $alert->setDistance($params['distance']);
        } else {
            $alert->setDistance(Model_Alert::DEFAULT_DISTANCE);
        }
        if (array_key_exists('ip', $params)) {
            $alert->setIp($params['ip']);
        }

        if (array_key_exists('contactStatus',  $params)) {
            $status = strtoupper($params['contactStatus']);
            switch ($params['contactStatus']) {
                case Model_Contact::STATUS_ACTIVE:
                case Model_Contact::STATUS_DEACTIVE:
                case Model_Contact::STATUS_SUSPEND:
                case Model_Contact::STATUS_UNSUBSCRIBED_ADMIN:
                case Model_Contact::STATUS_UNSUBSCRIBED_API:
                case Model_Contact::STATUS_UNSUBSCRIBED_USER:
                        $alert->setAlertStatus($status);
                    break;
            }
        }

        /*
         * Only use the status if the status is pending, otherwise let the
         * default handle it.
         */
        if (array_key_exists('status',  $params)) {
            if ($params['status'] == 'pending') {
                $alert->setAlertStatus(Model_Alert::STATUS_PENDING);
            }
        }

        if (array_key_exists('utm_source',  $params)) {
            $alert->setUtmSource($params['utm_source']);
        }
        if (array_key_exists('utm_medium',  $params)) {
            $alert->setUtmMedium($params['utm_medium']);
        }
        if (array_key_exists('utm_term',  $params)) {
            $alert->setUtmTerm($params['utm_term']);
        }
        if (array_key_exists('utm_content',  $params)) {
            $alert->setUtmContent($params['utm_content']);
        }
        if (array_key_exists('utm_campaign',  $params)) {
            $alert->setUtmCampaign($params['utm_campaign']);
        }

        $alert->insert();
        if (array_key_exists('ip', $params)) {
            $userIp = $params['ip'];
        } else {
            $userIp = $_SERVER['REMOTE_ADDR'];
        }

        Model_AlertOperationHistory::registryOperation(
                $alert,
                Model_AlertOperationHistory::OPERATION_CREATE,
                $userIp
            );

        return $alert;
    } // createAlert

    /**
     * Get newsletter subscription for contact by newsletter
     *
     * @param integer | Model_Newsletter $newsletter
     *
     * @return Model_Newsletter_Subscription | null
     */
    public function getNewsletterSubscription($newsletter)
    {
        $newsletterSubscription = null;

        if ($newsletter instanceof Model_Newsletter) {
            $newsletterId = $newsletter->getNewsletterId();
        } else {
            $newsletterId = $newsletter;
        }

        $dbh = Zend_Db_Table::getDefaultAdapter();
        $select = $dbh->select();
        $select->from('newsletter_subscriptions', '*')
            ->where('contactId = ?', $this->getContactId())
            ->where('newsletterId = ?', $newsletterId);
        $res = $select->query()->fetch();
        if ($res) {
            $newsletterSubscription = new Model_Newsletter_Subscription($res);
        }
        return $newsletterSubscription;
    } // getNewsletterSubscription

    /**
     * Subscribe contact on newsletter
     *
     * @paraminteger | Model_Newsletter $newsletter
     *
     * @return Model_Newsletter_Subscription
     */
    public function subscribeNewsletter($newsletter)
    {
        if ($newsletter instanceof Model_Newsletter) {
            $newsletterId = $newsletter->getNewsletterId();
        } else {
            $newsletterId = $newsletter;
        }

        $newsletterSubscription = $this->getNewsletterSubscription($newsletterId);
        if ($newsletterSubscription == null) {
            $newsletterSubscription = new Model_Newsletter_Subscription();
            $newsletterSubscription->setNewsletterId($newsletterId);
            $newsletterSubscription->setContactId($this->getContactId());
            $newsletterSubscription->setSubscriptionStatus(Model_Newsletter_Subscription::STATUS_ACTIVE);
            $newsletterSubscription->generateSecureId();
            $newsletterSubscription->insert();
        }

        return $newsletterSubscription;
    } // subscribeNewsletter

    /**
     * Get edit url
     *
     * @param Model_Site $site
     *
     * @return string
     */
    public function getEditUrl(Model_Site $site)
    {
        $baseUrl = $site->getBaseUrl();
        $url = $baseUrl . "/contact/edit?i=" . $this->getContactSecureId();
        return $url;
    }

    /**
     * Get location string for contact
     * By default database contains a valid location
     * Only for USA
     *
     * @return string | null
     */
    public function getLocation()
    {
        $location = null;
        if ($this->_country != 'US') {
            $location = null;
        } else if ($this->_zip) {
            $location = $this->_zip;
        } else if ($this->_city && $this->_state) {
            $location = $this->_city . ', ' . $this->_state;
        } else if ($this->_state) {
            $location = $this->_state;
        }

        return $location;
    } // getLocation
}
