<?php
/**
 * Model_AbstractModel
 *
 * PHP Version 5.3
 *
 * @category Abstraction
 * @package  Model
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */
abstract class Model_AbstractModel
{

    /**
     * Key field name
     *
     * @var string
     */
    protected $keyFields = array();

    /**
     * Data base table name for the model
     *
     * @var string
     */
    protected $_tableName;

    /**
     * Primary key field name
     *
     * @var string
     */
    protected $_primaryKey = 'id';

    /**
     * Data base fields
     *
     * @var array
     */
    protected $_tableFields;

    /**
     * Create model class, populate fields from DB
     * Parameter $data might be row from DB table as associative array
     * or id from DB table.
     *
     * @param array/int $data Data from DB table or record ID
     */
    public function __construct($data = null)
    {
        if (is_array($data)) {
            $this->fromArray($data);
        } elseif (is_numeric($data)) {
            $this->find($data);
        }
    } // __construct

    /**
     * Convert object to array.
     *
     * @return array
     */
    public function toArray()
    {
        $data = array();

        foreach ($this->_tableFields as $dbField => $classField) {
            if (is_array($classField)) {
                foreach ($classField as $childDbFiled => $childClassField) {
                    $data[$childDbFiled] = $this->$childClassField;
                }
            } else {
                $data[$dbField] = $this->$classField;
            }
        }

        return $data;
    } // toArray

    /**
     * Populate object from associative array
     *
     * @param array $data Array from DB table
     *
     * @return void
     */
    public function fromArray($data)
    {
        foreach ($this->_tableFields as $dbField => $classField) {
            if (is_array($classField)) {
                foreach ($classField as $childDbFiled => $childClassField) {
                    if (isset($data[$childDbFiled])) {
                        $this->$childClassField = $data[$childDbFiled];
                    }
                }
            } else {
                if (isset($data[$dbField])) {
                    $this->$classField = $data[$dbField];
                }
            }
        }
    } // fromArray

    /**
     * Find record in table with given ID and load it's values into object
     *
     * @param integer $id ID
     *
     * @return boolean true if record was found, else false
     */
    public function find($id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select();
        $select->from($this->_tableName);
        if (empty($this->keyFields)) {
            $select->where($this->_primaryKey . ' = ?', array($id));
        } else {
            $firstKey = $this->keyFields[$this->_tableName];
            $select->where($this->_tableName . '.' . $firstKey . ' = ?', array($id));
            $keys = $this->keyFields;
            unset($keys[$this->_tableName]);
            foreach ($keys as $table => $key) {
                $select->joinInner(
                    $table,
                    "{$table}.{$key} = {$this->_tableName}.{$firstKey}"
                );
            }
        }

        $data = $select->query(Zend_Db::FETCH_ASSOC)->fetch();
        if ($data) {
            $this->fromArray($data);
            return true;
        }

        return false;
    } // find

    /**
     * Insert object into DB
     *
     * @return integer Id of the inserted row
     */
    public function insert()
    {
        $primaryKey = $this->_primaryKey;
        $primaryField = $this->_tableFields[$primaryKey];

        if (empty($this->keyFields)) {

            $bind = $this->toArray();
            // We don't try to insert any value for the primary key
            unset($bind[$primaryKey]);
            $this->$primaryField = $this->_insert($this->_tableName, $bind);

        } else {

            $firstKey = $this->keyFields[$this->_tableName];
            $insert = array();

            foreach ($this->_tableFields[$this->_tableName]
                     as $dbField => $classField) {

                $insert[$dbField] = $this->$classField;
            }

            unset($insert[$firstKey]);
            $keys = $this->keyFields;
            unset($keys[$this->_tableName]);
            $this->id = $this->_insert($this->_tableName, $insert);
            foreach ($keys as $table => $key) {
                $insert = array();
                foreach ($this->_tableFields[$table] as $dbField => $classField) {
                    $insert[$dbField] = $this->$classField;
                }
                $insert[$key] = $this->id;
                $this->_insert($table, $insert);
            }

        }
        return $this->$primaryField;
    } // insert

    /**
     * Update object in DB
     * Return true if row was modified
     *
     * @return boolean
     */
    public function update()
    {
        if (empty($this->keyFields)) {
            $bind = $this->toArray();
            $primaryKey = $this->_primaryKey;
            return $this->_update($this->_tableName, $bind, $primaryKey);
        } else {
            $result = true;
            $firstKey = $this->keyFields[$this->_tableName];
            $insert = array();
            foreach (
                $this->_tableFields[$this->_tableName] as $dbField => $classField
            ) {
                $insert[$dbField] = $this->$classField;
            }
            $keys = $this->keyFields;
            unset($keys[$this->_tableName]);
            $this->_update($this->_tableName, $insert, $firstKey);
            foreach ($keys as $table => $key) {
                $insert = array();
                foreach ($this->_tableFields[$table] as $dbField => $classField) {
                    $insert[$dbField] = $this->$classField;
                }
                $insert[$key] = $this->id;
                $result = $result && $this->_update($table, $insert, $key);
            }
            return $result;
        }
    } // update

    /**
     * Delete object from DB
     *
     * @return boolean Return true is row was deleted from DB
     */
    public function delete()
    {
        if (empty($this->keyFields)) {

            $primaryKey = $this->_primaryKey;
            $primaryField = $this->_tableFields[$this->_primaryKey];

            return $this->_delete(
                $this->_tableName,
                $this->$primaryField,
                $primaryKey
            );

        } else {

            $result = true;
            foreach ($this->keyFields as $table => $key) {
                $result = $result && $this->_delete($table, $this->id, $key);
            }

            return $result;
        }
    } // delete

    /**
     * Retrieve all record from table
     *
     * @param array $params Where Clause
     *
     * @return array
     */
    public function selectAll($params = array())
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select();
        $select->from($this->_tableName);

        foreach ($params as $param => $value) {
            if (array_key_exists($param, $this->_tableFields)) {
                $select->where($param . ' = ?', $value);
            }
        }

        $data = $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
        return $data;
    } // selectAll

    /**
     * Insert data into table
     *
     * @param string $table Table
     * @param array  $bind  Bind Params
     *
     * @return integer Last Insert ID
     */
    protected function _insert($table, $bind)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = $db->quoteIdentifier($table);
        $cols = array();
        $values = array();
        foreach ($bind as $col => $val) {
            $cols[] = $db->quoteIdentifier($col);
            $values[] = '?';

        }

        // build the statement
        $sql = 'INSERT INTO '
             . $table
             . ' (' . implode(', ', $cols) . ') '
             . 'VALUES (' . implode(', ', $values) . ')';

        // execute the statement and return last inserted id
        $stmt = $db->prepare($sql);
        $stmt->execute(array_values($bind));

        return $db->lastInsertId();
    } // _insert

    /**
     * Update table row with given id.
     * Assume that $bind['id'] is table primary key value
     *
     * @param string $table Table
     * @param array  $bind  Bind Params
     * @param string $key   Key to reference
     *
     * @return bool
     */
    protected function _update($table, $bind, $key = 'id')
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = $db->quoteIdentifier($table);
        $id = $bind[$key];
        unset($bind[$key]);
        /**
         * Build "col = ?" pairs for the statement,
         * except for Zend_Db_Expr which is treated literally.
         */
        $set = array();
        foreach ($bind as $col => $val) {
            $set[] = $db->quoteIdentifier($col) . ' = ?';
        }

        $where = $key . ' = ?';

        $bindValues = array_values($bind);
        $bindValues[] = $id;

        /**
         * Build the UPDATE statement
         */
        $sql = 'UPDATE '
             . $table
             . ' SET ' . implode(', ', $set)
             . ' WHERE '. $where;
        // execute the statement and return the number of affected rows
        $stmt = $db->prepare($sql);
        $stmt->execute($bindValues);
        $stmt->rowCount();

        return $stmt->rowCount() > 0;
    } // _update

    /**
     * Delete row in DB with given ID
     *
     * @param string  $table Table
     * @param integer $id    ID
     * @param string  $key   ID Field
     *
     * @return boolean True, if we have deleted the row
     */
    protected function _delete($table, $id, $key = 'id')
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $table = $db->quoteIdentifier($table);

        $sql = 'DELETE FROM ' . $table . ' WHERE ' . $key . ' = ?';
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));

        return $stmt->rowCount() > 0;
    }
}
