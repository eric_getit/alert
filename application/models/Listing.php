<?php
/**
 * Model_Listing
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Listing information on one listing item.
 */
class Model_Listing
{
    /**
     * Organic listings
     *
     * @var string
     */
    const TYPE_ORGANIC = 'organic';

    /**
     * Backfill listings
     *
     * @var string
     */
    const TYPE_BACKFILL = 'backfill';

    /**
     * Listing source API name
     *
     * @var string
     */
    private $_source;

    /**
     * Listing ID from source
     *
     * @var integer
     */
    private $_id;

    /**
     * Creation date (ISO format)
     *
     * @var string
     */
    private $_date;

    /**
     * Title
     *
     * @var string
     */
    private $_title;

    /**
     * Location
     *
     * @var string
     */
    private $_location;

    /**
     * External details url
     *
     * @var string
     */
    private $_externalUrl;

    /**
     * External source name
     *
     * @var string
     */
    private $_externalSource = null;

    /**
     * Listing company
     *
     * @var string
     */
    private $_company;

    /**
     * Listing type
     *
     * @var string
     */
    private $_type;

    /**
     * Get ID
     *
     * @return integer $_id
     */
    public function getId()
    {
        return $this->_id;
    } // getId

    /**
     * Set ID
     *
     * @param integer $id ID
     *
     * @return Model_Listing
     */
    public function setId($id)
    {
        $this->_id = $id;
        return $this;
    } // setId

    /**
     * Get Source
     *
     * @return string $_source
     */
    public function getSource()
    {
        return $this->_source;
    } // getSource

    /**
     * Set Source
     *
     * @param string $source Source
     *
     * @return Model_Listing
     */
    public function setSource($source)
    {
        $this->_source = $source;
        return $this;
    } // setSource

    /**
     * Get Date
     *
     * @return string $_date
     */
    public function getDate()
    {
        return $this->_date;
    } // getDate

    /**
     * Set Date
     *
     * @param string $date Date
     *
     * @return Model_Listing
     */
    public function setDate($date)
    {
        $this->_date = $date;
        return $this;
    } // setDate

    /**
     * Get Title
     *
     * @return string $_title
     */
    public function getTitle()
    {
        return $this->_title;
    } // getTitle

    /**
     * Set Title
     *
     * @param string $title Title
     *
     * @return Model_Listing
     */
    public function setTitle($title)
    {
        $this->_title = $title;
        return $this;
    } // setTitle

    /**
     * Get Location
     *
     * @return string $_location
     */
    public function getLocation()
    {
        return $this->_location;
    } // getLocation

    /**
     * Set Location
     *
     * @param string $location Location
     *
     * @return Model_Listing
     */
    public function setLocation($location)
    {
        $this->_location = $location;
        return $this;
    } // setLocation

    /**
     * Get External URL
     *
     * @return string $_externalUrl
     */
    public function getExternalUrl()
    {
        return $this->_externalUrl;
    } // getExternalUrl

    /**
     * Set External URL
     *
     * @param string $externalUrl External URL
     *
     * @return Model_Listing
     */
    public function setExternalUrl($externalUrl)
    {
        $this->_externalUrl = $externalUrl;
        return $this;
    } // setExternalUrl

    /**
     * Get External Source
     *
     * @return string $_externalSource
     */
    public function getExternalSource()
    {
        return $this->_externalSource;
    } // getExternalSource

    /**
     * Set External Source
     *
     * @param string $externalSource External Source
     *
     * @return Model_Listing
     */
    public function setExternalSource($externalSource)
    {
        $this->_externalSource = $externalSource;
        return $this;
    } // setExternalSource

    /**
     * Get Company
     *
     * @return string $_company
     */
    public function getCompany()
    {
        return $this->_company;
    } // getCompany

    /**
     * Set Company
     *
     * @param string $company Company
     *
     * @return Model_Listing
     */
    public function setCompany($company)
    {
        $this->_company = $company;
        return $this;
    } // setCompany

    /**
     * Get listing type
     *
     * @return string
     */
    public function getType()
    {
        return $this->_type;
    } // getType

    /**
     * Set listing type
     *
     * @param string $type
     *
     * @return Model_Listing
     */
    public function setType($type)
    {
        $this->_type = $type;
        return $this;
    } // setType
}
