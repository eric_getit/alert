<?php
/**
 * Model_City
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Helper class to work with city table
 */
class Model_City extends Model_AbstractModel
{
    /**
     * Primary key
     *
     * @var integer
     */
    protected $_id;

    /**
     * City name
     *
     * @var string
     */
    protected $_name;

    /**
     * City name for search requests
     *
     * @var string
     */
    protected $_searchName;

    /**
     * City state code
     *
     * @var string
     */
    protected $_stateCode;

    /**
     * GEO Names ID for the location object
     *
     * @var integer
     */
    protected $_sourceId;

    /**
     * City population (based on GEO Names)
     *
     * @var integer
     */
    protected $_population;

    /**
     * Latitude
     *
     * @var float
     */
    protected $_lat;

    /**
     * Longitude
     *
     * @var float
     */
    protected $_lng;

    /**
     * Code in location in geoNames
     *
     * @var string
     */
    protected $_code;

    /**
     * Data base table name for the model
     *
     * @var string
     */
    protected $_tableName = 'location_us_city';

    /**
     * Data base fields
     *
     * @var array
     */
    protected $_tableFields = array(
        'id' =>'_id',
        'name' =>'_name',
        'searchName' =>'_searchName',
        'code' => '_code',
        'stateCode' =>'_stateCode',
        'sourceId' =>'_sourceId',
        'population' =>'_population',
        'lat' =>'_lat',
        'lng' =>'_lng',
    );

    /**
     * Get Id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->_id;
    } // getId

    /**
     * Get Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    } // getName

    /**
     * Get Search Name
     *
     * @return string
     */
    public function getSearchName()
    {
        return $this->_searchName;
    } // getSearchName

    /**
     * Get State Code
     *
     * @return string
     */
    public function getStateCode()
    {
        return $this->_stateCode;
    } // getStateCode

    /**
     * Get Source Id
     *
     * @return integer
     */
    public function getSourceId()
    {
        return $this->_sourceId;
    } // getSourceId

    /**
     * Get Population
     *
     * @return integer
     */
    public function getPopulation()
    {
        return $this->_population;
    } // getPopulation

    /**
     * Get Latitude
     *
     * @return float
     */
    public function getLat()
    {
        return $this->_lat;
    } // getLat

    /**
     * Get Longitude
     *
     * @return float
     */
    public function getLng()
    {
        return $this->_lng;
    } // getLng

    /**
     * Get Code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->_code;
    } // getCode
}
