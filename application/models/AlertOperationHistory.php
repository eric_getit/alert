<?php
/**
 * Model_AlertOperationHistory
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Search alerts operation history
 *
 * @author Eugene Churmanov
 */
class Model_AlertOperationHistory extends Model_AbstractModel
{
    /**
     * Operation type when Alert was created
     *
     * @var string
     */
    const OPERATION_CREATE = 'CREATE';

    /**
     * Operation type when Alert was unsubscribed by user
     *
     * @var string
     */
    const OPERATION_UNSUBSCRIBE_USER = 'UNSUBSCRIBE_USER';

    /**
     * Operation type when Alert was unsubscribed by API
     *
     * @var string
     */
    const OPERATION_UNSUBSCRIBE_API = 'UNSUBSCRIBE_API';

    /**
     * Operation type when Alert was unsubscribed by admin
     *
     * @var string
     */
    const OPERATION_UNSUBSCRIBE_ADMIN = 'UNSUBSCRIBE_ADMIN';

    /**
     * Operation type when Alert was suspended by API
     *
     * @var string
     */
    const OPERATION_SUSPEND_API = 'SUSPEND_API';

    /**
     * Operation type when Alert was suspended by admin
     *
     * @var string
     */
    const OPERATION_SUSPEND_ADMIN = 'SUSPEND_ADMIN';

    /**
     * Operation type when Alert was edited by user
     *
     * @var string
     */
    const OPERATION_UPDATED_USER = 'UPDATED_USER';

    /**
     * Operation type when Alert was edited by API
     *
     * @var string
     */
    const OPERATION_UPDATED_API = 'UPDATED_API';

    /**
     * Operation type when Alert was edited by Admin
     *
     * @var string
     */
    const OPERATION_UPDATED_ADMIN = 'UPDATED_ADMIN';

    /**
     * Operation type when Alert was restored by user
     *
     * @var string
     */
    const OPERATION_RESTORED_USER = 'RESTORED_USER';

    /**
     * Operation type when Alert was restored by API
     *
     * @var string
     */
    const OPERATION_RESTORED_API = 'RESTORED_API';

    /**
     * Operation type when Alert was restored by Admin
     *
     * @var string
     */
    const OPERATION_RESTORED_ADMIN = 'RESTORED_ADMIN';


    /**
     * Alert Operation ID (primary key)
     *
     * @var integer
     */
    protected $_alertOperationId;

    /**
     * Alert reference
     *
     * @var integer
     */
    protected $_alertId;

    /**
     * Operation type
     *
     * @var string
     */
    protected $_operation;

    /**
     * IP address from which the operation was performed
     *
     * @var string
     */
    protected $_ip;

    /**
     * Operation creation date and time
     *
     * @var string
     */
    protected $_createdDate;

    /**
     * Data base table name for the model
     *
     * @var string
     */
    protected $_tableName = 'alert_operation_history';

    /**
     * Primary key field name
     *
     * @var string
     */
    protected $_primaryKey = 'alertOperationId';

    /**
     * Data base fields
     *
     * @var array
     */
    protected $_tableFields = array (

            'alertOperationId' => '_alertOperationId',
            'alertId' => '_alertId',
            'operation' => '_operation',
            'ip' => '_ip',
            'createdDate' => '_createdDate',
    );

    /**
     * Get Alert Operation Id
     *
     * @return integer
     */
    public function getAlertOperationId() {
        return $this->_alertOperationId;
    } // getAlertOperationId

    /**
     * Get Alert Id
     *
     * @return integer
     */
    public function getAlertId() {
        return $this->_alertId;
    } // getAlertId

    /**
     * Get Operation
     *
     * @return string
     */
    public function getOperation() {
        return $this->_operation;
    } // getOperation

    /**
     * Get Ip address
     *
     * @return string
     */
    public function getIp() {
        return $this->_ip;
    } // getIp

    /**
     * Get Created Date
     *
     * @return string
     */
    public function getCreatedDate() {
        return $this->_createdDate;
    } // getCreatedDate

    /**
     * Set Alert Id
     *
     * @param integer $alertId
     *
     * @return Model_AlertOperationHistory
     */
    public function setAlertId($alertId) {
        $this->_alertId = $alertId;
        return $this;
    } // setAlertId

    /**
     * Set Operation
     *
     * @param string $operation
     *
     * @return Model_AlertOperationHistory
     */
    public function setOperation($operation) {
        $this->_operation = $operation;
        return $this;
    } // setOperation

    /**
     * Set Ip address
     *
     * @param string $ip
     *
     * @return Model_AlertOperationHistory
     */
    public function setIp($ip) {
        $this->_ip = $ip;
        return $this;
    } // setIp

    /**
     * Set Created Date
     *
     * @param string $createdDate
     *
     * @return Model_AlertOperationHistory
     */
    public function setCreatedDate($createdDate) {
        $this->_createdDate = $createdDate;
        return $this;
    } // setCreatedDate

    /**
     * Insert object into DB
     *
     * @return void
     */
    public function insert()
    {
        $this->setCreatedDate(date('Y-m-d H:i:s'));
        return parent::insert();
    } // insert


    /**
     * Get operation history for alert
     *
     * @param Model_Alert|number $alert
     *
     * @return array
     */
    public static function getHistoryForAlert($alert)
    {
        $alertId = null;
        if ($alert instanceof Model_Alert) {
            $alertId = $alert->getAlertId();
        } else {
            $alertId = intval($alert);
        }

        $select = Zend_Db_Table::getDefaultAdapter()->select();
        $select->from('alert_operation_history', '*')
            ->where('alertId = ?', $alertId)
            ->order('createdDate');
        $rows = $select->query()->fetchAll();
        $result = array();
        foreach ($rows as $row) {
            $result[] = new Model_AlertOperationHistory($row);
        }
        return $result;
    } //getHistoryForAlert

    /**
     * Registry operation
     *
     * @param Model_Alert|number $alert
     * @param string $action
     * @param string $ip
     *
     * @return Model_AlertOperationHistory
     */
    public static function registryOperation($alert, $action, $ip)
    {
        $alertId = null;
        if ($alert instanceof Model_Alert) {
            $alertId = $alert->getAlertId();
        } else {
            $alertId = intval($alert);
        }
        $operation = new Model_AlertOperationHistory();
        $operation->setAlertId($alertId);
        $operation->setIp($ip);
        $operation->setOperation($action);
        $operation->insert();


        return $operation;
    } // registryOperation
}
