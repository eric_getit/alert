<?php
/**
 * Model_ConfigDb
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Model for work with configuration
 */
class Model_ConfigDb extends Model_AbstractModel
{
    /**
     * All configuration values
     *
     * @var array
     */
    static protected $_values = array();

    /**
     * ID (primary key)
     *
     * @var integer
     */
    public $id;

    /**
     * Display Group reference
     *
     * @var integer
     */
    public $displayGroupId;

    /**
     * Name
     *
     * @var string
     */
    public $name;

    /**
     * Name of configuration for show
     *
     * @var string
     */
    public $displayName;

    /**
     * The text that is displayed as a tooltip
     *
     * @var string
     */
    public $displayHelp;

    /**
     * Admin Comment
     *
     * @var string
     */
    public $adminComment;

    /**
     * Configuration value
     *
     * @var string
     */
    public $value;

    /**
     * Date and time when the record was created
     *
     * @var string
     */
    public $created;

    /**
     * Date and time when the record was updated
     *
     * @var string
     */
    public $updated;

    /**
     * Data base fields
     *
     * @var array
     */
    protected $_tableFields = array (
        'id' => 'id',
        'name' => 'name',
        'adminComment' => 'adminComment',
        'configValue' => 'value',
        'displayName' => 'displayName',
        'displayGroupId' => 'displayGroupId',
        'displayHelp' => 'displayHelp',
        'created' => 'created',
        'updated' => 'updated',
    );

    /**
     * Data base table name for the model
     *
     * @var string
     */
    protected $_tableName = "config_db";

    /**
     * Get configuration value by key
     *
     * @param string $key
     *
     * @return array
     */
    public static function get($key)
    {
        if (!array_key_exists($key, self::$_values)) {
            $option = new Model_ConfigDb();
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select();
            $select->from($option->_tableName)
                ->where('name = ?', $key);
            $row = $select->query()->fetch();
            if ($row) {
                self::$_values[$key] = $row['configValue'];
            } else {
                self::$_values[$key] = null;
            }
        }
        return self::$_values[$key];
    } // get

    /**
     * Get key as a boolean value. The values 'Yes', 'True', '1' are converted to true.
     * Other values are converted to false.
     *
     * @param string $key
     *
     * @return boolean
     */
    public static function getBoolean($key)
    {
        $value = self::get($key);
        return $value == 1 || strtolower($value) == 'yes'|| strtolower($value) == 'true';
    } // getBoolean

    /**
     * Get item ID
     */
    public function getId()
    {
        return $this->id;
    } // getId

    /**
     * Get display name
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    } // getDisplayName

    /**
     * Get help message
     *
     * @return string
     */
    public function getDisplayHelp()
    {
        return $this->displayHelp;
    } // getDisplayHelp

    /**
     * Get config value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    } // getValue

    /**
     * Insert object into DB
     *
     * @return integer Id of the inserted row
     */
    public function insert()
    {
        $now = date('Y-m-d H:i:s');
        $this->created = $now;
        $this->updated = $now;

        return parent::insert();
    } // insert

    /**
     * Update object in DB
     * Return true if row was modified
     *
     * @return boolean
     */
    public function update()
    {
        $now = date('Y-m-d H:i:s');
        $this->updated = $now;

        return parent::update();
    } // update
}
