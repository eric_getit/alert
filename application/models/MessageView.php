<?php
/**
 * Model_MessageView
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 *
 * @author Eugene Kuznetsov
 *
 */
class Model_MessageView extends Model_AbstractModel
{

    /**
     * View ID (primary key)
     *
     * @var integer
     */
    protected $_viewId;

    /**
     * Alert reference
     *
     * @var integer
     */
    protected $_alertId;

    /**
     * Message Secure Id
     *
     * @var string
     */
    protected $_messageSecureId;

    /**
     * IP address from which the message has been viewed
     *
     * @var string
     */
    protected $_ip;

    /**
     * User Agent
     *
     * @var string
     */
    protected $_userAgent;

    /**
     * Date and time when the message has been viewed
     *
     * @var string
     */
    protected $_createdDate;

    /**
     * Data base table name for the model
     *
     * @var string
     */
    protected $_tableName = 'message_views';

    /**
     * Primary key field name
     *
     * @var string
     */
    protected $_primaryKey = 'viewId';

    /**
     * Data base fields
     *
     * @var array
     */
    protected $_tableFields = array(
        'viewId' => '_viewId',
        'alertId' => '_alertId',
        'messageSecureId' => '_messageSecureId',
        'ip' => '_ip',
        'userAgent' => '_userAgent',
        'createdDate' => '_createdDate'
    );

    /**
     * Add new view to DB
     *
     * @param Model_MessageArchive $message
     *
     * @return void
     */
    public static function addView(Model_MessageArchive $message)
    {
        $view = new self();
        $view->setMessageSecureId($message->getSecureId());
        $view->setAlertId($message->getAlertId());
        if (array_key_exists('REMOTE_ADDR', $_SERVER)) {
            $view->setIp($_SERVER['REMOTE_ADDR']);
        } else {
            $view->setIp('');
        }
        if (array_key_exists('HTTP_USER_AGENT', $_SERVER)) {
            $userAgent = $_SERVER['HTTP_USER_AGENT'];
            $view->setUserAgent($userAgent);
        } else {
            return;
        }
        if (empty($userAgent)) {
            return;
        }
        $notAvailable = array(
            'facebook',
            'Mediapartners-Google',
            'AppEngine-Google',
            'Microsoft Office Existence Discovery',
            'YahooExternalCache'
        );
        foreach ($notAvailable as $stop) {
            if (strpos(strtoupper($userAgent), strtoupper($stop)) !== false) {
                return;
            }
        }
        $view->setCreatedDate(date('Y-m-d H:i:s'));
        $view->insert();
    } // addView

    /**
     * Get View Id
     *
     * @return integer
     */
    public function getViewId()
    {
        return $this->_viewId;
    } // getViewId

    /**
     * Get Alert Id
     *
     * @return integer
     */
    public function getAlertId()
    {
        return $this->_alertId;
    } // getAlertId

    /**
     * Get Message Secure Id
     *
     * @return string
     */
    public function getMessageSecureId()
    {
        return $this->_messageSecureId;
    } // getMessageSecureId

    /**
     * Get IP Address
     *
     * @return string
     */
    public function getIp()
    {
        return $this->_ip;
    } // getIp

    /**
     * Get User Agent
     *
     * @return string
     */
    public function getUserAgent()
    {
        return $this->_userAgent;
    } // getUserAgent

    /**
     * Get Created Date
     *
     * @return string
     */
    public function getCreatedDate()
    {
        return $this->_createdDate;
    } // getCreatedDate

    /**
     * Set View Id
     *
     * @param integer $viewId
     *
     * @return Model_MessageView
     */
    public function setViewId($viewId)
    {
        $this->_viewId = $viewId;
        return $this;
    } // setViewId

    /**
     * Set Alert Id
     *
     * @param integer $alertId
     *
     * @return Model_MessageView
     */
    public function setAlertId($alertId)
    {
        $this->_alertId = $alertId;
        return $this;
    } // setAlertId

    /**
     * Set Message Secure Id
     *
     * @param string $messageSecureId
     *
     * @return Model_MessageView
     */
    public function setMessageSecureId($messageSecureId)
    {
        $this->_messageSecureId = $messageSecureId;
        return $this;
    } // setMessageSecureId

    /**
     * Set IP Address
     *
     * @param string $ip
     *
     * @return Model_MessageView
     */
    public function setIp($ip)
    {
        $this->_ip = $ip;
        return $this;
    } // setIp

    /**
     * Set User Agent
     *
     * @param string $userAgent
     *
     * @return Model_MessageView
     */
    public function setUserAgent($userAgent)
    {
        $this->_userAgent = $userAgent;
        return $this;
    } // setUserAgent

    /**
     * Set Created Date
     *
     * @param string $createdDate
     *
     * @return Model_MessageView
     */
    public function setCreatedDate($createdDate)
    {
        $this->_createdDate = $createdDate;
        return $this;
    } // setCreatedDate
}
