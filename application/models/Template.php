<?php
/**
 * Model_Template
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Templates for email messages
 */
class Model_Template extends Model_AbstractModel
{

    /**
     * Template ID (primary key)
     *
     * @var int
     */
    protected $_tmplId;

    /**
     * Sender email name
     *
     * @var string
     */
    protected $_tmplFromEmail;

    /**
     * Sender email address
     *
     * @var string
     */
    protected $_tmplEmail;

    /**
     * Template subject
     *
     * @var string
     */
    protected $_tmplSubject;

    /**
     * Template text body
     *
     * @var string
     */
    protected $_tmplBody;

    /**
     * Template HTML body
     *
     * @var string
     */
    protected $_tmplBodyHtml;

    /**
     * When template has been created
     *
     * @var string
     */
    protected $_tmplCreated;

    /**
     * When template has been modified
     *
     * @var string
     */
    protected $_tmplModified;

    /**
     * Reference to trans_email_email table
     *
     * @var int
     */
    protected $_emailId;

    /**
     * Template type
     *
     * @var string
     */
    protected $_tmplType;

    /**
     * BCC email addresses list
     *
     * @var string
     */
    protected $_tmplBcc;

    /**
     * UTM source for URLs in template
     *
     * @var string
     */
    protected $_tmplUtmSource;

    /**
     * UTM medium for URLs in template
     *
     * @var string
     */
    protected $_tmplUtmMedium;

    /**
     * UTM campaign for URLs in template
     *
     * @var string
     */
    protected $_tmplUtmCampaign;

    /**
     * Template from name
     *
     * @var string
     */
    protected $_tmplFromName;

    /**
     * Is template enabled
     *
     * @var bool
     */
    protected $_tmplEnabled;

    /**
     * Template reply to email address
     *
     * @var string
     */
    protected $_tmplReplyToEmail;

    /**
     * Template reply to email name
     *
     * @var string
     */
    protected $_tmplReplyToName;

    /**
     * Template table name
     *
     * @var string
     */
    protected $_tableName = 'trans_email_tmpl';

    /**
     * Primary key field name
     *
     * @var string
     */
    protected $_primaryKey = 'tmpl_id';

    /**
     * Map table columns to class properties
     *
     * @var array
     */
    protected $_tableFields = array(
            'tmpl_id' => '_tmplId',
            'tmpl_fromEmail' => '_tmplFromEmail',
            'tmpl_email' => '_tmplEmail',
            'tmpl_subject' => '_tmplSubject',
            'tmpl_body' => '_tmplBody',
            'tmpl_body_html' => '_tmplBodyHtml',
            'tmpl_created' => '_tmplCreated',
            'tmpl_modified' => '_tmplModified',
            'email_id' => '_emailId',
            'tmpl_type' => '_tmplType',
            'tmpl_bcc' => '_tmplBcc',
            'tmpl_utmSource' => '_tmplUtmSource',
            'tmpl_utmMedium' => '_tmplUtmMedium',
            'tmpl_utmCampaign' => '_tmplUtmCampaign',
            'tmpl_fromName' => '_tmplFromName',
            'tmpl_enable' => '_tmplEnabled',
            'tmpl_replyToEmail' => '_tmplReplyToEmail',
            'tmpl_replyToName' => '_tmplReplyToName',
    );

    /**
     * Get template ID
     *
     * @return int
     */
    public function getTmplId()
    {
        return $this->_tmplId;
    } // getTmplId

    /**
     * Set template ID
     *
     * @param  $tmplId
     *
     * @return Model_Template
     */
    public function setTmplId($tmplId)
    {
        $this->_tmplId = $tmplId;
        return $this;
    } // setTmplId

    /**
     * Get template from email
     *
     * @return string
     */
    public function getTmplFromEmail()
    {
        return $this->_tmplFromEmail;
    } // getTmplfromemail

    /**
     * Set template from email
     *
     * @param string $tmplFromEmail
     *
     * @return Model_Template
     */
    public function setTmplfromemail($tmplFromEmail)
    {
        $this->_tmplFromEmail = $tmplFromEmail;
        return $this;
    } // setTmplfromemail

    /**
     * Get template email
     *
     * @return string
     */
    public function getTmplEmail()
    {
        return $this->_tmplEmail;
    } // getTmplemail

    /**
     * Set template email
     *
     * @param string $tmplEmail
     *
     * @return Model_Template
     */
    public function setTmplEmail($tmplEmail)
    {
        $this->_tmplEmail = $tmplEmail;
        return $this;
    } // setTmplEmail

    /**
     * Get template subject
     *
     * @return string
     */
    public function getTmplSubject()
    {
        return $this->_tmplSubject;
    } // getTmplsubject

    /**
     * Set template subject
     *
     * @param string $tmplSubject
     *
     * @return Model_Template
     */
    public function setTmplSubject($tmplSubject)
    {
        $this->_tmplSubject = $tmplSubject;
        return $this;
    } // setTmplsubject

    /**
     * Get template text body
     *
     * @return string
     */
    public function getTmplBody()
    {
        return $this->_tmplBody;
    } // getTmplBody

    /**
     * Set template text body
     *
     * @param string $tmplBody
     *
     * @return Model_Template
     */
    public function setTmplBody($tmplBody)
    {
        $this->_tmplBody = $tmplBody;
        return $this;
    } // setTmplBody

    /**
     * Get template HTML body
     *
     * @return string
     */
    public function getTmplBodyHtml()
    {
        return $this->_tmplBodyHtml;
    } // getTmplBodyHtml

    /**
     * Get template HTML body
     *
     * @param string $tmplBodyHtml
     *
     * @return Model_Template
     */
    public function setTmplBodyHtml($tmplBodyHtml)
    {
        $this->_tmplBodyHtml = $tmplBodyHtml;
        return $this;
    } // setTmplBodyHtml

    /**
     * Get template created date
     *
     * @return string
     */
    public function getTmplCreated()
    {
        return $this->_tmplCreated;
    } // getTmplCreated

    /**
     * Set template created date
     *
     * @param string $tmplCreated
     *
     * @return Model_Template
     */
    public function setTmplCreated($tmplCreated)
    {
        $this->_tmplCreated = $tmplCreated;
        return $this;
    } // setTmplCreated

    /**
     * Get template last modified date
     *
     * @return string
     */
    public function getTmplModified()
    {
        return $this->_tmplModified;
    } // getTmplModified

    /**
     * Set template last modified date
     *
     * @param string $tmplModified
     *
     * @return Model_Template
     */
    public function setTmplModified($tmplModified)
    {
        $this->_tmplModified = $tmplModified;
        return $this;
    } // setTmplModified

    /**
     * Get email ID
     *
     * @return integer
     */
    public function getEmailId()
    {
        return $this->_emailId;
    } // getEmailId

    /**
     * Set email ID
     *
     * @param integer $emailId
     *
     * @return Model_Template
     */
    public function setEmailId($emailId)
    {
        $this->_emailId = $emailId;
        return $this;
    } // setEmailId

    /**
     * Get template type
     *
     * @return string
     */
    public function getTmplType()
    {
        return $this->_tmplType;
    } // getTmplType

    /**
     * Set template type
     *
     * @param string $tmplType
     *
     * @return Model_Template
     */
    public function setTmplType($tmplType)
    {
        $this->_tmplType = $tmplType;
        return $this;
    } // setTmplType

    /**
     * Get template BCC emails list
     *
     * @return string
     */
    public function getTmplBcc()
    {
        return $this->_tmplBcc;
    } // getTmplBcc

    /**
     * Set template BCC emails list
     *
     * @param string $tmplBcc
     *
     * @return Model_Template
     */
    public function setTmplBcc($tmplBcc)
    {
        $this->_tmplBcc = $tmplBcc;
        return $this;
    } // setTmplBcc

    /**
     * Get template UTM source
     *
     * @return string
     */
    public function getTmplUtmsSorce()
    {
        return $this->_tmplUtmSource;
    } // getTmplUtmsSorce

    /**
     * Set template UTM source
     *
     * @param string $tmplUtmSource
     *
     * @return Model_Template
     */
    public function setTmplUtmSource($tmplUtmSource)
    {
        $this->_tmplUtmSource = $tmplUtmSource;
        return $this;
    } // setTmplUtmSource

    /**
     * Get template UTM medium
     *
     * @return string
     */
    public function getTmplUtmMedium()
    {
        return $this->_tmplUtmMedium;
    } // getTmplUtmMedium

    /**
     * Set template UTM medium
     *
     * @param string $tmplUtmMedium
     *
     * @return Model_Template
     */
    public function setTmplUtmMedium($tmplUtmMedium)
    {
        $this->_tmplUtmMedium = $tmplUtmMedium;
        return $this;
    } // setTmplUtmMedium

    /**
     * Get template UTM campaign
     *
     * @return string
     */
    public function getTmplUtmCampaign()
    {
        return $this->_tmplUtmCampaign;
    } // getTmplUtmCampaign

    /**
     * Set template UTM campaign
     *
     * @param string $tmplUtmCampaign
     *
     * @return Model_Template
     */
    public function setTmplUtmCampaign($tmplUtmCampaign)
    {
        $this->_tmplUtmCampaign = $tmplUtmCampaign;
        return $this;
    } // setTmplUtmCampaign

    /**
     * Get template from name
     *
     * @return string
     */
    public function getTmplFromName()
    {
        return $this->_tmplFromName;
    } // getTmplFromName

    /**
     * Set template from name
     *
     * @param string $tmplFromName
     *
     * @return Model_Template
     */
    public function setTmplFromName($tmplFromName)
    {
        $this->_tmplFromName = $tmplFromName;
        return $this;
    } // setTmplFromName

    /**
     * Is template enabled
     *
     * @return bool
     */
    public function isTmplEnabled()
    {
        return $this->_tmplEnable;
    } // isTmplEnabled

    /**
     * Enable or disable template
     *
     * @param bool $tmplEnable
     *
     * @return Model_Template
     */
    public function setTmplEnabled($tmplEnable)
    {
        $this->_tmplEnable = $tmplEnable;
        return $this;
    } // setTmplEnabled

    /**
     * Get template reply to email
     *
     * @return string
     */
    public function getTmplReplyToEmail()
    {
        return $this->_tmplReplyToEmail;
    } // getTmplReplyToEmail

    /**
     * Set template reply to email
     *
     * @param string $tmplReplyToEmail
     *
     * @return Model_Template
     */
    public function setTmplReplyToEmail($tmplReplyToEmail)
    {
        $this->_tmplReplyToEmail = $tmplReplyToEmail;
        return $this;
    } // setTmplReplyToEmail

    /**
     * Get template reply to name
     *
     * @return string
     */
    public function getTmplReplyToName()
    {
        return $this->_tmplReplyToName;
    } // getTmplReplyToName

    /**
     * Set template reply to name
     *
     * @param string $tmplReplyToName
     *
     * @return Model_Template
     */
    public function setTmplReplyToName($tmplReplyToName)
    {
        $this->_tmplReplyToName = $tmplReplyToName;
        return $this;
    } // setTmplReplyToName
}
