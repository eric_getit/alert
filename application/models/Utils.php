<?php
/**
 * Model_Utils
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * General functions used in application.
 *
 */
class Model_Utils
{

    /**
     * Count show cities in all states page
     *
     * @var integer
     */
    const COUNT_SHOW_CITIES= 9;

    /**
     * Get current date and time in SQL format
     *
     * @return string
     */
    public static function sqlNow()
    {
        $now = new DateTime();
        $nowStr = $now->format('Y-m-d H:i:s');
        return $nowStr;
    } // sqlNow

    /**
     * Checks whether a connection to the database active.
     * If not then re-create the connection.
     *
     * @return Zend_Db_Adapter_Pdo_Mysql
     */
    public static function getConnection()
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();
        if (!$dbh->isConnected()) {
            $config = $dbh->getConfig();
            $dbh = Zend_Db::factory('pdo_mysql', $config);
            Zend_Db_Table::setDefaultAdapter($dbh);
        }

        return $dbh;
    } // getConnection

    /**
     * Shorten the string up to the maximum length
     *
     * @param string $str
     * @param integer $maxLen
     * @param boolean $addDots
     *
     * @return string
     */
    static function shortenText($str, $maxLen, $addDots = true)
    {
        if (strlen($str) > $maxLen) {
            // TODO : improve implementation
            $str = trim(substr($str, 0, $maxLen));

            $specialChars = array('.', ',', '!', ';', '?', ' ');

            while (in_array(substr($str, -1), $specialChars)) {
                $str = substr($str, 0, -1);
            }
            if ($addDots) {
               $str .= '...';
            }
        }
        return $str;
    } // shortenText

    /**
     * Generate unique token string
     *
     * @return string
     */
    public static function generateUniqueToken()
    {
        $code = "";
        $useChars = "ABCDEFGHGKLMNPQRSTUVWXYZ";
        $time = strval(time());
        for ($i = 0; $i < strlen($time); $i++) {
            $code .= $time[$i] . $useChars[mt_rand(0, strlen($useChars) - 1)];
        }

        return $code;
    } // generateUniqueToken

    /**
     * Generate GUID following RFC4122 section 4.4
     *
     * @see http://www.ietf.org/rfc/rfc4122.txt
     * @see http://en.wikipedia.org/wiki/Globally_unique_identifier
     * @see http://www.php.net/manual/ru/function.uniqid.php#88400
     * @see http://stackoverflow.com/a/15875555/2111430
     *
     * @return string
     */
    public static function guid()
    {
        $data = openssl_random_pseudo_bytes(16);

        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0010
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 01

        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    } // guid

    /**
     * Get list of time zones where current time is equal to the scheduled
     * alert sending time.
     *
     * Most of the time zones on land are offset from Coordinated
     * Universal Time (UTC) by a whole number of hours (UTC−12 to UTC+14).
     * We ignore offsets different then the whole number of hours.
     *
     * @see http://en.wikipedia.org/wiki/Time_zone
     *
     * Offsets relative to EST are in the range from EST+19 to EST-7.
     *
     * @param string $sendHour Hour when we should send alerts in the format HH     *
     * @param string $curHour Hour relative to which will be calculated timezones in the format HH
     *
     * @return array List of timezones
     */
    public static function getTimezones($sendHour, $curHour)
    {
        $timezones = array();

        $maxOffset = 19;
        $minOffset = -7;

        /*
         * $curHour + $timezone = $sendHour (mod 24)
         * $timezone = $sendHour - $curHour (mod 24)
         *
         * Potential solutions are
         * ($sendHour - $curHour) - 24,
         * $sendHour - $curHour,
         * ($sendHour - $curHour) + 24
         *
         *  @see http://en.wikipedia.org/wiki/Modular_arithmetic
         */

        $timezone = $sendHour - $curHour;

        /*
         * $timezone value might be in the range [-23, +23]
         * Allowed values are in the range [-7, 19]
         * Let's check all possible options
         */

        if (-7 <= $timezone - 24) {
            $timezones[] = 'EST' . ($timezone - 24);
        }
        if (-7 <= $timezone && $timezone <= 19) {
            $timezones[] = ($timezone >= 0 ? 'EST+' : 'EST') . $timezone;
        }
        if ($timezone + 24 <= 19) {
            $timezones[] = 'EST+' . ($timezone + 24);
        }

        return $timezones;
    } // getTimezones

    /**
     * Escape special characters for elastic search query
     *
     * @param string $str
     *
     * @return string
     */
    public static function escapeString($str)
    {
        $specialChars = array('\\', '+', '-', '&', '|', '!', '(', ')', '{', '}', '[', ']', '^', '"', '~', '*', '?', ':');
        $replaced = array('\\\\', '\+', '\-', '\&', '\|', '\!', '\(', '\)', '\{', '\}', '\[', '\]', '\^', '\"', '\~', '\*', '\?', '\:');
        return str_replace($specialChars, $replaced, $str);
    } // escapeString
}
