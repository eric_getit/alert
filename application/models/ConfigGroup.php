<?php
/**
 * Model_ConfigGroup
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Model for work with configuration groups
 */
class Model_ConfigGroup extends Model_AbstractModel
{

    /**
     * ID (primary key)
     *
     * @var integer
     */
    public $id;

    /**
     * System name of group
     *
     * @var integer
     */
    public $name;

    /**
     * Name of group for show
     *
     * @var string
     */
    public $displayName;

    /**
     * The text that is displayed as a tooltip
     *
     * @var string
     */
    public $displayHelp;

    /**
     * Position on screen
     *
     * @var integer
     */
    public $position;

    /**
     * Data base fields
     *
     * @var array
     */
    protected $_tableFields = array (
        'id' => 'id',
        'name' => 'name',
        'displayName' => 'displayName',
        'displayHelp' => 'displayHelp',
        'position' => 'position',
    );

    /**
     * Data base table name for the model
     *
     * @var string
     */
    protected $_tableName = "config_group";
}
