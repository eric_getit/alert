<?php
/**
 * Model_EmailClick
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Track email clicks
 * @author Eugene Kuznetsov
 */
class Model_EmailClick extends Model_AbstractModel
{
    /**
     * Primary key
     *
     * @var integer
     */
    protected $_id;

    /**
     * Foreign key reference to alerts table
     *
     * @var integer
     */
    protected $_alertId;

    /**
     * URL type
     *
     * @var string
     */
    protected $_type;

    /**
     * Email link code
     *
     * @var string
     */
    protected $_linkCode;

    /**
     * User IP address
     *
     * @var string
     */
    protected $_ip;

    /**
     * User browser agent
     *
     * @var string
     */
    protected $_userAgent;

    /**
     * Created date and time
     *
     * @var integer
     */
    protected $_created;

    /**
     * Data base table name for the model
     *
     * @var string
     */
    protected $_tableName = 'email_click';

    /**
     * Primary key field name
     *
     * @var string
     */
    protected $_primaryKey = 'id';

    /**
     * Data base fields
     *
     * @var array
     */
    protected $_tableFields = array(
        'id' => '_id',
        'alert_id' => '_alertId',
        'type' => '_type',
        'link_code' => '_linkCode',
        'ip' => '_ip',
        'user_agent' => '_userAgent',
        'created' => '_created'
    );

    /**
     * Add new click to DB
     *
     * @param Model_EmailLink $link
     *
     * @return void
     */
    public function addClick(Model_EmailLink $link)
    {
        if (array_key_exists('HTTP_USER_AGENT', $_SERVER)) {
            $userAgent = trim($_SERVER['HTTP_USER_AGENT']);
        } else {
            return;
        }
        if (empty($userAgent)) {
            return;
        }
        $notAvailable = array(
            'facebook',
            'Mediapartners-Google',
            'AppEngine-Google',
            'Microsoft Office Existence Discovery',
            'YahooExternalCache'
        );
        foreach ($notAvailable as $stop) {
            if (strpos(strtoupper($userAgent), strtoupper($stop)) !== false) {
                return;
            }
        }
        $this->setLinkCode($link->getCode());
        $this->setType($link->getType());
        if (array_key_exists('REMOTE_ADDR', $_SERVER)) {
            $this->setIp($_SERVER['REMOTE_ADDR']);
        } else {
            $this->setIp('');
        }
        $this->setUserAgent($userAgent);
        $this->setCreated(date('Y-m-d H:i:s'));
        $this->insert();
    } // addClick

    /**
     * Get Id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->_id;
    } // getId

    /**
     * Get Alert Id
     *
     * @return integer
     */
    public function getAlertId()
    {
        return $this->_alertId;
    } // getAlertId

    /**
     * Get URL type. For example: 'organic', 'external'
     *
     * @return string
     */
    public function getType()
    {
        $this->_type;
    } // getType

    /**
     * Get Link Code
     *
     * @return string
     */
    public function getLinkCode()
    {
        return $this->_linkCode;
    } // getLinkCode

    /**
     * Get user IP address
     *
     * @return string
     */
    public function getIp()
    {
        return $this->_ip;
    } // getIp

    /**
     * Get User Agent
     *
     * @return string
     */
    public function getUserAgent()
    {
        return $this->_userAgent;
    } // getUserAgent

    /**
     * Get Created Date
     *
     * @return the $_created
     */
    public function getCreated()
    {
        return $this->_created;
    } // getCreated

    /**
     * Set Id
     *
     * @param integer $id
     *
     * @return Model_EmailClick
     */
    public function setId($id)
    {
        $this->_id = $id;
        return $this;
    } // setId

    /**
     * Set alert ID
     *
     * @param integer $alertId
     *
     * @return Model_EmailClick
     */
    public function setAlertId($alertId)
    {
        $this->_alertId = $alertId;
        return $this;
    } // setAlertId

    /**
     * Set URL type
     *
     * @param string $type
     *
     * @return Model_EmailClick
     */
    public function setType($type)
    {
        $this->_type = $type;
        return $this;
    } // setType

    /**
     * Set gateway URL code
     *
     * @param string $linkCode
     *
     * @return Model_EmailClick
     */
    public function setLinkCode($linkCode)
    {
        $this->_linkCode = $linkCode;
        return $this;
    } // setLinkCode

    /**
     * Set IP adderss
     *
     * @param string $ip
     *
     * @return Model_EmailClick
     */
    public function setIp($ip)
    {
        $this->_ip = $ip;
        return $this;
    } // setIp

    /**
     * Set User Agent
     *
     * @param string $userAgent
     *
     * @return Model_EmailClick
     */
    public function setUserAgent($userAgent)
    {
        $this->_userAgent = $userAgent;
        return $this;
    } // setUserAgent

    /**
     * Set Created Date
     *
     * @param string $created
     *
     * @return Model_EmailClick
     */
    public function setCreated($created)
    {
        $this->_created = $created;
        return $this;
    } // setCreated
}
