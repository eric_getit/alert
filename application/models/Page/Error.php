<?php
/**
 * Model_Page_Error
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Page
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Class for construct Error page
 *
 * @author Eugene Churmanov
 *
 */
class Model_Page_Error extends Model_Page
{
    /**
     * Name of the template to display the page
     *
     * @var string
     */
    protected $_templateName = 'error.html.twig';

    /**
     * Page meta title
     *
     * @var string
     */
    protected $_metaTitle = 'Error';

    /**
     * Page title
     *
     * @var string
     */
    protected $_title = 'Error';


    /**
     * Create Error Page
     *
     * @param array $params
     *
     * @return self
     */
    public function __construct($params)
    {
        parent::__construct($params);
        $this->_twigVars['message'] = $params['message'];
        $this->_twigVars['trace'] = $params['trace'];
    } // __construct

}
