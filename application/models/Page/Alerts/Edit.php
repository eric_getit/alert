<?php
/**
 * Model_Page_Alerts_Edit
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Page\Alerts
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Class for construct Edit Page
 *
 * @author Eugene Churmanov
 *
 */
class Model_Page_Alerts_Edit extends Model_Page
{
    /**
     * Name of the template to display the page
     *
     * @var string
     */
    protected $_templateName = 'alerts/edit.html.twig';

    /**
     * Page meta title
     *
     * @var string
     */
    protected $_metaTitle = 'Edit Alert';

    /**
     * Page title
     *
     * @var string
     */
    protected $_title = 'Edit Alert';


    /**
     * Create Edit Page
     *
     * @param array $params
     *
     * @return self
     *
     * @throws Model_Exception_MissingRequireParam
     */
    public function __construct($params)
    {
        parent::__construct($params);
        if (!array_key_exists('alert', $params)) {
            throw new Model_Exception_MissingRequireParam('alert');
        }
    } // __construct

    /**
     * Init view parameters
     *
     * @return void
     */
    public function initView()
    {
        if (array_key_exists('submit', $this->_params)) {
            $this->_twigVars['errorMessages'] = array();
            if (array_key_exists('query', $this->_params)) {
                $this->_alert->setQueryTerms($this->_params['query']);
            }
            if (array_key_exists('location', $this->_params)) {
                $this->_alert->setLocation($this->_params['location']);
            }
            if (array_key_exists('distance', $this->_params)) {
                if ($this->_params['distance'] === '') {
                    $this->_alert->setDistance(null);
                } else {
                    $this->_alert->setDistance(intval($this->_params['distance']));
                }
            }
            if (array_key_exists('alertType', $this->_params)) {
                $type = $this->_alert->getAlertType();
                if ($this->_params['alertType'] == Model_Alert::TYPE_DAILY) {
                    $this->_alert->setAlertType(Model_Alert::TYPE_DAILY);
                    $this->_alert->updateNextProcessDate();
                }
                if ($this->_params['alertType'] == Model_Alert::TYPE_WEEKLY) {
                    $this->_alert->setAlertType(Model_Alert::TYPE_WEEKLY);
                    $this->_alert->updateNextProcessDate();
                }
                if ($type != $this->_alert->getAlertType()) {
                    $this->_twigVars['nextProcessDate'] = $this->_alert->getNextProcessDate();
                }
            }
            if (!$this->_alert->getQueryTerms() && !$this->_alert->getLocation()) {
                $this->_twigVars['errorMessages']['query'] = 'Enter your <b>keywords</b> or <b>location</b>.';
                $this->_twigVars['successUpdate'] = false;
            } else if ($this->_alert->getIsLocationValid() == 0) {
                $this->_twigVars['errorMessages']['location'] = 'Enter valid <b>location</b>.';
                $this->_twigVars['successUpdate'] = false;
            } else {
                $date = date('Y-m-d H:i:s');
                $this->_alert->setUpdatedDate($date);
                $this->_alert->update();
                Model_AlertOperationHistory::registryOperation(
                    $this->_alert,
                    Model_AlertOperationHistory::OPERATION_UPDATED_USER,
                    $_SERVER['REMOTE_ADDR']
                );
                $this->_twigVars['successUpdate'] = true;
            }
        }

        $this->_twigVars['contact'] = $this->_contact;
        $this->_twigVars['alert'] = $this->_alert;

        $this->_title = 'Update Alert "';
        if ($this->_alert->getQueryTerms()) {
            $this->_title .= $this->_alert->getQueryTerms();
        }
        if ($this->_alert->getQueryTerms() && $this->_alert->getLocation()) {
            $this->_title .= ' in ';
        }
        if ($this->_alert->getLocation()) {
            $this->_title .= $this->_alert->getLocation();
        }
        $this->_title .= '" for ';
        if ($this->_contact->getName()) {
            $this->_title .= $this->_contact->getName() . '('
                . $this->_contact->getEmail() . ')';
        } else {
            $this->_title .= $this->_contact->getEmail();
        }
        $this->_metaTitle = $this->_title;

        parent::initView();
    } // initView

}
