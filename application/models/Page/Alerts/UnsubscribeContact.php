<?php
/**
 * Model_Page_Alerts_UnsubscribeContact
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Page\Alerts
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Class for construct Unsubscribe Page
 *
 * @author Eugene Churmanov
 *
 */
class Model_Page_Alerts_UnsubscribeContact extends Model_Page
{
    /**
     * Name of the template to display the page
     *
     * @var string
     */
    protected $_templateName = 'alerts/unsubscribe-contact.html.twig';

    /**
     * Page meta title
     *
     * @var string
     */
    protected $_metaTitle = 'Unsubscribe Subscription';

    /**
     * Page title
     *
     * @var string
     */
    protected $_title = 'Unsubscribe Subscription';


    /**
     * Create Unsubscribe Page
     *
     * @param array $params
     *
     * @return self
     *
     * @throws Model_Exception_MissingRequireParam
     */
    public function __construct($params)
    {
        if (!array_key_exists('alert', $params) && !array_key_exists('contact', $params)) {
            throw new Model_Exception_MissingRequireParam('alert or contact');
        }
        parent::__construct($params);
    } // __construct

    /**
     * Init view parameters
     *
     * @return void
     */
    public function initView()
    {
        if (array_key_exists('confirm', $this->_params)) {
            if ($this->_params['confirm'] == 'yes') {
                $this->_contact->setContactStatus(Model_Contact::STATUS_UNSUBSCRIBED_USER);
                $this->_contact->update();
                $alerts = $this->_contact->getActiveAlerts();
                foreach ($alerts as $alert) {
                    /* @var $alert Model_Alert */
                    $alert->setAlertStatus(Model_Alert::STATUS_UNSUBSCRIBED_USER);
                    $alert->update();
                    Model_AlertOperationHistory::registryOperation(
                            $alert,
                            Model_AlertOperationHistory::OPERATION_UNSUBSCRIBE_USER,
                            $_SERVER['REMOTE_ADDR']
                        );
                }
            }
        }
        if ($this->_contact->getContactStatus() != Model_Contact::STATUS_ACTIVE) {
            $this->_twigVars['confirm'] = true;
        }

        $this->_twigVars['contact'] = $this->_contact;

        $this->_title = 'Unsubscribe from all Alerts for "' . $this->_contact->getEmail() . '"';

        $this->_metaTitle = $this->_title;

        parent::initView();
    } // initView

}
