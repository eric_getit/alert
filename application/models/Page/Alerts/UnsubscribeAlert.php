<?php
/**
 * Model_Page_Alerts_UnsubscribeAlert
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Page\Alerts
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Class for construct Unsubscribe Page
 *
 * @author Eugene Churmanov
 *
 */
class Model_Page_Alerts_UnsubscribeAlert extends Model_Page
{
    /**
     * Name of the template to display the page
     *
     * @var string
     */
    protected $_templateName = 'alerts/unsubscribe-alert.html.twig';

    /**
     * Page meta title
     *
     * @var string
     */
    protected $_metaTitle = 'Unsubscribe Alert';

    /**
     * Page title
     *
     * @var string
     */
    protected $_title = 'Unsubscribe Alert';

    /**
     * Create Unsubscribe Page
     *
     * @param array $params
     *
     * @return self
     *
     * @throws Model_Exception_MissingRequireParam
     */
    public function __construct($params)
    {
        parent::__construct($params);
        if (!array_key_exists('alert', $params)) {
            throw new Model_Exception_MissingRequireParam('alert');
        }
    } // __construct

    /**
     * Init view parameters
     *
     * @return void
     */
    public function initView()
    {
        if (array_key_exists('confirm', $this->_params)) {
            if ($this->_params['confirm'] == 'yes') {
                $this->_alert->setAlertStatus(Model_Alert::STATUS_UNSUBSCRIBED_USER);
                $this->_alert->update();
                Model_AlertOperationHistory::registryOperation(
                        $this->_alert,
                        Model_AlertOperationHistory::OPERATION_UNSUBSCRIBE_USER,
                        $_SERVER['REMOTE_ADDR']
                    );
            }
        }
        if ($this->_alert->getAlertStatus() != Model_Alert::STATUS_ACTIVE) {
            $this->_twigVars['confirm'] = true;
        }

        $this->_twigVars['contact'] = $this->_contact;
        $this->_twigVars['alert'] = $this->_alert;

        $this->_title = 'Unsubscribe Alert "';
        if ($this->_alert->getQueryTerms()) {
            $this->_title .= $this->_alert->getQueryTerms();
        }
        if ($this->_alert->getQueryTerms() && $this->_alert->getLocation()) {
            $this->_title .= ' in ';
        }
        if ($this->_alert->getLocation()) {
            $this->_title .= $this->_alert->getLocation();
        }
        $this->_title .= '" for ';
        if ($this->_contact->getName()) {
            $this->_title .= $this->_contact->getName() . ' ('
                . $this->_contact->getEmail() . ')';
        } else {
            $this->_title .= $this->_contact->getEmail();
        }
        $this->_metaTitle = $this->_title;

        parent::initView();
    } // initView

}
