<?php
/**
 * Model_Location
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Model_Location
 *
 * Store location information for the request
 *
 * Parse location string
 */
class Model_Location
{
    /**
     * Country 2 letters code following 'ISO 3166-1 alpha-2' standard
     *
     * @var string
     */
    protected $_countryCode;

    /**
     * Country name in English
     *
     * @var string
     */
    protected $_country;

    /**
     * Two letters state code
     *
     * @var string
     */
    protected $_stateCode;

    /**
     * Long state name
     *
     * @var string
     */
    protected $_state;

    /**
     * City name
     *
     * @var string
     */
    protected $_city;

    /**
     * Digital Zip code
     *
     * @var string
     */
    protected $_zipCode;

    /**
     * Population in location
     *
     * @var integer
     */
    protected $_population;

    /**
     * Latitude
     *
     * @var float
     */
    protected $_lat;

    /**
     * Longitude
     *
     * @var float
     */
    protected $_lng;

    /**
     * Is location valid
     *
     * @var boolean
     */
    protected $_valid;

    /**
     * Table for cities
     *
     * @var string
     */
    protected $_tableCity = 'location_us_city';

    /**
     * Table for zip codes
     *
     * @var string
     */
    protected $_tableZip = 'location_us_zip';

    /**
     * Get Country Code
     *
     * @return string $_countryCode
     */
    public function getCountryCode()
    {
        return $this->_countryCode;
    } // getCountryCode

    /**
     * Get Country
     *
     * @return string $_country
     */
    public function getCountry()
    {
        return $this->_country;
    } // getCountry

    /**
     * Get full state name
     *
     * @return string
     */
    public function getState()
    {
        return $this->_state;
    } // getState

    /**
     * Get state abbreviation
     *
     * @return string
     */
    public function getStateCode()
    {
        return $this->_stateCode;
    } // getStateCode

    /**
     * Get city name
     *
     * @return string
     */
    public function getCity()
    {
        return $this->_city;
    } // getCity

    /**
     * Get Zip Code
     *
     * @return integer $_zipCode
     */
    public function getZipCode()
    {
        return $this->_zipCode;
    } // getZipCode

    /**
     * Get Population
     *
     * @return integer $_population
     */
    public function getPopulation()
    {
        return $this->_population;
    } // getPopulation

    /**
     * Get Latitude
     *
     * @return float $_lat
     */
    public function getLat()
    {
        return $this->_lat;
    } // getLat

    /**
     * Get Longitude
     *
     * @return float $_lng
     */
    public function getLng()
    {
        return $this->_lng;
    } // getLng

    /**
     * Build location for given location string
     *
     * Possible location formats are:
     * 1.  <Long state name>
     * 2.  <Short state code>
     * 3.  <City name>
     * 4.  <City name>, <short state code>
     * 5.  <City name>, <Long state name>
     * 6.  <Zip code>
     * 7.  <City name> <Zip code>
     * 8.  <City name>, <short state code> <Zip code>
     * 9.  <City name>, <Long state name> <Zip code>
     * 10. <City name> <short state code>
     * 11. <City name> <Long state name>
     *
     * @param string $location Location
     *
     * @return Model_Location
     */
    public function __construct($location = null)
    {
        $this->_country = 'United States';
        $this->_countryCode = 'US';
        $this->_valid = true;

        if ($location == null || preg_match('/^us[a]{0,1}$/i', $location)) {
            return;
        }

        // Cleanup the location string
        $location = str_replace('-', ' ', $location);
        $location = str_replace(',', ' ', $location);
        $location = trim($location);
        while (strpos($location, '  ') !== false) {
            $location = str_replace('  ', ' ', $location);
        }

        $locationParts = explode(' ', $location);

        // Check if location ends up with zip/postal code
        $zip = $locationParts[count($locationParts)-1];
        if (is_numeric($zip)) {
            if ($this->_findLocationByZip($zip)) {
                // We have found zip code, nothing left to do here.
                return;
            }
        }

        // Start with longest state name (remember District of Columbia)
        $stateLength = 3;

        while ($stateLength > 0) {
            // Take state name from the end of location and check it
            $state = implode(' ', array_slice($locationParts, -$stateLength));
            $stateCode = Model_StateList::normalize($state);
            if ($stateCode) {

                if (count($locationParts) == $stateLength) {
                    // We have only state name in location
                    $this->_stateCode = $stateCode;
                    $this->_state = Model_StateList::getName($stateCode);
                    // Just a state name, nothing more to do.
                    return;
                } else {
                    //
                    $city = implode(' ', array_slice($locationParts, 0, -$stateLength));
                    if ($this->_findLocationByCity($city, $stateCode)) {
                        // We have found location matching city and state
                        return;
                    }
                }
            }
            $stateLength--;
        }

        // We failed to match the location with 'city name state name' pattern.
        // Try to find biggest city for the location
        $this->_valid = $this->_findLocationByCity($location, null);
    } // __construct

    /**
     * Find biggest city for given name and state code
     *
     * @param string $city      City name
     * @param string $stateCode State code, might be NULL
     *
     * @return boolean
     */
    private function _findLocationByCity($city, $stateCode)
    {
        $city = preg_replace('/^st[. ]{1}/i', 'Saint ', $city, 1);
        $city = preg_replace('/\s{2,}/', ' ', $city);

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select();
        $select->from($this->_tableCity);
        $select->where('searchName = ?', $city);
        if ($stateCode != null) {
            $select->where('stateCode = ?', $stateCode);
        }
        $select->order('population desc');
        $select->limit(1);
        $data = $select->query()->fetch();

        if (is_array($data)) {

            $this->_populateFromArray($data);

        } else {

            $select = $db->select();
            $select->from($this->_tableCity . '_synonyms', 'cityId');
            $select->where('searchName = ?', $city);
            if ($stateCode != null) {
                $select->where('stateCode = ?', $stateCode);
            }
            $select->limit(1);
            $data = $select->query()->fetch();

            if (is_array($data)) {

                $cityId = $data['cityId'];
                $modelCity = new Model_City($cityId);
                $arrayCity = $modelCity->toArray();
                $this->_populateFromArray($arrayCity);

            } else {

                $select = $db->select();
                $select->from($this->_tableZip);
                $select->where('searchName = ?', $city);

                if ($stateCode != null) {
                    $select->where('stateCode = ?', $stateCode);
                }

                $select->limit(1);
                $data = $select->query()->fetch();

                if (is_array($data)) {
                    $data['name'] = $data['city'];
                    $data['population'] = null;

                    $this->_populateFromArray($data);
                }
            }
        }

        return $data !== false;
    } // _findLocationByCity

    /**
     * Search location by Zip code and return location data
     * locationId => city table ID
     * id => city table ID
     * city => City Name
     * name => City Name
     * state => State Code
     * zip => Zip Code
     * lat => location lat
     * lng => location lng
     *
     * Return FALSE if zip not found
     *
     * @param string $zip Zip Code
     *
     * @return boolean
     */
    private function _findLocationByZip($zip)
    {
        $fields = array(
            'zip',
            'city as name',
            'stateCode',
            'locationId',
            'lat',
            'lng'
        );

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select();
        $select->from($this->_tableZip, $fields);
        $select->where('zip = ?', $zip);
        $data = $select->query()->fetch();

        if (is_array($data)) {
            $data['population'] = null;
            $this->_populateFromArray($data);
        }

        return $data !== false;
    } // _findLocationByZip

    /**
     * Populate fields from DB array
     *
     * @param array $data Table Data
     *
     * @return void
     */
    private function _populateFromArray($data)
    {
        $this->_stateCode = $data['stateCode'];
        $this->_state = Model_StateList::getName($data['stateCode']);
        $this->_city = $data['name'];
        $this->_population = $data['population'];
        $this->_lat = floatval($data['lat']);
        $this->_lng = floatval($data['lng']);
        if (isset($data['zip'])) {
            $this->_zipCode = $data['zip'];
        }
    } // _populateFromArray

    /**
     * Get location text to be used in canonical URL or canonical text
     *
     * @param bool $includeZipCode Should we include ZIP code in result text
     *
     * @return string | null
     */
    public function getCanonical($includeZipCode = true)
    {
        $location = null;

        if ($this->_zipCode != null) {
            if ($includeZipCode) {

                $location = $this->_city . ', '
                    . $this->_stateCode . ' '
                    . $this->_zipCode;

            } else {
                $location = $this->_city . ', ' . $this->_stateCode;
            }
        } elseif ($this->_city != null) {
            $location = $this->_city . ', ' . $this->_stateCode;
        } elseif ($this->_state != null) {
            $location = $this->_state;
        }

        return $location;
    } // getCanonical

    /**
     * Is location empty (no zip code, city and state defined)
     *
     * @return boolean
     */
    public function isEmpty()
    {
        /*
        NOTE: Paranoia version might be:
        return empty($this->stateCode) && empty($this->city) && empty($this->zipCode)

        But we know that when we have city or zip code defined we should also know
        the state
        */

        return empty($this->_stateCode);
    } // isEmpty

    /**
     * Get Valid
     *
     * @return boolean $_valid
     */
    public function getValid()
    {
        return $this->_valid;
    } // getValid

    /**
     * Set Valid
     *
     * @param boolean $valid Valid
     *
     * @return Model_Location
     */
    public function setValid($valid)
    {
        $this->_valid = $valid;
        return $this;
    } // setValid

    public static function locationAutocomplete($locationStart)
    {
        $locations = array();
        $loc = new Model_Location();
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $sql = <<<SQL
select id as cityId
    from location_us_city
    where searchName like ? and code='P'
union
select cityId
    from location_us_city_synonyms
    where searchName like ?;
SQL;
        $sql = $dbh->quoteInto($sql, $locationStart . '%');

        $rows = $dbh->query($sql)->fetchAll();
        if (count($rows) > 0) {
            $ids = array();
            foreach ($rows as $row) {
                $ids[] = $row['cityId'];
            }
            $select = $dbh->select();
            $select->from($loc->_tableCity, '*')
                ->where('id in (?)', $ids)
                ->order('population desc')
                ->limit(10);
            $rows = $select->query()->fetchAll();
            foreach ($rows as $row) {
                $location = new Model_Location();
                $location->_populateFromArray($row);
                $locations[] = $location;
            }
        }
        return $locations;
    }
}
