<?php
/**
 * Model_Newsletter_Subscription
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Newsletter
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Newsletter Subscription - Association between Contact and Newsletter
 */
class Model_Newsletter_Subscription extends Model_AbstractModel
{
    /**
     * Status for active subscription
     *
     * @var string
     */
    const STATUS_ACTIVE = 'ACTIVE';

    /**
     * Status for diactivated subscription
     *
     * @var string
     */
    const STATUS_DEACTIVE = 'DEACTIVE';

    /**
     * Site from Newsletter
     *
     * @var Model_Site
     */
    protected $_site;

    /**
     * Primary key field
     *
     * @var integer
     */
    protected $_subscriptionId;

    /**
     * Subscription Secure Id
     *
     * @var string
     */
    protected $_subscriptionSecureId;

    /**
     * Newsletter Id
     *
     * @var integer
     */
    protected $_newsletterId;

    /**
     * Newsletter
     *
     * @var Model_Newsletter
     */
    protected $_newsletter;

    /**
     * Subscribtion Status
     *
     * @var string
     */
    protected $_subscriptionStatus;

    /**
     * Contact Id
     *
     * @var integer
     */
    protected $_contactId;

    /**
     * Contact object
     *
     * @var Model_Contact
     */
    protected $_contact;

    /**
     * Created date and time
     *
     * @var string
     */
    protected $_createdDate;

    /**
     * Updated date and time
     *
     * @var string
     */
    protected $_updatedDate;

    /**
     * Data base table name for the model
     *
     * @var string
     */
    protected $_tableName = 'newsletter_subscriptions';

    /**
     * Primary key field name
     *
     * @var string
     */
    protected $_primaryKey = 'subscriptionId';

    /**
     * Data base fields
     *
     * @var array
     */
    protected $_tableFields = array(
        'subscriptionId' => '_subscriptionId',
        'newsletterId' => '_newsletterId',
        'subscriptionSecureId' => '_subscriptionSecureId',
        'subscriptionStatus' => '_subscriptionStatus',
        'contactId' => '_contactId',
        'createdDate' => '_createdDate',
        'updatedDate' => '_updatedDate',
    );

    /**
     * Insert object into DB
     *
     * @return void
     */
    public function insert()
    {
        $date = date('Y-m-d H:i:s');
        $this->setCreatedDate($date);
        $this->setUpdatedDate($date);
        parent::insert();
    } // insert

    /**
     * Update object in DB
     *
     * @return boolean
     */
    public function update()
    {
        $date = date('Y-m-d H:i:s');
        $this->setUpdatedDate($date);
        return parent::update();
    } // update

    /**
     * Get subscription by secure Id
     *
     * @param string $secureId
     *
     * @return Model_Newsletter_Subscription
     */
    public static function getBySecureId($secureId)
    {
        $subscription = new Model_Newsletter_Subscription();
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $select = $dbh->select();
        $select->from($subscription->_tableName, '*')
        ->where('subscriptionSecureId = ?', $secureId);
        $row = $select->query()->fetch();
        if ($row) {
            $subscription->fromArray($row);
        } else {
            $subscription = null;
        }
        return $subscription;
    } // getBySecureId

    /**
     * Generate secure Id for subscription
     *
     * @return string
     */
    public function generateSecureId()
    {
        $this->_subscriptionSecureId = Model_Utils::guid();
        return $this->_subscriptionSecureId;
    } // generateSecureId

    /**
     * Get subscription secure Id
     *
     * @return string
     */
    public function getSecureId()
    {
        return $this->_subscriptionSecureId;
    } // getSecureId

    /**
     * Get Subscription Secure Id
     *
     * @return string
     */
    public function getSubscriptionSecureId()
    {
        return $this->_subscriptionSecureId;
    } // getSubscriptionSecureId

    /**
     * Get Subscription Id
     *
     * @return integer
     */
    public function getSubscriptionId()
    {
        return $this->_subscriptionId;
    } // getSubscriptionId

    /**
     * Get Newsletter Id
     *
     * @return integer
     */
    public function getNewsletterId()
    {
        return $this->_newsletterId;
    } // getNewsletterId

    /**
     * Get Newsletter
     *
     * @return Model_Newsletter
     */
    public function getNewsletter()
    {

        if ($this->_newsletter == null) {
            $this->_newsletter = new Model_Newsletter($this->_newsletterId);
        }
        return $this->_newsletter;
    } // getNewsletter

    /**
     * Get Subscription Status
     *
     * @return string
     */
    public function getSubscriptionStatus()
    {
        return $this->_subscriptionStatus;
    } // getSubscriptionStatus

    /**
     * Get Contact Id
     *
     * @return integer
     */
    public function getContactId()
    {
        return $this->_contactId;
    } // getContactId

    /**
     * Get Contact
     *
     * @return Model_Contact
     */
    public function getContact()
    {
        if ($this->_contact == null && $this->_contactId != null) {
            $this->_contact = new Model_Contact($this->_contactId);
        }
        return $this->_contact;
    } // getContact

    /**
     * Get Created Date
     *
     * @return string
     */
    public function getCreatedDate()
    {
        return $this->_createdDate;
    } // getCreatedDate

    /**
     * Get Updated Date
     *
     * @return string
     */
    public function getUpdatedDate()
    {
        return $this->_updatedDate;
    } // getUpdatedDate

    /**
     * Set Newsletter Id
     *
     * @param integer $newsletterId
     *
     * @return Model_Newsletter_Subscription
     */
    public function setNewsletterId($newsletterId)
    {
        $this->_newsletterId = $newsletterId;
        return $this;
    } // setNewsletterId

    /**
     * Set Subscription Status
     *
     * @param string $subscribtionStatus
     *
     * @return Model_Newsletter_Subscription
     */
    public function setSubscriptionStatus($subscriptionStatus)
    {
        $this->_subscriptionStatus = $subscriptionStatus;
        return $this;
    } // setSubscriptionStatus

    /**
     * Set Contact Id
     *
     * @param integer $contactId
     *
     * @return Model_Newsletter_Subscription
     */
    public function setContactId($contactId)
    {
        $this->_contactId = $contactId;
        return $this;
    } // setContactId

    /**
     * Set Created Date
     *
     * @param string $createdDate
     *
     * @return Model_Newsletter_Subscription
     */
    public function setCreatedDate($createdDate)
    {
        $this->_createdDate = $createdDate;
        return $this;
    } // setCreatedDate

    /**
     * Set Updated Date
     *
     * @param string $updatedDate
     *
     * @return Model_Newsletter_Subscription
     */
    public function setUpdatedDate($updatedDate)
    {
        $this->_updatedDate = $updatedDate;
        return $this;
    } // setUpdatedDate

    /**
     * Get url for unsubscribe from this newsletter
     *
     * @return string
     */
    public function getUnsubscribeUrl()
    {
        $site = $this->getSite();
        $baseUrl = $site->getBaseUrl();

        return $baseUrl . '/newsletters/unsubscribe?subscription=' . $this->_subscriptionSecureId;
    } // getUnsubscribeUrl

    /**
     * Get url for unsubscribe from all newsletter
     *
     * @return string
     */
    public function getUnsubscribeAllUrl()
    {
        $site = $this->getSite();
        $baseUrl = $site->getBaseUrl();

        $contact = $this->getContact();

        return $baseUrl . '/newsletters/unsubscribe-all?subscription=' . $this->_subscriptionSecureId;
    } // getUnsubscribeAllUrl

    /**
     * Set site from newsletter
     *
     * @param Model_Site $site
     *
     * @return Model_Newsletter_Subscription
     */
    public function setSite(Model_Site $site)
    {
        $this->_site = $site;
        return $this;
    } // setSite

    /**
     * Get Site
     *
     * @return void
     */
    public function getSite()
    {
        if (!$this->_site) {
            $newsletter = new Model_Newsletter($this->_newsletterId);
            $this->_site = new Model_Site($newsletter->getSiteId());
        }
        return $this->_site;
    } // getSite


}
