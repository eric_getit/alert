<?php
/**
 * Model_Newsletter_Queue
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model\Newsletter
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Queue for the sending newsletter
 */
class Model_Newsletter_Queue extends Model_AbstractModel
{
    /**
     * Status for new queue
     *
     * @var string
     */
    const STATUS_NEW = 'new';

    /**
     * Status for prepared queue
     *
     * @var string
     */
    const STATUS_PREPARED = 'prepared';

    /**
     * Primary key field
     *
     * @var integer
     */
    protected $_queueId;

    /**
     * Report date
     *
     * @var string
     */
    protected $_newsletterId;

    /**
     * Queue status
     *
     * @var string
     */
    protected $_queueStatus;

    /**
     * Queue subject
     *
     * @var string
     */
    protected $_subject;

    /**
     * Queue message
     *
     * @var string
     */
    protected $_message;

    /**
     * Time to start sending
     *
     * @var string
     */
    protected $_sendTime;

    /**
     * Email from header name
     *
     * @var string
     */
    protected $_fromEmail;

    /**
     * Email from header address
     *
     * @var string
     */
    protected $_fromName;

    /**
     * Created date and time
     *
     * @var string
     */
    protected $_createdDate;

    /**
     * Updated date and time
     *
     * @var string
     */
    protected $_updatedDate;

    /**
     * Data base table name for the model
     *
     * @var string
     */
    protected $_tableName = 'newsletter_queue';

    /**
     * Primary key field name
     *
     * @var string
     */
    protected $_primaryKey = 'queueId';

    /**
     * Data base fields
     *
     * @var array
     */
    protected $_tableFields = array(
        'queueId' => '_queueId',
        'newsletterId' => '_newsletterId',
        'queueStatus' => '_queueStatus',
        'subject' => '_subject',
        'message' => '_message',
        'sendTime' => '_sendTime',
        'fromEmail' => '_fromEmail',
        'fromName' => '_fromName',
        'createdDate' => '_createdDate',
        'updatedDate' => '_updatedDate',
    );

    /**
     * Insert object into DB
     *
     * @return void
     */
    public function insert()
    {
        $date = date('Y-m-d H:i:s');
        $this->setCreatedDate($date);
        $this->setUpdatedDate($date);
        parent::insert();
    } // insert

    /**
     * Update object in DB
     *
     * @return boolean
     */
    public function update()
    {
        $date = date('Y-m-d H:i:s');
        $this->setUpdatedDate($date);
        return parent::update();
    } // update

    /**
     * Get Queue Id
     *
     * @return integer
     */
    public function getQueueId()
    {
        return $this->_queueId;
    } // getQueueId

    /**
     * Get Newsletter Id
     *
     * @return string
     */
    public function getNewsletterId()
    {
        return $this->_newsletterId;
    } // getNewsletterId

    /**
     * Get Queue Status
     *
     * @return string
     */
    public function getQueueStatus()
    {
        return $this->_queueStatus;
    } // getQueueStatus

    /**
     * Get Subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->_subject;
    } // getSubject

    /**
     * Get Message
     * @return string
     */
    public function getMessage()
    {
        return $this->_message;
    } // getMessage

    /**
     * Get Send Time
     *
     * @return string
     */
    public function getSendTime()
    {
        return $this->_sendTime;
    } // getSendTime

    /**
     * Get From Email
     *
     * @return string
     */
    public function getFromEmail()
    {
        return $this->_fromEmail;
    } // getFromEmail

    /**
     * Get From Name
     *
     * @return string
     */
    public function getFromName()
    {
        return $this->_fromName;
    } // getFromName

    /**
     * Get Created Date
     *
     * @return string
     */
    public function getCreatedDate()
    {
        return $this->_createdDate;
    } // getCreatedDate

    /**
     * Get Updated Date
     *
     * @return string
     */
    public function getUpdatedDate()
    {
        return $this->_updatedDate;
    } // getUpdatedDate

    /**
     * Set Newsletter Id
     *
     * @param string $newsletterId
     *
     * @return Model_Newsletter_Queue
     */
    public function setNewsletterId($newsletterId)
    {
        $this->_newsletterId = $newsletterId;
        return $this;
    } // setNewsletterId

    /**
     * Set Queue Status
     *
     * @param string $queueStatus
     *
     * @return Model_Newsletter_Queue
     */
    public function setQueueStatus($queueStatus)
    {
        $this->_queueStatus = $queueStatus;
        return $this;
    } // setQueueStatus

    /**
     * Set Subject
     *
     * @param string $subject
     *
     * @return Model_Newsletter_Queue
     */
    public function setSubject($subject)
    {
        $this->_subject = $subject;
        return $this;
    } // setSubject

    /**
     * Set Message
     *
     * @param string $message
     *
     * @return Model_Newsletter_Queue
     */
    public function setMessage($message)
    {
        $this->_message = $message;
        return $this;
    } // setMessage

    /**
     * Set Send Time
     *
     * @param string $sendTime
     *
     * @return Model_Newsletter_Queue
     */
    public function setSendTime($sendTime)
    {
        $this->_sendTime = $sendTime;
        return $this;
    } // setSendTime

    /**
     * Set From Email
     *
     * @param string $fromEmail
     *
     * @return Model_Newsletter_Queue
     */
    public function setFromEmail($fromEmail)
    {
        $this->_fromEmail = $fromEmail;
        return $this;
    } // setFromEmail

    /**
     * Set From Name
     *
     * @param string $fromName
     *
     * @return Model_Newsletter_Queue
     */
    public function setFromName($fromName)
    {
        $this->_fromName = $fromName;
        return $this;
    } // setFromName

    /**
     * Set Created Date
     *
     * @param string $createdDate
     *
     * @return Model_Newsletter_Queue
     */
    public function setCreatedDate($createdDate)
    {
        $this->_createdDate = $createdDate;
        return $this;
    } // setCreatedDate

    /**
     * Set Updated Date
     *
     * @param string $updatedDate
     *
     * @return Model_Newsletter_Queue
     */
    public function setUpdatedDate($updatedDate)
    {
        $this->_updatedDate = $updatedDate;
        return $this;
    } // setUpdatedDate

    /**
     * Return active subscriptions for queue
     *
     * @return array
     */
    public function getActiveSubscriptions()
    {
        $contactActive = Model_Contact::STATUS_ACTIVE;
        $subscribeActive = Model_Newsletter_Subscription::STATUS_ACTIVE;
        $sql = <<<SQL
    SELECT newsletter_subscriptions.* FROM newsletter_subscriptions
    WHERE newsletterId = {$this->_newsletterId}
        AND subscriptionStatus = "{$subscribeActive}"
        AND contactId in (SELECT contactId FROM contacts WHERE contactStatus = "{$contactActive}")
SQL;

        $result = array();
        $dbh = Zend_Db_Table::getDefaultAdapter();

        $rows = $dbh->query($sql)->fetchAll();
        foreach ($rows as $row) {
            $result[] = new Model_Newsletter_Subscription($row);
        }
        return $result;
    } // getActiveSubscriptions

    /**
     * Processing queue for newsletter
     *
     * @return void
     */
    public static function prepareQueues()
    {
        echo '--> Start preparing newsletter.' . PHP_EOL;

        $queues = Model_Newsletter_Queue::getScheduledQueues();

        echo '--> Found ' . number_format(count($queues)) . ' queues.' . PHP_EOL;

        /* @var $queue Model_Newsletter_Queue */
        foreach ($queues as $queue) {
            $newsletter = new Model_Newsletter($queue->getNewsletterId());
            echo '--> Processing queue "' . $queue->getSubject()
                . '" for newsletter "' . $newsletter->getNewsletterName() . '"' . PHP_EOL;

            $queue->prepare();
        }
    } // prepareQueues

    /**
     * Get scheduled newsletters
     *
     * @return array
     */
    public static function getScheduledQueues()
    {
        $sql = 'select * from newsletter_queue
            where queueStatus = "' . Model_Newsletter_Queue::STATUS_NEW . '"
            and sendTime <= ?
            order by sendTime';
        $bind = array();
        $bind[] = Model_Utils::sqlNow();
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $rows = $dbh->fetchAll($sql, $bind, Zend_Db::FETCH_ASSOC);

        $queues = array();
        foreach ($rows as $row) {
            $queues[] = new Model_Newsletter_Queue($row);
        }
        return $queues;
    } // getScheduledQueues

    /**
     * Prepare single item in queue
     *
     * @return void
     */
    public function prepare()
    {
        $result = $this->getActiveSubscriptions();

        $count = count($result);

        echo '---> Found ' . number_format($count) . ' subscribers' . PHP_EOL;

        if ($count > 0) {
            $result = $this->_prepareMessages($result);
        }

        $this->setQueueStatus(Model_Newsletter_Queue::STATUS_PREPARED);
        $this->update();

    } // prepare

    /**
     * Prepare emails messages for the newsletter
     *
     * @param array $subscriptions
     *
     * @return void
     */
    public function _prepareMessages(array $subscriptions)
    {
        $newsletter = new Model_Newsletter($this->getNewsletterId());
        foreach ($subscriptions as $subscription) {
            /* @var $subscription Model_Newsletter_Subscription */
            $archive = new Model_MessageArchive();
            $archive->setNewsletter($newsletter);
            $archive->setNewsletterQueue($this);
            $archive->insert();

            $now = new DateTime();
            $site = $newsletter->getSite();
            $subscription->setSite($site);
            $contact = $subscription->getContact();

            $placeholders = array();
            $placeholders['MESSAGE'] = $this->getMessage();
            $placeholders['SUBJECT'] = $this->getSubject();
            $placeholders['DATE'] = $now->format('m/j/y');
            $placeholders['UNSUBSCRIBE_NEWSLETTER'] = $subscription->getUnsubscribeUrl();
            $placeholders['EDIT_CONTACT'] = $contact->getEditUrl($site);

            $placeholders['IMG_SRC'] = $site->getBaseUrl() . '/picture/' . $archive->getSecureId() . '/logo.gif';

            $template = new \GetIt\Email\Template\SmsTemplate('email newsletter');
            $template->addMerges($placeholders);

            // Hardcoding this for now, since none of the emails I pass in will be verified.
            $fromEmail = 'contact@getitllc.com';
            $fromPerson = $this->getFromName() ? $this->getFromName() : $newsletter->getFromName();
            $template->setFrom($fromEmail, $fromPerson);
            $template->setTo($contact->getEmail(), $contact->getName());


            $archive->setTemplate($template);
        } // _prepareMessages
    }
}
