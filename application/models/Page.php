<?php
/**
 * Model_Page
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  Model
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Seceleton of all pages
 * @author Eugene Churmanov
 *
 */
class Model_Page
{
    /**
     * Twig Environment
     *
     * @var Twig_Environment
     */
    protected $_twig;

    /**
     * Variables that are passed to the template
     *
     * @var array
     */
    protected $_twigVars = array();

    /**
     * Template Name
     *
     * @var string
     */
    protected $_templateName = '';

    /**
     * Page Meta Title
     *
     * @var string
     */
    protected $_metaTitle = '';

    /**
     * Page Title
     *
     * @var string
     */
    protected $_title = '';

    /**
     * Twig Configuration
     *
     * @var array
     */
    protected static $_twigConfig = array(
        'templateDir' => '',
    );

    /**
     * Page parameters
     *
     * @var array
     */
    protected $_params = array();

    /**
     * Alert Object
     *
     * @var Model_Alert
     */
    protected $_alert = null;

    /**
     * Contact object
     *
     * @var Model_Contact
     */
    protected $_contact = null;

    /**
     * Construct Page
     *
     * @param array $params
     *
     * @return self
     *
     * @throws Model_Exception_UnknownId
     */
    public function __construct($params)
    {
        $templateDir = APPLICATION_PATH . '/templates';
        $template = 'default';

        if (array_key_exists('alert', $params)) {
            $alert = Model_Alert::getBySecureId($params['alert']);
            if ($alert == null) {
                throw new Model_Exception_UnknownId();
            }
            $this->_alert = $alert;
            $this->_contact = $alert->getContact();
        } else if (array_key_exists('contact', $params)){
            $contact = Model_Contact::getBySecureId($params['contact']);
            if ($contact == null) {
                throw new Model_Exception_UnknownId();
            }
            $this->_contact = $contact;
        }

        //TODO: Add logic for getting right template

        $templateDir .= "/" . $template;

        self::$_twigConfig['templateDir'] = $templateDir;
        $this->_twigVars = $params;

        if ($this->_alert) {
            $site = $this->_alert->getSite();
            if ($site) {
                $this->_twigVars['domain'] = $site->getDomain();
                $this->_twigVars['SITE_NICHE'] = $site->getSiteNiche();
                $this->_twigVars['SITE_NAME'] = $site->getSiteName();
                $this->_twigVars['NEWSLETTER_DOMAIN'] = $site->getNewsletterDomain();
            }
        }

        $this->_params = $params;
    } // __construct

    /**
     * Get Twig environment instance
     *
     * @return Twig_Environment
     */
    public function getTwig()
    {
        if ($this->_twig == NULL) {
            $loader = new Twig_Loader_Filesystem(self::$_twigConfig['templateDir']);
            $this->_twig = new Twig_Environment(
                    $loader,
                    self::$_twigConfig['options']
            );
            $this->_twig->addExtension(new Twig_Extension_Debug());
        }

        return $this->_twig;
    } // getTwig

    /**
     * Get Temolate Name
     *
     * @return string
     */
    public function getTemplateName()
    {
        return $this->_templateName;
    } // getTemplateName

    /**
     * Get page parameters
     *
     * @return array
     */
    public function getParams()
    {
        return $this->_params;
    } // getParams

    /**
     * Get page parameter for the given name
     *
     * @param string $name Parameter name
     * @param string $default Default value
     *
     * @return string
     */
    public function getParam($name, $default)
    {
        if (array_key_exists($name, $this->_params)) {
            $value = $this->_params[$name];
        } else {
            $value = $default;
        }
        return $value;
    } // getParam

    /**
     * Get Meta Title
     *
     * @return string
     */
    public function getMetaTitle()
    {
        return $this->_metaTitle;
    } // getMetaTitle

    /**
     * Get page title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    } // getTitle

    /**
     * Set page title
     *
     * @param string $title
     *
     * @return void
     */
    public function setTitle($title)
    {
        $this->_title = $title;
    } // setTitle

    /**
     * Set Twig template parameters
     *
     * @return void
     */
    public function initView()
    {
        $this->_twigVars['page'] = $this;
        $this->_twigVars['title'] = $this->getTitle();
        $this->_twigVars['metaTitle'] = $this->getMetaTitle();
    } // initView

    /**
     * Render Page
     *
     * @return void
     */
    public function render()
    {
        $twig = $this->getTwig();
        $templateName = $this->getTemplateName();
        $template = $twig->loadTemplate($templateName);
        $response = Zend_Controller_Front::getInstance()->getResponse();
        $response->appendBody($template->render($this->_twigVars), 'default');
    } // render

    /**
     * Set Twig Options for construct Twig_Environment later
     *
     * @param array $option
     *
     * @return void
     */
    public static function setTwitOptions(array $options = array())
    {
        foreach ($options as $option => $value) {
            self::$_twigConfig[$option] = $value;
        }
    } // setTwitOptions
}
