<?php
/**
 * AdminBootstrap
 *
 * PHP Version 5.3
 *
 * @category Class
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Bootstrap the admin site
 */
class AdminBootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    /**
     * Initialise Zend autoloader
     *
     * @return void
     */
    protected function _initAutoload()
    {
        $autoloader = new Zend_Application_Module_Autoloader(array(
            'namespace' => '',
            'basePath' => APPLICATION_PATH
        ));
    } // _initAutoload

    /**
     * Add plugin to check if user is authorized
     *
     * @return void
     */
    protected function _initPlugin()
    {
        $front = Zend_Controller_Front::getInstance();
        $front->registerPlugin(new Model_Login());
    } // _initPlugin

    /**
     * Get the client IP address from Amazon LB cluster.
     *
     * @return void
     */
    protected function _initRemoteAddr()
    {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $_SERVER['REMOTE_ADDR'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
    } // _initRemoteAddr
}
