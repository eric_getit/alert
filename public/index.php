<?php
// Allow profiling with special magic parameter
// Define path to configurations folder
defined('CONFIGS_PATH')
    || define('CONFIGS_PATH', realpath(dirname(__FILE__) . '/../configs'));
// var folder: logs, sessions, cache, temporary files
defined('VAR_PATH')
    || define('VAR_PATH', realpath(dirname(__FILE__) . '/../var'));
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));
// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'development'));

defined('APPLICATION_MODULE')
    || define('APPLICATION_MODULE', (getenv('APPLICATION_MODULE')?getenv('APPLICATION_MODULE'):'site'));

defined('TEST_PATH')
|| define('TEST_PATH', realpath(dirname(__FILE__) . '/../tests'));

define('PROFILE_ENABLED', false);

$loader = require_once realpath(__DIR__ . '/../vendor/autoload.php');
$loader->add('GetIt', realpath(APPLICATION_PATH . '/../library/TransEmail/src'));
$loader->add('Ses', realpath(APPLICATION_PATH . '/../library/TransEmail/src'));
$loader->add('Sqs', realpath(APPLICATION_PATH . '/../library/TransEmail/src'));
$loader->add('Zend', realpath(APPLICATION_PATH . '/../library/TransEmail/src'));

$domain = $_SERVER['HTTP_HOST'];
$adminDomains = array(
        'alert-admin.getitcorporate.com',
        'alert-admin-test.getitcorporate.com',
        'alert-admin.getitcorporate.tld',
        'alert-admin.tld',
        'alert.local'
);
if (in_array($domain, $adminDomains)) {
    $config = include CONFIGS_PATH . '/admin.php';
} else {
    $config = include CONFIGS_PATH . '/application.php';
}

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    $config
);
$application->bootstrap();
$application->run();
