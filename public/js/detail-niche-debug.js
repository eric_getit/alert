$(function() {
	/* from script.js */
	$('#states > h3').bind('click', function() {
		$('#states > .content').toggle();
		$('#states > h3').toggleClass('open');
	});

	/* $('input[placeholder], textarea[placeholder]').placeholder(); */
	/* end: from script.js */
	
    var form = $('#friendAlertFormNiche');
    $(form).find('input#alertSubmit').click(function(e) {
        $(this).val('Sending');
        $(this).prop('disabled', true);
        e.preventDefault();
        sendAjaxFriend(form);
    });

    $('.send-btn-detail').click(function(e) {
        e.preventDefault();
        if (EmailAddress($('.subscribeemailDetail')).valid) {
            $('#subscribeFormDetail').submit();
            return false;
        } else {
            $('#subscribeFormDetail').find('ul').html('<li class="errors">Please enter a valid email address.</li>');
        }
    });

    $('.send-btn').click(function(e) {
        e.preventDefault();
        if (EmailAddress($('.subscribeemail')).valid) {
            $('#freeSubscribeForm').submit();
            return false;
        } else {
            $('#freeSubscribeForm').find('ul').html('<li class="errors">Please enter a valid email address.</li>');
        }
    });

});

function sendAjaxFriend(form) {
    var error = $(form).find('.errors');
    error.remove();
    var formData = {};
    formData.alertFrom = $(form).find('#alertFrom').val();
    formData.alertTo = $(form).find('#alertTo').val();
    formData.alertUrl = window.location.href;
    formData.alertSearch = $(form).find('#alertSearch').val();
    $.ajax({
        type : "POST",
        url : "/send-friend-alert",
        data : formData,
        dataType : "json",
        success : function(data) {
            $('input#alertSubmit').prop('disabled', false);
            $('input#alertSubmit').val('Send');
            if (data.status == 'error') {
                for ( var i in data.errors) {
                    if (i == 'remove') {
                        continue;
                    }
                    var input = $('#' + data.errors[i].element);
                    input.after('<ul class="errors"><li>* ' + data.errors[i].message + '</li></ul>');
                }
            } else {
                $(form).hide();
                $(form).after(data.message);
                $('.friendEmail .restore a').click(function(e) {
                    $(form).show();
                    $('.friendEmail .restore').remove();
                    $(form).find('#alertTo').val('');
                });
            }
        }
    });
}

function EmailAddress(e) {
    var v = e.val();
    if (v.indexOf('@') > 64 || v.length - v.indexOf('@') > 255) {
        return {
            valid: false,
            code: "emailAddressLengthExceeded"
        };
    }
    if (v.indexOf('@') < 1 || v.indexOf('@') != v.lastIndexOf('@')) {
        return {
            valid: false,
            code: "emailAddressInvalidFormat"
        };
    }
    if (v.indexOf('.', v.indexOf('@')) == -1 || v.lastIndexOf('.') == v.length - 1 || v.charAt(v.indexOf('@') + 1) == '.') {
        return {
            valid: false,
            code: "emailAddressInvalidHostname"
        };
    }
    return {
        valid: true
    };
}
