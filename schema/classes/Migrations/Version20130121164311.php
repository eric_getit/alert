<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Add the originalCreatedDate to the contacts table
 */
class Version20130121164311 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $table = $schema->getTable('contacts');
        if (!$table->hasColumn('originalCreatedDate')) {
            $table->addColumn('originalCreatedDate', 'datetime', array('notnull' => false));
        }
    }

    public function down(Schema $schema)
    {

        $table = $schema->getTable('contacts');
        if ($table->hasColumn('originalCreatedDate')) {
            $table->dropColumn('originalCreatedDate');
        }
    }
}
