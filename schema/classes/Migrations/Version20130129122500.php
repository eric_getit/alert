<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Rename column name from sentDate to sendDate for consistancy
 */
class Version20130129122500 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $sql = <<<SQL
        ALTER TABLE message_archive ADD INDEX `send` (sendDate);
SQL;
        $this->addSql($sql);
    }

    public function down(Schema $schema)
    {

        $this->addSql("ALTER TABLE message_archive DROP INDEX `send`");
    }
}
