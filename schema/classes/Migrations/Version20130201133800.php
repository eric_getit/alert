<?php
/**
 * Link alerts with sites base at alert contact data
 * @author eugene
 *
 */
namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130201133800 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $dbh = \Zend_Db_Table::getDefaultAdapter();
        $sql = <<<SQL
    SELECT distinct contactId FROM alerts WHERE siteId is null;
SQL;
        $rows = $dbh->query($sql)->fetchAll(\PDO::FETCH_COLUMN, 0);
        foreach ($rows as $contactId) {
            $domain = $dbh->query("SELECT site FROM contacts WHERE contactId = {$contactId}")->fetchColumn(0);
            $domain = $dbh->quote(\Model_Site::normalizeDomain($domain));
            $sql = <<<SQL
    UPDATE alerts
        SET siteId = (SELECT siteId FROM sites WHERE domain = {$domain} LIMIT 1)
    WHERE contactId = {$contactId} AND siteId is null;
SQL;
            $this->addSql($sql);
        }

    }

    public function down(Schema $schema)
    {


    }
}
