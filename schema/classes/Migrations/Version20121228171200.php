<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20121228171200 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $contactsTable = $schema->getTable('contacts');
        if (!$contactsTable->hasColumn('country')) {
            $contactsTable->addColumn('country', 'string', array('notnull' => false, 'length' => 256));
        }
        if (!$contactsTable->hasColumn('state')) {
            $contactsTable->addColumn('state', 'string', array('notnull' => false, 'length' => 256));
        }
        if (!$contactsTable->hasColumn('city')) {
            $contactsTable->addColumn('city', 'string', array('notnull' => false, 'length' => 256));
        }
        if (!$contactsTable->hasColumn('zip')) {
            $contactsTable->addColumn('zip', 'string', array('notnull' => false, 'length' => 256));
        }

    }

    public function down(Schema $schema)
    {
        $contactsTable = $schema->getTable('contacts');
        if (!$contactsTable->hasColumn('country')) {
            $contactsTable->dropColumn('country');
        }
        if (!$contactsTable->hasColumn('state')) {
            $contactsTable->dropColumn('state');
        }
        if (!$contactsTable->hasColumn('city')) {
            $contactsTable->dropColumn('city');
        }
        if (!$contactsTable->hasColumn('zip')) {
            $contactsTable->dropColumn('zip');
        }
    }
}
