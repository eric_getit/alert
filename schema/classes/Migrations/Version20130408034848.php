<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130408034848 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $table = $schema->getTable('message_views');
        if (!$table->hasColumn('messageSecureId')) {
            $table->addColumn('messageSecureId', 'string', array('length'=>'255', 'notnull' => true));
        }

    }

    public function down(Schema $schema)
    {
        $table = $schema->getTable('message_views');
        if ($table->hasColumn('messageSecureId')) {
            $table->dropColumn('messageSecureId');
        }
    }
}
