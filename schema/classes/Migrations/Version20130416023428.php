<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130416023428 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $sql = "INSERT INTO config_group
            (name, displayName, displayHelp, position)
            VALUES
            ('sendTime', 'Send Time', 'Send Time settings.', 1)";
        $this->addSql($sql);
    }

    public function down(Schema $schema)
    {
        $sql = "DELETE FROM config_group
            WHERE name = 'sendTime'";
        $this->addSql($sql);

    }
}
