<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130128034839 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $sql = '
            CREATE TABLE `config_db` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `name` varchar(255) NOT NULL,
              `displayName` varchar(255) NOT NULL,
              `adminComment` int(11) DEFAULT NULL,
              `configValue` text,
              `displayGroupId` int(11) NOT NULL,
              `displayHelp` varchar(255) NOT NULL,
              `created` datetime DEFAULT NULL,
              `updated` datetime DEFAULT NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `name` (`name`)
            ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8
        ';
        $this->addSql($sql);

    }

    public function down(Schema $schema)
    {
        $sql = 'DROP TABLE `config_db`;';
        $this->addSql($sql);

    }
}
