<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130416023433 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $dbh = \Zend_Db_Table::getDefaultAdapter();
        $select = $dbh->select();
        $select->from('config_group', array('id'));
        $select->where('name = "sendTime"');
        $id = $select->query()->fetchColumn();
        if ($id) {
            $this->addSql("INSERT INTO `config_db`
                (
                    `name`,
                    `displayName`,
                    `configValue`,
                    `displayGroupId`,
                    `displayHelp`
                )
                VALUES
                (
                    'sendTime',
                    'Time',
                    '04:00',
                    $id,
                    'Time in which we begin to send alerts.'
                ),
                (
                    'useTimezones',
                    'Use Timezones',
                    'No',
                    $id,
                    'Sends alerts based on user timezones.'
                )"
            );
        }

    }

    public function down(Schema $schema)
    {
        $this->addSql("DELETE FROM `config_db` WHERE `name`='sendTime'");
        $this->addSql("DELETE FROM `config_db` WHERE `name`='useTimezones'");

    }
}
