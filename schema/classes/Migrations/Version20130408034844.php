<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130408034844 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $table = $schema->getTable('message_archive');
        if ($table->hasColumn('viewsNumber')) {
            $table->dropColumn('viewsNumber');
        }

    }

    public function down(Schema $schema)
    {
        $table = $schema->getTable('message_archive');
        if (!$table->hasColumn('viewsNumber')) {
            $table->addColumn('viewsNumber', 'integer', array('length'=>'11', 'notnull' => false));
        }

    }
}
