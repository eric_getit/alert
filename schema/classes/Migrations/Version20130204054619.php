<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130204054619 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $sql = "CREATE TABLE `logged_user_cookies` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `date` datetime NOT NULL,
          `email` varchar(255) DEFAULT NULL,
          `site_id` int(11) NOT NULL,
          `secret_word` varchar(255) NOT NULL,
          `user_id` int(11) DEFAULT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Store user cookies for auto sign in'";
        $this->addSql($sql);

    }

    public function down(Schema $schema)
    {
        $sql = 'DROP TABLE `logged_user_cookies`';
        $this->addSql($sql);

    }
}
