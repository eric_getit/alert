<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130123042224 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $dbh = \Zend_Db_Table::getDefaultAdapter();
        $select = $dbh->select();
        $select->from('trans_email_email', array('email_id'));
        $select->where('email_key = "email alert"');
        $id = $select->query()->fetchColumn();

        $this->abortIf(!$id, 'Email not defined!');

        $bodyHtml = <<<BODYHTML
<html>
<head>
    <meta http-equiv="Content-Type" content="text/HTML; charset=utf-8" />
    <title>Email Alert</title>
    <style>
       body {
            font-size: 12px;
        }
        table {
            width: 100%;
            border:0;
            padding:0;
            border-collapse:collapse;
            margin-bottom:10px;
        }

        table td {
            padding-top: 10px;
            padding-bottom: 5px;
            border-bottom: 1px solid #888888;
        }

        th {
            padding-bottom: 5px;
            border-bottom: 1px solid #888888;
            text-align: left;
        }

        th.title {
            width: 60%;
        }

        th.location {
            width: 20%;
            text-align:left;
        }

        th.date {
            width: 20%;
            text-align:left;
        }
    </style>
</head>
<body>
{% if FIRST_MESSAGE %}
    <p>****************************************</p>

    <p>**We've activated your industry leading job alert service! Starting
    today, you will receive daily alerts of only the best {{SITE_NICHE|lower}}
    opportunities available. Want to refine or adjust your search? Please
    do so here. You may unsubscribe from your daily alert at any time.**
    </p>

    <p>****************************************</p>
{% endif %}

<p>1 - {{COUNT}} New {{CANONICAL_QUERY}} - {{DATE}}</p>

<p><a href="{{EDIT_ALERT_URL}}">Edit the location or keyword</a> or <a href="{{UNSUBSCRIBE_ALERT_URL}}">cancel</a> this alert.</p>

<table>
    <tr>
        <th class="title">Title</th>
        <th class="location">Location</th>
        <th class="date">Date</th>
    </tr>
    {% for listing in LISTINGS %}
        <tr>
            <td class="title"><a href="{{ listing.url }}">{{ listing.title }}</a></td>
            <td class="location">{{ listing.location }}</td>
            <td class="date">{{ listing.date }}</td>
        </tr>
    {% endfor %}
</table>

</br></br>

<p>Not what you're looking for? <a href="{{EDIT_ALERT_URL}}">Improve these results here</a>.</p>
<p>Got a job? <a href="{{UNSUBSCRIBE_ALERT_URL}}">Cancel this alert</a> or <a href="{{VIEW_ALL_ALERTS_URL}}">unsubscribe</a> from all alerts.</p>
<p>You have received this email because you subscribed to it, are a member, or someone forwarded it to you.</p>
<p>Feel free to contact us if you have any questions:</p>
<p>Get It, LLC | 128 N. Pitt St | Alexandria, VA 22314</p>
</body>
</html>
BODYHTML;
        $bodyText = <<<BODYTEXT
{% if FIRST_MESSAGE %}
****************************************

**We've activated your industry leading job alert service! Starting
today, you will receive daily alerts of only the best {{SITE_NICHE|lower}}
opportunities available. Want to refine or adjust your search? Please
do so here. You may unsubscribe from your daily alert at any time.**

****************************************

{% endif %}
1 - {{COUNT}} New {{CANONICAL_QUERY}} - {{DATE}}

Edit the location or keyword - {{EDIT_ALERT_URL}}
or cancel - {{UNSUBSCRIBE_ALERT_URL}} - this alert.

{% for listing in LISTINGS %}
    {{ listing.title }}
    <{{ listing.url }}>
    {{ listing.location }}
    {{ listing.date }}


{% endfor %}

Not what you're looking for? Improve these results here.

Got a job? Cancel this alert - {{UNSUBSCRIBE_ALERT_URL}} or unsubscribe from all alerts - {{VIEW_ALL_ALERTS_URL}}.

You have received this email because you subscribed to it, are a member, or someone forwarded it to you.

Feel free to contact us if you have any questions:

Get It, LLC | 128 N. Pitt St | Alexandria, VA 22314
BODYTEXT;
        $dbh->update('trans_email_tmpl', array('tmpl_body_html' => $bodyHtml, 'tmpl_body' => $bodyText,),'email_id=' . $id);

    }

    public function down(Schema $schema)
    {
    $dbh = \Zend_Db_Table::getDefaultAdapter();
        $select = $dbh->select();
        $select->from('trans_email_email', array('email_id'));
        $select->where('email_key = "email alert"');
        $id = $select->query()->fetchColumn();

        $this->abortIf(!$id, 'Email not defined!');

        $bodyHtml = <<<BODYHTML
<html>
<head>
    <meta http-equiv="Content-Type" content="text/HTML; charset=utf-8" />
    <title>Email Alert</title>
    <style>
       body {
            font-size: 12px;
        }
        table {
            width: 100%;
            border:0;
            padding:0;
            border-collapse:collapse;
            margin-bottom:10px;
        }

        table td {
            padding-top: 10px;
            padding-bottom: 5px;
            border-bottom: 1px solid #888888;
        }

        th {
            padding-bottom: 5px;
            border-bottom: 1px solid #888888;
            text-align: left;
        }

        th.title {
            width: 60%;
        }

        th.location {
            width: 20%;
            text-align:left;
        }

        th.date {
            width: 20%;
            text-align:left;
        }
    </style>
</head>
<body>
<p>1 - {{COUNT}} New {{CANONICAL_QUERY}} - {{DATE}}</p>

<p><a href="{{EDIT_ALERT_URL}}">Edit the location or keyword</a> or <a href="{{UNSUBSCRIBE_ALERT_URL}}">cancel</a> this alert.</p>

{{LISTINGS_HTML}}

<p>Not what you're looking for? <a href="{{EDIT_ALERT_URL}}">Improve these results here</a>.</p>
<p>Got a job? <a href="{{UNSUBSCRIBE_ALERT_URL}}">Cancel this alert</a> or <a href="{{VIEW_ALL_ALERTS_URL}}">unsubscribe</a> from all alerts.</p>
<p>You have received this email because you subscribed to it, are a member, or someone forwarded it to you.</p>
<p>Feel free to contact us if you have any questions:</p>
<p>Get It, LLC | 128 N. Pitt St | Alexandria, VA 22314</p>
</body>
</html>
BODYHTML;
        $bodyText = <<<BODYTEXT
1 - {{COUNT}} New {{CANONICAL_QUERY}} - {{DATE}}

Edit the location or keyword - {{EDIT_ALERT_URL}}
or cancel - {{UNSUBSCRIBE_ALERT_URL}} - this alert.

{{LISTINGS_TEXT}}

Not what you're looking for? Improve these results here.

Got a job? Cancel this alert - {{UNSUBSCRIBE_ALERT_URL}} or unsubscribe from all alerts - {{VIEW_ALL_ALERTS_URL}}.

You have received this email because you subscribed to it, are a member, or someone forwarded it to you.

Feel free to contact us if you have any questions:

Get It, LLC | 128 N. Pitt St | Alexandria, VA 22314
BODYTEXT;
        $dbh->update('trans_email_tmpl', array('tmpl_body_html' => $bodyHtml, 'tmpl_body' => $bodyText,),'email_id=' . $id);

    }
}
