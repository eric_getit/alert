<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20121224030921 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $sql = <<<SQL
INSERT INTO sources
(sourceName, sourceClass, sourceUrl, sourceKey, createdDate, updatedDate)
VALUES
('BLE', 'BLE', 'http://www.jobinventory.com/api/search', '246558013', now(), now())
SQL;
       $this->addSql($sql);

    }

    public function down(Schema $schema)
    {
        $this->addSql('TRUNCATE TABLE sources');

    }
}
