<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Move text and HTML body columns to a different table.
 */
class Version20130204235532 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $date = new \DateTime('2013-01-21');
        $weekInterval = new \DateInterval('P1W');

        for ($i=0; $i<51; $i++) {
            $tableName = 'message_bodies_' . $date->format('Ymd');

            if (!$schema->hasTable($tableName)) {
                $table = $schema->createTable($tableName);
                $table->addColumn('messageId', 'integer', array('notnull' => true));
                $table->addColumn('bodyText', 'text', array('notnull' => false));
                $table->addColumn('bodyHtml', 'text', array('notnull' => false));
                $table->setPrimaryKey(array('messageId'));
            }

            $date->add($weekInterval);
        }
    } // up

    public function down(Schema $schema)
    {
        // No way down, we'll remove this manually
    }
}
