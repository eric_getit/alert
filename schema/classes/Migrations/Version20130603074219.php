<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130603074219 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $table = $schema->getTable('email_link');
        if ($table->hasColumn('first_click')) {
            $table->dropColumn('first_click');
        }
        if ($table->hasColumn('last_click')) {
            $table->dropColumn('last_click');
        }
        if ($table->hasColumn('clicks')) {
            $table->dropColumn('clicks');
        }
    }

    public function down(Schema $schema)
    {
        $table = $schema->getTable('email_link');
        if (!$table->hasColumn('first_click')) {
            $table->addColumn('first_click', 'datetime', array('notnull' => false));
        }
        if (!$table->hasColumn('last_click')) {
            $table->addColumn('last_click', 'datetime', array('notnull' => false));
        }
        if (!$table->hasColumn('clicks')) {
            $table->addColumn('clicks', 'integer', array('notnull' => false));
        }
    }
}
