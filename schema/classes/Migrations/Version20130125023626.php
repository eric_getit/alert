<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Add prepared date field for message archive table!
 */
class Version20130125023626 extends AbstractMigration
{
public function up(Schema $schema)
    {
        $table = $schema->getTable('message_archive');
        if (!$table->hasColumn('preparedDate')) {
            $table->addColumn('preparedDate', 'datetime', array('notnull' => false));
        }
    }

    public function down(Schema $schema)
    {
        $table = $schema->getTable('message_archive');
        if ($table->hasColumn('preparedDate')) {
            $table->dropColumn('preparedDate');
        }
    }
}
