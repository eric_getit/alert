<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Table for general report
 */
class Version20130128225039 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        if (!$schema->hasTable('report_general')) {
            $table = $schema->createTable('report_general');
            $table->addColumn('reportGeneralId', 'integer', array('autoincrement' => true, 'notnull' => true));
            $table->addColumn('reportDate', 'date', array('notnull' => true));
            $table->addColumn('sentEmails', 'integer', array('notnull' => false));
            $table->addColumn('newAlerts', 'integer', array('notnull' => false));
            $table->addColumn('activeAlerts', 'integer', array('notnull' => false));
            $table->addColumn('newSubscribers', 'integer', array('notnull' => false));
            $table->addColumn('activeSubscribers', 'integer', array('notnull' => false));
            $table->addColumn('organicLinks', 'integer', array('notnull' => false));
            $table->addColumn('backfillLinks', 'integer', array('notnull' => false));
            $table->addColumn('organicClicks', 'integer', array('notnull' => false));
            $table->addColumn('backfillClicks', 'integer', array('notnull' => false));
            $table->setPrimaryKey(array('reportGeneralId'));
        }
    }

    public function down(Schema $schema)
    {
        if ($schema->hasTable('report_general')) {
            $schema->dropTable('report_general');
        }
    }
}
