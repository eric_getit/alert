<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20121229155500 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $contactsTable = $schema->getTable('contacts');
        if (!$contactsTable->hasColumn('contactStatus')) {
            $contactsTable->addColumn('contactStatus', 'string', array('notnull' => false, 'length' => 256));
        }
    }

    public function down(Schema $schema)
    {
        $contactsTable = $schema->getTable('contacts');
        if (!$contactsTable->hasColumn('contactStatus')) {
            $contactsTable->dropColumn('contactStatus');
        }
    }
}
