<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130408042056 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $dbh = \Zend_Db_Table::getDefaultAdapter();
        $sql = <<<SQL
UPDATE message_views
SET messageSecureId = (
    SELECT secureId from message_archive
    WHERE message_archive.messageId = message_views.messageId
    )
SQL;
        $dbh->query($sql);

        $table = $schema->getTable('message_views');
        if ($table->hasColumn('messageId')) {
            $table->dropColumn('messageId');
        }

    }

    public function down(Schema $schema)
    {
        //manually
    }
}
