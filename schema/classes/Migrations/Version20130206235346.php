<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Create table to store trans email statistics
 */
class Version20130206235346 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        if (!$schema->hasTable('report_trans_email')) {
            $table = $schema->createTable('report_trans_email');
            $table->addColumn('reportTransEmailId', 'integer', array('autoincrement' => true, 'notnull' => true));
            $table->addColumn('reportDate', 'date', array('notnull' => true));
            $table->addColumn('permanentBounces', 'date', array('notnull' => true));
            $table->addColumn('transientBounces', 'integer', array('notnull' => false));
            $table->addColumn('undeterminedBounces', 'integer', array('notnull' => false));
            $table->addColumn('complaints', 'integer', array('notnull' => false));
            $table->addColumn('deliveryAttempts', 'integer', array('notnull' => false));
            $table->addColumn('deliveryRejects', 'integer', array('notnull' => false));
            $table->setPrimaryKey(array('reportTransEmailId'));
        }

    }

    public function down(Schema $schema)
    {
        if ($schema->hasTable('report_trans_email')) {
            $schema->dropTable('report_trans_email');
        }
    }
}
