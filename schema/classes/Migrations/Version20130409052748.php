<?php
/**
 * Version20130409052748
 *
 * PHP Version 5.3
 *
 * @category Migrations
 * @package  Migrations
 * @author   Get It <devteam@getit.me>
 * @license  Get It, LLC
 * @link     http://www.getit.me/
 */

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Create new table for the Overview report
 */
class Version20130409052748 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        if (!$schema->hasTable('report_overview')) {
            $sql = <<<SQL
CREATE TABLE report_overview (
  reportGeneralId int(11) NOT NULL AUTO_INCREMENT,
  reportDate datetime NOT NULL,
  sentEmails int(11) DEFAULT NULL,
  newAlerts int(11) DEFAULT NULL,
  activeAlerts int(11) DEFAULT NULL,
  newSubscribers int(11) DEFAULT NULL,
  activeSubscribers int(11) DEFAULT NULL,
  organicLinks int(11) DEFAULT NULL,
  backfillLinks int(11) DEFAULT NULL,
  organicClicks int(11) DEFAULT NULL,
  backfillClicks int(11) DEFAULT NULL,
  messageViews int(11) DEFAULT NULL,
  totalAlerts int(11) DEFAULT NULL,
  PRIMARY KEY (reportGeneralId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=100
SQL;
            $this->addSql($sql);
        }
    } // up

    public function down(Schema $schema)
    {
        if (!$schema->hasTable('report_overview')) {
            $schema->dropTable('report_overview');
        }
    } // down
}
