<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Table for bounces report
 */
class Version20130130071008 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        if (!$schema->hasTable('report_bounces')) {
            $table = $schema->createTable('report_bounces');
            $table->addColumn('reportBouncesId', 'integer', array('autoincrement' => true, 'notnull' => true));
            $table->addColumn('reportDate', 'date', array('notnull' => true));
            $table->addColumn('complaints', 'integer', array('notnull' => false));
            $table->addColumn('transientBounces', 'integer', array('notnull' => false));
            $table->addColumn('permanentBounces', 'integer', array('notnull' => false));
            $table->addColumn('undeterminedBounces', 'integer', array('notnull' => false));
            $table->setPrimaryKey(array('reportBouncesId'));
        }
    }

    public function down(Schema $schema)
    {
        if ($schema->hasTable('report_bounces')) {
            $schema->dropTable('report_bounces');
        }
    }
}
