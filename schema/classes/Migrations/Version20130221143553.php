<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Alter index on message_archive table secureId field to be unique
 */
class Version20130221143553 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $sql = 'ALTER TABLE message_archive DROP INDEX idx_secureId';
        $this->addSql($sql);


        $sql = 'ALTER TABLE message_archive ADD UNIQUE INDEX idx_secureId USING BTREE (secureId)';
        $this->addSql($sql);
    } // up

    public function down(Schema $schema)
    {
        $sql = 'ALTER TABLE message_archive DROP INDEX idx_secureId';
        $this->addSql($sql);

        $sql = 'ALTER TABLE message_archive ADD INDEX idx_secureId USING BTREE (secureId)';
        $this->addSql($sql);
    } // down
}
