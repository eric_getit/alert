<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130117024622 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $contactsTable = $schema->getTable('alerts');
        if (!$contactsTable->hasColumn('bouncesCount')) {
            $contactsTable->addColumn('bouncesCount', 'integer', array('notnull' => false, 'length' => 11));
        }
        if (!$contactsTable->hasColumn('complaintsCount')) {
            $contactsTable->addColumn('complaintsCount', 'integer', array('notnull' => false, 'length' => 11));
        }
        if (!$contactsTable->hasColumn('lastBounceDate')) {
            $contactsTable->addColumn('lastBounceDate', 'datetime', array('notnull' => false));
        }
        if (!$contactsTable->hasColumn('lastComplaintDate')) {
            $contactsTable->addColumn('lastComplaintDate', 'datetime', array('notnull' => false));
        }

    }

    public function down(Schema $schema)
    {
        $contactsTable = $schema->getTable('alerts');
        if (!$contactsTable->hasColumn('bouncesCount')) {
            $contactsTable->dropColumn('bouncesCount');
        }
        if (!$contactsTable->hasColumn('complaintsCount')) {
            $contactsTable->dropColumn('complaintsCount');
        }
        if (!$contactsTable->hasColumn('lastBounceDate')) {
            $contactsTable->dropColumn('lastBounceDate');
        }
        if (!$contactsTable->hasColumn('lastComplaintDate')) {
            $contactsTable->dropColumn('lastComplaintDate');
        }

    }
}
