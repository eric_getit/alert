<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130121051253 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $sql = 'CREATE  TABLE `email_click` (
          `id` INT NOT NULL AUTO_INCREMENT ,
          `alert_id` INT NOT NULL ,
          `link_id` INT NOT NULL ,
          `ip` VARCHAR(255) NOT NULL ,
          `user_agent` TEXT NOT NULL ,
          `created` DATETIME NOT NULL ,
          PRIMARY KEY (`id`) )
        ENGINE = InnoDB
        DEFAULT CHARACTER SET = utf8
        COLLATE = utf8_general_ci;';
        $this->addSql($sql);

    }

    public function down(Schema $schema)
    {
        $sql = 'DROP  TABLE `email_click`;';
        $this->addSql($sql);

    }
}
