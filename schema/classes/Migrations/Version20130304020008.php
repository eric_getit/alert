<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130304020008 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $sql = 'ALTER TABLE alerts MODIFY alertSecureId VARCHAR(255)';
        $this->addSql($sql);

        $sql = 'ALTER TABLE contacts MODIFY contactSecureId VARCHAR(255)';
        $this->addSql($sql);

        $sql = 'ALTER TABLE newsletter_subscriptions MODIFY subscriptionSecureId VARCHAR(255)';
        $this->addSql($sql);

    }

    public function down(Schema $schema)
    {
        $sql = 'ALTER TABLE alerts MODIFY alertSecureId VARCHAR(20)';
        $this->addSql($sql);

        $sql = 'ALTER TABLE contacts MODIFY contactSecureId VARCHAR(20)';
        $this->addSql($sql);

        $sql = 'ALTER TABLE newsletter_subscriptions MODIFY subscriptionSecureId VARCHAR(50)';
        $this->addSql($sql);

    }
}
