<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130131121100 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $dbh = \Zend_Db_Table::getDefaultAdapter();
        $sql = <<<SQL
    select contactId, email, count(*) as cnt
        from contacts
        group by email
        having count(*) > 1
        order by contactId
SQL;
        $canonicalContacts = $dbh->query($sql)->fetchAll();
        foreach ($canonicalContacts as $contactInfo) {
            $email = $dbh->quote($contactInfo['email']);
            $sql = "SELECT contactId FROM contacts WHERE email = {$email}";
            $duplicateContacts = $dbh->query($sql)->fetchAll(\PDO::FETCH_COLUMN, 0);
            $ids = implode(', ', $duplicateContacts);
            $sql = <<<SQL
    UPDATE alerts SET contactId = {$contactInfo['contactId']}
    WHERE contactId in ({$ids}) AND contactId != {$contactInfo['contactId']}
SQL;
            $this->addSql($sql);
            $this->addSql("DELETE FROM contacts WHERE contactId in ({$ids}) AND contactId != {$contactInfo['contactId']}");
        }

    }

    public function down(Schema $schema)
    {


    }
}
