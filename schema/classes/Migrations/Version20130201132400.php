<?php
/**
 * Fill site table with data from contacts table site data
 * @author eugene
 *
 */
namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130201132400 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $dbh = \Zend_Db_Table::getDefaultAdapter();
        $sql = <<<SQL
    SELECT distinct site, siteName, siteContactInfo FROM contacts;
SQL;
        $rows = $dbh->query($sql)->fetchAll();
        $values = array();
        foreach ($rows as $row) {
            $domain = $dbh->quote(\Model_Site::normalizeDomain($row['site']));
            $siteName = $dbh->quote($row['siteName']);
            $siteNiche = trim(
                str_replace(
                    array('Get', 'Jobs', 'Job Finder'), '', $row['siteName']
                )
            );
            $siteNiche = $dbh->quote($siteNiche);
            $newsletterDomain = $dbh->quote(null);
            $fromName = $dbh->quote(null);
            $fromEmail = $dbh->quote(null);
            $data = json_decode($row['siteContactInfo'], true);
            if (!empty($data)) {
                if (array_key_exists('siteContactEmail', $data)) {
                    $fromEmail = $dbh->quote($data['siteContactEmail']);
                }
                if (array_key_exists('siteContactName', $data)) {
                    $fromName = $dbh->quote($data['siteContactName']);
                }
                if (array_key_exists('NEWSLETTER_DOMAIN', $data)) {
                    $newsletterDomain = $dbh->quote($data['NEWSLETTER_DOMAIN']);
                }
            }
            $values[] = "({$domain}, {$siteName}, {$newsletterDomain}, "
                . "{$siteNiche}, {$fromName}, {$fromEmail}, NOW(), NOW())";
        }

        if (!empty($values)) {
            $vals = implode(', ', $values);
            $sql = <<<SQL
    INSERT IGNORE sites
        (domain, siteName, newsletterDomain, siteNiche, fromName, fromEmail, createdDate, updatedDate)
    VALUES
        {$vals};
SQL;
            $this->addSql($sql);
        }
    }

    public function down(Schema $schema)
    {


    }
}
