<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Change deactive statuses to user unsubscribes cause mainly this was the only way to get deactivated.
 */
class Version20130220160900 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $sql = 'update alerts
        set alertStatus = "UNSUBSCRIBED_ADMIN"
        where alertStatus = "UNSUBSCRIBED"';
        $this->addSql($sql);

        $sql = 'update contacts
        set contactStatus = "UNSUBSCRIBED_ADMIN"
        where contactStatus = "UNSUBSCRIBED";';
        $this->addSql($sql);
    } // up

    public function down(Schema $schema)
    {
        $sql = 'update alerts
        set alertStatus = "UNSUBSCRIBED"
        where alertStatus = "UNSUBSCRIBED_ADMIN"';
        $this->addSql($sql);

        $sql = 'update contacts
        set contactStatus = "UNSUBSCRIBED"
        where contactStatus = "UNSUBSCRIBED_ADMIN";';
        $this->addSql($sql);
    } // down
}
