<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20121224024702 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql");
        $sql = <<<SQL
CREATE TABLE sources (
    sourceId int not null primary key auto_increment,
    sourceName varchar(512),
    sourceClass varchar(1024),
    sourceUrl varchar(1024),
    sourceKey varchar(1024),
    createdDate datetime,
    updatedDate datetime
);
SQL;
        $this->addSql($sql);

    }

    public function down(Schema $schema)
    {
        $this->addSql("DROP TABLE sources");

    }
}
