<?php
/**
 * Cleanup contacts table from site information
 * @author eugene
 *
 */
namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130201135000 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $table = $schema->getTable('contacts');
        if ($table->hasColumn('site')) {
            $table->dropColumn('site');
        }
        if ($table->hasColumn('siteName')) {
            $table->dropColumn('siteName');
        }
        if ($table->hasColumn('siteContactInfo')) {
            $table->dropColumn('siteContactInfo');
        }

    }

    public function down(Schema $schema)
    {
    }
}
