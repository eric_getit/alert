<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Add type to the email click table
 */
class Version20130320073454 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $table = $schema->getTable('email_click');
        $table->addColumn('type', 'string', array('length'=>'255', 'notnull' => false));
        $table->addColumn('link_code', 'string', array('length'=>'255', 'notnull' => false));
    } // up

    public function down(Schema $schema)
    {
        $table = $schema->getTable('email_click');
        $table->dropColumn('type');
        $table->dropColumn('link_code');
    } // down
}
