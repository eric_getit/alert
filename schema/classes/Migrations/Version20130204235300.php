<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130204235300 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        if ($schema->hasTable('newsletter_queue')) {
            $table = $schema->getTable('newsletter_queue');
            if ($table->hasColumn('newsletterExternalId')) {
                $table->dropColumn('newsletterExternalId');
            }
            if (!$table->hasColumn('newsletterId')) {
                $table->addColumn('newsletterId', 'integer', array('notnull' => false));
            }
        }
    }

    public function down(Schema $schema)
    {
        if ($schema->hasTable('newsletter_queue')) {
            $table = $schema->getTable('newsletter_queue');
            if ($table->hasColumn('newsletterId')) {
                $table->dropTable('newsletterId');
            }
            if (!$table->hasColumn('newsletterExternalId')) {
                $table->addColumn('newsletterExternalId', 'integer', array('notnull' => false));
            }
        }
    }
}
