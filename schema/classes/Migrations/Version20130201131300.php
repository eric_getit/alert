<?php
/**
 * Added siteId field for linkin alert and site entities
 * @author eugene
 *
 */
namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130201131300 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $table = $schema->getTable('alerts');
        if (!$table->hasColumn('siteId')) {
            $table->addColumn('siteId', 'integer', array('notnull' => false));
            $table->addIndex(array('siteId'), 'siteId');
        }
    }

    public function down(Schema $schema)
    {
        $table = $schema->getTable('alerts');
        if ($table->hasColumn('siteId')) {
            $table->dropColumn('siteId');
        }

    }
}
