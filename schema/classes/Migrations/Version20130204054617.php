<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130204054617 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $sql = "
            CREATE TABLE `admin_user` (
              `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
              `email` varchar(255) NOT NULL COMMENT 'Email, used to identify user',
              `status` enum('active','inactive') NOT NULL DEFAULT 'active' COMMENT 'Admin account status',
              `passwd` varchar(255) NOT NULL COMMENT 'Account password',
              `firstName` varchar(255) DEFAULT NULL COMMENT 'Administrator first name',
              `lastName` varchar(255) DEFAULT NULL COMMENT 'Administrator last name',
              `adminComment` int(11) DEFAULT NULL COMMENT 'Comment for record',
              PRIMARY KEY (`id`),
              UNIQUE KEY `email` (`email`)
            ) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8
        ";
        $this->addSql($sql);
        $this->addSql("INSERT INTO `admin_user`
            (`id`, `email`, `status`, `passwd`, `firstName`, `lastName`, `adminComment`)
        VALUES
            (1,'superadmin','active','59d7b4334d35c08dd8fbfe95c3efd2e3','superadmin','superadmin',NULL),
            (2,'admin','active','59d7b4334d35c08dd8fbfe95c3efd2e3','admin','admin',NULL),
            (3,'user','active','59d7b4334d35c08dd8fbfe95c3efd2e3','user','user',NULL),
            (4,'jacob','active','59d7b4334d35c08dd8fbfe95c3efd2e3','Jacob','',NULL),
            (5,'casey','active','59d7b4334d35c08dd8fbfe95c3efd2e3','Casey','',NULL),
            (6,'victor','active','59d7b4334d35c08dd8fbfe95c3efd2e3','Victor','Smirnov',NULL),
            (7,'eugene','active','db82206b1d49042d1a710e9c88c21d36','Eugene','Kuznetsov',NULL)"
        );

    }

    public function down(Schema $schema)
    {
        $sql = 'DROP TABLE `admin_user`';
        $this->addSql($sql);

    }
}
