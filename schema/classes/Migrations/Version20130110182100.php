<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130110182100 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $dbh = \Zend_Db_Table::getDefaultAdapter();
        $select = $dbh->select();
        $select->from('trans_email_email', array('email_id'));
        $select->where('email_key = "email alert"');
        $id = $select->query()->fetchColumn();

        $this->abortIf(!$id, 'Email not defined!');

        $subject = "{{COUNT}} New {{CANONICAL_QUERY}}";
        $bodyHtml = <<<BODYHTML
<html>
<head>
    <meta http-equiv="Content-Type" content="text/HTML; charset=utf-8" />
    <title>Email Alert</title>
    <style>
       body {
            font-size: 12px;
        }
        table {
            width: 100%;
            border:0;
            padding:0;
            border-collapse:collapse;
            margin-bottom:10px;
        }

        table td {
            padding-top: 10px;
            padding-bottom: 5px;
            border-bottom: 1px solid #888888;
        }

        th {
            padding-bottom: 5px;
            border-bottom: 1px solid #888888;
            text-align: left;
        }

        th.title {
            width: 60%;
        }

        th.location {
            width: 20%;
            text-align:left;
        }

        th.date {
            width: 20%;
            text-align:left;
        }
    </style>
</head>
<body>
<p>1 - {{COUNT}} New {{CANONICAL_QUERY}} - {{DATE}}</p>

<p><a href="{{EDIT_ALERT_URL}}">Edit the location, distance or keyword</a> or <a href="{{UNSUBSCRIBE_ALERT_URL}}">cancel</a> this alert.</p>

{{LISTINGS}}

<p>Not what you're looking for? Improve these results here.</p>
<p>Got a job? <a href="{{UNSUBSCRIBE_ALERT_URL}}">Cancel this alert</a> or <a href="{{VIEW_ALL_ALERTS_URL}}">unsubscribe</a> from all alerts.</p>
<p>You have received this email because you subscribed to it, are a member, or someone forwarded it to you.</p>
<p>Feel free to contact us if you have any questions:</p>
<p>Get It, LLC | 128 N. Pitt St | Alexandria, VA 22314</p>
</body>
</html>
BODYHTML;
        $dbh->update('trans_email_tmpl', array('tmpl_body_html' => $bodyHtml, 'tmpl_subject' => $subject,),'email_id=' . $id);

    }

    public function down(Schema $schema)
    {
        // this down() migration is autogenerated, please modify it to your needs

    }
}
