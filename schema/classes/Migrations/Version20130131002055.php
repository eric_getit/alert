<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130131002055 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        if (!$schema->hasTable('newsletter')) {
            $table = $schema->createTable('newsletter');
            $table->addColumn('newsletterId', 'integer', array('autoincrement' => true, 'notnull' => true));
            $table->addColumn('externalId', 'string', array('length' => 255, 'notnull' => true));
            $table->addColumn('newsletterName', 'string', array('length' => 1024, 'notnull' => false));
            $table->addColumn('domain', 'string', array('length' => 1024, 'notnull' => false));
            $table->addColumn('siteName', 'string', array('length' => 1024, 'notnull' => false));
            $table->addColumn('fromName', 'string', array('length' => 2048, 'notnull' => false));
            $table->addColumn('fromEmail', 'string', array('length' => 2048, 'notnull' => false));
            $table->addColumn('createdDate', 'datetime', array('notnull' => false));
            $table->addColumn('updatedDate', 'datetime', array('notnull' => false));
            $table->setPrimaryKey(array('newsletterId'));
            $table->addUniqueIndex(array('externalId'), 'idx_externalId');
            // max key length is 767 bytes, utf8 3 byte - 1 character => externalId => 255
        }

        if (!$schema->hasTable('newsletter_subscriptions')) {
            $table = $schema->createTable('newsletter_subscriptions');
            $table->addColumn('subscriptionId', 'integer', array('autoincrement' => true, 'notnull' => true));
            $table->addColumn('newsletterId', 'integer', array('length' => 11, 'notnull' => true));
            $table->addColumn('subscriptionStatus', 'string', array('length' => 512, 'notnull' => false));
            $table->addColumn('contactId', 'integer', array('length' => 11, 'notnull' => false));
            $table->addColumn('createdDate', 'datetime', array('notnull' => false));
            $table->addColumn('updatedDate', 'datetime', array('notnull' => false));
            $table->setPrimaryKey(array('subscriptionId'));
        }

        if (!$schema->hasTable('newsletter_queue')) {
            $table = $schema->createTable('newsletter_queue');
            $table->addColumn('queueId', 'integer', array('autoincrement' => true, 'notnull' => true));
            $table->addColumn('newsletterExternalId', 'string', array('length' => 255, 'notnull' => true));
            $table->addColumn('queueStatus', 'string', array('length' => 512, 'notnull' => false));
            $table->addColumn('subject', 'string', array('length' => 2048, 'notnull' => false));
            $table->addColumn('message', 'text', array('notnull' => false));
            $table->addColumn('sendTime', 'datetime', array('notnull' => false));
            $table->addColumn('fromEmail', 'string', array('length' => 2048, 'notnull' => false));
            $table->addColumn('fromName', 'string', array('length' => 2048, 'notnull' => false));
            $table->addColumn('createdDate', 'datetime', array('notnull' => false));
            $table->addColumn('updatedDate', 'datetime', array('notnull' => false));
            $table->setPrimaryKey(array('queueId'));
        }

    }

    public function down(Schema $schema)
    {
        if ($schema->hasTable('newsletter')) {
            $schema->dropTable('newsletter');
        }

        if ($schema->hasTable('newsletter_subscriptions')) {
            $schema->dropTable('newsletter_subscriptions');
        }

        if ($schema->hasTable('newsletter_queue')) {
            $schema->dropTable('newsletter_queue');
        }

    }
}
