<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130112104901 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        if (!$schema->hasTable('location_us_city')) {
            $sql = <<<SQL
CREATE TABLE `location_us_city` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sourceId` int(11) NOT NULL,
  `code` varchar(10) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `stateCode` varchar(2) DEFAULT NULL,
  `population` bigint(20) DEFAULT NULL,
  `lat` double(15,12) DEFAULT NULL,
  `lng` double(15,12) DEFAULT NULL,
  `searchName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_source` (`sourceId`),
  UNIQUE KEY `uk_name_state` (`name`,`stateCode`),
  KEY `idx_population` (`population`),
  KEY `code` (`code`),
  KEY `idx_state` (`stateCode`),
  KEY `location_us_city_idx1` (`stateCode`,`code`(1),`population`),
  KEY `idx_searchName` (`searchName`),
  KEY `idx_searchName_state` (`stateCode`,`searchName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL;
            $this->addSql($sql);
        }

        if (!$schema->hasTable('location_us_city_synonyms')) {
            $sql = <<<SQL
CREATE TABLE `location_us_city_synonyms` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cityId` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `stateCode` varchar(2) NOT NULL,
  `searchName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_state` (`stateCode`),
  KEY `idx_searchName` (`searchName`),
  KEY `idx_searchName_state` (`stateCode`,`searchName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL;
            $this->addSql($sql);
        }

        if (!$schema->hasTable('location_us_zip')) {
            $sql = <<<SQL
CREATE TABLE `location_us_zip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zip` varchar(10) NOT NULL,
  `city` varchar(255) DEFAULT NULL,
  `stateCode` varchar(2) DEFAULT NULL,
  `locationId` int(11) DEFAULT NULL,
  `lat` float DEFAULT NULL,
  `lng` float DEFAULT NULL,
  `searchName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `location_us_zip_idx1` (`zip`),
  KEY `idx_searchName` (`searchName`),
  KEY `idx_searchName_state` (`stateCode`,`searchName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL;
            $this->addSql($sql);
        }

    }

    public function down(Schema $schema)
    {
        if ($schema->hasTable('location_us_city')) {
            $schema->dropTable('location_us_city');
        }

        if ($schema->hasTable('location_us_city_synonyms')) {
            $schema->dropTable('location_us_city_synonyms');
        }

        if ($schema->hasTable('location_us_zip')) {
            $schema->dropTable('location_us_zip');
        }
    }
}
