<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130603084049 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $table = $schema->getTable('email_link');
        $table->addUniqueIndex(array('code'), 'uniq_code');

    }

    public function down(Schema $schema)
    {
       //manualy need set primary key to id field that was droped in previos migration

    }
}
