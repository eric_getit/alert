<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20121227095234 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $sql = <<<SQL
CREATE TABLE if not exists send_email_statistics (
id INT NOT NULL AUTO_INCREMENT,
send DATETIME NOT NULL,
name VARCHAR(255) NOT NULL,
PRIMARY KEY p_id (id)
) ENGINE = InnoDB;
SQL;
        $this->addSql($sql);

    }

    public function down(Schema $schema)
    {
        $this->addSql('DROP TABLE send_email_statistics');

    }
}
