<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130610150400 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("ALTER TABLE alert_operation_history ADD INDEX alertId (alertId)");

    }

    public function down(Schema $schema)
    {
        $this->addSql("ALTER TABLE alert_operation_history DROP INDEX alertId");

    }
}
