<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130128044305 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $dbh = \Zend_Db_Table::getDefaultAdapter();
        $select = $dbh->select();
        $select->from('config_group', array('id'));
        $select->where('name = "defaultUTM"');
        $id = $select->query()->fetchColumn();
        if ($id) {
            $this->addSql("INSERT INTO `config_db`
                (
                    `name`,
                    `displayName`,
                    `configValue`,
                    `displayGroupId`,
                    `displayHelp`
                )
                VALUES
                (
                    'utmSource',
                    'Utm Source',
                    '',
                    $id,
                    'A UTM Source tag names the site that is driving the traffic.'
                ),
                (
                    'utmMedium',
                    'Utm Medium',
                    '',
                    $id,
                    'A UTM Medium tag names the type of traffic.'
                ),
                (
                    'utmTerm',
                    'Utm Term',
                    '',
                    $id,
                    'A UTM Term tag is used for search campaigns and tells you what keywords were used.'
                ),
                (
                    'utmContent',
                    'Utm Content',
                    '',
                    $id,
                    'A UTM Content tag gives you a sense of what was in the add or post that led to the click-through.'
                ),
                (
                    'utmCampaign',
                    'Utm Campaign',
                    '',
                    $id,
                    'A UTM Campaign tag names the specific campaign that this traffic is a part of.'
                )"
            );
        }
    }

    public function down(Schema $schema)
    {
        $this->addSql("DELETE FROM `config_db` WHERE `name`='utmSource'");
        $this->addSql("DELETE FROM `config_db` WHERE `name`='utmMedium'");
        $this->addSql("DELETE FROM `config_db` WHERE `name`='utmTerm'");
        $this->addSql("DELETE FROM `config_db` WHERE `name`='utmContent'");
        $this->addSql("DELETE FROM `config_db` WHERE `name`='utmCampaign'");
    }
}
