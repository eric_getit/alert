<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130315025726 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $sql = 'UPDATE alerts SET isLocationValid = 1';
        $this->addSql($sql);

    }

    public function down(Schema $schema)
    {

    }
}
