<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130201123900 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        if (!$schema->hasTable('sites')) {
            $table = $schema->createTable('sites');
            $table->addColumn('siteId', 'integer', array('autoincrement' => true, 'notnull' => true));
            $table->addColumn('domain', 'string', array('length' => 255, 'notnull' => true));
            $table->addColumn('siteName', 'string', array('length' => 1024, 'notnull' => false));
            $table->addColumn('newsletterDomain', 'string', array('length' => 1024, 'notnull' => false));
            $table->addColumn('siteNiche', 'string', array('length' => 1024, 'notnull' => false));
            $table->addColumn('fromName', 'string', array('length' => 2048, 'notnull' => false));
            $table->addColumn('fromEmail', 'string', array('length' => 2048, 'notnull' => false));
            $table->addColumn('createdDate', 'datetime', array('notnull' => false));
            $table->addColumn('updatedDate', 'datetime', array('notnull' => false));
            $table->setPrimaryKey(array('siteId'));
            $table->addUniqueIndex(array('domain'), 'domain');
            // max key length is 767 bytes, utf8 3 byte - 1 character => externalId => 255
        }

    }

    public function down(Schema $schema)
    {
        if ($schema->hasTable('sites')) {
            $schema->dropTable('sites');
        }

    }
}
