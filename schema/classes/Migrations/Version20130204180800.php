<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130204180800 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        if ($schema->hasTable('newsletter')) {
            $table = $schema->getTable('newsletter');
            if (!$table->hasColumn('siteId')) {
                $table->addColumn('siteId', 'integer', array('notnull' => false));
            }
        }
    }

    public function down(Schema $schema)
    {
        if ($schema->hasTable('newsletter')) {
            $table = $schema->getTable('newsletter');
            if ($table->hasColumn('siteId')) {
                $table->dropTable('siteId');
            }
        }
    }
}
