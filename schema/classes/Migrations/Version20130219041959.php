<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Change deactive statuses to user unsubscribes cause mainly this was the only way to get deactivated.
 */
class Version20130219041959 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $sql = 'update alerts
        set alertStatus = "UNSUBSCRIBED_USER"
        where alertStatus = "DEACTIVE"';
        $this->addSql($sql);

        $sql = 'update contacts
        set contactStatus = "UNSUBSCRIBED_USER"
        where contactStatus = "DEACTIVE";';
        $this->addSql($sql);
    } // up

    public function down(Schema $schema)
    {
        $sql = 'update alerts
        set alertStatus = "DEACTIVE"
        where alertStatus = "UNSUBSCRIBED_USER"';
        $this->addSql($sql);

        $sql = 'update contacts
        set contactStatus = "DEACTIVE"
        where contactStatus = "UNSUBSCRIBED_USER";';
        $this->addSql($sql);
    } // down
}
