<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Add bcc field for message archive table!
 */
class Version20130125023300 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $table = $schema->getTable('message_archive');
        if (!$table->hasColumn('bcc')) {
            $table->addColumn('bcc', 'string', array('notnull' => false, 'length' => 256));
        }
    }

    public function down(Schema $schema)
    {
        $table = $schema->getTable('message_archive');
        if ($table->hasColumn('bcc')) {
            $table->dropColumn('bcc');
        }
    }
}
