<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Drop bodyText and bodyHtml columns
 */
class Version20130205032944 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $table = $schema->getTable('message_archive');
        if ($table->hasColumn('bodyText')) {

            $dbh = \Zend_Db_Table::getDefaultAdapter();
            $sql = 'select count(*) from message_archive
                    where bodyText is not NULL or bodyHtml is not NULL';
            $notEmptyCount = $dbh->fetchOne($sql);
            $this->abortIf(
                    $notEmptyCount > 0,
                    'Please run populateMessageBodyTables.php script first to move message bodies to message_bodies tables!'
                    );

            $table->dropColumn('bodyText');
            $table->dropColumn('bodyHtml');
        }

        if (!$table->hasIndex('message_archive_idx2')) {
            $table->addIndex(array('contactId'), 'message_archive_idx2');
        }

        if (!$table->hasIndex('message_archive_idx3')) {
            $table->addIndex(array('preparedDate'), 'message_archive_idx3');
        }
    }

    public function down(Schema $schema)
    {
        // Restore from backup.

    }
}
