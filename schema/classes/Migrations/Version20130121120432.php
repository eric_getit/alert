<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema,
    Doctrine\DBAL\Schema\Table;

/**
 * Create message_archive table
 */
class Version20130121120432 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        if (!$schema->hasTable('message_archive')) {
            $table = $schema->createTable('message_archive');
            $table->addColumn('messageId', 'integer', array('autoincrement' => true, 'notnull' => true));
            $table->addColumn('alertId', 'integer', array('notnull' => false));
            $table->addColumn('contactId', 'integer', array('notnull' => false));
            $table->addColumn('sentDate', 'string', array('length' => 64, 'notnull' => false));
            $table->addColumn('fromEmail', 'string', array('length' => 2048, 'notnull' => false));
            $table->addColumn('fromName', 'string', array('length' => 2048, 'notnull' => false));
            $table->addColumn('toEmail', 'string', array('length' => 2048, 'notnull' => false));
            $table->addColumn('toName', 'string', array('length' => 2048, 'notnull' => false));
            $table->addColumn('subject', 'string', array('length' => 2048, 'notnull' => false));
            $table->addColumn('bodyText', 'text', array('notnull' => false));
            $table->addColumn('bodyHtml', 'text', array('notnull' => false));
            $table->setPrimaryKey(array('messageId'));
        }
    } // up

    public function down(Schema $schema)
    {
        if ($schema->hasTable('message_archive')) {
            $schema->dropTable('message_archive');
        }
    } // down
}
