<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130508012005 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("UPDATE sources SET sourceClass = 'Ble' where sourceClass = 'BLE'");

    }

    public function down(Schema $schema)
    {
        $this->addSql("UPDATE sources SET sourceClass = 'BLE' where sourceClass = 'Ble'");

    }
}
