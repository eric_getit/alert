<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130416032651 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $table = $schema->getTable('contacts');
        if (!$table->hasColumn('sendTimezone')) {
            $table->addColumn('sendTimezone', 'string', array('notnull' => false, 'length' => 255));
        }
        if (!$table->hasColumn('userTimezone')) {
            $table->addColumn('userTimezone', 'string', array('notnull' => false, 'length' => 255));
        }
        $table->addIndex(array('sendTimezone'), 'sendTZ_idx');

    }

    public function down(Schema $schema)
    {
        $table = $schema->getTable('contacts');
        if ($table->hasColumn('sendTimezone')) {
            $table->dropColumn('sendTimezone');
        }
        if ($table->hasColumn('userTimezone')) {
            $table->dropColumn('userTimezone');
        }

    }
}
