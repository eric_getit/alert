<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130117073635 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $contactsTable = $schema->getTable('alerts');
        if (!$contactsTable->hasColumn('sesMessageId')) {
            $contactsTable->addColumn('sesMessageId', 'string', array('notnull' => false, 'length' => 256));
        }

    }

    public function down(Schema $schema)
    {
        $contactsTable = $schema->getTable('alerts');
        if (!$contactsTable->hasColumn('sesMessageId')) {
            $contactsTable->dropColumn('sesMessageId');
        }

    }
}
