<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130114051012 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $sql = 'DROP TABLE send_email_statistics';
        $this->addSql($sql);

    }

    public function down(Schema $schema)
    {
        $sql = 'CREATE TABLE `send_email_statistics` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `send` datetime NOT NULL,
          `name` varchar(255) NOT NULL,
          `request` text,
          `response` text,
          `status` varchar(255) DEFAULT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB;';
        $this->addSql($sql);

    }
}
