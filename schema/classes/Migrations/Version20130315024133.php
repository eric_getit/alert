<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130315024133 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $table = $schema->getTable('alerts');
        $table->addColumn('isLocationValid', 'integer', array('notnull' => false, 'length' => 1));
    }

    public function down(Schema $schema)
    {
        $table = $schema->getTable('alerts');
        $table->dropColumn('isLocationValid');
    }
}
