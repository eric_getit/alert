<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Rename column name from sentDate to sendDate for consistancy
 */
class Version20130123153115 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $table = $schema->getTable('message_archive');
        if ($table->hasColumn('sentDate')) {
            $sql = 'alter table message_archive change sentDate sendDate datetime default null';
            $this->addSql($sql);
        }
    }

    public function down(Schema $schema)
    {

        $table = $schema->getTable('message_archive');
        if ($table->hasColumn('sendDate')) {
            $sql = 'alter table message_archive change sendDate sentDate datetime default null';
            $this->addSql($sql);
        }
    }
}
