<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Add the originalCreatedDate to the contacts table
 */
class Version20130123122700 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $table = $schema->getTable('alerts');
        if (!$table->hasColumn('distance')) {
            $table->addColumn('distance', 'integer', array('notnull' => false));
        }
    }

    public function down(Schema $schema)
    {

        $table = $schema->getTable('alerts');
        if ($table->hasColumn('distance')) {
            $table->dropColumn('distance');
        }
    }
}
