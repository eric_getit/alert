<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130121050529 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $sql = 'CREATE  TABLE `email_link` (
          `id` INT NOT NULL AUTO_INCREMENT ,
          `code` VARCHAR(255) NOT NULL ,
          `url` VARCHAR(255) NOT NULL ,
          `type` VARCHAR(255) NOT NULL ,
          `alert_id` INT NOT NULL ,
          `created` DATETIME NOT NULL ,
          `first_click` DATETIME NULL ,
          `last_click` DATETIME NULL ,
          `clicks` INT NOT NULL DEFAULT 0 ,
          PRIMARY KEY (`id`) ,
          INDEX `IN_CODE` (`code`) )
        ENGINE = InnoDB
        DEFAULT CHARACTER SET = utf8
        COLLATE = utf8_general_ci;';
        $this->addSql($sql);

    }

    public function down(Schema $schema)
    {
        $sql = 'DROP  TABLE `email_link`;';
        $this->addSql($sql);

    }
}
