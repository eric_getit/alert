<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20121226140700 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $alertsTable = $schema->getTable('alerts');
        if (!$alertsTable->hasColumn('alertStatus')) {
            $alertsTable->addColumn('alertStatus', 'string', array('notnull' => false, 'length' => '128'));
            $alertsTable->addIndex(array('alertStatus'), 'status');
        }

    }

    public function down(Schema $schema)
    {
        $alertsTable = $schema->getTable('alerts');
        if (!$alertsTable->hasColumn('alertStatus')) {
            $alertsTable->dropColumn('alertStatus');
        }
    }
}
