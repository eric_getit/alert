<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Added column to count message views
 */
class Version20130204070016 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $table = $schema->getTable('report_general');
        if (!$table->hasColumn('messageViews')) {
            $table->addColumn('messageViews', 'integer', array('notnull' => false));
        }
    } // up

    public function down(Schema $schema)
    {
        $table = $schema->getTable('report_general');
        if ($table->hasColumn('messageViews')) {
            $table->dropColumn('messageViews');
        }
    } // down
}
