<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130201013947 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $table = $schema->getTable('message_archive');
        if (!$table->hasColumn('secureId')) {
            $table->addColumn('secureId', 'string', array('length' => 256, 'notnull' => false));
            $table->addIndex(array('secureId'), 'idx_secureId');
        }
        if (!$table->hasColumn('viewsNumber')) {
            $table->addColumn('viewsNumber', 'integer', array('length' => 11, 'notnull' => false));
        }

    }

    public function down(Schema $schema)
    {
        $table = $schema->getTable('message_archive');
        if ($table->hasColumn('secureId')) {
            $table->dropColumn('secureId');
        }
        if ($table->hasColumn('viewsNumber')) {
            $table->dropColumn('viewsNumber');
        }

    }
}
