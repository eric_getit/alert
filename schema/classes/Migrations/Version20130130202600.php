<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Rename column name from sentDate to sendDate for consistancy
 */
class Version20130130202600 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $sql = <<<SQL
        ALTER TABLE alerts ADD INDEX `contactId` (contactId);
SQL;
        $this->addSql($sql);
    }

    public function down(Schema $schema)
    {

        $this->addSql("ALTER TABLE alerts DROP INDEX `contactId`");
    }
}
