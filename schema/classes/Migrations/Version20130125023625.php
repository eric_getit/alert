<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Add status field for message archive table!
 */
class Version20130125023625 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $table = $schema->getTable('message_archive');
        if (!$table->hasColumn('status')) {
            $table->addColumn('status', 'string', array('notnull' => true, 'length' => 256));
        }
    }

    public function down(Schema $schema)
    {
        $table = $schema->getTable('message_archive');
        if ($table->hasColumn('status')) {
            $table->dropColumn('status');
        }
    }
}
