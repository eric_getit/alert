<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130416034856 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $sql = 'ALTER TABLE alerts MODIFY nextProcessDate DATE';
        $this->addSql($sql);

    }

    public function down(Schema $schema)
    {
        $sql = 'ALTER TABLE alerts MODIFY nextProcessDate DATETIME';
        $this->addSql($sql);

    }
}
