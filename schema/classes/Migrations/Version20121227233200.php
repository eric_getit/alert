<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20121227233200 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $sql = <<<SQL
create table session (
    id varchar(32) primary key not null,
     modified int,
    lifetime int,
    data text
);
SQL;
        $this->addSql($sql);
    }

    public function down(Schema $schema)
    {
        $this->addSql("DROP table session");

    }
}
