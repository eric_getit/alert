<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130128043728 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $sql = "INSERT INTO config_group
            (name, displayName, displayHelp, position)
            VALUES
            ('defaultUTM', 'Default UTM', 'Default UTM settings.', 1)";
        $this->addSql($sql);

    }

    public function down(Schema $schema)
    {
        $sql = "DELETE FROM config_group
            WHERE name = 'defaultUTM'";
        $this->addSql($sql);

    }
}
