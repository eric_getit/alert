<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Rename column name from sentDate to sendDate for consistancy
 */
class Version20130130204700 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $sql = <<<SQL
        ALTER TABLE contacts ADD INDEX `email` (email(512));
SQL;
        $this->addSql($sql);
    }

    public function down(Schema $schema)
    {

        $this->addSql("ALTER TABLE contacts DROP INDEX `email`");
    }
}
