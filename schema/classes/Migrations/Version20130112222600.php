<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130112222600 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $contactsTable = $schema->getTable('alerts');
        if (!$contactsTable->hasColumn('utmSource')) {
            $contactsTable->addColumn('utmSource', 'string', array('notnull' => false, 'length' => 512));
        }
        if (!$contactsTable->hasColumn('utmMedium')) {
            $contactsTable->addColumn('utmMedium', 'string', array('notnull' => false, 'length' => 512));
        }
        if (!$contactsTable->hasColumn('utmTerm')) {
            $contactsTable->addColumn('utmTerm', 'string', array('notnull' => false, 'length' => 512));
        }
        if (!$contactsTable->hasColumn('utmContent')) {
            $contactsTable->addColumn('utmContent', 'string', array('notnull' => false, 'length' => 512));
        }
        if (!$contactsTable->hasColumn('utmCampaign')) {
            $contactsTable->addColumn('utmCampaign', 'string', array('notnull' => false, 'length' => 512));
        }
    }

    public function down(Schema $schema)
    {
        $contactsTable = $schema->getTable('alerts');
        if (!$contactsTable->hasColumn('utmSource')) {
            $contactsTable->dropColumn('utmSource');
        }
        if (!$contactsTable->hasColumn('utmMedium')) {
            $contactsTable->dropColumn('utmMedium');
        }
        if (!$contactsTable->hasColumn('utmTerm')) {
            $contactsTable->dropColumn('utmTerm');
        }
        if (!$contactsTable->hasColumn('utmContent')) {
            $contactsTable->dropColumn('utmContent');
        }
        if (!$contactsTable->hasColumn('utmCampaign')) {
            $contactsTable->dropColumn('utmCampaign');
        }
    }
}
