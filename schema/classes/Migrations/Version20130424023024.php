<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130424023024 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $sql = "INSERT INTO config_group
            (name, displayName, displayHelp, position)
            VALUES
            ('general', 'General', 'General settings.', 1)";
        $this->addSql($sql);

    }

    public function down(Schema $schema)
    {
        $sql = "DELETE FROM config_group
            WHERE name = 'general'";
        $this->addSql($sql);

    }
}
