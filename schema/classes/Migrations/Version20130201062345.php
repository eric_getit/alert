<?php
namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Add links counters to the message_archive
 */
class Version20130201062345 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        if ($schema->hasTable('message_archive')) {
            $table = $schema->getTable('message_archive');
            if (!$table->hasColumn('organicLinks')) {
                $table->addColumn('organicLinks', 'integer', array('notnull' => false));
            }
            if (!$table->hasColumn('backfillLinks')) {
                $table->addColumn('backfillLinks', 'integer', array('notnull' => false));
            }
        }
    } // up

    public function down(Schema $schema)
    {
        if ($schema->hasTable('message_archive')) {
            $table = $schema->getTable('message_archive');
            if ($table->hasColumn('organicLinks')) {
                $table->dropColumn('organicLinks');
            }
            if ($table->hasColumn('backfillLinks')) {
                $table->dropColumn('backfillLinks');
            }
        }
    } // down
}
