<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Alter index on message_archive table secureId field to be unique
 */
class Version20130301160700 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $sql = <<<SQL
    CREATE TABLE alert_operation_history (
        alertOperationId int not null primary key auto_increment,
        alertId int not null,
        operation varchar(255) not null,
        ip varchar(100) not null,
        createdDate datetime
) ENGINE=InnoDB DEFAULT CHARSET=UTF8;
SQL;
        $this->addSql($sql);
    } // up

    public function down(Schema $schema)
    {
        $sql = 'DROP TABLE alert_operation_history';
        $this->addSql($sql);
    } // down
}
