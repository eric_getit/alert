<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130117235648 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $contactsTable = $schema->getTable('contacts');
        if ($contactsTable->hasColumn('contactStatus')) {
            $contactsTable->addIndex(array('contactStatus'), 'idx_contactStatus');
        }

    }

    public function down(Schema $schema)
    {
        $contactsTable = $schema->getTable('contacts');
        if ($contactsTable->hasIndex('idx_contactStatus')) {
            $contactsTable->dropIndex('idx_contactStatus');
        }

    }
}
