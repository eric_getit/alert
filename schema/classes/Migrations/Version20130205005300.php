<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130205005300 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        if ($schema->hasTable('newsletter_subscriptions')) {
            $table = $schema->getTable('newsletter_subscriptions');
            if (!$table->hasColumn('subscriptionSecureId')) {
                $table->addColumn('subscriptionSecureId', 'string', array('length'=>'50','notnull' => false));
            }
        }
    }

    public function down(Schema $schema)
    {
        if ($schema->hasTable('newsletter_subscriptions')) {
            $table = $schema->getTable('newsletter_subscriptions');
            if ($table->hasColumn('subscriptionSecureId')) {
                $table->dropTable('subscriptionSecureId');
            }
        }
    }
}
