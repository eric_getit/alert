<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20121229185300 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("update contacts set contactStatus = '" . \Model_Contact::STATUS_ACTIVE . "';");
    }

    public function down(Schema $schema)
    {
    }
}
