<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130204181100 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $sql = <<<SQL
    UPDATE newsletter SET siteId = (SELECT siteId FROM sites WHERE sites.domain = newsletter.domain limit 1) WHERE siteId is null
SQL;
        $this->addSql($sql);
    }

    public function down(Schema $schema)
    {

    }
}
