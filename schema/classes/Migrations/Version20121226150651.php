<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20121226150651 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $sql = "UPDATE alerts SET alertStatus = '" . \Model_Alert::STATUS_ACTIVE . "'";
        $this->addSql($sql);

    }

    public function down(Schema $schema)
    {
        // this down() migration is autogenerated, please modify it to your needs

    }
}
