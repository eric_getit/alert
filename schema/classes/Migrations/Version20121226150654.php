<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20121226150654 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $alertsTable = $schema->getTable('alerts');
        if (!$alertsTable->hasColumn('nextProcessDate')) {
            $alertsTable->addColumn('nextProcessDate', 'datetime', array('notnull' => false));
        }

    }

    public function down(Schema $schema)
    {
        $alertsTable = $schema->getTable('alerts');
        if (!$alertsTable->hasColumn('nextProcessDate')) {
            $alertsTable->dropColumn('nextProcessDate');
        }

    }
}
