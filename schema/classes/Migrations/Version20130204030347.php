<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130204030347 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        if (!$schema->hasTable('message_views')) {
            $table = $schema->createTable('message_views');
            $table->addColumn('viewId', 'integer', array('autoincrement' => true, 'notnull' => true));
            $table->addColumn('messageId', 'integer', array('notnull' => true));
            $table->addColumn('alertId', 'integer', array('notnull' => true));
            $table->addColumn('ip', 'string', array('length' => 100, 'notnull' => false));
            $table->addColumn('userAgent', 'text', array('notnull' => false));
            $table->addColumn('createdDate', 'datetime', array('notnull' => true));
            $table->setPrimaryKey(array('viewId'));
        }
    }

    public function down(Schema $schema)
    {
        if ($schema->hasTable('message_views')) {
            $schema->dropTable('message_views');
        }
    }
}
