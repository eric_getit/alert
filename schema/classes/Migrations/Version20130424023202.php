<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130424023202 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $dbh = \Zend_Db_Table::getDefaultAdapter();
        $select = $dbh->select();
        $select->from('config_group', array('id'));
        $select->where('name = "general"');
        $id = $select->query()->fetchColumn();
        if ($id) {
            $this->addSql("INSERT INTO `config_db`
                (
                    `name`,
                    `displayName`,
                    `configValue`,
                    `displayGroupId`,
                    `displayHelp`
                )
                VALUES
                (
                    'whitelabeling',
                    'Use Whitelabeling',
                    'No',
                    $id,
                    'Turn off and on the whitelabeling of alert backfill results.'
                )"
            );
        }

    }

    public function down(Schema $schema)
    {
        $this->addSql("DELETE FROM `config_db` WHERE `name`='whitelabeling'");

    }
}
