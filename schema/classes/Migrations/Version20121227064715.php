<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20121227064715 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $sql = 'INSERT INTO `trans_email_email`
            (
            `email_key`,
            `email_trigger`,
            `email_status`,
            `email_created`,
            `email_modified`)
                VALUES
            (
            "email alert",
            "Custom",
            "active",
            "' . date('Y-m-d H:i:s') . '",
            "' . date('Y-m-d H:i:s') . '"
        );';
        $this->addSql($sql);



    }

    public function down(Schema $schema)
    {
        $sql = "DELETE FROM `trans_email_email` WHERE email_key = 'email alert'";
        $this->addSql($sql);

    }
}
