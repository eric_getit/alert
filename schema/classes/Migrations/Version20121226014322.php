<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20121226014322 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $alertsTable = $schema->getTable('alerts');
        if (!$alertsTable->hasColumn('lastProcessDate')) {
            $alertsTable->addColumn('lastProcessDate', 'datetime', array('notnull' => false));
        }
        if (!$alertsTable->hasColumn('lastSendDate')) {
            $alertsTable->addColumn('lastSendDate', 'datetime', array('notnull' => false));
        }

    }

    public function down(Schema $schema)
    {
        $alertsTable = $schema->getTable('alerts');
        if (!$alertsTable->hasColumn('lastProcessDate')) {
            $alertsTable->dropColumn('lastProcessDate');
        }
        if (!$alertsTable->hasColumn('lastSendDate')) {
            $alertsTable->dropColumn('lastSendDate');
        }

    }
}
