<?php
$config = array(
    'phpSettings' => array(
        'display_startup_errors' => 0,
        'display_errors' => 0,
        'date' => array(
            'timezone' => 'US/Eastern'
        )
    ),
    'bootstrap' => array(
        'path'  => APPLICATION_PATH . '/Bootstrap.php',
        'class' => 'Bootstrap'
    ),
    'resources' => array(
        'db' => array(
            'adapter' => 'pdo_mysql',
            'isDefaultTableAdapter' => true,
            'params' => array(
                'host' => '@prod-db-host@',
                'username' => '@prod-db-user@',
                'password' => '@prod-db-pass@',
                'dbname' => '@prod-db-name@',
                'driver_options' => array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"),
            ),
        ),
        'session' => array(
            'use_only_cookies' => true,
            'remember_me_seconds' => 0,
            'cookie_lifetime' => 7200,
            'gc_maxlifetime' => 7200,
            'saveHandler' => array(
                'class' => 'Zend_Session_SaveHandler_DbTable',
                'options' => array(
                    'name' => 'session',
                    'primary' => 'id',
                    'modifiedColumn' => 'modified',
                    'dataColumn' => 'data',
                    'lifetimeColumn' => 'lifetime',
                )
            )
        ),
        'frontController' => array(
            'controllerDirectory' => APPLICATION_PATH . '/controllers',
            'noViewRenderer' => true,
        ),
        'view' => '',
    ),
    'aws' => array(
        'key' => '@aws-key@',
        'secret' => '@aws-secret@',
        'region' => '@aws-region@',
    ),
    'transemail' => array(
        'driver' => array(
            'ses' => array(
                'class' => 'GetIt\Email\Drivers\SesDriver',
                'isDefault' => true,
                'options' => array(
                    'Ses_Key_Id' => 'AKIAIMXT52XARSLVCOYQ',
                    'Ses_Access_Key' => 'xlrn0pRHMgZs3LxRT/4an9nztv279SKxxmowRVkS',
                ),
            ),
            'testdriver' => array(
                'class' => 'GetIt\Email\Drivers\SendmailDriver',
                'isDefault' => false,
                'options' => array(
                    'transport' => array(
                        'class' => 'Zend\Mail\Transport\Sendmail',
                    ),
                ),
            ),
        ),
        'queue' => array(
            'sqs' => array(
                'class' => 'GetIt\Queue\Drivers\SqsDriver',
                'isDefault' => true,
                'options' => array(
                    'Sqs_Key_Id' => 'AKIAIMXT52XARSLVCOYQ',
                    'Sqs_Access_Key' => 'xlrn0pRHMgZs3LxRT/4an9nztv279SKxxmowRVkS',
                    'Account_Number' => '756105848615',
                ),
            ),
        ),
    ),
    'backup' => array(
            'path' => '/backup',
            'tmp_path' => '/backup/tables_alert',
            'file_prefix' => 'alert',
            'exclude' => array(
                    'tables' => array('/^event_archive_.*/'),
                    'data' => array('/^event$/'),
            ),
            'expire' => 10,
    ),
    'logger' => array(
        'filename' => VAR_PATH . '/app.log'
    ),
    'twig' => array(
        'templateDir' => APPLICATION_PATH . '/templates',
        'options' => array(
            'cache' => VAR_PATH . '/cache/twig',
        ),
    ),
    'connections' => array(
        'gearman' => array(
            'host' => '@gearman-host@',
            'port' => '@gearman-port@',
        ),
        'redis' => array(
            'scheme' => 'tcp',
            'host' => '@redis-host@',
            'port' => '@redis-port@'
        ),
        'supervisor' => array(
            'host' => '@supervisor-host@',
            'port' => '@supervisor-port@',
        ),
    ),
);
if (APPLICATION_ENV == 'development') {
    $config['phpSettings']['display_startup_errors'] = 1;
    $config['phpSettings']['display_errors'] = 1;
    $config['twig']['options']['debug'] = true;
    $config['twig']['options']['auto_reload'] = true;
}
if (APPLICATION_ENV == 'testing') {
    $config['phpSettings']['display_startup_errors'] = 1;
    $config['phpSettings']['display_errors'] = 1;

    $config['resources']['frontController']['params']['displayExceptions'] = 1;

    $config['resources']['db']['adapter'] = "pdo_mysql";
    $config['resources']['db']['params']['host'] = "@test-db-host@";
    $config['resources']['db']['params']['username'] = "@test-db-user@";
    $config['resources']['db']['params']['password'] = "@test-db-pass@";
    $config['resources']['db']['params']['dbname'] = "@test-db-name@";
    $config['resources']['db']['isDefaultTableAdapter'] = true;
}

return $config;
