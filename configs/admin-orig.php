<?php
$config = array(
    'phpSettings' => array(
        'display_startup_errors' => 0,
        'display_errors' => 0,
        'date' => array(
            'timezone' => 'US/Eastern'
        )
    ),
    'bootstrap' => array(
        'path'  => APPLICATION_PATH . '/AdminBootstrap.php',
        'class' => 'AdminBootstrap'
    ),
    'resources' => array(
        'db' => array(
            'adapter' => 'pdo_mysql',
            'isDefaultTableAdapter' => true,
            'params' => array(
                'host' => '@prod-db-host@',
                'username' => '@prod-db-user@',
                'password' => '@prod-db-pass@',
                'dbname' => '@prod-db-name@',
                'driver_options' => array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"),
            ),
        ),
        'session' => array(
            'use_only_cookies' => true,
            'remember_me_seconds' => 0,
            'cookie_lifetime' => 7200,
            'gc_maxlifetime' => 7200,
            'saveHandler' => array(
                'class' => 'Zend_Session_SaveHandler_DbTable',
                'options' => array(
                    'name' => 'session',
                    'primary' => 'id',
                    'modifiedColumn' => 'modified',
                    'dataColumn' => 'data',
                    'lifetimeColumn' => 'lifetime',
                )
            )
        ),
        'frontController' => array(
            'noViewRenderer' => true,
            'controllerDirectory' => APPLICATION_PATH . '/admin-controllers',
            'defaultControllerName' => 'Contacts',
        ),
    ),
    'admin' => array(
        'twigTemplatesPath' => APPLICATION_PATH . '/templates/default',
        'twigOptions' => array(
            'auto_reload' => true,
            'cache' => VAR_PATH . '/cache/twig',
            'debug' => true,
        ),
        'projectName' => 'Alert Admin',
        'menu' => array(
            array(
                'href' => '/contacts',
                'name' => 'Contacts',
                'type' => 'item',
            ),
            array(
                'name'  => 'Newsletters',
                'type'  => 'dropdown',
                'items' => array(
                    array(
                        'href' => '/newsletters',
                        'name' => 'Newsletters',
                        'type' => 'item',
                    ),
                    array(
                        'href' => '/newsletters-queue',
                        'name' => 'Newsletters Queue',
                        'type' => 'item',
                    ),
                )
            ),
            array(
                'href' => '/sites',
                'name' => 'Sites',
                'type' => 'item',
            ),
            array(
                'href' => '/templates',
                'name' => 'Templates',
                'type' => 'item',
            ),
            array(
                'name'  => 'Configuration',
                'type'  => 'dropdown',
                'items' => array(
                    array(
                        'href' => '/configuration/general',
                        'name' => 'General',
                        'type' => 'item',
                    ),
                    array(
                        'href' => '/configuration/default-UTM',
                        'name' => 'Default UTM',
                        'type' => 'item',
                    ),
                    array(
                        'href' => '/configuration/send-time',
                        'name' => 'Send Time',
                        'type' => 'item',
                    ),
                )
            ),
            array(
                'href' => '/message-archive',
                'name' => 'Message Archive',
                'type' => 'item',
            ),
            array(
                'name'  => 'Reports',
                'type'  => 'dropdown',
                'items' => array(
                    array(
                        'href' => '/reports/overview',
                        'name' => 'Overview',
                        'type' => 'item',
                    ),
                    array(
                            'href' => '/reports/overview-old',
                            'name' => 'Overview Old',
                            'type' => 'item',
                    ),
                ),
            ),
            array(
                'href' => '/logout',
                'name' => 'Logout',
                'type' => 'item',
            ),
        ),
    ),
);
if (APPLICATION_ENV == 'development') {
    $config['phpSettings']['display_startup_errors'] = 1;
    $config['phpSettings']['display_errors'] = 1;
    $config['twig']['options']['debug'] = true;
    $config['twig']['options']['auto_reload'] = true;
}

return $config;
