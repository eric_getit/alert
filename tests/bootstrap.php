<?php
/**
 * bootstrap.php
 *
 * PHP Version 5.3
 *
 * @category script
 * @package  Tests
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Bootstrap for unit tests
 */

ini_set('memory_limit', '1024M');

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV') || define('APPLICATION_ENV', 'testing');

// Define path to configurations folder
defined('CONFIGS_PATH')
|| define('CONFIGS_PATH', realpath(dirname(__FILE__) . '/../configs'));
// var folder: logs, sessions, cache, temporary files
defined('VAR_PATH')
|| define('VAR_PATH', realpath(dirname(__FILE__) . '/../var'));

defined('TEST_PATH')
|| define('TEST_PATH', realpath(dirname(__FILE__)));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

// We need these parameters to build GUID like IDs
$_SERVER['REQUEST_TIME'] = 'REQUEST_TIME';
$_SERVER['HTTP_USER_AGENT'] = 'HTTP_USER_AGENT';
$_SERVER['SERVER_ADDR'] = 'SERVER_ADDR';
$_SERVER['SERVER_PORT'] = 'SERVER_PORT';
$_SERVER['REMOTE_ADDR'] = 'REMOTE_ADDR';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

$loader = require_once realpath(__DIR__ . '/../vendor/autoload.php');
$loader->add('GetIt', realpath(APPLICATION_PATH . '/../library/TransEmail/src'));
$loader->add('Ses', realpath(APPLICATION_PATH . '/../library/TransEmail/src'));
$loader->add('Sqs', realpath(APPLICATION_PATH . '/../library/TransEmail/src'));
$loader->add('Zend', realpath(APPLICATION_PATH . '/../library/TransEmail/src'));

require_once 'application/Utils.php';
require_once 'application/ControllerTestCase.php';
require_once 'application/DbTestCase.php';
require_once 'application/MockupGearmanJob.php';
require_once 'application/MockupRedisClient.php';

$config = require_once CONFIGS_PATH . '/application.php';

$application = new Zend_Application(
    APPLICATION_ENV,
    $config
);
$application->bootstrap();
