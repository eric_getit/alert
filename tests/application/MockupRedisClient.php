<?php
/**
 * MockupRedisClient
 *
 * PHP Version 5.3
 *
 * @category Tests
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Mockup class to test Gearman workers, because they use redis client
 * This class exposes the needed mockup interface to interact with Redis.
 */
class MockupRedisClient
{
    /**
     * Custom storage. Аnalog storage in radis.
     *
     * @var array
     */
    private static $_data = array();

    /**
     * @link http://redis.io/commands/exists
     *
     * @param string $key
     *
     * @return boolean
     */
    public function exists($key)
    {
        return array_key_exists($key, self::$_data);
    } // exists

    /**
     * @link http://redis.io/commands/hgetall
     *
     * @param string $key
     *
     * @return array
     */
    public function hgetall($key)
    {
        $result = array();
        if ($this->exists($key)) {
            $result = self::$_data[$key];
        }

        return $result;
    } // hgetall

    /**
     * @link http://redis.io/commands/hmset
     *
     * @param string $key
     * @param array $values
     *
     * @return boolean
     */
    public function hmset($key, $values)
    {
        $data = $this->hgetall($key);
        $data = array_merge($data, $values);
        self::$_data[$key] = $data;

        return true;
    } // hmset

    /**
     * @link http://redis.io/commands/set
     *
     * @param string $key
     * @param array $value
     *
     * @return boolean
     */
    public function set($key, $value)
    {
        self::$_data[$key] = $value;

        return true;
    } // set

    /**
     * http://redis.io/commands/hset
     *
     * @param string $key
     * @param string $field
     * @param string $value
     *
     * @return boolean
     */
    public function hset($key, $field, $value)
    {
        self::$_data[$key][$field] = $value;

        return true;
    } // hset

    /**
     * @link http://redis.io/commands/del
     *
     * @param string $key
     *
     * @return boolean
     */
    public function del($key)
    {
        $result = false;
        if ($this->exists($key)) {
            unset(self::$_data[$key]);
            $result = true;
        }

        return $result;
    } // del

    /**
     * @link http://redis.io/commands/hincrby
     *
     * @param string $key
     * @param string $field
     * @param integer $value
     *
     * @return integer | null
     */
    public function hincrby($key, $field, $value)
    {
        $result = null;
        if ($this->exists($key)) {
              $result = self::$_data[$key][$field] += $value;
        }

        return $result;
    } // hincrby

    /**
     * @link http://redis.io/commands/hincrbyfloat
     *
     * @param string $key
     * @param string $field
     * @param float $value
     *
     * @return float | null
     */
    public function hincrbyfloat($key, $field, $value)
    {
        $result = null;
        if ($this->exists($key)) {
              $result = self::$_data[$key][$field] += $value;
        }

        return $result;
    } // hincrbyfloat

    /**
     * @link http://redis.io/commands/llen
     *
     * @param string $key
     *
     * @return integer
     */
    public function llen($key)
    {
        $result = 0;
        if ($this->exists($key)) {
            $result = count(self::$_data[$key]);
        }

        return $result;
    } // llen

    /**
     * @link http://redis.io/commands/lrange
     *
     * @param string $key
     * @param integer $start
     * @param integer $stop
     *
     * @return array
     */
    public function lrange($key, $start, $stop)
    {
        $result = array();
        $count = $this->llen($key);
        if ($start < 0) {
            $start = $count + $start;
        }
        if ($count > 0 && $start <= $stop) {
            $result = array_slice(self::$_data[$key], $start, $stop - $start + 1);
        }

        return $result;
    } // lrange

    /**
     * @link http://redis.io/commands/rpush
     *
     * @param string $key
     * @param string $value
     *
     * @return array
     */
    public function rpush($key, $value)
    {
        if ($this->exists($key)) {
            self::$_data[$key][] = $value;
        } else {
            self::$_data[$key] = array($value);
        }

        return $this->llen($key);
    } // rpush

    /**
     * Is connection to redis exists
     *
     * @return boolean
     */
    public function isConnected()
    {
        return true;
    } // isConnected

    public function connect(){}
    public function disconnect(){}
}
