<?php
/**
 * NewsletterTest
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  AlertTests
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Test the logic for the Newsletter class
 */
class NewsletterTest extends DbTestCase
{
    /**
     * (non-PHPdoc)
     * @see PHPUnit_Extensions_Database_TestCase::getDataSet()
     */
    protected function getDataSet()
    {
        $xmlFile = TEST_PATH . '/dataSets/Newsletter.xml';
        return $this->createXMLDataSet($xmlFile);
    } // getDataSet

    /**
     * Test registry method of newsletter model
     * @expectedException Model_Exception_MissingRequireParam
     */
    public function testRegistry()
    {
        $params = array(
            'id' => 'wef34r3fsd',
            'name' => 'Get Nursing Jobs - Weekly Newsletters',
            'domain' => 'getnursingjobs.com',
            'siteName' => 'Get Nursing Jobs',
            'fromEmail' => 'Jacob Peebles',
            'fromName' => 'contact@getnursingjobs.com',
            'siteNiche' => 'Nursing',
            'newsletterDomain' => 'getnursingjobs.com'
        );
        $newsletter = Model_Newsletter::registry($params);
        $this->assertInstanceOf('Model_Newsletter', $newsletter);

        $this->assertAttributeEquals('newsletter', '_tableName', $newsletter);
        $this->assertAttributeEquals($params['id'], '_externalId', $newsletter);
        $this->assertAttributeEquals($params['name'], '_newsletterName', $newsletter);
        $this->assertAttributeEquals($params['domain'], '_domain', $newsletter);
        $this->assertAttributeEquals($params['fromEmail'], '_fromEmail', $newsletter);
        $this->assertAttributeEquals($params['fromName'], '_fromName', $newsletter);

        $site = Model_Site::getByDomain('getnursingjobs.com');
        $this->assertEquals($newsletter->getSiteId(), $site->getSiteId());

        unset($params['id']);
        $newsletter = Model_Newsletter::registry($params);

    } // testRegistry

    /**
     * Test registry queue method of newsletter model
     * @expectedException Model_Exception_MissingRequireParam
     */
    public function testRegistryQueue()
    {
        $params = array(
            'id' => 'wef34r3fsd',
            'name' => 'Get Nursing Jobs - Weekly Newsletters',
            'domain' => 'getnursingjobs.com',
            'siteName' => 'Get Nursing Jobs',
            'fromEmail' => 'Jacob Peebles',
            'fromName' => 'contact@getnursingjobs.com',
            'siteNiche' => 'Nursing',
            'newsletterDomain' => 'getnursingjobs.com'
        );
        $newsletter = Model_Newsletter::registry($params);

        $data = array(
            'subject' => 'Job Finder for Hydrology',
            'message' => 'This is the bulk of the message. It should have a footer attached to it from the broadcast system',
            'sendTime' => date('Y-m-d H:i:s')
        );

        $queue = $newsletter->registryQueue($data);
        $this->assertInstanceOf('Model_Newsletter_Queue', $queue);

        $this->assertAttributeEquals('newsletter_queue', '_tableName', $queue);
        $this->assertAttributeEquals(Model_Newsletter_Queue::STATUS_NEW, '_queueStatus', $queue);
        $this->assertAttributeEquals($data['subject'], '_subject', $queue);
        $this->assertAttributeEquals($data['message'], '_message', $queue);
        $this->assertAttributeEquals($data['sendTime'], '_sendTime', $queue);
        $this->assertEquals($queue->getFromName(), $newsletter->getFromName());
        $this->assertEquals($queue->getFromEmail(), $newsletter->getFromEmail());
        $this->assertEquals($queue->getNewsletterId(), $newsletter->getNewsletterId());

        unset($data['subject']);
        $newsletter->registryQueue($data);

    } // testRegistryQueue

    /**
     * Test get site method of newsletter model
     */
    public function testGetSite()
    {
        $params = array(
            'id' => 'wef34r3fsd',
            'name' => 'Get Nursing Jobs - Weekly Newsletters',
            'domain' => 'getnursingjobs.com',
            'siteName' => 'Get Nursing Jobs',
            'fromEmail' => 'Jacob Peebles',
            'fromName' => 'contact@getnursingjobs.com',
            'siteNiche' => 'Nursing',
            'newsletterDomain' => 'getnursingjobs.com'
        );
        $newsletter = Model_Newsletter::registry($params);
        $site = $newsletter->getSite();
        $this->assertInstanceOf('Model_Site', $site);

        $this->assertAttributeEquals($params['fromName'], '_fromName', $site);
        $this->assertAttributeEquals($params['fromEmail'], '_fromEmail', $site);
        $this->assertAttributeEquals($params['siteName'], '_siteName', $site);
        $this->assertAttributeEquals($params['siteNiche'], '_siteNiche', $site);
        $this->assertAttributeEquals($params['domain'], '_domain', $site);
    } // testGetSite

    /**
     * Test insert method of newsletter model
     */
    public function testInsert()
    {
        $params = array(
            'id' => 'wef34r3fsd',
            'name' => 'Get Nursing Jobs - Weekly Newsletters',
            'domain' => 'getnursingjobs.com',
            'siteName' => 'Get Nursing Jobs',
            'fromEmail' => 'Jacob Peebles',
            'fromName' => 'contact@getnursingjobs.com',
            'siteNiche' => 'Nursing',
            'newsletterDomain' => 'getnursingjobs.com'
        );
        $newsletter = Model_Newsletter::registry($params);
        $this->assertNotNull($newsletter->getCreatedDate());
        $this->assertNotNull($newsletter->getUpdatedDate());
    } // testInsert

    /**
     * Test get by external id method of newsletter model
     */
    public function testGetByExternalId()
    {
        $params = array(
            'id' => 'wef34r3fsd',
            'name' => 'Get Nursing Jobs - Weekly Newsletters',
            'domain' => 'getnursingjobs.com',
            'siteName' => 'Get Nursing Jobs',
            'fromEmail' => 'Jacob Peebles',
            'fromName' => 'contact@getnursingjobs.com',
            'siteNiche' => 'Nursing',
            'newsletterDomain' => 'getnursingjobs.com'
        );
        Model_Newsletter::registry($params);
        $newsletter = Model_Newsletter::getByExternalId($params['id']);
        $this->assertInstanceOf('Model_Newsletter', $newsletter);

        $this->assertAttributeEquals('newsletter', '_tableName', $newsletter);
        $this->assertAttributeEquals($params['id'], '_externalId', $newsletter);
        $this->assertAttributeEquals($params['name'], '_newsletterName', $newsletter);
        $this->assertAttributeEquals($params['domain'], '_domain', $newsletter);
        $this->assertAttributeEquals($params['fromEmail'], '_fromEmail', $newsletter);
        $this->assertAttributeEquals($params['fromName'], '_fromName', $newsletter);

        $newsletter = Model_Newsletter::getByExternalId('123efsfs34');
        $this->assertNull($newsletter);
    } // testGetByExternalId
}
