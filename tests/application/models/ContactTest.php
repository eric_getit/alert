<?php
/**
 * ContactTest
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  AlertTests
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Test Model_Alert class
 *
 * @category Tests
 * @group model
 * @group Contact
 */
class ContactTest extends PHPUnit_Framework_TestCase
{
    public function testGetBySecureId()
    {
        $secureId = 'dc4e420c';
        $model = Model_Contact::getBySecureId($secureId);
        $this->assertInstanceOf('Model_Contact', $model);
        $this->assertEquals(1, $model->getContactId());

    }

    public function testCreation()
    {
        $params = array(
            'contactSourceId' => '234234',
            'name' => 'Eugene',
            'address' => 'Houston, TX',
            'email' => 'contact@getit.me',
            'site' => 'www.getnichejobs.com',
            'siteName' => 'Get Niche Jobs',
            'siteContactInfo' => "{'siteId': 6421}",
            'createdDate' => '2012-21-13 12:21:20',
            'updatedDate' => '2012-21-13 12:21:20',
        );
        $model = new Model_Contact($params);
        $model->insert();
        $this->assertEquals(2, $model->getContactId());
        $secureId = $model->generateSecureId();
        $this->assertRegExp('/[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}/', $secureId);
        $this->assertEquals($secureId, $model->getContactSecureId());
        $model->update();

        $newModel = Model_Contact::getBySecureId($secureId);
        $this->assertInstanceOf('Model_Contact', $newModel);
        $this->assertEquals($model->getContactId(), $newModel->getContactId());

    } // testgetConfig

    public function testSourceCredetial()
    {
        $model = Model_Contact::getBySourceCredential('123');
        $this->assertInstanceOf('Model_Contact', $model);
        $this->assertEquals(1, $model->getContactId());
    }

    public function testSourceCredetialWrong()
    {
        $model = Model_Contact::getBySourceCredential('1234444');
        $this->assertEmpty($model);
    }

    /**
     *
     * @expectedException Model_Exception_MissingRequireParam
     * @dataProvider createAlertException
     */
    public function testCreateAlertEmpty($params)
    {
        $model = new Model_Contact(1);
        $model->createAlert($params);
    }

    /**
     * Dataprovider for testCreateAlertEmpty
     */
    public function createAlertException()
    {
        return array(
            array(array()),
            array(array('siteName' => '123', 'email' => 'e@e', 'siteContactInfo' => '123')),
            array(array('domain' => '123', 'email' => 'e@e', 'siteContactInfo' => '123')),
            array(array('domain' => '123', 'siteName' => '234', 'siteContactInfo' => '123')),
            array(array('domain' => '123', 'siteName' => '234', 'email' => 'e@e')),
            array(array('domain' => '123', 'siteName' => '234', 'email' => 'e@e', 'siteContactInfo' => '123')),
            array(array('domain' => '123', 'siteName' => '234', 'email' => 'e@e', 'siteContactInfo' => '123', 'alertType'=>'DAILY')),
            array(array('domain' => '123', 'siteName' => '234', 'email' => 'e@e', 'siteContactInfo' => '123', 'query'=>'DAILY')),
            array(array('domain' => '123', 'siteName' => '234', 'email' => 'e@e', 'siteContactInfo' => '123', 'location'=>'DAILY')),
        );
    }


}