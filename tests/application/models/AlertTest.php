<?php
/**
 * AlertTest
 *
 * PHP Version 5.3
 *
 * @category Unit tests
 * @package  application\models
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Test Model_Alert class
 * @group model
 * @group Alert
 *
 * @category Tests
 */
class AlertTest extends DbTestCase
{
    /**
     * (non-PHPdoc)
     * @see PHPUnit_Extensions_Database_TestCase::getDataSet()
     */
    protected function getDataSet()
    {
        $xmlFile = TEST_PATH . '/dataSets/Alert.xml';
        return $this->createXMLDataSet($xmlFile);
    } // getDataSet

    public function testGetBySecureId()
    {
        $secureId = '8c3140b2';
        $model = Model_Alert::getBySecureId($secureId);
        $this->assertInstanceOf('Model_Alert', $model);
        $this->assertEquals(1, $model->getAlertId());

    }

    public function testCreation()
    {
        $secureId = '8cwed2b2';
        $params = array(
            'contactId' => 1,
            'alertType' => 'DAILY',
            'location' => 'Dallas, TX',
            'queryTerms' => 'php developer',
            'ip' => '127.0.0.1',
            'nextProcessDate' => '2012-21-12',
            'createdDate' => '2012-21-12 12:21:20',
            'updatedDate' => '2012-21-12 12:21:20',
        );

        $model = new Model_Alert($params);
        $secureId = $model->generateSecureId();
        $modelId = $model->insert();

        $this->assertEquals($modelId, $model->getAlertId());
        $this->assertEquals($secureId, $model->getAlertSecureId());

        $newModel = Model_Alert::getBySecureId($secureId);
        $this->assertInstanceOf('Model_Alert', $newModel);
        $this->assertEquals($model->getAlertId(), $newModel->getAlertId());
    } // testCreation

    /**
     * Test method to update alert with API parameters
     */
    public function testUpdateAlert()
    {
        $params = array();
        $params['query'] = 'Java developer';
        $params['location'] = 'Alexandria, VA';
        $params['distance'] = '10';
        $params['alertType'] = 'WEEKLY';
        $params['status'] = 'active';

        $secureId = '8c3140b2';
        $alert = Model_Alert::getBySecureId($secureId);
        $alert->updateAlert($params);

        $alertFromDb = Model_Alert::getBySecureId($secureId);
        $this->assertEquals('Java developer', $alertFromDb->getQueryTerms());
        $this->assertEquals('Alexandria, VA', $alertFromDb->getLocation());
        $this->assertEquals('10', $alertFromDb->getDistance());
        $this->assertEquals('WEEKLY', $alertFromDb->getAlertType());
        $this->assertEquals('active', $alertFromDb->getAlertStatus());
    } // testUpdateAlert

    /**
     * Test building API request for the alert
     */
    public function testGetRequest()
    {
        $alertId = 1;
        $alert = new Model_Alert($alertId);
        $request = $alert->getRequest();

        $this->assertInstanceOf('Model_Source_Request', $request);
        $this->assertEquals(1, $request->getPeriod());
        $this->assertEquals('sales', $request->getQuery());
        $this->assertEquals('Dallas, TX', $request->getLocation());
        $this->assertEquals(Model_Source_Request::SORT_DATE, $request->getSort());
        $this->assertEquals(0, $request->getStart());
        $this->assertEquals(10, $request->getLimit());
        $this->assertEquals(25, $request->getProximity());
    } // testGetRequest

    /**
     * Test building external API request for the alert
     *
     * Regression test for the http://jira.getitcorporate.com/browse/ALT-126 ticket
     */
    public function testGetRequestDistrictOfColumbia()
    {
        $alertId = 2;
        $alert = new Model_Alert($alertId);
        $request = $alert->getRequest();

        $this->assertInstanceOf('Model_Source_Request', $request);
        $this->assertEquals(1, $request->getPeriod());
        $this->assertEquals('Janitorial  cleaning offices', $request->getQuery());
        $this->assertEquals('District Of Columbia', $request->getLocation());
        $this->assertEquals(Model_Source_Request::SORT_DATE, $request->getSort());
        $this->assertEquals(0, $request->getStart());
        $this->assertEquals(10, $request->getLimit());
        $this->assertEquals(10, $request->getProximity());
    } // testGetRequestDistrictOfColumbia

    /*
     * Default should be missing two items
     */
    public function testDefaultUtm()
    {
        $expected = 'http://www.getnoobjobs.com?utm_source=alert&utm_medium=email&utm_campaign=alerts';
        $domain = 'www.getnoobjobs.com';

        $a = new Model_Alert();

        $url = $a->setUtm($domain);
        $this->assertEquals($expected, $url);
    }

    public function testUtmSource()
    {
        $expected = 'http://www.getnoobjobs.com?utm_source=1sc&utm_medium=email&utm_campaign=alerts';
        $domain = 'www.getnoobjobs.com';

        $a = new Model_Alert();
        $a->setUtmSource('1sc');

        $url = $a->setUtm($domain);
        $this->assertEquals($expected, $url);
    }

    public function testUtmMedium()
    {
        $expected = 'http://www.getnoobjobs.com?utm_source=alert&utm_medium=medium&utm_campaign=alerts';
        $domain = 'www.getnoobjobs.com';

        $a = new Model_Alert();
        $a->setUtmMedium('medium');

        $url = $a->setUtm($domain);
        $this->assertEquals($expected, $url);
    }

    public function testUtmCampaign()
    {
        $expected = 'http://www.getnoobjobs.com?utm_source=alert&utm_medium=camppy&utm_campaign=alerts';
        $domain = 'www.getnoobjobs.com';

        $a = new Model_Alert();
        $a->setUtmMedium('camppy');

        $url = $a->setUtm($domain);
        $this->assertEquals($expected, $url);
    }

    public function testUtmTerm()
    {
        $expected = 'http://www.getnoobjobs.com?utm_source=alert&utm_medium=email&utm_campaign=alerts&utm_term=blocker';
        $domain = 'www.getnoobjobs.com';

        $a = new Model_Alert();
        $a->setUtmTerm('blocker');

        $url = $a->setUtm($domain);
        $this->assertEquals($expected, $url);
    }

    public function testUtmContact()
    {
        $expected = 'http://www.getnoobjobs.com?utm_source=alert&utm_medium=email&utm_campaign=alerts&utm_content=Nadda';
        $domain = 'www.getnoobjobs.com';

        $a = new Model_Alert();
        $a->setUtmContent('Nadda');

        $url = $a->setUtm($domain);
        $this->assertEquals($expected, $url);
    }

    public function testUpdateNextProcessDate()
    {
        // Test daily alert. Normal situation
        $nextProcessDate = new DateTime();
        $alert = new Model_Alert();
        $alert->setAlertType(Model_Alert::TYPE_DAILY);
        $alert->setNextProcessDate($nextProcessDate->format('Y-m-d'));
        $alert->updateNextProcessDate();

        $nextProcessDate->add(new DateInterval('P1D'));
        // NOTE: Right now we store only date without time
        $this->assertEquals($nextProcessDate->format('Y-m-d'), $alert->getNextProcessDate());

        // Test weekly alert. Normal situation.
        $nextProcessDate = new DateTime();
        $nextProcessDate->sub(new DateInterval('P5D'));
        $alert = new Model_Alert();
        $alert->setAlertType(Model_Alert::TYPE_WEEKLY);
        $alert->setNextProcessDate($nextProcessDate->format('Y-m-d'));
        $alert->updateNextProcessDate();

        $nextProcessDate = new DateTime();
        $nextProcessDate->add(new DateInterval('P7D'));
        // NOTE:  Right now we store only date without time
        $this->assertEquals($nextProcessDate->format('Y-m-d'), $alert->getNextProcessDate());
    }

    public function testUnsubscribeUrl()
    {
        /* @var $mock Model_Alert */
        $mock = $this->getMock('Model_Alert', array('getUnsubscribeUrl'), array(1));
        $mock->expects($this->once())
             ->method('getUnsubscribeUrl')
             ->will(
                 $this->returnValue('getnichejobs.getitcorporate..com/alerts/unsubscribe?alert=8c3140b2')
             );

        try {
            $apiResult = $this->_prepageApiResults();
            $mock->prepareMessage($apiResult, Model_Utils::guid());
        } catch (Exception $e) {
            $this->assertEquals('Bad unsubscribe alert url.', $e->getMessage());
        }

        $mock = $this->getMock('Model_Alert', array('getUnsubscribeUrl'), array(1));
        $mock->expects($this->once())
             ->method('getUnsubscribeUrl')
             ->will($this->returnValue(''));

        try {
            $mock->prepareMessage($apiResult, Model_Utils::guid());
        } catch (Exception $e) {
            $this->assertEquals('Empty unsubscribe alert url.', $e->getMessage());
        }

    }

    public function testUnsubscribeAllUrl()
    {
        /* @var $mock Model_Alert */
        $mock = $this->getMock('Model_Alert', array('getUnsubscribeAllUrl'), array(1));
        $mock->expects($this->once())
             ->method('getUnsubscribeAllUrl')
             ->will($this->returnValue('getn?ichejobs.getitcorporate.com/alerts/unsubscribe-all?alert=8c3140b2'));

        try {
            $apiResult = $this->_prepageApiResults();
            $mock->prepareMessage($apiResult, Model_Utils::guid());
        } catch (Exception $e) {
            $this->assertEquals('Bad view all alerts url.', $e->getMessage());
        }

        $mock = $this->getMock('Model_Alert', array('getUnsubscribeAllUrl'), array(1));
        $mockMehod = $mock->expects($this->once())->method('getUnsubscribeAllUrl');
        $mockMehod->will($this->returnValue(''));

        try {
            $mock->prepareMessage($apiResult, Model_Utils::guid());
        } catch (Exception $e) {
            $this->assertEquals('Empty view all alerts url.', $e->getMessage());
        }
    }

    public function testEditUrl()
    {
        /* @var $mock Model_Alert */
        $mock = $this->getMock('Model_Alert', array('getEditUrl'), array(1));
        $mock->expects($this->once())
             ->method('getEditUrl')
             ->will($this->returnValue('://getnichejobs.getitcorporate.com/alerts/edit?alert=8c3140b2'));

        try {
            $apiResult = $this->_prepageApiResults();
            $mock->prepareMessage($apiResult, Model_Utils::guid());
        } catch (Exception $e) {
            $this->assertEquals('Bad edit alert url.', $e->getMessage());
        }

        $mock = $this->getMock('Model_Alert', array('getEditUrl'), array(1));
        $mockMehod = $mock->expects($this->once())->method('getEditUrl');
        $mockMehod->will($this->returnValue(''));

        try {
            $mock->prepareMessage($apiResult, Model_Utils::guid());
        } catch (Exception $e) {
            $this->assertEquals('Empty edit alert url.', $e->getMessage());
        }

    }

    public function testBadListing()
    {
        /* @var $mock Model_Alert */
        $alert = new Model_Alert(1);

        $apiResult = $this->_prepageApiResults();
        $apiResult['listings'][0]->setExternalUrl('http:/getnichejobs.getitcorporate.com/email/dbc2856e');

        try {
            $alert->prepareMessage($apiResult, Model_Utils::guid());
        } catch (Exception $e) {
            $this->assertEquals('Bad listing URL.', $e->getMessage());
        }

        try {
            $apiResult = $this->_prepageApiResults();
            $apiResult['listings'][0]->setTitle('');
            $alert->prepareMessage($apiResult, Model_Utils::guid());
        } catch (Exception $e) {
            $this->assertEquals($e->getMessage(), 'Empty listing title.');
        }

        try {
            $apiResult = $this->_prepageApiResults();
            $apiResult['listings'][0]->setDate('');
            $alert->prepareMessage($apiResult, Model_Utils::guid());
        } catch (Exception $e) {
            $this->assertEquals($e->getMessage(), 'Empty listing date.');
        }

        try {
            $apiResult = $this->_prepageApiResults();
            $apiResult['listings'][0]->setLocation('');
            $alert->prepareMessage($apiResult, Model_Utils::guid());
        } catch (Exception $e) {
            $this->fail('Error in listing location.');
        }

        try {
            $apiResult = $this->_prepageApiResults();
            $apiResult['listings'][0]->setLocation('Texas, DI');
            $alert->prepareMessage($apiResult, Model_Utils::guid());
        } catch (Exception $e) {
            $this->assertEquals($e->getMessage(), 'Failed to validate location Texas, DI');
        }
    }

    /**
     * Prepare API results for testing related methods
     *
     * @return array
     */
    private function _prepageApiResults()
    {
        $listings = array();

        $listing = new Model_Listing();
        $listing->setSource('BLE');
        $listing->setId('1180289102');
        $listing->setDate('2013-04-25 03:30:06');
        $listing->setTitle('General Manager');
        $listing->setLocation('Arlington, TX');
        $listing->setExternalUrl('http://getnichejobs.getitcorporate.com/email/dbc2856e');
        $listing->setExternalSource('www.personified.com');
        $listing->setCompany('A Personified Client Company');
        $listing->setType('backfill');
        $listings[] = $listing;

        $listing = new Model_Listing();
        $listing->setSource('BLE');
        $listing->setId('1180289061');
        $listing->setDate('2013-04-25 02:12:12');
        $listing->setTitle('Manager of Veterinary Services-Veterinarian');
        $listing->setLocation('Dallas, TX');
        $listing->setExternalUrl('http://getnichejobs.getitcorporate.com/email/317c5a4d');
        $listing->setExternalSource('www.ziprecruiter.com');
        $listing->setCompany('The Pursell Group LLC');
        $listing->setType('backfill');
        $listings[] = $listing;

        $apiResult = array();
        $apiResult['listings'] = $listings;
        $apiResult['count'] = 2;
        $apiResult['organicCount'] = 0;
        $apiResult['backfillCount'] = 2;

        return $apiResult;
    } // _prepageApiResults
}
