<?php
/**
 * NewsletterQueueTest
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  AlertTests
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Test the logic for the Newsletter Queue class
 */
class NewsletterQueueTest extends DbTestCase
{
    /**
     * (non-PHPdoc)
     * @see PHPUnit_Extensions_Database_TestCase::getDataSet()
     */
    protected function getDataSet()
    {
        $xmlFile = TEST_PATH . '/dataSets/Newsletter.xml';
        return $this->createXMLDataSet($xmlFile);
    } // getDataSet

    /**
     * Test get active subscriptions of newsletter queue model
     */
    public function testGetActiveSubscriptions()
    {
        $params = array(
            'id' => 'wef34r3fsd',
            'name' => 'Get Nursing Jobs - Weekly Newsletters',
            'domain' => 'getnursingjobs.com',
            'siteName' => 'Get Nursing Jobs',
            'fromEmail' => 'Jacob Peebles',
            'fromName' => 'contact@getnursingjobs.com',
            'siteNiche' => 'Nursing',
            'newsletterDomain' => 'getnursingjobs.com'
        );
        $newsletter = Model_Newsletter::registry($params);
        $this->assertInstanceOf('Model_Newsletter', $newsletter);

        $params = array(
            'contactSourceId' => '234234',
            'name' => 'Eugene',
            'address' => 'Houston, TX',
            'email' => 'contact@getit.me',
            'site' => 'www.getnichejobs.com',
            'siteName' => 'Get Niche Jobs',
            'siteContactInfo' => "{'siteId': 6421}",
            'createdDate' => '2012-21-13 12:21:20',
            'updatedDate' => '2012-21-13 12:21:20',
            'contactStatus' => Model_Contact::STATUS_ACTIVE
        );
        $contact = new Model_Contact($params);
        $contact->insert();

        $subscription = $contact->subscribeNewsletter($newsletter);

        $queue = new Model_Newsletter_Queue();
        $queue->setNewsletterId($newsletter->getNewsletterId());
        $activeSubscription = $queue->getActiveSubscriptions();

        $this->assertInstanceOf('Model_Newsletter_Subscription', $activeSubscription[0]);

        $this->assertAttributeEquals(Model_Newsletter_Subscription::STATUS_ACTIVE, '_subscriptionStatus', $activeSubscription[0]);
        $this->assertAttributeEquals($newsletter->getNewsletterId(), '_newsletterId', $activeSubscription[0]);
        $this->assertAttributeEquals($contact->getContactId(), '_contactId', $activeSubscription[0]);

    } // testGetActiveSubscriptions

    /**
     * Test prepare method of newsletter model
     */
    public function testPrepare()
    {
        $params = array(
            'id' => 'wef34r3fsd',
            'name' => 'Get Nursing Jobs - Weekly Newsletters',
            'domain' => 'getnursingjobs.com',
            'siteName' => 'Get Nursing Jobs',
            'fromEmail' => 'Jacob Peebles',
            'fromName' => 'contact@getnursingjobs.com',
            'siteNiche' => 'Nursing',
            'newsletterDomain' => 'getnursingjobs.com'
        );
        $newsletter = Model_Newsletter::registry($params);
        $this->assertInstanceOf('Model_Newsletter', $newsletter);

        $params = array(
            'contactSourceId' => '234234',
            'name' => 'Eugene Kuznetsov',
            'address' => 'Houston, TX',
            'email' => 'eugenekuznetsov@soft-rose.com',
            'site' => 'www.getnichejobs.com',
            'siteName' => 'Get Niche Jobs',
            'siteContactInfo' => "{'siteId': 6421}",
            'createdDate' => '2012-21-13 12:21:20',
            'updatedDate' => '2012-21-13 12:21:20',
            'contactStatus' => Model_Contact::STATUS_ACTIVE
        );
        $contact = new Model_Contact($params);
        $contact->insert();

        $subscription = $contact->subscribeNewsletter($newsletter);

        $queue = new Model_Newsletter_Queue();
        $queue->setNewsletterId($newsletter->getNewsletterId());

        $queue->prepare();

        $this->assertAttributeEquals(Model_Newsletter_Queue::STATUS_PREPARED, '_queueStatus', $queue);

        $queryTable = $this->getConnection()->createQueryTable(
            'message_archive',
            'select fromEmail, fromName, toEmail, toName
            from message_archive where toEmail = "eugenekuznetsov@soft-rose.com"'
        );
        $expectedTable = $this->createXmlDataSet(
            TEST_PATH . DIRECTORY_SEPARATOR .
            'dataSets' . DIRECTORY_SEPARATOR .
            'MessageArchive.xml'
        )->getTable('message_archive');

        $this->assertTablesEqual($expectedTable, $queryTable);
    } // testPrepare

}
