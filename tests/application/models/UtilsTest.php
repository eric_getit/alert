<?php
/**
 * UtilsTest
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  AlertTests
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Test some functions in Utils class
 *
 * @category Tests
 * @group model
 * @group Utils
 */
class UtilsTest extends PHPUnit_Framework_TestCase
{
    /**
     * Test getTimezones function
     */
    public function testGetTimezones()
    {
        $sendHour = '04';
        $curHour = '10';
        $expected = array('EST-6', 'EST+18');
        $actual = Model_Utils::getTimezones($sendHour, $curHour);
        $this->assertEquals($expected, $actual);

        $sendHour = '04';
        $curHour = '04';
        $expected = array('EST+0');
        $actual = Model_Utils::getTimezones($sendHour, $curHour);
        $this->assertEquals($expected, $actual);

        $sendHour = '04';
        $curHour = '01';
        $expected = array('EST+3');
        $actual = Model_Utils::getTimezones($sendHour, $curHour);
        $this->assertEquals($expected, $actual);

        $sendHour = '04';
        $curHour = '23';
        $expected = array('EST+5');
        $actual = Model_Utils::getTimezones($sendHour, $curHour);
        $this->assertEquals($expected, $actual);
    } // testGetTimezones
}
