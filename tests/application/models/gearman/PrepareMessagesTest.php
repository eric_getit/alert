<?php
/**
 * PrepareMessagesTest
 *
 * PHP Version 5.3
 *
 * @category Tests
 * @package  models/gearman
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Test prepare message worker
 * @group model
 * @group gearman
 * @group PrepareMessages
 *
 * @category Tests
 */
class PrepareMessagesTest extends DbTestCase
{
    /**
     * (non-PHPdoc)
     * @see PHPUnit_Extensions_Database_TestCase::getDataSet()
     */
    protected function getDataSet()
    {
        $xmlFile = TEST_PATH . '/dataSets/Alert.xml';
        return $this->createXMLDataSet($xmlFile);
    } // getDataSet

    public function testPrepareMessage()
    {
        $alert = new Model_Alert(1);
        $listings = array();

        $listing = new Model_Listing();
        $listing->setSource(Model_Listing::TYPE_ORGANIC);
        $listing->setId('1176680407');
        $listing->setDate('2013-03-19 04:40:45');
        $listing->setTitle('Quality Assurance / Quality Control Manager');
        $listing->setExternalUrl('http://www.getqualitycontrolmanagerjobs.com/quality-assurance-quality-control-manager-bremerton-washington-37826103.htm?utm_source=Jobinventory&utm_medium=Organic&utm_campaign=Jobinventory');
        $listing->setExternalSource('www.getqualitycontrolmanagerjobs.com');
        $listing->setLocation(Model_StateList::getName('WA'));
        $listing->setCompany('');
        $listings[] = $listing;

        $listing = new Model_Listing();
        $listing->setSource(Model_Listing::TYPE_BACKFILL);
        $listing->setId('1176680396');
        $listing->setDate('2013-03-19 04:40:44');
        $listing->setTitle('Senior Engineer - Water/Wastewater');
        $listing->setExternalUrl('http://www.getwastewaterengineeringjobs.com/senior-engineer-water-wastewater-las-cruces-new-mexico-37826099.htm?utm_source=Jobinventory&utm_medium=Organic&utm_campaign=Jobinventory');
        $listing->setExternalSource('www.getwastewaterengineeringjobs.com');
        $listing->setLocation(Model_StateList::getName('NM'));
        $listing->setCompany('');
        $listings[] = $listing;

        $redisClient = new MockupRedisClient();
        Model_ConnectionManager::setRedisClient($redisClient);

        $message = new Model_Workers_Message();
        $message->setAlert($alert);
        $message->setContact(new Model_Contact(1));

        $api = array();
        $api['listings'] = $listings;
        $api['organicCount'] = 1;
        $api['backfillCount'] = 1;

        $message->setApiResult($api);
        $messageId = Model_Utils::guid();
        $message->setMessageId($messageId);
        $message->setStatus(Model_Workers_Message::STATUS_CREATED);
        $message->save();

        $job = new MockupGearmanJob();
        $job->setWorkload($messageId);

        $config = include CONFIGS_PATH . '/application.php';
        $worker = new Model_Workers_PrepareMessages();

        $worker->baseHandling($job);

        $message = Model_Workers_Message::load($job->result);
        $result = $message->getMessageData();

        $this->assertEquals(1, $result['alertId']);
        $this->assertEquals(1, $result['contactId']);
        $this->assertEquals(1, $result['organicLinks']);
        $this->assertEquals(1, $result['backfillLinks']);
        $this->assertEquals('contact@getitllc.com', $result['fromEmail']);
        $this->assertEquals('Get Niche Jobs', $result['fromName']);
        $this->assertEquals('contact@getit.me', $result['toEmail']);
        $this->assertEquals('Contacotr', $result['toName']);
        $this->assertEquals('2 New Sales Jobs in Dallas, TX', $result['subject']);
        $this->assertEquals('', $result['bcc']);
    } // testPrepareMessage
}
