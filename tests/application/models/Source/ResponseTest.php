<?php
/**
 * ResponseTest
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  AlertTests
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Tests for Response model
 *
 * @group source
 * @group model
 */
class ResponseTest extends PHPUnit_Framework_TestCase
{
    /**
     * Test the Response getters and setters
     */
    public function testGettersSetters()
    {
        $success = false;
        $totalCount = 0;
        $listings = array();
        $errorMessage = 'Test message';

        $response = new Model_Source_Response();
        $response->setSuccess($success);
        $response->setErrorMessage($errorMessage);
        $response->setListings($listings);
        $response->setTotalCount($totalCount);

        $this->assertAttributeEquals($success, '_success', $response);
        $this->assertAttributeEquals($errorMessage, '_errorMessage', $response);
        $this->assertAttributeEquals($listings, '_listings', $response);
        $this->assertAttributeEquals($totalCount, '_totalCount', $response);

        $this->assertEquals($success, $response->getSuccess());
        $this->assertEquals($errorMessage, $response->getErrorMessage());
        $this->assertEquals($listings, $response->getListings());
        $this->assertEquals($totalCount, $response->getTotalCount());

    } // testGettersSetters
}