<?php
/**
 * RequestTest
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  AlertTests
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Tests for Request model
 *
 * @group source
 * @group model
 */
class RequestTest extends PHPUnit_Framework_TestCase
{
    /**
     * Test the Request getters and setters
     */
    public function testGettersSetters()
    {
        $start = 0;
        $limit = 10;
        $period = 1;
        $sort = Model_Source_Request::SORT_DATE;
        $query = 'sales';
        $location = 'Atlanta, GA';
        $userLocation = 'Atlanta, GA';
        $proximity = 25;
        $site = 'gethydrologyjobs.com';

        $request = new Model_Source_Request();
        $request->setStart($start);
        $request->setLimit($limit);
        $request->setPeriod($period);
        $request->setSort($sort);
        $request->setQuery($query);
        $request->setLocation($location);
        $request->setUserLocation($userLocation);
        $request->setProximity($proximity);
        $request->setSite($site);

        $this->assertAttributeEquals($start, '_start', $request);
        $this->assertAttributeEquals($limit, '_limit', $request);
        $this->assertAttributeEquals($period, '_period', $request);
        $this->assertAttributeEquals($sort, '_sort', $request);
        $this->assertAttributeEquals($query, '_query', $request);
        $this->assertAttributeEquals($location, '_location', $request);
        $this->assertAttributeEquals($userLocation, '_userLocation', $request);
        $this->assertAttributeEquals($proximity, '_proximity', $request);
        $this->assertAttributeEquals($site, '_site', $request);

        $this->assertEquals($start, $request->getStart());
        $this->assertEquals($limit, $request->getLimit());
        $this->assertEquals($period, $request->getPeriod());
        $this->assertEquals($sort, $request->getSort());
        $this->assertEquals($query, $request->getQuery());
        $this->assertEquals($location, $request->getLocation());
        $this->assertEquals($userLocation, $request->getUserLocation());
        $this->assertEquals($proximity, $request->getProximity());
        $this->assertEquals($site, $request->getSite());
    } // testGettersSetters
}
