<?php
/**
 * BLETest
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  AlertTests
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Tests for BLE source model
 *
 * @group source
 * @group model
 */
class BLETest extends PHPUnit_Framework_TestCase
{
    /**
     * Test the BLE constructor
     */
    public function testConstructor()
    {
        $sourceId = 1;
        $sourceName = 'BLE';
        $sourceClass = 'Ble';
        $sourceKey = '128439371';
        $sourceUrl = 'http://api.buzzvertical.com/api/search';

        $dbh = Zend_Db_Table::getDefaultAdapter();
        $select = $dbh->select();
        $select->from('sources', '*');
        $select->where('sourceId = ?', 1);
        $row = $select->query()->fetch();

        $request = new Model_Source_Request();
        $request->setQuery('sales');
        $request->setLocation('Atlanta, GA');
        $request->setStart(0);
        $request->setLimit(1);

        $source = new Model_Source_Ble($row);
        $source->setRequest($request);

        $this->assertEquals($request, $source->getRequest());
        $this->assertAttributeEquals($request, '_request', $source);
        $this->assertAttributeEquals($sourceId, '_sourceId', $source);
        $this->assertAttributeEquals($sourceName, '_sourceName', $source);
        $this->assertAttributeEquals($sourceKey, '_sourceKey', $source);
        $this->assertAttributeEquals($sourceUrl, '_sourceUrl', $source);
    } // testConstructor

    /**
     * Test for getResponse method
     */
    public function testGetResponse()
    {
        $dbh = Zend_Db_Table::getDefaultAdapter();
        $select = $dbh->select();
        $select->from('sources', '*');
        $select->where('sourceId = ?', 1);
        $row = $select->query()->fetch();
        $sourceName = 'BLE';

        $request = new Model_Source_Request();
        $request->setQuery('sales');
        $request->setLocation('Atlanta, GA');
        $request->setProximity(25);
        $request->setStart(0);
        $request->setLimit(10);
        $request->setPeriod(1);
        $request->setSort(Model_Source_Request::SORT_DATE);
        $request->setSite('gethydrologyjobs.com');

        $sourceUrlOrganic = 'http://api.buzzvertical.com/api/search?format=PHP&key=128439371&q=sales&location=Atlanta%2C+GA&period=1&order=date&type=organic&site=gethydrologyjobs.com&radius=25&offset=0&limit=10';
        $sourceUrlExternal = 'http://api.buzzvertical.com/api/search?format=PHP&key=128439371&q=sales&location=Atlanta%2C+GA&period=1&order=date&type=external&radius=25&offset=0&limit=9';

        $result = array(
            'request' => array(
                'query' => 'sales',
                'locationCity' => 'Atlanta',
                'locationState' => 'GA',
                'radius' => 0,
                'limit' => 10,
                'sort' => 'Relevance',
                'page' => 1,
                'title' => false,
            ),
            'count' => 6298,
            'rows' => array(
                0 => array(
                    'id' => 1154971099,
                    'date' => '2012-11-29 23:06:05',
                    'externalUrl' => 'http://www.jobinventory.com/d/Text-Marketing-Sales-Specialist-Jobs-Atlanta-GA-1154971099.html?utm_source=API&utm_medium=www&utm_campaign=Referral+2',
                    'title' => 'Text Marketing Sales Specialist',
                    'source' => 'www.ziprecruiter.com',
                    'locationCountry' => 'US',
                    'locationState' => 'GA',
                    'locationCity' => 'Atlanta',
                    'locationZip' => NULL,
                    'company' => 'Digital Sales & Marketing, LLC',
                ),
            ),
        );
        $result = serialize($result);

        /* @var $mock Model_Source_Ble */
        $mock = $this->getMock('Model_Source_Ble', array('_httpRequest'), array($row));
        $mock->setRequest($request);
        $mockMehod = $mock->expects($this->at(0))
                          ->method('_httpRequest')
                          ->with(
                              $this->equalTo($sourceUrlOrganic),
                              $this->equalTo(null)
                            )
                          ->will($this->returnValue($result));

        $mockMehod = $mock->expects($this->at(1))
                          ->method('_httpRequest')
                          ->with(
                              $this->equalTo($sourceUrlExternal),
                              $this->equalTo(null)
                            )
                          ->will($this->returnValue($result));

        $response = $mock->getResponse();

        $this->assertEquals(true, $response->getSuccess());
        $this->assertEquals(12596, $response->getTotalCount());
        $this->assertNull($response->getErrorMessage());
        $this->assertEquals(2, count($response->getListings()));

        $listings = $response->getListings();

        $listing = $listings[0];
        $this->assertInstanceOf('Model_Listing', $listing);
        $this->assertEquals($sourceName, $listing->getSource());
        $this->assertEquals('2012-11-29 23:06:05', $listing->getDate());
        $this->assertEquals('Text Marketing Sales Specialist', $listing->getTitle());
        $this->assertEquals('Atlanta, GA', $listing->getLocation());
        $this->assertEquals('Digital Sales & Marketing, LLC', $listing->getCompany());
        $this->assertEquals('www.ziprecruiter.com', $listing->getExternalSource());

        $listing = $listings[1];
        $this->assertInstanceOf('Model_Listing', $listing);
        $this->assertEquals($sourceName, $listing->getSource());
        $this->assertEquals('2012-11-29 23:06:05', $listing->getDate());
        $this->assertEquals('Text Marketing Sales Specialist', $listing->getTitle());
        $this->assertEquals('Atlanta, GA', $listing->getLocation());
        $this->assertEquals('Digital Sales & Marketing, LLC', $listing->getCompany());
        $this->assertEquals('www.ziprecruiter.com', $listing->getExternalSource());
    } // testGetResponse
}
