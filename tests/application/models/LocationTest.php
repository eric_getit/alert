<?php
/**
 * LocationTest
 *
 * PHP Version 5.3
 *
 * @category Unit tests
 * @package  application\models
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Test parsing different location strings
 */
class LocationTest extends PHPUnit_Framework_TestCase
{
    /**
     * Test different spelling for Saint in city names.
     * Should return Saint Johnsbury, VT for each name from the list
     * - st Johnsbury, VT
     * - St Johnsbury, VT
     * - ST Johnsbury, VT
     * - st. Johnsbury, VT
     * - St. Johnsbury, VT
     * - ST. Johnsbury, VT
     *
     */
    public function testSaint()
    {
        $saints = array('st', 'St', 'ST', 'st.', 'St.', 'ST.');

        foreach ($saints as $saint) {
            $location = new Model_Location($saint . ' Johnsbury, VT');
            $this->assertAttributeEquals('US', '_countryCode', $location);
            $this->assertAttributeEquals('United States', '_country', $location);
            $this->assertAttributeEquals('VT', '_stateCode', $location);
            $this->assertAttributeEquals('Vermont', '_state', $location);
            $this->assertAttributeEquals('Saint Johnsbury', '_city', $location);

            $this->assertEquals('Saint Johnsbury, VT', $location->getCanonical(false));
            $this->assertEquals('Saint Johnsbury, VT', $location->getCanonical(true));
        }
    } // testSaint

    /**
     * Test state name 'Ohio'
     *
     */
    public function testSt()
    {
        $location = new Model_Location('Ohio');

        $this->assertAttributeEquals('US', '_countryCode', $location);
        $this->assertAttributeEquals('United States', '_country', $location);
        $this->assertAttributeEquals('OH', '_stateCode', $location);
        $this->assertAttributeEquals('Ohio', '_state', $location);
        $this->assertAttributeEmpty('_city', $location);
        $this->assertAttributeEmpty('_zipCode', $location);
        $this->assertAttributeEmpty('_population', $location);
        $this->assertAttributeEmpty('_lat', $location);
        $this->assertAttributeEmpty('_lng', $location);

        $this->assertEquals('Ohio', $location->getCanonical(false));
        $this->assertEquals('Ohio', $location->getCanonical(true));
    } // testSt

    /**
     * Test two words state names 'North Dakota' and 'West Virginia'
     *
     */
    public function testTwoWordState()
    {
        $location = new Model_Location('North Dakota');

        $this->assertAttributeEquals('US', '_countryCode', $location);
        $this->assertAttributeEquals('United States', '_country', $location);
        $this->assertAttributeEquals('ND', '_stateCode', $location);
        $this->assertAttributeEquals('North Dakota', '_state', $location);
        $this->assertAttributeEmpty('_city', $location);
        $this->assertAttributeEmpty('_zipCode', $location);
        $this->assertAttributeEmpty('_population', $location);
        $this->assertAttributeEmpty('_lat', $location);
        $this->assertAttributeEmpty('_lng', $location);

        $this->assertEquals('North Dakota', $location->getCanonical(false));
        $this->assertEquals('North Dakota', $location->getCanonical(true));

        $location = new Model_Location('West Virginia');

        $this->assertAttributeEquals('US', '_countryCode', $location);
        $this->assertAttributeEquals('United States', '_country', $location);
        $this->assertAttributeEquals('WV', '_stateCode', $location);
        $this->assertAttributeEquals('West Virginia', '_state', $location);
        $this->assertAttributeEmpty('_city', $location);
        $this->assertAttributeEmpty('_zipCode', $location);
        $this->assertAttributeEmpty('_population', $location);
        $this->assertAttributeEmpty('_lat', $location);
        $this->assertAttributeEmpty('_lng', $location);

        $this->assertEquals('West Virginia', $location->getCanonical(false));
        $this->assertEquals('West Virginia', $location->getCanonical(true));
    } // testTwoWordState

    /**
     * Test 'District Of Columbia' state
     * Regression test for the ticket http://jira.getitcorporate.com/browse/ALT-126
     *
     */
    public function testDistrictOfColumbiaState()
    {
        $location = new Model_Location('District Of Columbia');

        $this->assertAttributeEquals('US', '_countryCode', $location);
        $this->assertAttributeEquals('United States', '_country', $location);
        $this->assertAttributeEquals('DC', '_stateCode', $location);
        $this->assertAttributeEquals('District Of Columbia', '_state', $location);
        $this->assertAttributeEmpty('_city', $location);
        $this->assertAttributeEmpty('_zipCode', $location);
        $this->assertAttributeEmpty('_population', $location);
        $this->assertAttributeEmpty('_lat', $location);
        $this->assertAttributeEmpty('_lng', $location);

        $this->assertEquals('District Of Columbia', $location->getCanonical(false));
        $this->assertEquals('District Of Columbia', $location->getCanonical(true));
    } // testDistrictOfColumbiaState

    /**
     * Test state code 'OH'
     *
     */
    public function testStateCode()
    {
        $location = new Model_Location('OH');

        $this->assertAttributeEquals('US', '_countryCode', $location);
        $this->assertAttributeEquals('United States', '_country', $location);
        $this->assertAttributeEquals('OH', '_stateCode', $location);
        $this->assertAttributeEquals('Ohio', '_state', $location);
        $this->assertAttributeEmpty('_city', $location);
        $this->assertAttributeEmpty('_zipCode', $location);
        $this->assertAttributeEmpty('_population', $location);
        $this->assertAttributeEmpty('_lat', $location);
        $this->assertAttributeEmpty('_lng', $location);

        $this->assertEquals('Ohio', $location->getCanonical(false));
        $this->assertEquals('Ohio', $location->getCanonical(true));
    } // testStateCode

    /**
     * Test city name without state code 'Canton'
     * Expect to get largest city with given name
     *
     */
    public function testCityCanton()
    {
        $location = new Model_Location('Canton');

        $this->assertAttributeEquals('US', '_countryCode', $location);
        $this->assertAttributeEquals('United States', '_country', $location);
        $this->assertAttributeEquals('MI', '_stateCode', $location);
        $this->assertAttributeEquals('Michigan', '_state', $location);
        $this->assertAttributeEquals('Canton', '_city', $location);
        $this->assertAttributeEmpty('_zipCode', $location);
        $this->assertAttributeGreaterThan(0, '_population', $location);
        $this->assertAttributeEquals('0.738425244574', '_lat', $location);
        $this->assertAttributeEquals('-1.457038558677', '_lng', $location);

        $this->assertEquals('Canton, MI', $location->getCanonical(false));
        $this->assertEquals('Canton, MI', $location->getCanonical(true));
    } // testCityCanton

    /**
     * Test city, state code - 'Canton, OH'
     *
     */
    public function testCityCommaSt()
    {
        $location = new Model_Location('Canton, OH');

        $this->assertAttributeEquals('US', '_countryCode', $location);
        $this->assertAttributeEquals('United States', '_country', $location);
        $this->assertAttributeEquals('OH', '_stateCode', $location);
        $this->assertAttributeEquals('Ohio', '_state', $location);
        $this->assertAttributeEquals('Canton', '_city', $location);
        $this->assertAttributeEmpty('_zipCode', $location);
        $this->assertAttributeGreaterThan(0, '_population', $location);
        $this->assertAttributeEquals('0.712076008857', '_lat', $location);
        $this->assertAttributeEquals('-1.42032189267', '_lng', $location);

        $this->assertEquals('Canton, OH', $location->getCanonical(false));
        $this->assertEquals('Canton, OH', $location->getCanonical(true));

        $location = new Model_Location('Knik-Fairview, AK');

        $this->assertAttributeEquals('US', '_countryCode', $location);
        $this->assertAttributeEquals('United States', '_country', $location);
        $this->assertAttributeEquals('Alaska', '_state', $location);
        $this->assertAttributeEquals('AK', '_stateCode', $location);
        $this->assertAttributeEquals('Knik-Fairview', '_city', $location);
        $this->assertAttributeEmpty('_zipCode', $location);
        $this->assertAttributeGreaterThan(0, '_population', $location);
        $this->assertAttributeEquals('1.073597750528', '_lat', $location);
        $this->assertAttributeEquals('-2.611014655379', '_lng', $location);

        $this->assertEquals('Knik-Fairview, AK', $location->getCanonical(false));
        $this->assertEquals('Knik-Fairview, AK', $location->getCanonical(true));
    } // testCityCommaSt

    /**
     * Test city, state name - 'Canton, Ohio'
     *
     */
    public function testCityCommaState()
    {
        $location = new Model_Location('Canton, Ohio');

        $this->assertAttributeEquals('US', '_countryCode', $location);
        $this->assertAttributeEquals('United States', '_country', $location);
        $this->assertAttributeEquals('Ohio', '_state', $location);
        $this->assertAttributeEquals('OH', '_stateCode', $location);
        $this->assertAttributeEquals('Canton', '_city', $location);
        $this->assertAttributeEmpty('_zipCode', $location);
        $this->assertAttributeGreaterThan(0, '_population', $location);
        $this->assertAttributeEquals('0.712076008857', '_lat', $location);
        $this->assertAttributeEquals('-1.42032189267', '_lng', $location);

        $this->assertEquals('Canton, OH', $location->getCanonical(false));
        $this->assertEquals('Canton, OH', $location->getCanonical(true));
    } // testCityCommaState

    /**
     * Test zip/postal code - '44705'
     *
     */
    public function testZip()
    {
        $location = new Model_Location('44705');

        $this->assertAttributeEquals('US', '_countryCode', $location);
        $this->assertAttributeEquals('United States', '_country', $location);
        $this->assertAttributeEquals('Ohio', '_state', $location);
        $this->assertAttributeEquals('OH', '_stateCode', $location);
        $this->assertAttributeEquals('Canton', '_city', $location);
        $this->assertAttributeEquals('44705', '_zipCode', $location);
        $this->assertAttributeEmpty('_population', $location);
        $this->assertAttributeEquals('0.712546', '_lat', $location);
        $this->assertAttributeEquals('-1.41965', '_lng', $location);

        $this->assertEquals('Canton, OH', $location->getCanonical(false));
        $this->assertEquals('Canton, OH 44705', $location->getCanonical(true));
    } // testZip

    /**
     * Test city postal code - 'Canton 44705'
     *
     */
    public function testCityZip()
    {
        $location = new Model_Location('Canton 44705');

        $this->assertAttributeEquals('US', '_countryCode', $location);
        $this->assertAttributeEquals('United States', '_country', $location);
        $this->assertAttributeEquals('Ohio', '_state', $location);
        $this->assertAttributeEquals('OH', '_stateCode', $location);
        $this->assertAttributeEquals('Canton', '_city', $location);
        $this->assertAttributeEquals('44705', '_zipCode', $location);
        $this->assertAttributeEmpty('_population', $location);
        $this->assertAttributeEquals('0.712546', '_lat', $location);
        $this->assertAttributeEquals('-1.41965', '_lng', $location);

        $this->assertEquals('Canton, OH', $location->getCanonical(false));
        $this->assertEquals('Canton, OH 44705', $location->getCanonical(true));
    } // testCityZip

    /**
     * Test city, state code postal code - 'Canton, OH 44705'
     *
     */
    public function testCityCommaStZip()
    {
        $location = new Model_Location('Canton, OH 44705');

        $this->assertAttributeEquals('US', '_countryCode', $location);
        $this->assertAttributeEquals('United States', '_country', $location);
        $this->assertAttributeEquals('Ohio', '_state', $location);
        $this->assertAttributeEquals('OH', '_stateCode', $location);
        $this->assertAttributeEquals('Canton', '_city', $location);
        $this->assertAttributeEquals('44705', '_zipCode', $location);
        $this->assertAttributeEmpty('_population', $location);
        $this->assertAttributeEquals('0.712546', '_lat', $location);
        $this->assertAttributeEquals('-1.41965', '_lng', $location);

        $this->assertEquals('Canton, OH', $location->getCanonical(false));
        $this->assertEquals('Canton, OH 44705', $location->getCanonical(true));
    } // testCityCommaStZip

    /**
     * Test city, state code postal code - 'Canton, Ohio 44705'
     *
     */
    public function testCityCommaStateZip()
    {
        $location = new Model_Location('Canton, Ohio 44705');

        $this->assertAttributeEquals('US', '_countryCode', $location);
        $this->assertAttributeEquals('United States', '_country', $location);
        $this->assertAttributeEquals('Ohio', '_state', $location);
        $this->assertAttributeEquals('OH', '_stateCode', $location);
        $this->assertAttributeEquals('Canton', '_city', $location);
        $this->assertAttributeEquals('44705', '_zipCode', $location);
        $this->assertAttributeEmpty('_population', $location);
        $this->assertAttributeEquals('0.712546', '_lat', $location);
        $this->assertAttributeEquals('-1.41965', '_lng', $location);

        $this->assertEquals('Canton, OH', $location->getCanonical(false));
        $this->assertEquals('Canton, OH 44705', $location->getCanonical(true));
    } // testCityCommaStateZip

    /**
     * Test city state code - 'Knik Fairview AK'
     *
     */
    public function testCitySpaceSt()
    {
        $location = new Model_Location('Knik Fairview AK');

        $this->assertAttributeEquals('US', '_countryCode', $location);
        $this->assertAttributeEquals('United States', '_country', $location);
        $this->assertAttributeEquals('Alaska', '_state', $location);
        $this->assertAttributeEquals('AK', '_stateCode', $location);
        $this->assertAttributeEquals('Knik-Fairview', '_city', $location);
        $this->assertAttributeEmpty('_zipCode', $location);
        $this->assertAttributeGreaterThan(0, '_population', $location);
        $this->assertAttributeEquals('1.073597750528', '_lat', $location);
        $this->assertAttributeEquals('-2.611014655379', '_lng', $location);

        $this->assertEquals('Knik-Fairview, AK', $location->getCanonical(false));
        $this->assertEquals('Knik-Fairview, AK', $location->getCanonical(true));
    } // testCitySpaceSt

    /**
     * Test city state name:
     * - 'Cherry Hill New Jersey'
     * - 'Derry New Hampshire'
     * - 'Canton Ohio'
     *
     */
    public function testCitySpaceState()
    {
        $location = new Model_Location('Cherry Hill New Jersey');

        $this->assertAttributeEquals('US', '_countryCode', $location);
        $this->assertAttributeEquals('United States', '_country', $location);
        $this->assertAttributeEquals('New Jersey', '_state', $location);
        $this->assertAttributeEquals('NJ', '_stateCode', $location);
        $this->assertAttributeEquals('Cherry Hill', '_city', $location);
        $this->assertAttributeEmpty('_zipCode', $location);
        $this->assertAttributeGreaterThan(0, '_population', $location);
        $this->assertAttributeEquals('0.696994444257', '_lat', $location);
        $this->assertAttributeEquals('-1.309533278675', '_lng', $location);

        $this->assertEquals('Cherry Hill, NJ', $location->getCanonical(false));
        $this->assertEquals('Cherry Hill, NJ', $location->getCanonical(true));


        $location = new Model_Location('Derry New Hampshire');

        $this->assertAttributeEquals('US', '_countryCode', $location);
        $this->assertAttributeEquals('United States', '_country', $location);
        $this->assertAttributeEquals('New Hampshire', '_state', $location);
        $this->assertAttributeEquals('NH', '_stateCode', $location);
        $this->assertAttributeEquals('Derry', '_city', $location);
        $this->assertAttributeEmpty('_zipCode', $location);
        $this->assertAttributeGreaterThan(0, '_population', $location);
        $this->assertAttributeEquals('0.748408353362', '_lat', $location);
        $this->assertAttributeEquals('-1.244896057025', '_lng', $location);

        $this->assertEquals('Derry, NH', $location->getCanonical(false));
        $this->assertEquals('Derry, NH', $location->getCanonical(true));


        $location = new Model_Location('Canton Ohio');

        $this->assertAttributeEquals('US', '_countryCode', $location);
        $this->assertAttributeEquals('United States', '_country', $location);
        $this->assertAttributeEquals('Ohio', '_state', $location);
        $this->assertAttributeEquals('OH', '_stateCode', $location);
        $this->assertAttributeEquals('Canton', '_city', $location);
        $this->assertAttributeEmpty('_zipCode', $location);
        $this->assertAttributeGreaterThan(0, '_population', $location);
        $this->assertAttributeEquals('0.712076008857', '_lat', $location);
        $this->assertAttributeEquals('-1.42032189267', '_lng', $location);

        $this->assertEquals('Canton, OH', $location->getCanonical(false));
        $this->assertEquals('Canton, OH', $location->getCanonical(true));
    } // testCitySpaceState

    /**
     * Test empty, null values
     *
     */
    public function testIsNull()
    {
        $location = new Model_Location(NULL);

        $this->assertAttributeEquals('US', '_countryCode', $location);
        $this->assertAttributeEquals('United States', '_country', $location);
        $this->assertAttributeEmpty('_state', $location);
        $this->assertAttributeEmpty('_stateCode', $location);
        $this->assertAttributeEmpty('_city', $location);
        $this->assertAttributeEmpty('_zipCode', $location);
        $this->assertAttributeEmpty('_population', $location);
        $this->assertAttributeEmpty('_lat', $location);
        $this->assertAttributeEmpty('_lng', $location);

        $location = new Model_Location();

        $this->assertAttributeEquals('US', '_countryCode', $location);
        $this->assertAttributeEquals('United States', '_country', $location);
        $this->assertAttributeEmpty('_state', $location);
        $this->assertAttributeEmpty('_stateCode', $location);
        $this->assertAttributeEmpty('_city', $location);
        $this->assertAttributeEmpty('_zipCode', $location);
        $this->assertAttributeEmpty('_population', $location);
        $this->assertAttributeEmpty('_lat', $location);
        $this->assertAttributeEmpty('_lng', $location);
    } // testIsNull

    /**
     * Test city with many words:
     * - 'East Fresnos Colonia'
     * - 'San Francisco, CA'
     *
     */
    public function testManyWordsCity()
    {
        $location = new Model_Location('East Fresnos Colonia');

        $this->assertAttributeEquals('US', '_countryCode', $location);
        $this->assertAttributeEquals('United States', '_country', $location);
        $this->assertAttributeEquals('Texas', '_state', $location);
        $this->assertAttributeEquals('TX', '_stateCode', $location);
        $this->assertAttributeEquals('East Fresnos Colonia', '_city', $location);
        $this->assertAttributeEmpty('_zipCode', $location);
        $this->assertAttributeGreaterThanOrEqual(0, '_population', $location);
        $this->assertAttributeEquals('0.456128535506', '_lat', $location);
        $this->assertAttributeEquals('-1.701176959775', '_lng', $location);

        $this->assertEquals('East Fresnos Colonia, TX', $location->getCanonical(false));
        $this->assertEquals('East Fresnos Colonia, TX', $location->getCanonical(true));


        $location = new Model_Location('San Francisco, CA');

        $this->assertAttributeEquals('US', '_countryCode', $location);
        $this->assertAttributeEquals('United States', '_country', $location);
        $this->assertAttributeEquals('California', '_state', $location);
        $this->assertAttributeEquals('CA', '_stateCode', $location);
        $this->assertAttributeEquals('San Francisco', '_city', $location);
        $this->assertAttributeEmpty('_zipCode', $location);
        $this->assertAttributeGreaterThan(0, '_population', $location);
        $this->assertAttributeEquals('0.65929690321', '_lat', $location);
        $this->assertAttributeEquals('-2.136621947382', '_lng', $location);

        $this->assertEquals('San Francisco, CA', $location->getCanonical(false));
        $this->assertEquals('San Francisco, CA', $location->getCanonical(true));
    } // testManyWordsCity

    /**
     * Test city state when city and state name has several words
     * - 'Derry Village, New Hampshire'
     *
     */
    public function testTwoWordCityAndState()
    {
        $location = new Model_Location('Derry Village, New Hampshire');

        $this->assertAttributeEquals('US', '_countryCode', $location);
        $this->assertAttributeEquals('United States', '_country', $location);
        $this->assertAttributeEquals('New Hampshire', '_state', $location);
        $this->assertAttributeEquals('NH', '_stateCode', $location);
        $this->assertAttributeEquals('Derry Village', '_city', $location);
        $this->assertAttributeEmpty('_zipCode', $location);
        $this->assertAttributeGreaterThan(0, '_population', $location);
        $this->assertAttributeEquals('0.748602259442', '_lat', $location);
        $this->assertAttributeEquals('-1.244629370715', '_lng', $location);

        $this->assertEquals('Derry Village, NH', $location->getCanonical(false));
        $this->assertEquals('Derry Village, NH', $location->getCanonical(true));
    } // testTwoWordCityAndState

    /**
     * Test city name from synonyms table
     * - 'Toledo Village, TX'
     *
     */
    public function testSynonymsTable()
    {
        $location = new Model_Location('Toledo Village, TX');

        $this->assertAttributeEquals('US', '_countryCode', $location);
        $this->assertAttributeEquals('United States', '_country', $location);
        $this->assertAttributeEquals('Texas', '_state', $location);
        $this->assertAttributeEquals('TX', '_stateCode', $location);
        $this->assertAttributeEquals('South Toledo Bend', '_city', $location);
        $this->assertAttributeEmpty('_zipCode', $location);
        $this->assertAttributeGreaterThan(0, '_population', $location);
        $this->assertAttributeEquals('0.543726610664', '_lat', $location);
        $this->assertAttributeEquals('-1.633708814078', '_lng', $location);

        $this->assertEquals('South Toledo Bend, TX', $location->getCanonical(false));
        $this->assertEquals('South Toledo Bend, TX', $location->getCanonical(true));
    } // testSynonymsTable

    /**
     * Test non existing location
     *
     */
    public function testLocationNotFound()
    {
        $location = new Model_Location('abracadabra');

        $this->assertTrue($location->isEmpty());
    } // testLocationNotFound
}
