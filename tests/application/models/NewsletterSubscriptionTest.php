<?php
/**
 * NewsletterSubscriptionTest
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  AlertTests
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Test the logic for the newsletter subscription class
 */
class NewsletterSubscriptionTest extends DbTestCase
{
    /**
     * (non-PHPdoc)
     * @see PHPUnit_Extensions_Database_TestCase::getDataSet()
     */
    protected function getDataSet()
    {
        $xmlFile = TEST_PATH . '/dataSets/Newsletter.xml';
        return $this->createXMLDataSet($xmlFile);
    } // getDataSet

    /**
     * Test getBySecureId method of newsletter subscription model
     */
    public function testGetBySecureId()
    {
        $params = array(
            'id' => 'wef34r3fsd',
            'name' => 'Get Nursing Jobs - Weekly Newsletters',
            'domain' => 'getnursingjobs.com',
            'siteName' => 'Get Nursing Jobs',
            'fromEmail' => 'Jacob Peebles',
            'fromName' => 'contact@getnursingjobs.com',
            'siteNiche' => 'Nursing',
            'newsletterDomain' => 'getnursingjobs.com'
        );
        $newsletter = Model_Newsletter::registry($params);
        $this->assertInstanceOf('Model_Newsletter', $newsletter);

        $params = array(
            'contactSourceId' => '234234',
            'name' => 'Eugene',
            'address' => 'Houston, TX',
            'email' => 'contact@getit.me',
            'site' => 'www.getnichejobs.com',
            'siteName' => 'Get Niche Jobs',
            'siteContactInfo' => "{'siteId': 6421}",
            'createdDate' => '2012-21-13 12:21:20',
            'updatedDate' => '2012-21-13 12:21:20',
            'contactStatus' => Model_Contact::STATUS_ACTIVE
        );
        $contact = new Model_Contact($params);
        $contact->insert();

        $subscription = $contact->subscribeNewsletter($newsletter);
        $secureId = $subscription->getSecureId();

        $findSubscription = Model_Newsletter_Subscription::getBySecureId($secureId);
        $this->assertInstanceOf('Model_Newsletter_Subscription', $findSubscription);

        $this->assertAttributeEquals($subscription->getSubscriptionStatus(), '_subscriptionStatus', $findSubscription);
        $this->assertAttributeEquals($subscription->getCreatedDate(), '_createdDate', $findSubscription);
        $this->assertAttributeEquals($subscription->getNewsletterId(), '_newsletterId', $findSubscription);
        $this->assertAttributeEquals($subscription->getContactId(), '_contactId', $findSubscription);

    } // testGetBySecureId

    /**
     * Test GetUnsubscribeAllUrl method of newsletter subscription model
     */
    public function testGetUnsubscribeAllUrl()
    {
        $params = array(
            'id' => 'wef34r3fsd',
            'name' => 'Get Nursing Jobs - Weekly Newsletters',
            'domain' => 'getnursingjobs.com',
            'siteName' => 'Get Nursing Jobs',
            'fromEmail' => 'Jacob Peebles',
            'fromName' => 'contact@getnursingjobs.com',
            'siteNiche' => 'Nursing',
            'newsletterDomain' => 'getnursingjobs.com'
        );
        $newsletter = Model_Newsletter::registry($params);
        $this->assertInstanceOf('Model_Newsletter', $newsletter);

        $params = array(
            'contactSourceId' => '234234',
            'name' => 'Eugene',
            'address' => 'Houston, TX',
            'email' => 'contact@getit.me',
            'site' => 'www.getnichejobs.com',
            'siteName' => 'Get Niche Jobs',
            'siteContactInfo' => "{'siteId': 6421}",
            'createdDate' => '2012-21-13 12:21:20',
            'updatedDate' => '2012-21-13 12:21:20',
            'contactStatus' => Model_Contact::STATUS_ACTIVE
        );
        $contact = new Model_Contact($params);
        $contact->insert();

        $subscription = $contact->subscribeNewsletter($newsletter);
        $secureId = $subscription->generateSecureId();

        $this->assertEquals('http://getnursingjobs.getitcorporate.com/newsletters/unsubscribe-all?subscription=' . $secureId, $subscription->getUnsubscribeAllUrl());
    } // testGetUnsubscribeAllUrl

    /**
     * Test GetUnsubscribeUrl method of newsletter subscription model
     */
    public function testGetUnsubscribeUrl()
    {
        $params = array(
            'id' => 'wef34r3fsd',
            'name' => 'Get Nursing Jobs - Weekly Newsletters',
            'domain' => 'getnursingjobs.com',
            'siteName' => 'Get Nursing Jobs',
            'fromEmail' => 'Jacob Peebles',
            'fromName' => 'contact@getnursingjobs.com',
            'siteNiche' => 'Nursing',
            'newsletterDomain' => 'getnursingjobs.com'
        );
        $newsletter = Model_Newsletter::registry($params);
        $this->assertInstanceOf('Model_Newsletter', $newsletter);

        $params = array(
            'contactSourceId' => '234234',
            'name' => 'Eugene',
            'address' => 'Houston, TX',
            'email' => 'contact@getit.me',
            'site' => 'www.getnichejobs.com',
            'siteName' => 'Get Niche Jobs',
            'siteContactInfo' => "{'siteId': 6421}",
            'createdDate' => '2012-21-13 12:21:20',
            'updatedDate' => '2012-21-13 12:21:20',
            'contactStatus' => Model_Contact::STATUS_ACTIVE
        );
        $contact = new Model_Contact($params);
        $contact->insert();

        $subscription = $contact->subscribeNewsletter($newsletter);
        $secureId = $subscription->generateSecureId();

        $this->assertEquals('http://getnursingjobs.getitcorporate.com/newsletters/unsubscribe?subscription=' . $secureId, $subscription->getUnsubscribeUrl());
    } // testGetUnsubscribeUrl

    /**
     * Test getSite method of newsletter subscription model
     */
    public function testGetSite()
    {
        $params = array(
            'id' => 'wef34r3fsd',
            'name' => 'Get Nursing Jobs - Weekly Newsletters',
            'domain' => 'getnursingjobs.com',
            'siteName' => 'Get Nursing Jobs',
            'fromEmail' => 'Jacob Peebles',
            'fromName' => 'contact@getnursingjobs.com',
            'siteNiche' => 'Nursing',
            'newsletterDomain' => 'getnursingjobs.com'
        );
        $newsletter = Model_Newsletter::registry($params);
        $this->assertInstanceOf('Model_Newsletter', $newsletter);

        $params = array(
            'contactSourceId' => '234234',
            'name' => 'Eugene',
            'address' => 'Houston, TX',
            'email' => 'contact@getit.me',
            'site' => 'www.getnichejobs.com',
            'siteName' => 'Get Niche Jobs',
            'siteContactInfo' => "{'siteId': 6421}",
            'createdDate' => '2012-21-13 12:21:20',
            'updatedDate' => '2012-21-13 12:21:20',
            'contactStatus' => Model_Contact::STATUS_ACTIVE
        );
        $contact = new Model_Contact($params);
        $contact->insert();

        $subscription = $contact->subscribeNewsletter($newsletter);
        $site = $subscription->getSite();
        $this->assertInstanceOf('Model_Site', $site);
        $this->assertAttributeEquals('getnursingjobs.com', '_domain', $site);

    } // testGetSite
}
