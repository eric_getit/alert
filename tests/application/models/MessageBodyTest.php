<?php
/**
 * MessageBodyTest
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  AlertTests
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Test the logic for the MessageBody class
 */
class MessageBodyTest extends DbTestCase
{
    /**
     * (non-PHPdoc)
     * @see PHPUnit_Extensions_Database_TestCase::getDataSet()
     */
    protected function getDataSet()
    {
        $xmlFile = TEST_PATH . '/dataSets/MessageBody.xml';
        return $this->createXMLDataSet($xmlFile);
    } // getDataSet

    /**
     * Test MessageBody contructor
     */
    public function testContructorWithRawData()
    {
        $data = array();
        $data['messageId'] = '123';
        $data['preparedDate'] = '2013-02-05';
        $data['bodyText'] = 'Message body text format';
        $data['bodyHtml'] = 'Message body HTML format';

        $body = new Model_MessageBody($data);

        $this->assertAttributeEquals('message_bodies_20130204', '_tableName', $body);
        $this->assertAttributeEquals($data['messageId'], '_messageId', $body);
        $this->assertAttributeEquals($data['bodyText'], '_bodyText', $body);
        $this->assertAttributeEquals($data['bodyHtml'], '_bodyHtml', $body);
    } // testContructorWithRawData

    /**
     * Test MessageBody contructor
     */
    public function testConstrictorWithMessageArchive()
    {
        $data = array();
        $data['messageId'] = '1';
        $data['bodyText'] = 'body text value';
        $data['bodyHtml'] = 'body HTML value';

        $messageArchive = new Model_MessageArchive(1);
        $messageBody = new Model_MessageBody($messageArchive);

        $this->assertAttributeEquals('message_bodies_20130204', '_tableName', $messageBody);
        $this->assertAttributeEquals($data['messageId'], '_messageId', $messageBody);
        $this->assertAttributeEquals($data['bodyText'], '_bodyText', $messageBody);
        $this->assertAttributeEquals($data['bodyHtml'], '_bodyHtml', $messageBody);
    } // testConstrictorWithMessageArchive

    public function testInsert()
    {
        $data = array();
        $data['messageId'] = '123';
        $data['preparedDate'] = '2013-02-05';
        $data['bodyText'] = 'Message body text format';
        $data['bodyHtml'] = 'Message body HTML format';

        $body = new Model_MessageBody($data);
        $body->insert();

        $messageArchive = new Model_MessageArchive(123);
        $messageBody = new Model_MessageBody($messageArchive);

        $this->assertAttributeEquals('message_bodies_20130204', '_tableName', $messageBody);
        $this->assertAttributeEquals($data['messageId'], '_messageId', $messageBody);
        $this->assertAttributeEquals($data['bodyText'], '_bodyText', $messageBody);
        $this->assertAttributeEquals($data['bodyHtml'], '_bodyHtml', $messageBody);
    }
}
