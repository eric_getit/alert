<?php
/**
 * MockupGearmanJob
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  AlertTests
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Mockup class to test Gearman workers
 *
 */
class MockupGearmanJob
{
    public $result;

    /**
     * Workload for testing
     * @var string
     */
    private $_workload;

    /**
     * Set workload (for testing)
     * @param string $workload
     */
    public function setWorkload($workload)
    {
        $this->_workload = $workload;
    } // setWorkload

    /**
     * Returns the workload for the job. This is serialized data that is to be processed by the worker.
     * @return string
     */
    public function workload()
    {
        return $this->_workload;
    } // workload

    /**
     * Returns the size of the job's work load (the data the worker is to process) in bytes.
     * @return int
     */
    public function workloadSize()
    {
        return strlen($this->_workload);
    } // workloadSize

    public function sendComplete($message)
    {
        $this->result = $message;
    }

}
