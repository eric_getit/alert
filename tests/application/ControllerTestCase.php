<?php
/**
 * ControllerTestCase
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  AlertTests
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

abstract class ControllerTestCase extends Zend_Test_PHPUnit_ControllerTestCase
{
    protected $application;

    public function setUp()
    {
        $config = include CONFIGS_PATH . '/application.php';
        $this->bootstrap = new Zend_Application(APPLICATION_ENV, $config);
        parent::setUp();
        $logger = new Zend_Log();
        $writer = new Zend_Log_Writer_Stream(VAR_PATH . '/app.log');
        $logger->addWriter($writer);
        $filter = new Zend_Log_Filter_Priority(Zend_Log::CRIT);
        $logger->addFilter($filter);
        Zend_Registry::set('logger', $logger);

    }
}