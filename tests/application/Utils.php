<?php
/**
 * Utils
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  AlertTests
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Common functions used in unit test
 * (we don't like copy/paste)
 */
class Utils
{
    /**
     * Delete all files in the folder except for '.empty' file need for git
     *
     * @param string $path
     * @return void
     */
    public static function cleanupFolder($path)
    {
        $handle=opendir($path);
        $file = readdir($handle);

        while ($file !== false) {
            if ($file != '.empty') {
                $filePath = $path . DIRECTORY_SEPARATOR . $file;
                if (is_file($filePath)) {
                    unlink($filePath);
                }
            }
            $file = readdir($handle);
        }

        closedir($handle);
    } // cleanupFolder
}
