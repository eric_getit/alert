<?php
/**
 * DbTestCase
 *
 * PHP Version 5.3
 *
 * @category Class
 * @package  AlertTests
 * @author   Get It <devteam@getit.me>
 * @copyright 2013 Get It, LLC
 * @link     http://www.getit.me/
 */

/**
 * Basic model for test cases with DB operations
 */
abstract class DbTestCase extends PHPUnit_Extensions_Database_TestCase
{
    /**
     * PDO instance. Only instantiate pdo once for test clean-up/fixture load
     * @var PDO
     */
    static private $pdo = null;

    /**
     * DB connection. Only instantiate PHPUnit_Extensions_Database_DB_IDatabaseConnection once per test
     * @var PHPUnit_Extensions_Database_DB_IDatabaseConnection
     */
    private $conn = null;

    /**
     * (non-PHPdoc)
     * @see PHPUnit_Extensions_Database_TestCase::getConnection()
     */
    protected function getConnection()
    {
        if ($this->conn === NULL) {

            $config = Zend_Db_Table::getDefaultAdapter()->getConfig();
            $schema = $config['dbname'];

            if (self::$pdo == NULL) {
                $dsn = 'mysql:host=' . $config['host'] . ';dbname=' . $config['dbname'];
                $username = $config['username'];
                $password = $config['password'];
                self::$pdo = new PDO($dsn, $username, $password);
                self::$pdo->query('SET foreign_key_checks=0');

            }

            $this->conn = $this->createDefaultDBConnection(self::$pdo, $schema);
        }

        return $this->conn;
    } // getConnection
}
